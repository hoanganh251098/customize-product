# README #

### What is this repository for? ###

* Iframe cho sản phẩm customize
* PHP image filter
* Một số dịch vụ python cho sản phẩm customize

### Cài đặt môi trường dev cho iframe ###
Môi trường dev cho iframe yêu cầu [Node.js] v14+.

Cài đặt các dependencies:
```sh
cd iframe
npm install
```
Sau đó chạy lệnh sau để khởi động webpack dev server
```sh
npm start
```
Truy cập vào url `http://localhost:8080/?product=test&mode=debug`

### Setup các dịch vụ python ###
Yêu cầu python3.6+
Cài đặt các dependencies
```sh
cd python-services
python3 -m venv venv
./venv/bin/pip3 install flask requests pyjwt
```
Khởi động server bằng lệnh:
```sh
./venv/bin/python3 broker.py
```

### PHP Image Filter ###
Yêu cầu php7.0

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

[node.js]: <http://nodejs.org>