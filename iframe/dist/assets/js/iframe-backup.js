var HEART_GENERATOR_URL = "https://heart.navitee.com/texttopng/";
var SQUARE_GENERATOR_URL = "https://storage.navitee.com/text_shape_square.php"
var MAPBOX_API = {
    username: 'nafchan00',
    accessToken: 'pk.eyJ1IjoibmFmY2hhbjAwIiwiYSI6ImNraHpxMXdwejB2Z3gyeWtiNzZ2ZWFvYmcifQ.8dfBdxpPoYKv32GEQGeBMQ',
    style: 'mapbox://styles/mapbox/emerald-v8'
};

var MAPBOX_STYLES = {
    'default': {
        styleUrl: 'mapbox://styles/mapbox/emerald-v8',
        username: 'nafchan00',
        accessToken: 'pk.eyJ1IjoibmFmY2hhbjAwIiwiYSI6ImNraHpxMXdwejB2Z3gyeWtiNzZ2ZWFvYmcifQ.8dfBdxpPoYKv32GEQGeBMQ'
    },
    'white': {
        styleUrl: 'mapbox://styles/hfnuser0000/cknxyoxk03i7e17rsn1ugzpkj',
        username: 'hfnuser0000',
        accessToken: 'pk.eyJ1IjoiaGZudXNlcjAwMDAiLCJhIjoiY2tuNnJmeXA3MGdycTJ3bWw2M2l6eXVyNSJ9.ABZHs1qooTswkFSLQqk-8Q'
    }
};

window.onanimationiteration = console.log;

const global = {};

Math.logBase = (n, base) => Math.log(n) / Math.log(base);

class EventEmitter2 {
    constructor() {
        this.el = $('<div></div>');
    }

    emit(name, ...message) {
        this.el.trigger(name, message);
    }

    on(name, callback) {
        this.el.on(name, (e, ...message) => {
            callback(...message);
        });
    }

    unbind(name) {
        this.el.off(name);
    }

    unbindAll() {
        this.el.unbind();
    }
}

const App = {
    productName: null,
    mapStyle: null,
    events: new EventEmitter2(),
    canvas: {
        instance: null,
        exportInstance: null,
        deselectAll() {
            this.instance.discardActiveObject().renderAll();
        },
        refresh() {
            console.log('refresh here');
            this.clear();
            for (const layer of App.layers) {
                layer.objects && layer.objects.forEach(obj => {
                    if (obj ?? false) {
                        obj.set({
                            dirty: true
                        });
                        this.instance.add(obj);
                    }
                });
            }
            this.instance.renderAll();
        },
        clear() {
            const canvas = this.instance;
            canvas.remove(...canvas.getObjects());
        },
        onClick(obj, callback) {
            this.instance.on('mouse:up', (e) => {
                const target = e.target;
                if (target == obj) {
                    callback();
                }
            });
        },
        exportBase64() {
            const refreshExportCanvas = () => {
                const canvas = this.exportInstance;
                canvas.remove(...canvas.getObjects());
                for (const layer of App.layers) {
                    layer.objects && layer.objects.forEach(obj => {
                        if (obj ?? false) {
                            obj.set({
                                dirty: true
                            });
                            canvas.add(obj);
                        }
                    });
                }
                canvas.renderAll();
            };

            for(const layer of App.layers) {
                if(layer.type == 'fg') {
                    layer.objects = [layer.allObjects[1]];
                }
            }
            refreshExportCanvas();
            const base64 = this.exportInstance.toDataURL();

            for(const layer of App.layers) {
                if(layer.type == 'fg') {
                    layer.objects = [layer.allObjects[0]];
                }
            }

            App.canvas.refresh();

            return base64;
        },
        export() {
            const exportData = [];
            const tryJSON = (obj, logger=null) => {
                try {
                    return (obj.toJSON && obj.toJSON()) || (JSON.stringify(obj) && obj);
                }
                catch(e) {
                    const cloneObj = (obj ?? null) == null ? null : (obj.constructor == Array ? [] : {});
                    if(cloneObj == null) return cloneObj;
                    for(const [k, v] of Object.entries(obj)) {
                        const blacklist = ['__layer', 'editors'];
                        if(['object', 'string', 'number', 'boolean'].includes(typeof v) && !blacklist.includes(k)) {
                            // console.log('>> ' + k);
                            cloneObj[k] = tryJSON(v);
                            // console.log('<<');
                        }
                    }
                    return cloneObj;
                }
            };

            for(const layer of App.layers) {
                const exportLayerData = tryJSON(layer);
                exportData.push(exportLayerData);
            }

            return JSON.stringify(exportData);
        }
    },
    layers: [],
    dropzone: {
        get instance() {
            return Dropzone.forElement('#myAwesomeDropzone');
        },
        init() {
            Dropzone.autoDiscover = false;
            $('#myAwesomeDropzone').dropzone({
                url: '/upload',
                autoProcessQueue: false,
                addRemoveLinks: true,
                init() {
                    $('#myAwesomeDropzone').toggleClass('dropzone', true);
                    Dropzone.forElement('#myAwesomeDropzone');
                }
            });
        }
    },
    editors: {
        backgrounds: {
            __eventEmitter: null,
            get __groupEl() {
                // if(!App.isMobile) {
                    return $(`.group.background-group`);
                // }
                // else {
                    // return $('.tab-content[data-tab="backgrounds"]');
                // }
            },
            onBackgroundSelect(callback) {
                this.__eventEmitter.on('background_select', () => {
                    callback(+$('.splide__slide.is-active > :eq(0)').attr('data-index'));
                })
            },
            fetch(backgrounds) {
                this.__eventEmitter = new EventEmitter;
                const html = backgrounds.map((v, idx) => v.type == 'color'
                    ? `<div class="bg" style="background-color: ${v.value};" data-type="color" data-index="${idx}"></div>`
                    : `<img class="bg" src="${v.value}" data-type="image" data-index="${idx}">`).map(v => `<li class="splide__slide">${v}</li>`).join('\n');
                this.__groupEl.find('.splide__list').html(html);
                let splide;
                // if(!App.isMobile) {
                    splide = new Splide( '.editor-wrapper .splide', {
                        perPage: 1,
                        keyboard: false
                    } ).mount();
                // }
                // else {
                    // splide = new Splide( '.editor-wrapper-mobile .splide', {
                        // perPage: 1
                    // } ).mount();
                // }
                splide.on('moved', () => {
                    this.__eventEmitter.emit('background_select');
                });
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);
                // if(App.isMobile) {
                    // $('.tab-indicators span[data-target="backgrounds"]').toggleClass('hidden', false);
                // }
            }
        },
        imageGallery: {
            __popoverKiller: null,
            __eventEmitter: null,
            launch() {
                $('#imageGalleryModal').modal('show');
            },
            close() {
                $('#imageGalleryModal').modal('hide');
            },
            getUploadedFiles() {
                return App.dropzone.instance.files;
            },
            onImageSelect(callback) {
                $(document).on('click', '#imageGalleryModal .dz-image', function () {
                    const itemIdx = $(this).closest('.dz-preview').index() - 1;
                    callback(App.dropzone.instance.files[itemIdx]);
                });
            },
            onAutofill(callback) {
                const othis = this;
                this.__eventEmitter.on('autofill', () => {
                    callback.call(this);
                });
            },
            unbindAll() {
                $(document).off('click', '#imageGalleryModal .dz-image');
            },
            init() {
                const othis = this;
                this.__eventEmitter = new EventEmitter;
                $('#imageGalleryModal .autofill-btn').popover({
                    content: '<i class="fa fa-exclamation-triangle" style="margin-right: 1em; color: #ff9a72;"></i>No images to autofill!',
                    container: '#imageGalleryModal',
                    placement: 'top',
                    html: true,
                });
                $('#imageGalleryModal .autofill-btn').unbind();
                $('#imageGalleryModal .autofill-btn').on('click', function() {
                    $(this).popover('hide');
                    $(this).popover('disable');
                    if(othis.getUploadedFiles().length == 0) {
                        $(this).popover('enable');
                        $(this).popover('show');
                        if(othis.__popoverKiller) {
                            clearTimeout(othis.__popoverKiller);
                        }
                        othis.__popoverKiller = setTimeout(() => {
                            $(this).popover('hide');
                        }, 2000);
                    }
                    console.log('here');
                    othis.__eventEmitter.emit('autofill');
                });
                $(document).on('click', '#imageGalleryModal .dz-image', function () {
                    othis.close();
                });
            }
        },
        images: {
            // new
            __prototype: {
                __cropbox: null,
                __imgData: null,
                __layer: {},
                __eventEmitter: null,
                onApply(callback) {
                    this.__eventEmitter.on('apply', () => {
                        console.log('Apply on id: ' + this.__id);
                        callback.call(this);
                    });
                    return this;
                },
                onChangeImage(callback) {
                    this.__eventEmitter.on('change_image', () => {
                        callback.call(this);
                    });
                },
                apply() {
                    this.__eventEmitter.emit('apply');
                },
                getCroppedDataURL() {
                    return this.__cropbox && this.__cropbox.cropper('getCroppedCanvas').toDataURL();
                },
                setLayer(layer) {
                    this.__layer = layer;
                    return this;
                },
                setImage(b64) {
                    this.__imgData = b64;
                    this.__cropbox && this.__cropbox.cropper('destroy');
                    this.__cropbox = null;
                    $(`#editorImage_${this.__id}`).remove();
                    return this;
                },
                launch() {
                    const imgData = this.__imgData;
                    const othis = this;
                    if(!$(`#editorImage_${this.__id}`).length) {
                        const el = $($.parseHTML(`
                        <div class="modal fade scroll-hidden" tabindex="-1" role="dialog" id="editorImage_${this.__id}">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Image Editor</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                            style="outline:none; color:#000000;">
                                            <span aria-hidden="true" class="fa fa-times"></span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="cropper-image">
                                            <img src="${imgData}" style="display:none;">
                                        </div>
                                        <div class="d-flex justify-content-end">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-light change-image-btn">Change image</button>
                                        <button class="btn btn-primary apply-btn">Apply</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `));
                        $(document).on('click', `#editorImage_${this.__id} .apply-btn`, function() {
                            othis.__eventEmitter.emit('apply');
                        });
                        $(document).on('click', `#editorImage_${this.__id} .change-image-btn`, function() {
                            othis.__eventEmitter.emit('change_image');
                        });
                        $('body').append(el);
                    }
                    $(`#editorImage_${this.__id}`).modal('show');
                    $(`#editorImage_${this.__id}`).one('shown.bs.modal', function() {
                        if(!othis.__cropbox) {
                            $(`#editorImage_${othis.__id}`).find('.modal-body .cropper-image img').attr('style', '');
                            othis.__cropbox = App.helper.initCropper(
                                $(`#editorImage_${othis.__id}`).find('.modal-body .cropper-image img')[0], 
                                othis.__layer.config.width / othis.__layer.config.height);
                        }
                    });
                    this.__eventEmitter.emit('apply');
                    return this;
                },
                close() {
                    $(`#editorImage_${this.__id}`).modal('hide');
                    return this;
                },
                init() {
                    this.__eventEmitter = new EventEmitter;
                    return this;
                },
                clone() {
                    const newObj = {};
                    Object.assign(newObj, this);
                    newObj.init();
                    return newObj;
                }
            },
            __children: [],
            get __groupEl() {
                // if(!App.isMobile) {
                    return $(`.group.gallery-group`);
                // }
                // else {
                    // return $('.tab-content[data-tab="images"]');
                // }
            },
            newEditor() {
                const imgEditor = this.__prototype.clone();
                this.__children.push(imgEditor);
                imgEditor.__id = this.__children.length;
                return imgEditor;
            },
            onOpenGallery(callback) {
                this.__groupEl.on('click', '.open-gallery-btn', function() {
                    callback();
                });
            },
            setCollapse(value) {
                $('.group').toggleClass('active', false);
                this.__groupEl.toggleClass('active', !value);
                for (const group of $('.group:not(.active) .group-body.collapsible')) {
                    setCollapse(group, true);
                }
                setCollapse(this.__groupEl.find('.group-body')[0], value);
            },
            get(idx) {
                return this.__children[idx];
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);
                // if(App.isMobile) {
                    // $('.tab-indicators span[data-target="images"]').toggleClass('hidden', false);
                // }
            }
        },
        texts: {
            __prototype: {
                __el: null,
                __layer: {},
                onTextChange(callback) {
                    const othis = this;
                    this.__el.on('change', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'change');
                    });
                    this.__el.on('keyup', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'keyup');
                    });
                },
                setTitle(title) {
                    this.__el.find('label').html(title);
                    return this;
                },
                setLayer(layer) {
                    this.__layer = layer;
                    return this;
                },
                setText(value) {
                    this.__el.find('textarea').val(value);
                    this.__el.find('textarea').trigger('change');
                    return this;
                },
                setPlaceholder(value) {
                    this.__el.find('textarea').attr('placeholder', value);
                    return this;
                },
                getValue() {
                    return this.__el.find('textarea').val();
                },
                focus() {
                    this.__el.find('textarea').focus();
                },
                init() {
                    const html = `
                    <div class="form-group">
                        <label></label>
                        <textarea class="form-control" rows="1" spellcheck="false"></textarea>
                    </div>`;
                    this.__el = $($.parseHTML(html));
                    const rowsCalib = (el) => {
                        const protoEl = $('#fontSizeCalculationArea textarea:eq(0)');
                        protoEl.width(el.width());
                        protoEl.val(el.val());
                        el.height(protoEl[0].scrollHeight);
                    };
                    this.__el.on('change', 'textarea', function () {
                        rowsCalib($(this));
                    });
                    if(!App.isMobile) {
                        this.__el.on('input', 'textarea', function () {
                            rowsCalib($(this));
                        });
                    }
                },
                clone() {
                    const newObj = {};
                    Object.assign(newObj, this);
                    newObj.init();
                    return newObj;
                }
            },
            __children: [],
            get __groupEl() {
                // if(!App.isMobile) {
                    return $(`.group.text-group`);
                // }
                // else {
                    // return $('.tab-content[data-tab="texts"]');
                // }
            },
            newEditor() {
                const editor = this.__prototype.clone();
                this.__children.push(editor);
                // if(!App.isMobile) {
                    this.__groupEl.find('.group-body').append(editor.__el);
                // }
                // else {
                    // this.__groupEl.append(editor.__el);
                // }
                return editor;
            },
            setCollapse(value) {
                // if(!App.isMobile) {
                    $('.group').toggleClass('active', false);
                    this.__groupEl.toggleClass('active', !value);
                    for (const group of $('.group:not(.active) .group-body')) {
                        setCollapse(group, true);
                    }
                    setCollapse(this.__groupEl.find('.group-body')[0], value);
                // }
                // else {
                    // $('.tab-indicators span[data-target="texts"]').click();
                // }
            },
            calib() {
                $('textarea').trigger('change');
            },
            get(idx) {
                return this.__children[idx];
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);
                // if(App.isMobile) {
                    // $('.tab-indicators span[data-target="texts"]').toggleClass('hidden', false);
                    // $('.tab-indicators span[data-target="texts"]').click(() => {
                        // setTimeout(() => {
                            // this.calib();
                        // });
                    // });
                // }
            }
        },
        spiral: {
            child: {
                __el: null,
                onTextChange(callback) {
                    const othis = this;
                    this.__el.on('change', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'change');
                    });
                    this.__el.on('keyup', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'keyup');
                    });
                },
                setTitle(title) {
                    this.__el.find('label').html(title);
                    return this;
                },
                setLayer(layer) {
                    this.__layer = layer;
                    return this;
                },
                setText(value) {
                    this.__el.find('textarea').val(value);
                    this.__el.find('textarea').trigger('change');
                    return this;
                },
                setPlaceholder(value) {
                    this.__el.find('textarea').attr('placeholder', value);
                    return this;
                },
                getValue() {
                    return this.__el.find('textarea').val();
                },
                focus() {
                    this.__el.find('textarea').focus();
                },
                init() {
                    const html = `
                    <div class="form-group">
                        <label></label>
                        <textarea class="form-control" rows="1" spellcheck="false"></textarea>
                    </div>`;
                    this.__el = $(App.editors.spiral.__groupEl.find('.form-group'));
                    const rowsCalib = (el) => {
                        const protoEl = $('#fontSizeCalculationArea textarea:eq(0)');
                        protoEl.width(el.width());
                        protoEl.val(el.val());
                        el.height(protoEl[0].scrollHeight);
                    };
                    this.__el.on('change', 'textarea', function () {
                        rowsCalib($(this));
                    });
                    if(!App.isMobile) {
                        this.__el.on('input', 'textarea', function () {
                            rowsCalib($(this));
                        });
                    }
                }
            },
            get __groupEl() {
                // if(!App.isMobile) {
                    return $(`.group.spiral-group`);
                // }
                // else {
                    // return $('.tab-content[data-tab="texts"]');
                // }
            },
            setCollapse(value) {
                // if(!App.isMobile) {
                    $('.group').toggleClass('active', false);
                    this.__groupEl.toggleClass('active', !value);
                    for (const group of $('.group:not(.active) .group-body')) {
                        setCollapse(group, true);
                    }
                    setCollapse(this.__groupEl.find('.group-body')[0], value);
                // }
                // else {
                    // $('.tab-indicators span[data-target="texts"]').click();
                // }
            },
            calib() {
                $('textarea').trigger('change');
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);
                // if(App.isMobile) {
                    // $('.tab-indicators span[data-target="texts"]').toggleClass('hidden', false);
                    // $('.tab-indicators span[data-target="texts"]').click(() => {
                        // setTimeout(() => {
                            // this.calib();
                        // });
                    // });
                // }
                this.child.init();
            }
        },
        square: {
            child: {
                __el: null,
                onTextChange(callback) {
                    const othis = this;
                    this.__el.on('change', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'change');
                    });
                    this.__el.on('keyup', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'keyup');
                    });
                },
                setTitle(title) {
                    this.__el.find('label').html(title);
                    return this;
                },
                setLayer(layer) {
                    this.__layer = layer;
                    return this;
                },
                setText(value) {
                    this.__el.find('textarea').val(value);
                    this.__el.find('textarea').trigger('change');
                    return this;
                },
                setPlaceholder(value) {
                    this.__el.find('textarea').attr('placeholder', value);
                    return this;
                },
                getValue() {
                    return this.__el.find('textarea').val();
                },
                focus() {
                    this.__el.find('textarea').focus();
                },
                init() {
                    const html = `
                    <div class="form-group">
                        <label></label>
                        <textarea class="form-control" rows="1" spellcheck="false"></textarea>
                    </div>`;
                    this.__el = $(App.editors.square.__groupEl.find('.form-group'));
                    const rowsCalib = (el) => {
                        const protoEl = $('#fontSizeCalculationArea textarea:eq(0)');
                        protoEl.width(el.width());
                        protoEl.val(el.val());
                        el.height(protoEl[0].scrollHeight);
                    };
                    this.__el.on('change', 'textarea', function () {
                        rowsCalib($(this));
                    });
                    if(!App.isMobile) {
                        this.__el.on('input', 'textarea', function () {
                            rowsCalib($(this));
                        });
                    }
                }
            },
            get __groupEl() {
                // if(!App.isMobile) {
                    return $(`.group.square-group`);
                // }
                // else {
                    // return $('.tab-content[data-tab="texts"]');
                // }
            },
            setCollapse(value) {
                // if(!App.isMobile) {
                    $('.group').toggleClass('active', false);
                    this.__groupEl.toggleClass('active', !value);
                    for (const group of $('.group:not(.active) .group-body')) {
                        setCollapse(group, true);
                    }
                    setCollapse(this.__groupEl.find('.group-body')[0], value);
                // }
                // else {
                    // $('.tab-indicators span[data-target="texts"]').click();
                // }
            },
            calib() {
                $('textarea').trigger('change');
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);
                // if(App.isMobile) {
                //     $('.tab-indicators span[data-target="texts"]').toggleClass('hidden', false);
                //     $('.tab-indicators span[data-target="texts"]').click(() => {
                //         setTimeout(() => {
                //             this.calib();
                //         });
                //     });
                // }
                this.child.init();
            }
        },
        backgroundText: {
            get __groupEl() {
                // if(!App.isMobile) {
                    return $(`.group.background-text-group`);
                // }
                // else {
                //     return $('.tab-content[data-tab="background-text"]');
                // }
            },
            onTextChange(callback) {
                const othis = this;
                this.__groupEl.on('change', 'textarea', function () {
                    callback.call(othis, $(this).val(), 'change');
                });
                if(!App.isMobile) {
                    this.__groupEl.on('input', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'input');
                    });
                }
            },
            setTitle(title) {
                this.__groupEl.find('label').html(title);
                return this;
            },
            setText(value) {
                this.__groupEl.find('textarea').val(value);
                this.__groupEl.find('textarea').trigger('change');
                return this;
            },
            setPlaceholder(value) {
                this.__groupEl.find('textarea').attr('placeholder', value);
                return this;
            },
            setCollapse(value) {
                // if(!App.isMobile) {
                    $('.group').toggleClass('active', false);
                    this.__groupEl.toggleClass('active', !value);
                    for (const group of $('.group:not(.active) .group-body')) {
                        setCollapse(group, true);
                    }
                    setCollapse(this.__groupEl.find('.group-body')[0], value);
                // }
                // else {
                //     $('.tab-indicators span[data-target="background-text"]').click();
                // }
            },
            focus() {
                this.__groupEl.find('textarea').focus();
            },
            calib() {
                $('textarea').trigger('input');
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);
                // if(App.isMobile) {
                //     $('.tab-indicators span[data-target="background-text"]').toggleClass('hidden', false);
                //     $('.tab-indicators span[data-target="background-text"]').click(() => {
                //         setTimeout(() => {
                //             this.calib();
                //         });
                //     });
                // }
                const rowsCalib = (el) => {
                    const protoEl = $('#fontSizeCalculationArea textarea:eq(0)');
                    protoEl.width(el.width());
                    protoEl.val(el.val());
                    el.height(protoEl[0].scrollHeight);
                };
                this.__groupEl.on('change', 'textarea', function () {
                    rowsCalib($(this));
                });
                if(!App.isMobile) {
                    this.__groupEl.on('input', 'textarea', function () {
                        rowsCalib($(this));
                    });
                }
            }
        },
        maps: {
            __prototype2: classFactory({
                get groupEl() {
                    return $(`.group.map-group`);
                },
                setLayer(layerData) {
                    this.__layer = layerData;
                },
                export(callback) {
                    const actualPixelRatio = window.devicePixelRatio;
                    const dpi = 4 * App.document.resolution * App.document.width / 4096
                        * (this.__layer.config.width / App.document.width);
                    Object.defineProperty(window, 'devicePixelRatio', {
                        get: function() {return dpi / 96}
                    });
                    const width = this.modal.el.find('.editor-map').width();
                    const height = this.modal.el.find('.editor-map').height();
                    var hidden = document.createElement('div');
                    hidden.className = 'hidden-map';
                    document.body.appendChild(hidden);
                    var container = document.createElement('div');
                    container.style.width = width;
                    container.style.height = height;
                    hidden.appendChild(container);
                    console.log(this);
                    const map = this.map;
                    var zoom = map.getZoom();
                    var center = map.getCenter();
                    var bearing = map.getBearing();
                    var pitch = map.getPitch();
                    var style = map.getStyle().sprite;
                    const mapStyle = App.helper.getMapStyleInfo();
                    var renderMap = new mapboxgl.Map({
                        container: container,
                        center: center,
                        zoom: zoom,
                        style: mapStyle.styleUrl,
                        bearing: bearing,
                        pitch: pitch,
                        interactive: false,
                        preserveDrawingBuffer: true,
                        fadeDuration: 0,
                        attributionControl: false
                    });

                    renderMap.once('idle', () => {
                        const format = 'png';
                        const base64 = renderMap.getCanvas().toDataURL();
                        const m = $(this.marker.getElement());
                        const editor = m.closest('.editor-map');
                        const markerAlignment = {
                            left    : m.position().left / editor.width(),
                            top     : m.position().top  / editor.height(),
                            width   : m.width()         / editor.width(),
                            height  : m.height()        / editor.height()
                        };

                        renderMap.remove();
                        hidden.parentNode.removeChild(hidden);
                        Object.defineProperty(window, 'devicePixelRatio', {
                            get: function() {return actualPixelRatio}
                        });

                        callback({
                            'map': base64,
                            'marker': markerAlignment
                        });
                    });
                },
                init(data) {
                    const editor = this;
                    this.openButton = $($.parseHTML(`
                    <button class="btn btn-light">Open map editor</button>`));
                    this.openButton.css('width', '100%');

                    // if(!App.isMobile) {
                        App.editors.maps.__groupEl.find('.group-body').append(this.openButton);
                    // }
                    // else {
                    //     App.editors.maps.__groupEl.append(this.openButton);
                    // }
                    if(this.openButton.index() > 0) {
                        this.openButton.css('margin-top', '1em');
                    }
                    this.modal = new (classFactory({
                        get el() {
                            return $(`#rep-modal-${this.__id}`);
                        },
                        launch() {
                            this.el.modal('show');
                        },
                        close() {
                            this.el.modal('hide');
                        },
                        on(eventName, callback) {
                            this.__eventEmitter.on(eventName, callback);
                        },
                        init(data) {
                            this.__eventEmitter = new EventEmitter;
                            const el = $($.parseHTML(`
                            <div class="modal fade scroll-hidden editor-map-modal" tabindex="-1" role="dialog" id="rep-modal-${this.__id}">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Map Editor</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                                style="outline:none; color:#000000;">
                                                <span aria-hidden="true" class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <input type="text" class="form-control place-input" placeholder="Input your place">
                                            </div>
                                            <div class="d-flex justify-content-center map-container">
                                                <div class="editor-map"></div>
                                            </div>
                                            <button class="apply-btn btn btn-secondary width-100 mt-2" 
                                                style="padding:1rem 0;background-color:white;border-radius:0;border-color:#3c535833;">
                                                <i class="fa fa-check"></i> Apply
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>`));
                            const eventMap = {
                                'shown.bs.modal'    : 'shown',
                                'show.bs.modal'     : 'show',
                                'hidden.bs.modal'   : 'hidden',
                                'hide.bs.modal'     : 'hide'
                            };
                            for(const [k, v] of Object.entries(eventMap)) {
                                $(document).on(k, `#rep-modal-${this.__id}`, (...args) => {
                                    this.__eventEmitter.emit(v, ...args);
                                });
                            }
                            $(document).on('click', `#rep-modal-${this.__id} .apply-btn`, () => {
                                this.__eventEmitter.emit('apply');
                            });
                            $('body').append(el);
                        }
                    }));
                    this.openButton.on('click', () => {
                        this.modal.launch();
                    });

                    if(true) {
                        const config = {
                            "mapOptions": {
                                "zoom": 10,
                                "styles": "ckjmh73zj1vdp19o2782cxn27",
                                "places": null
                            },
                            "markerLocation": {
                                "lat": null,
                                "lng": null
                            }
                        };
                        const maskInfo = data.mask.config;
                        const markerInfo = data.marker.config;
                        const markerIconURL = `/design-upload/extract/${App.productName}/` + markerInfo.image;

                        // aka init for the first time
                        const afterModalShown = () => {
                            if(editor.modal.el.find('.mapboxgl-canvas').length) return;

                            const el = editor.modal.el.find('.editor-map');

                            // map container calibration
                            const maskRatio = maskInfo.width / maskInfo.height;
                            el.height(el.width() / maskRatio);

                            const mapStyle = App.helper.getMapStyleInfo();
                            mapboxgl.accessToken = mapStyle.accessToken;
                            const map = new mapboxgl.Map({
                                container: el[0],
                                style: mapStyle.styleUrl,
                                center: [ // starting position
                                    App.editors.maps.__prevLocation.lng,
                                    App.editors.maps.__prevLocation.lat,
                                ],
                                zoom: 9,                    // starting zoom
                                bearing: 0,
                                pitch: 0,
                                preserveDrawingBuffer: true
                            });
                            map.addControl(new mapboxgl.NavigationControl());

                            const input = editor.modal.el.find('.place-input')[0];
                            const autocomplete = new google.maps.places.Autocomplete(input);
                            autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
                            autocomplete.addListener("place_changed", () => {
                                const place = autocomplete.getPlace();
                                map.flyTo({
                                    center: [
                                        place.geometry.location.lng(),
                                        place.geometry.location.lat()
                                    ],
                                    speed: 2,
                                    curve: 1,
                                    essential: true
                                });
                            });


                            editor.modal.on('shown', () => {
                                setTimeout(() => {
                                    map.resize();
                                }, 200);
                            });

                            editor.map = map;

                            const markerEl = $(document.createElement('div'));            
                            const markerWidthRatio  = markerInfo.width  / maskInfo.width;
                            const markerHeightRatio = markerInfo.height / maskInfo.height;
                            const markerWidth   = el.width()    * markerWidthRatio;
                            const markerHeight  = el.height()   * markerHeightRatio;

                            markerEl.css('background-image', `url(${markerIconURL})`);
                            markerEl.css('background-size', '100%');
                            markerEl.css('width', markerWidth+'px');
                            markerEl.css('height', markerHeight+'px');
                            const marker = new mapboxgl.Marker(markerEl[0], {
                                anchor: 'center',
                                draggable: true
                            });

                            marker.setLngLat([
                                config.markerLocation.lng, config.markerLocation.lat
                            ])
                            // .addTo(map);

                            editor.marker = marker;
                            marker.setLngLat(map.getCenter());

                            map.on('moveend', function() {
                                marker.setLngLat(map.getCenter());
                            });

                            $('.mapboxgl-ctrl-bottom-left').remove();
                            $('.mapboxgl-ctrl-bottom-right').remove();
                        };

                        editor.modal.on('shown', () => {
                            const el = editor.modal.el.find('.editor-map');
                            el.height(el.width() / maskInfo.width * maskInfo.height);
                            const maxHeight = $(window).height() * 0.7;
                            if(el.height() > maxHeight) {
                                const scaleDownRatio = maxHeight / el.height();
                                el.height(el.height()   * scaleDownRatio);
                                el.width(el.width()     * scaleDownRatio);
                            }
                            afterModalShown();
                            if(!editor.__lastLocation) { editor.__lastLocation = editor.map.getCenter(); }
                            editor.map.setCenter(editor.__lastLocation);
                        });
                    }
                }
            }),
            __prototype: {
                __el: null,
                __map: null,
                __tileLayer: null,
                __layer: {},
                onMapChange(callback) {
                },
                onPlaceChange(callback) {
                    this.__el.find('input').on('placeChanged', e => {
                        callback(e.message);
                    });
                },
                onApply(callback) {
                    this.__el.on('click', 'button.apply-btn', () => {
                        this.toDataURL(b64 => {
                            callback.call(this, b64);
                        });
                    });
                },
                setTitle(title) {
                    this.__el.find('.form-item-label').text(title);
                    return this;
                },
                setLayer(layer) {
                    this.__layer = layer;
                    return this;
                },
                syncLocation() { },
                getLocation() {
                    // not implemented
                },
                setCollapse(value) {
                    const subgroupBodyEl = this.__el.find('.subgroup-body');
                    $('.subgroup').toggleClass('active', false);
                    this.__el.toggleClass('active', !value);
                    for (const el of $('.subgroup-body.collapsible:not(.active)')) {
                        if (el != subgroupBodyEl[0]) {
                            setCollapse(el, true);
                        }
                    }
                    setCollapse(subgroupBodyEl[0], value);
                    if (!value) {
                        setTimeout(() => {
                            subgroupBodyEl.closest('.subgroup')[0].scrollIntoView();
                        }, 300);
                    }
                    return this;
                },
                focus() {
                    this.__el.find('input').focus(); // focus in location input
                },
                toDataURL(callback) {
                    this.__el.find('.editor-map *').css('transition', 'none');
                    html2canvas(this.__el.find('.editor-map')[0], {
                        useCORS: !0,
                        allowTaint: !1,
                        scale: 1,
                        optimized: !1,
                        ignoreElements: function (t) {
                            return "IFRAME" === t.nodeName;
                        },
                    })
                    .then(canvas => {
                        callback(canvas.toDataURL());
                    });
                },
                // real initializer
                fetch(data) {
                    console.log(data);
                    App.editors.maps.__groupEl.toggleClass('active', true);
                    const maskInfo = data.mask.config;
                    const markerInfo = data.marker.config;
                    (() => {
                        const latlngDefault = [39.7642543, -104.9955352];
                        const config = {
                            "mapOptions": {
                                "center": {
                                    "lat": 39.7642543,
                                    "lng": -104.9955352
                                },
                                "zoom": 10,
                                "styles": "ckjmh73zj1vdp19o2782cxn27",
                                "places": null
                            },
                            "markerLocation": {
                                "lat": null,
                                "lng": null
                            },
                            "markerOffset": {
                                "x": 284.568572227176,
                                "y": 284.5455220218516
                            }
                        };
                        // I dont understand it either :(
                        const tileLayer = L.tileLayer("https://api.mapbox.com/styles/v1/" + MAPBOX_API.username + "/" + 
                            config.mapOptions.styles + "/tiles/{z}/{x}/{y}?access_token=" + MAPBOX_API.accessToken, {
                            tileSize: 512,
                            zoomOffset: -1,
                            minZoom: 1,
                            crossOrigin: true
                        });
                        this.__tileLayer = tileLayer;
                        const maskRatio = maskInfo.width / maskInfo.height;
                        this.__el.find('.editor-map').height(this.__el.find('.editor-map').width() / maskRatio);
                        this.__map = L.map(this.__el.find('.editor-map')[0], {
                            zoomControl: false
                        }).addLayer(tileLayer).setView(
                            [
                                config.mapOptions.center.lat,
                                config.mapOptions.center.lng
                            ],
                            config.mapOptions.zoom);
                        // invalidate map size after subgroup body expanded
                        const subgroupBodyEl = this.__el.find('.subgroup-body');
                        subgroupBodyEl.on('transitionend', () => {
                            if(subgroupBodyEl.attr('data-collapsed') == 'expanding') {
                                this.__map.invalidateSize();
                            }
                        });
                        // map marker
                        const scaleMask = 1; // why?
                        const markerLocation = config.markerLocation;
                        const markerWidth = $('.editor-map').width() * scaleMask / 8;
                        const markerHeight = $('.editor-map').height() * scaleMask / 8;
                        this.__marker = L.marker(
                            [
                                markerLocation.lat || latlngDefault[0],
                                markerLocation.lng || latlngDefault[1],
                            ],
                            {
                                icon: L.icon({
                                    iconUrl: `/design-upload/extract/${App.productName}/` + markerInfo.image,
                                    iconSize: [
                                        markerWidth, markerHeight
                                    ],
                                    iconAnchor: [
                                        (markerWidth) / 2,
                                        markerHeight,
                                    ],
                                }),
                                draggable: true,
                                autoPan: true,
                            }
                        ).addTo(this.__map);
                    })();
                    App.editors.maps.__groupEl.toggleClass('active', false);
                },
                init() {
                    const html = `
                    <div class="subgroup">
                        <div class="subgroup-title">
                            <i class="fas fa-sliders-h"></i>
                            <span>Map Editor</span>
                        </div>
                        <div class="subgroup-body collapsible editor-map-wrapper" data-collapsed="false">
                            <div class="row" style="padding: 0 1rem;">
                                <div class="col-12 mt-1 d-flex justify-content-center">
                                    <div class="editor-map"></div>
                                </div>
                                <div class="col-12 mt-1 d-flex apply-btn-wrapper">
                                    <button class="btn btn-dark apply-btn width-100">Apply</button>
                                </div>
                            </div>
                        </div>
                    </div>`;
                    this.__el = $($.parseHTML(html));
                },
                clone() {
                    const newObj = {};
                    Object.assign(newObj, this);
                    newObj.init();
                    return newObj;
                }
            },
            __children: [],
            __prevLocation: {
                lng: -94.58703058563479,
                lat: 39.13043540145199
            },
            get __groupEl() {
                // if(!App.isMobile) {
                    return $(`.group.map-group`);
                // }
                // else {
                //     return $('.tab-content[data-tab="maps"]');
                // }
            },
            newEditor(data) {
                const editor = new this.__prototype2(data);
                this.__children.push(editor);
                // if(!App.isMobile) {
                    this.__groupEl.find('.group-body').append(editor.__el);
                // }
                // else {
                //     this.__groupEl.append(editor.__el);
                // }
                return editor;
            },
            setCollapse(value) {
                $('.group').toggleClass('active', false);
                this.__groupEl.toggleClass('active', !value);
                for (const group of $('.group:not(.active) .group-body')) {
                    setCollapse(group, true);
                }
                setCollapse(this.__groupEl.find('.group-body')[0], value);
            },
            get(idx) {
                return this.__children[idx];
            },
            init() {
                this.__children = [];
                this.__groupEl.toggleClass('hidden', false);
                // if(App.isMobile) {
                //     $('.tab-indicators span[data-target="maps"]').toggleClass('hidden', false);
                // }
            }
        },
        song: {
            get __groupEl() {
                // if(!App.isMobile) {
                    return $(`.group.song-group`);
                // }
                // else {
                //     return $('.tab-content[data-tab="song"]');
                // }
            },
            setCollapse(value) {
                $('.group').toggleClass('active', false);
                this.__groupEl.toggleClass('active', !value);
                for (const group of $('.group:not(.active) .group-body')) {
                    setCollapse(group, true);
                }
                setCollapse(this.__groupEl.find('.group-body')[0], value);
            },
            onApply(callback) {
                this.__groupEl.on('click', '.apply-btn', () => {
                    callback(this.__groupEl.find('textarea').val());
                });
            },
            getLyrics() {
                return this.__groupEl.find('textarea').val();
            },
            setLyrics(text) {
                this.__groupEl.find('textarea').val(text);
            },
            focus() {
                this.__groupEl.find('textarea').focus();
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);
                // if(App.isMobile) {
                //     $('.tab-indicators span[data-target="song"]').toggleClass('hidden', false);
                // }
            }
        },
        spotify: {
            child: {
                __el: null,
                onSearchInputChange(callback) {
                    let lastChanged = 0;
                    const self = this;
                    const groupEl = App.editors.spotify.__groupEl;
                    groupEl.on('change input', '.search-input', function () {
                        lastChanged = (new Date).getTime();
                        setTimeout(() => {
                            if((new Date).getTime() - lastChanged < 500) return;
                            callback(
                                groupEl.find('.search-input').val()
                            );
                        }, 500);
                    });
                },
                onSearchResultSelect(callback) {
                    const groupEl = App.editors.spotify.__groupEl;
                    groupEl.on('click', '.search-result', function() {
                        callback($(this).attr('data-uri'));
                        $('.search-result').toggleClass('selected', false);
                        $(this).toggleClass('selected', true);
                    });
                },
                onTextChange(callback) {
                    const othis = this;
                    this.__el.on('change', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'change');
                    });
                    this.__el.on('keyup', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'keyup');
                    });
                },
                setSearchResults(data) {
                    const groupEl = App.editors.spotify.__groupEl;
                    groupEl.find('.search-results').html(data.tracks.items.map(item => `
                        <div class="search-result" data-uri="${item.uri}">
                            <span class="artist">${item.artists[0].name}</span><br>
                            <span class="song-name">${item.name}</span>
                        </div>
                    `).join(''));
                },
                setTitle(title) {
                    this.__el.find('label').html(title);
                    return this;
                },
                setLayer(layer) {
                    this.__layer = layer;
                    return this;
                },
                setText(value) {
                    this.__el.find('textarea').val(value);
                    this.__el.find('textarea').trigger('change');
                    return this;
                },
                setPlaceholder(value) {
                    this.__el.find('textarea').attr('placeholder', value);
                    return this;
                },
                getValue() {
                    return this.__el.find('textarea').val();
                },
                focus() {
                    this.__el.find('textarea').focus();
                },
                init() {
                    const html = `
                    <div class="form-group">
                        <label></label>
                        <textarea class="form-control" rows="1" spellcheck="false"></textarea>
                    </div>`;
                    this.__el = $(App.editors.spotify.__groupEl.find('.form-group'));
                    const rowsCalib = (el) => {
                        const protoEl = $('#fontSizeCalculationArea textarea:eq(0)');
                        protoEl.width(el.width());
                        protoEl.val(el.val());
                        el.height(protoEl[0].scrollHeight);
                    };
                    this.__el.on('change', 'textarea', function () {
                        rowsCalib($(this));
                    });
                    if(!App.isMobile) {
                        this.__el.on('input', 'textarea', function () {
                            rowsCalib($(this));
                        });
                    }
                }
            },
            get __groupEl() {
                // if(!App.isMobile) {
                    return $(`.group.spotify-group`);
                // }
                // else {
                //     return $('.tab-content[data-tab="texts"]');
                // }
            },
            setCollapse(value) {
                // if(!App.isMobile) {
                    $('.group').toggleClass('active', false);
                    this.__groupEl.toggleClass('active', !value);
                    for (const group of $('.group:not(.active) .group-body')) {
                        setCollapse(group, true);
                    }
                    setCollapse(this.__groupEl.find('.group-body')[0], value);
                // }
                // else {
                //     $('.tab-indicators span[data-target="texts"]').click();
                // }
            },
            calib() {
                $('textarea').trigger('change');
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);
                // if(App.isMobile) {
                //     $('.tab-indicators span[data-target="texts"]').toggleClass('hidden', false);
                //     $('.tab-indicators span[data-target="texts"]').click(() => {
                //         setTimeout(() => {
                //             this.calib();
                //         });
                //     });
                // }
                this.child.init();
            }
        },
        theme: {
            onChooseTheme(callback) {
                this.eventEmitter.on('themeChoose', callback);
            },
            get groupEl() {
                return $('.group.theme-group');
            },
            get selectedID() {
                const selectedOption = this.groupEl.find('.theme-option.selected');
                if(selectedOption.length == 0) return -1;
                const id = selectedOption.closest('.theme-option-wrapper').index();
                return id;
            },
            get selectedFilter() {
                const themeID = this.selectedID;
                if(themeID == -1) return null;
                return this.filters[themeID];
            },
            init() {
                const filters = [
                    null,
                    fabric.Image.filters.BlackWhite,
                    fabric.Image.filters.Grayscale,
                    fabric.Image.filters.Vintage,
                    fabric.Image.filters.Kodachrome,
                    fabric.Image.filters.Technicolor,
                    fabric.Image.filters.Polaroid,
                    fabric.Image.filters.Brownie,
                ];
                this.filters = filters;
                const self = this;
                this.eventEmitter = new EventEmitter2;
                this.groupEl.on('click', '.theme-option', function() {
                    $('.theme-group .theme-option').toggleClass('selected', false);
                    $(this).toggleClass('selected', true);
                    self.eventEmitter.emit('themeChoose', self.selectedFilter);
                });
                this.groupEl.toggleClass('hidden', false);
                this.initialized = true;
            }
        },
        __devProto: classFactory({
            get __groupEl() {
                // if(!App.isMobile) {
                    return $(`.group.dev-group`).closest('.group');
                // }
                // else {
                //     return $('.tab-content[data-tab="dev"]');
                // }
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);

                const layout = new Shitonen({
                    html: `
                        <div>
                            <div class="layers">
                                {{ layers }}
                            </div>
                            <div class="width-100 mt-3 pt-2" style="border-top: 1px solid #aaa;">
                                <button class="btn btn-light font-weight-normal">
                                    <i class="fa fa-copy mr-2"></i>
                                    Copy JSON
                                </button>
                            </div>
                        </div>
                        `,
                    shit: {
                        layers: []
                    },
                    shitConfig: {
                        layers: { escape: false }
                    }
                });
                const layers = App.layers.map(layer => {
                    if(['map'].includes(layer.type)) return null;
                    return new Shitonen({
                        html: `
                            <div class="subgroup">
                                <div class="subgroup-title">
                                    <i class="fa fa-{{icon}}"></i>
                                    <span class="font-weight-normal">{{title}} </span>
                                    <span class="font-weight-normal">{{status}}</span>
                                </div>
                                <div class="subgroup-body collapsible" data-collapsed="true" style="height: 0px;">
                                    <div class="content-wrapper" style="margin: .25rem 1.5rem;">
                                        <div class="d-flex align-items-stretch mt-2">
                                            <button class="btn btn-light visible-toggle" style="border:1px solid #4c596a33 !important;border-right: none !important;width:4rem;">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                            <div class="opacity-wrapper d-flex align-items-center" style="flex: 1;background-color: #ffffffe6;border: 1px solid #4c596a33;border-left: none;padding-right:1.25em;">
                                                <span class="opacity-value-text mr-2" style="width: 2rem">${(layer.data.opacity ?? layer.config.opacity) * 100}</span>
                                                <input class="ranger" type="range" min="0" max="100" value="${(layer.data.opacity ?? layer.config.opacity) * 100}">
                                            </div>
                                        </div>
                                        <div class="d-flex align-items-stretch mt-2">
                                            <button class="btn btn-light" style="line-height:1;border:1px solid #4c596a33 !important;border-right: none !important;width:4rem;">
                                                X
                                            </button>
                                            <div class="left-wrapper d-flex align-items-center" style="flex: 1;background-color: #ffffffe6;border: 1px solid #4c596a33;border-left: none;padding-right:1.25em;">
                                                <span class="mr-2" style="width: 2rem">--</span>
                                                <input class="ranger" type="range" min="-20" max="120" value="${(layer.data.left ?? layer.config.left) / App.document.width * 100}">
                                            </div>
                                        </div>
                                        <div class="d-flex align-items-stretch mt-2">
                                            <button class="btn btn-light" style="line-height:1;border:1px solid #4c596a33 !important;border-right: none !important;width:4rem;">
                                                Y
                                            </button>
                                            <div class="top-wrapper d-flex align-items-center" style="flex: 1;background-color: #ffffffe6;border: 1px solid #4c596a33;border-left: none;padding-right:1.25em;">
                                                <span class="mr-2" style="width: 2rem">--</span>
                                                <input class="ranger" type="range" min="-20" max="110" value="${(layer.data.top ?? layer.config.top) / App.document.height * 100}">
                                            </div>
                                        </div>
                                        <div class="d-flex align-items-stretch mt-2">
                                            <button class="btn btn-light" style="text-transform:none;line-height:1;border:1px solid #4c596a33 !important;border-right: none !important;width:4rem;">
                                                Aa
                                            </button>
                                            <div class="font-size-wrapper d-flex align-items-center" style="flex: 1;background-color: #ffffffe6;border: 1px solid #4c596a33;border-left: none;padding-right:1.25em;">
                                                <span class="mr-2" style="width: 2rem">--</span>
                                                <input class="ranger" type="range" min="0" max="100" value="{{initialFontSizeLevel}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`,
                        shit: {
                            data: layer,
                            icon: 'question',
                            status: '',
                            title: '',
                            initialFontSizeLevel: 100,
                        },
                        init() {
                            const iconmap = {
                                'fg'        : 'image',
                                'text'      : 'font',
                                'mask'      : 'mask',
                                'map-mask'  : 'map',
                                'marker'    : 'map-marker-alt',
                                'bgcolor'   : 'palette',
                            };
                            this.shit.icon = iconmap[layer.type] ?? 'question';
                            this.shit.title = `#${layer.id} ${layer.type}`;
                            if(layer.type == 'text') {
                                this.shit.title += ' - ' + layer.typeMetadata.label;
                            }
                            this.query('.visible-toggle').onclick = e => {
                                const el = $(this.query('.visible-toggle'));
                                layer.data.visible = !layer.data.visible;
                                App.canvas.refresh();
                                if(!layer.data.visible) {
                                    el.find('i').attr('class', 'fa fa-eye-slash');
                                    this.shit.status = '(hidden)';
                                }
                                else {
                                    el.find('i').attr('class', 'fa fa-eye');
                                    this.shit.status = '';
                                }
                            };
                            const updateOpacity = e => {
                                layer.data.opacity = (+this.query('.opacity-wrapper input').value) / 100;
                                this.query('.opacity-wrapper .opacity-value-text').textContent = this.query('.opacity-wrapper input').value;
                            };
                            const updateLeft = e => {
                                layer.data.left = (+this.query('.left-wrapper input').value) / 100 * App.document.width;
                                App.canvas.refresh();
                            };
                            const updateTop = e => {
                                layer.data.top = (+this.query('.top-wrapper input').value) / 100 * App.document.height;
                                App.canvas.refresh();
                            };
                            this.query('.opacity-wrapper input').onchange = updateOpacity;
                            this.query('.opacity-wrapper input').oninput = updateOpacity;
                            this.query('.left-wrapper input').onchange = updateLeft;
                            this.query('.left-wrapper input').oninput = updateLeft;
                            this.query('.top-wrapper input').onchange = updateTop;
                            this.query('.top-wrapper input').oninput = updateTop;
                            if(layer.type == 'text') {
                            console.log((layer.data.left ?? layer.config.left) / App.document.width * 100);
                                const baseFontSize = App.document.width / 200;
                                const updateFontSize = e => {
                                    const el = $(this.query('.font-size-wrapper input'));
                                    layer.data.fontSize = baseFontSize * Math.pow(1.04, +el.val());
                                    layer.data.textValue = layer.data.textValue;
                                    App.canvas.refresh();
                                };
                                this.query('.font-size-wrapper input').onchange = updateFontSize;
                                this.query('.font-size-wrapper input').oninput = updateFontSize;
                                this.shit.initialFontSizeLevel = Math.logBase(layer.config.text.font.sizes[0] / baseFontSize, 1.04);
                            }
                            else {
                                $(this.query('.font-size-wrapper')).parent().toggleClass('d-flex', false);
                                $(this.query('.font-size-wrapper')).parent().toggleClass('d-none', true);
                            }
                        }
                    });
                }).filter(v => v ?? null != null);
                layout.shit.layers = layers;

                $('#dev-tools-wrapper').html('');
                $('#dev-tools-wrapper').append(layout.element);
            }
        }),
        init() {
            this.imageGallery.init();
            if(App.debugMode) this.dev = new this.__devProto;
        }
    },
    helper: {
        __private: {},
        layerQuery(selector) {
            const result = [];
            for(const layer of App.layers) {
                const matchType = selector.type != undefined
                    ? layer.type == selector.type
                    : true
                    ;
                if(!matchType) continue;
                let matchMeta = true;
                for(const [metaName, metaValue] of Object.entries(selector.meta ?? {})) {
                    if(layer.typeMetadata[metaName] != metaValue) {
                        matchMeta = false;
                        break;
                    }
                }
                if(matchMeta) result.push(layer);
            }
            return result;
        },
        layerAssert(layer, condition, callback) {
            const matchType = condition.type != undefined
                ? layer.type == condition.type
                : true
                ;
            if(!matchType) return false;
            let matchMeta = true;
            for(const [metaName, metaValue] of Object.entries(condition.meta ?? {})) {
                if(layer.typeMetadata[metaName] != metaValue) {
                    matchMeta = false;
                    break;
                }
            }
            if(!matchMeta) return false;
            callback && callback();
            return true;
        },
        getMapStyleInfo() {
            const styleAlias = App.mapStyle;
            if(MAPBOX_STYLES[styleAlias]) {
                return MAPBOX_STYLES[styleAlias];
            }
            return MAPBOX_STYLES.default;
        },
        postBase64(b64) {
            $.ajax({
                url: 'savebase64',
                method: 'post',
                data: JSON.stringify({
                    base64: b64,
                    debug: App.debugMode
                })
            })
            .done(res => {
                const filename = res['filename'];
                const url = 'http://customize.navitee.com/design-upload/base64/'+filename;
                if(App.debugMode) {
                    window.open(url, '_blank');
                }
            })
            .fail(err => console.log(err));
        },
        loadFont(name, url, callback) {
            const hash = (str) => {
                let result = '';
                for (const [cid, c] of [...str].entries()) {
                    result += c.charCodeAt(0) * (cid + 1);
                }
                return (+result) % 123456789101112;
            };
            const fontLoaderClass = `font-loader-${hash(name)}`;
            if ($(`.${fontLoaderClass}`).length == 0) {
                $('body').append(`
                <defs class="${fontLoaderClass}">
                    <style>
                        @font-face {
                            font-family: ${JSON.stringify(name)};
                            src: url(${JSON.stringify(url)});
                        }
                    </style>
                </defs>
                `);
            }
            const font = new FontFaceObserver(name);
            if(!this.__private.fontloader) this.__private.fontloader = {};
            const promise = font.load(null, 10000);
            if(!this.__private.fontloader[name]) {
                promise.catch(e => {
                    Toastify({
                        text: `Failed to load font '${name}': ${e}`,
                        duration: 3000,
                        close: true,
                        gravity: "top", // `top` or `bottom`
                        position: "left", // `left`, `center` or `right`
                        backgroundColor: "#bd2300",
                        stopOnFocus: true, // Prevents dismissing of toast on hover
                        onClick: function(){} // Callback after click
                    }).showToast();
                    console.error(`Failed to load font '${name}': ${e}`);
                });
                this.__private.fontloader[name] = true;
            }
            callback && promise.then(callback);
        },
        initCropper(el, ratio, readyCallback) {
            return $(el).cropper({
                // initialAspectRatio: ratio ?? 16 / 9,
                aspectRatio: ratio ?? 16 / 9,
                crop: function (event) {
                },
                zoom: function (e) {
                    e.preventDefault(); // Prevent zoom
                },
                ready: readyCallback
            });
        },
        quickCrop(dataURL, ratio, callback) {
            const hash = (str) => {
                let result = '';
                for (const [cid, c] of [...str].entries()) {
                    result += c.charCodeAt(0) * (cid + 1);
                }
                return (+result) % 123456789101112;
            };
            const hashcode = hash(''+Math.random());
            $('#cropArea').append(`<img id="cropimg${hashcode}">`);
            $(`#cropArea #cropimg${hashcode}`).attr('src', dataURL);
            this.initCropper($(`#cropArea #cropimg${hashcode}`)[0], ratio, () => {
                const croppedDataURL = $(`#cropArea #cropimg${hashcode}`).cropper('getCroppedCanvas').toDataURL();
                $(`#cropArea #cropimg${hashcode}`).cropper('destroy');
                $(`#cropArea #cropimg${hashcode}`).remove();
                callback(croppedDataURL);
            });
        },
        lockObject(obj) {
            obj.set({
                lockMovementX: !0,
                lockMovementY: !0,
                hasControls: !1,
                hoverCursor: "pointer",
            });
        },
        rgbToHex(r, g, b, a) {
            const red = ('0' + (+(r ?? 0)).toString(16)).slice(-2);
            const green = ('0' + (+(g ?? 0)).toString(16)).slice(-2);
            const blue = ('0' + (+(b ?? 0)).toString(16)).slice(-2);
            const alpha = ('0' + (+(a ?? 0)).toString(16)).slice(-2);
            if (a == undefined) {
                return '#' + red + green + blue;
            }
            return '#' + red + green + blue + alpha;
        },
        setTextObjectValue(textObject, newText, layerData) {
            const widthLimit = layerData.config.width;
            const heightLimit = layerData.config.height;
            const originalFontSize = layerData.data.fontSize || layerData.config.text.font.sizes[0];
            textObject.set({fontSize: originalFontSize});
            newText = newText.replaceAll('\r', '\n');
            textObject.set('text', newText);
            const calculatedFontSize = {
                width: originalFontSize * widthLimit / textObject.width,
                height: originalFontSize * heightLimit / textObject.height * 1.4
            };
            textObject.set('fontSize', Math.min(originalFontSize, calculatedFontSize.width, calculatedFontSize.height));
            // textObject.set('fontSize', Math.min(originalFontSize, calculatedFontSize.width));
            textObject.set({
                top: layerData.data.top + (layerData.config.height - textObject.height) / 2
            });
        },
        loadSVG(path, callback) {
            $.ajax({
                url: path,
                type: 'get',
                dataType: 'html',
                success(res) {
                    fabric.loadSVGFromString(res, (objects, options) => {
                        const svg = fabric.util.groupSVGElements(objects, options);
                        callback(svg);
                    });
                },
                error(err) {
                    console.log('Failed to load SVG', err);
                }
            });
        },
        downloadURI(uri) {
            var link = document.createElement("a");
            link.download = undefined;
            link.href = uri;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            delete link;
        },
        openBase64InNewTab(b64) {
            var image = new Image();
            image.src = b64;

            var w = window.open("");
            w.document.write(image.outerHTML);
        },
        openInNewTab(url) {
            window.open(url, '_blank');
        },
        asyncLoop(callback, interval = 50) {
            let lock = false;
            const intervalID = setInterval(() => {
                if (!lock) {
                    lock = true;
                    const breaker = () => { clearInterval(intervalID) };
                    callback(breaker);
                    lock = false;
                }
            }, interval);
        },
        setLayerProp(layer, name, value) {
            Object.defineProperty(layer.data, name, {
                get() {
                    return value;
                },
                set(v) {
                    const oldValue = value;
                    value = v;
                    App.events.emit('layerPropChange', {
                        layer: layer,
                        propName: name,
                        propValue: v,
                        oldValue: oldValue
                    });
                }
            });
        },
        formatDuration(duration_ms) {
            const h = parseInt(duration_ms / 3600000);
            const m = parseInt((duration_ms % 3600000) / 60000);
            const s = parseInt((duration_ms % 60000) / 1000);
            // console.log(res);
            const prefix = function(n) {
                n = ''+n;
                if(n.length <= 2) return ('0' + n).slice(-2);
                return n;
            };
            const duration = (h > 0 ? prefix(h) + ':' : '') + prefix(m) + ':' + prefix(s);
            return duration;
        },
        createAutoResizeTextarea() {
            const scrollHiddenCSS = `
                overflow: hidden !important;
                -ms-overflow-style: none !important;
                scrollbar-width: none !important;
            `;
            if($('body > .autoresize-textarea-zone').length == 0) {
                $('body').append('<div class="autoresize-textarea-zone"></div>');
                $('body > .autoresize-textarea-zone').attr('style', `visibility:hidden;position:fixed;z-index:-9999999;`);
            }
            const hiddenTextarea = new Shitonen({
                html: `
                    <textarea style="{{style}}" rows="1"></textarea>
                `,
                shit: {
                    style: scrollHiddenCSS,
                    value: ''
                },
                init() {
                    this.shit.on('init change', 'value', _ => {
                        this.element.value = this.shit.value;
                    });
                    $('body > .autoresize-textarea-zone').append(this.element);
                }
            });
            const displayTextarea = new Shitonen({
                html: `<textarea class="form-control"></textarea>`,
                shit: {
                    value: ''
                },
                init() {
                    $(this.element).toggleClass('autoresize-textarea', true);
                    CustomCSS.add('.autoresize-textarea', `
                        resize: none !important;
                        ${scrollHiddenCSS}
                    `);
                    const update = _ => {
                        this.shit.value = this.element.value;
                    };
                    this.element.oninput = update;
                    this.element.onchange = update;
                    const copyStyle = (name) => {
                        const css = $(this.element).css(name);
                        $(hiddenTextarea.element).css(name, css);
                    };
                    this.shit.on('init change', 'value', _ => {
                        hiddenTextarea.shit.value = this.shit.value;
                        $(this.element).val(this.shit.value);
                        copyStyle('width');
                        copyStyle('font-family');
                        copyStyle('font-size');
                        copyStyle('font-weight');
                        copyStyle('line-height');
                        const height = hiddenTextarea.element.scrollHeight;
                        $(this.element).css('height', height+'px');
                    });
                }
            });
            return displayTextarea;
        },
        createFabricSVG(svg, cb) {
            fabric.loadSVGFromString(svg, function(objects, options) {
                var svgGroup = fabric.util.groupSVGElements(objects, options);
                svgGroup.set({
                    lockMovementX: !0,
                    lockMovementY: !0,
                    hasControls: !1,
                    hoverCursor: "pointer"
                });
                cb && cb(svgGroup);
            });
        },
        getSpiralTextBase64({
            width=100,
            height=100,
            lineHeight=1,
            start=600,
            end=18000,
            text='',
            letterSpacing=2,
            color='#000',
            fontSize=20,
            fontFamily='Georgia'
        }={}) {
            if($('#spiralCanvas').length == 0) {
                $(document.body).append('<canvas id="spiralCanvas"></canvas>');
            }

            const path = [];
            const trueLineHeight = fontSize / 4 * lineHeight;
            for (i = 0; i < 7200; i++) {
                const angle = 0.1 * i;
                const deg = angle / Math.PI * 180;
                if(deg >= start && deg <= end) {
                    const x = (trueLineHeight + trueLineHeight * angle) * Math.cos(angle);
                    const y = (trueLineHeight + trueLineHeight * angle) * Math.sin(angle);
                    path.push(x);
                    path.push(y);
                }
            }

            const canvas = document.getElementById("spiralCanvas");
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext("2d");
            function drawPath(path) {
                ctx.save();
                ctx.strokeStyle = "#00000000";
                ctx.lineWidth = 1;
                ctx.beginPath();
                ctx.moveTo(path[0], path[1]);
                for (var i = 2; i < path.length; i += 2) {
                    ctx.lineTo(path[i], path[i + 1]);
                }
                ctx.stroke();
                ctx.restore();
            }

            ctx.font = `${fontSize}px ${fontFamily}`;
            ctx.textBaseline = "middle";
            ctx.fillStyle = color;
            ctx.lineWidth = letterSpacing;
            ctx.strokeStyle = "#00000000";

            ctx.translate(width / 2, height / 2);
            drawPath(path);
            ctx.textPath(text, path);

            return canvas.toDataURL();
        }
    },
    layerProcessor: {
        fg(layerData, callback, options={}) {
            const finished = !!options.finished;
            const imgUrl = `/design-upload/extract/${App.productName}/${layerData.config.preview ?? layerData.config.image}`;
            const fullsizeImgUrl = `/design-upload/extract/${App.productName}/${layerData.config.image}`;
            let data = {
                __loaded: 0,
                __threshold: 2,
                objects: [],
                get loaded() { return this.__loaded; },
                set loaded(v) { this.__loaded = v; }
            };
            fabric.Image.fromURL(imgUrl, img => {
                img.set({
                    left: layerData.data.left,
                    top: layerData.data.top,
                    evented: false,
                });
                img.scaleToWidth(layerData.config.width);
                img.scaleToHeight(layerData.config.height);
                App.helper.lockObject(img);
                data.objects[0] = img;
                data.loaded += 1;
                callback(data.objects);
            }, { crossOrigin: 'anonymous' });
            fabric.Image.fromURL(fullsizeImgUrl, img => {
                img.set({
                    left: layerData.data.left,
                    top: layerData.data.top,
                    evented: false,
                });
                img.scaleToWidth(layerData.config.width);
                img.scaleToHeight(layerData.config.height);
                App.helper.lockObject(img);
                data.objects[1] = img;
                data.loaded += 1;
            }, { crossOrigin: 'anonymous' });
        },
        text(layerData, callback) {
            const textValue = layerData.data.textValue ?? layerData.config.text.value;
            const fontConfig = layerData.config.text.font;
            const fontName = fontConfig.name;
            const fontUrl = `/design-upload/extract/${App.productName}/${fontConfig.path}`;
            App.helper.loadFont(fontName, fontUrl, () => {
                const textObject = new fabric.Text(textValue, {
                    left: layerData.data.left,
                    top: layerData.data.top,
                    fill: App.helper.rgbToHex(...fontConfig.colors[0]),
                    fontFamily: fontName,
                    fontSize: fontConfig.sizes[0],
                    splitByGrapheme: true,
                    textAlign: "center"
                });
                if(fontConfig.alignment[0] == 'center') {
                    textObject.set({
                        left: layerData.data.left + layerData.config.width / 2
                    });
                }
                /* STROKE EFFECT */
                if (fontConfig.stroke) {
                    textObject.set({
                        stroke: fontConfig.stroke.color,
                        strokeWidth: fontConfig.stroke.width,
                        strokeUniform: true,
                        paintFirst: "stroke",
                    });
                }
                App.helper.lockObject(textObject);
                textObject.set({
                    originX: fontConfig.alignment[0],
                });
                callback(textObject);
            });
        },
        backgroundText(layerData, callback) {
            const rawTextValue = (layerData.value ? layerData.value : layerData.config.text.value).replaceAll('\n', ' ').replaceAll('\r', ' ');
            const textValue = rawTextValue.split(' ').filter(v => !!v).join(' ');
            const words = textValue.split(' ').filter(v => !!v);
            const fontConfig = layerData.config.text.font;
            const fontName = fontConfig.name;
            const fontUrl = `/design-upload/extract/${App.productName}/${fontConfig.path}`;
            App.helper.loadFont(fontName, fontUrl, () => {
                const textConfig = {
                    fontSize: fontConfig.sizes[0],
                    fill: App.helper.rgbToHex(...fontConfig.colors[0]),
                    fontFamily: fontName,
                    splitByGrapheme: true
                };
                const exec = () => {
                    const generateLine = (text, lineIdx, lineLength, options) => {
                        const words = text.split(' ');
                        const selectableWordsIdx = words.map((v, idx) => !!v ? idx : -1).filter(v => v != -1);
                        if(selectableWordsIdx.length == 0 || typeof lineLength != 'number') {
                            return '';
                        }
                        let line = '';
                        let selectedCounter = 0;
                        const startIdx = selectableWordsIdx[(lineIdx) % selectableWordsIdx.length] ?? 0;
                        for(let i = 0; i < 999; i++) {
                            const trueIdx = (i + startIdx) % words.length;
                            const word = words[trueIdx]
                            line += word;
                            if(!!word) {
                                selectedCounter += 1;
                                if(selectedCounter >= lineLength) {
                                    break;
                                }
                            }
                            line += ' '
                        }
                        return line;
                    };
                    const sum = (arr) => [0, ...arr].reduce((acc, v) => acc + v);
                    const generateDummyText = (value) => {
                        return new fabric.Text(value, textConfig);
                    };
                    const measure = lru_cache((text) => {
                        const words = text.split(' ');
                        const obj = generateDummyText(text);
                        const data = {
                            width: obj.width,
                            height: obj.height,
                            value: text
                        };
                        if(words.length > 1) {
                            data.words = words.map(w => measure(w));
                            data.nrSpaces = words.length - 1;
                        } else {
                            data.words = [data];
                        }
                        return data;
                    });
                    const nrLines = Math.floor(layerData.config.height / measure(textValue).height);
                    const heightOfLine = layerData.config.height / nrLines;
                    const nrWords = Math.max(1, Math.floor(layerData.config.width / measure(textValue).width * textValue.split(' ').length));
                    const calculateLine = lru_cache((text, width) => {
                        const textMeasure = measure(text);
                        const oneSpaceWidth = measure(' ').width;
                        let minSpaceWidth = oneSpaceWidth * 2.5;
                        let words = [...textMeasure.words];
                        const calcSpaceWidth = () => ((width - sum(words.map(w => w.width))) / (words.length - 1));
                        while(calcSpaceWidth() < minSpaceWidth && words.length > 1) {
                            words.pop();
                        }
                        const spaceWidth = calcSpaceWidth();
                        let accumulatedOffset = 0;
                        return words.map(w => {
                            const data = {
                                offset: accumulatedOffset,
                                width: w.width,
                                value: w.value,
                            };
                            accumulatedOffset += w.width + spaceWidth;
                            return data;
                        });
                    });

                    const objects = [];
                    for(let i = 0; i < nrLines; i++) {
                        const lineText = generateLine(textValue, i % 2, nrWords);
                        const lineStrat = calculateLine(lineText, layerData.config.width);
                        for(const [wordIdx, wordStrat] of lineStrat.entries()) {
                            const textObj = new fabric.Text(wordStrat.value, textConfig);
                            textObj.set({
                                left: layerData.config.left + wordStrat.offset,
                                top: i * heightOfLine,
                            });
                            App.helper.lockObject(textObj);
                            objects.push(textObj);
                        }
                    }

                    return objects;
                };

                callback(exec());
            });
        },
        mask(layerData, callback) {
            const svgLocation = `/design-upload/extract/${App.productName}/${layerData.config.image}`;
            App.helper.loadSVG(svgLocation, svg => {
                svg.set({
                    left: layerData.data.left,
                    top: layerData.data.top,
                    fill: '#333d',
                    width: layerData.config.width,
                    height: layerData.config.height,
                    perPixelTargetFind: true
                });
                App.helper.lockObject(svg);
                callback(svg);
            })
        },
        image(layerData, callback, options) {
            const horizontalAlign = options?.alignment[0] ?? 'left';
            const verticalAlign = options?.alignment[0] ?? 'top';
            fabric.Image.fromURL(layerData.value, img => {
                const mask = layerData.objects[1];
                mask.set('fill', '#e6a51201');
                const clipPath = new fabric.Path(mask.path, {
                    left: layerData.data.left,
                    top: layerData.data.top,
                    absolutePositioned: true,
                    fill: "transparent",
                    perPixelTargetFind: true
                });
                img.set({
                    dirty: true,
                    clipPath: clipPath,
                    left: layerData.data.left,
                    top: layerData.data.top,
                    evented: false
                });
                img.scaleToHeight(clipPath.height);
                const selectedFilter = App.editors.theme.selectedFilter;
                if(selectedFilter) {
                    img.filters[0] = new selectedFilter;
                }
                const filterMap = {
                    'BlackWhite'    : fabric.Image.filters.BlackWhite,
                    'Grayscale'     : fabric.Image.filters.Grayscale,
                    'Vintage'       : fabric.Image.filters.Vintage,
                    'Kodachrome'    : fabric.Image.filters.Kodachrome,
                    'Technicolor'   : fabric.Image.filters.Technicolor,
                    'Polaroid'      : fabric.Image.filters.Polaroid,
                    'Brownie'       : fabric.Image.filters.Brownie,
                };
                for(const filterName of layerData.config.filters ?? []) {
                    img.filters.push(filterMap[fontName]);
                }
                img.applyFilters();
                App.helper.lockObject(img);
                callback(img);
            }, { crossOrigin: 'anonymous' });
        },
        bgcolor(layerData, callback) {
            const customBackgrounds = layerData.typeMetadata.changecolor.split('/');
            const backgrounds = [];
            const objects = [];
            let resolved = 0;
            const addObj = (idx, obj) => {
                obj.set({evented: false});
                obj.scaleToWidth(layerData.config.width);
                obj.scaleToHeight(layerData.config.height);
                App.helper.lockObject(obj);
                objects[idx] = obj;
                resolved += 1;
                if(resolved == customBackgrounds.length) {
                    callback(backgrounds, objects);
                }
            };
            for(const customBg of customBackgrounds) {
                const idx = backgrounds.length;
                // color
                if(customBg[0] != '$') {
                    const obj = new fabric.Rect({
                        left: 0,
                        top: 0,
                        width: layerData.config.width,
                        height: layerData.config.height,
                        fill: '#' + customBg
                    });
                    backgrounds.push({
                        type: 'color',
                        value: '#' + customBg
                    });
                    addObj(idx, obj);
                }
                // image
                else {
                    const bgURL = `/design-upload/extract/${App.productName}/` + layerData.config.images[customBg.slice(1)].match(/images\/.*$/g)[0];
                    backgrounds.push({
                        type: 'image',
                        value: bgURL
                    });
                    fabric.Image.fromURL(bgURL, img => {
                        img.set({
                            left: 0,
                            top: 0
                        });
                        addObj(idx, img);
                    }, { crossOrigin: 'anonymous' });
                }
            }
        },
        song(components, lyrics, callback) {
            $.ajax({
                url: HEART_GENERATOR_URL,
                type: 'post',
                data: JSON.stringify({
                    contents: lyrics,
                    color: '000000'
                }),
                dataType: 'json',
                contentType: 'application/json'
            })
            .done(res => {
                const url = res.data;
                fabric.Image.fromURL(
                    url,
                    img => {
                        var maskConfig = components.mask.config;
                        img.set({
                            left: maskConfig.left,
                            top: maskConfig.top,
                        });
                        img.scaleToWidth(maskConfig.width);
                        img.scaleToHeight(maskConfig.height);
                        App.helper.lockObject(img);
                        callback(img);
                    },
                    { crossOrigin: "anonymous" }
                );
            })
            .fail(err => {
                console.log(err);
            });
        }
    },
    test() {
        const textobj = new fabric.Text('MyText', {
            width: 1000,
            top: 5,
            left: 5,
            fontSize: 450,
            textAlign: 'center',
        });
        for(let i = 1; i < 11; i++) {
            textobj.set({fontSize: i * 50});
            console.log(textobj.width / i);
        }
    },
    init({ productName, canvasWidth, canvasHeight, layers }) {
        const firstTimeLoad = !App.initialized;
        App.initialized = true;

        $('.group:not(.option-group)').toggleClass('hidden', true);
        // $('.tab-indicators span:not([data-target="select-style"])').toggleClass('hidden', true);
        // $('.tab-content:not([data-tab="select-style"])').toggleClass('hidden', true);
        $(`.group.text-group`).find('.group-body').html('');
        $(`.group.map-group`).find('.group-body').html('');
        $(`.group.background-group`).find('.group-body .splide__list').html('');
        // $('.tab-content[data-tab="texts"]').html('');
        // $('.tab-content[data-tab="maps"]').html('');
        // $('.tab-content[data-tab="backgrounds"] .splide__list').html('');

        if(App.canvas.instance) {
            App.canvas.instance.dispose();
        }

        const canvas = new fabric.Canvas('canvas', {
            subTargetCheck: true,
            originX: "left",
            originY: "top",
        });
        canvas.selection = false;
        canvas.on('mouse:up', (e) => {
            const target = e.target;
            for (const layer of layers) {
                for (const [objId, obj] of layer.objects.entries()) {
                    if (obj == target) {
                        console.log(layer, 'LayerID: ' + layer.id);
                        layer.onClick && layer.onClick();
                    }
                }
            }
        });
        canvas.setWidth(canvasWidth);
        canvas.setHeight(canvasHeight);

        const zoomRatio = Math.pow($('#canvas').height() / canvasHeight, 0.7);
        canvas.setDimensions({ width: canvasWidth * zoomRatio, height: canvasHeight * zoomRatio });
        canvas.setZoom(zoomRatio);
        App.canvas.zoomRatio = zoomRatio;

        const exportCanvas = new fabric.Canvas('exportCanvas', {
            originX: "left",
            originY: "top",
        });
        exportCanvas.setWidth(canvasWidth);
        exportCanvas.setHeight(canvasHeight);
        const dimensionLimit = 10240;
        const exportZoomRatio = canvasWidth >= canvasHeight ? Math.min(dimensionLimit, canvasWidth) / canvasWidth : Math.min(dimensionLimit, canvasHeight) / canvasHeight;
        exportCanvas.setDimensions({ width: canvasWidth * exportZoomRatio, height: canvasHeight * exportZoomRatio });
        exportCanvas.setZoom(exportZoomRatio);

        const autoCalib = () => {
            const originalRatio = canvasWidth / canvasHeight;
            const distortedRatio = $('#canvas').width() / $('#canvas').height();
            const windowWidth = $(window).width();
            const windowHeight = $(window).height();
            const baseDimension = originalRatio >= (windowWidth / windowHeight) ? 'width' : 'height';

            /*
            #canvas, .upper-canvas {
                max-height: 90vh;
                max-width: 80vw;
            }
            */

            const maxWidthRatio = App.isMobile ? 1 : 0.5;
            const maxHeightRatio = App.isMobile ? 999 : 0.8;

            if(baseDimension == 'width') {
                $('#canvas').width(windowWidth * maxWidthRatio); 
            }
            else {
                $('#canvas').height(windowHeight * maxHeightRatio); 
            }
    
            if (distortedRatio >= originalRatio) {
                $('#canvas').closest('.canvas-container').find('canvas').height($('#canvas').height());
                $('#canvas').closest('.canvas-container').find('canvas').width(canvasWidth * $('#canvas').height() / canvasHeight);
            }
            else {
                $('#canvas').closest('.canvas-container').find('canvas').width($('#canvas').width());
                $('#canvas').closest('.canvas-container').find('canvas').height(canvasHeight * $('#canvas').width() / canvasWidth);
            }
            $('#canvas').closest('.canvas-container').width($('#canvas').width());
            $('#canvas').closest('.canvas-container').height($('#canvas').height());
        };
        autoCalib();
        $(window).resize(autoCalib);
        const urlParam = new URLSearchParams(window.location.search);

        if(firstTimeLoad) {
            const lastWindowSize = {
                width: $(window).width(),
                height: $(window).height()
            };
            setInterval(() => {
                const changed = $(window).width() != lastWindowSize.width || $(window).height() != lastWindowSize.height;
                if(changed) {
                    lastWindowSize.width = $(window).width();
                    lastWindowSize.height = $(window).height();
                    $(window).trigger('resize');
                }
            }, 500);

            // on click outside of canvas
            $(document).on('click', 'body > .main-content', e => {
                if(!$('.canvas-container')[0].contains(e.target)) {
                    App.canvas.deselectAll();
                }
            });
        }

        this.canvas.instance = canvas;
        this.canvas.exportInstance = exportCanvas;
        this.productName = productName;
        this.debugMode = urlParam.get('debug') == 'true';
        this.layers = layers;
        if(firstTimeLoad) {
            this.dropzone.init();
        }
        this.editors.init();
        this.events.unbindAll();
    }
};

const ctrlMain = () => {
    /*
    Initialize:
        step 1: create, align & lock object
        step 2: create connections between data, object and editor
    On Finish:
        step 1: export to preview image and full size image
        step 2: send finished result to parent
    */
    let editorCount = {
        images: 0,
        texts: 0,
        maps: 0,
        backgrounds: 0,
    };
    let spotifyInfo = {};
    for (const layerData of App.layers) {
        const required = layerData.typeMetadata.required == 'true';
        const module = layerData.typeMetadata.module;
        const type = layerData.type;
        App.helper.setLayerProp(layerData, 'visible', true);
        App.helper.setLayerProp(layerData, 'left', layerData.config.left);
        App.helper.setLayerProp(layerData, 'top', layerData.config.top);
        App.helper.setLayerProp(layerData, 'width', layerData.config.width);
        App.helper.setLayerProp(layerData, 'height', layerData.config.height);
        App.helper.setLayerProp(layerData, 'opacity', 1);
        switch (layerData.type) {
            case 'fg': {
                App.layerProcessor.fg(layerData, images => {
                    layerData.objects = [images[0]];
                    layerData.allObjects = images;
                    App.canvas.refresh();
                });
            }
                break;
            case 'text': {
                const requiredStar = '<span style="color: #e6a512;"> *</span>';
                if(editorCount.texts == 0) {
                    if(layerData.typeMetadata.module != 'background') {
                        App.editors.texts.init();
                    }
                    else if(layerData.typeMetadata.module == 'background') {
                        App.editors.backgroundText.init();
                    }
                }
                if(layerData.typeMetadata.module != 'background') {
                    App.helper.setLayerProp(layerData, 'textValue', layerData.config.text.value);
                    const editor = App.editors.texts.newEditor();
                    editorCount.texts += 1;
                    editor.setTitle(layerData.typeMetadata.label+(required ? requiredStar : '') ?? `Customize text ${editorCount.texts}`)
                        .setText(layerData.data.textValue)
                        .setPlaceholder(layerData.data.textValue);
                    layerData.editors = [editor];
                    App.layerProcessor.text(layerData, text => {
                        layerData.objects = [text];
                        App.helper.setTextObjectValue(text, layerData.data.textValue, layerData);
                        App.canvas.refresh();
                        App.canvas.onClick(text, () => {
                            App.editors.texts.setCollapse(false);
                            editor.focus();
                        });
                    });
                    App.events.on('layerPropChange', ({layer, propName, propValue}) => {
                        if(propName == 'textValue' && layer == layerData) {
                            editor.setText(propValue);
                        }
                    });
                    editor.onTextChange((newText, eventType) => {
                        if (!newText.trim() && required && eventType == 'change') {
                            newText = layerData.config.text.value;
                            editor.setText(newText);
                        }
                        if(newText != layerData.data.textValue) {
                            App.helper.setTextObjectValue(layerData.objects[0], newText, layerData);
                            layerData.data.textValue = newText;
                            App.canvas.refresh();
                        }
                    });
                }
                else if(layerData.typeMetadata.module == 'background') {
                    const editor = App.editors.backgroundText;
                    editor.setTitle(layerData.typeMetadata.label+(required ? requiredStar : '') ?? `Background text`)
                        .setText(layerData.config.text.value)
                        .setPlaceholder(layerData.config.text.value);
                    layerData.editors = [editor];

                    const generateBgText = () => {
                        App.layerProcessor.backgroundText(layerData, objects => {
                            layerData.objects = objects;
                            App.canvas.refresh();
                            for(const obj of objects) {
                                App.canvas.onClick(obj, () => {
                                    editor.setCollapse(false);
                                    editor.focus();
                                });
                            }
                        });
                    };
                    generateBgText();
                    let timeoutID = null;
                    let lastApply = (new Date).getTime();
                    editor.onTextChange((newText, eventType) => {
                        clearTimeout(timeoutID);
                        timeoutID = setTimeout(() => {
                            if (!newText.trim() && eventType == 'change') {
                                newText = layerData.config.text.value;
                                editor.setText(newText);
                            }
                            layerData.value = newText;
                            generateBgText();
                            App.canvas.clear();
                            App.canvas.refresh();
                        }, 500);
                        lastApply = (new Date).getTime();
                    });
                }
            }
                break;
            case 'song-mask': {
                App.editors.song.init();
                components = (() => {
                    const components = {
                        mask: layerData
                    };
                    for(const layer of App.layers) {
                        if(layer.type == 'text' && layer.typeMetadata.module == 'song_lyrics') {
                            components.text = layer;
                        }
                    }
                    return components;
                })();
                const lyrics = components.text.config.text.value;
                App.editors.song.setLyrics(lyrics);
                const handleLyrics = img => {
                    layerData.objects = [img];
                    App.canvas.onClick(img, () => {
                        App.editors.song.setCollapse(false);
                        App.editors.song.focus();
                    });
                    App.canvas.refresh();
                };
                App.layerProcessor.song(components, lyrics, handleLyrics);
                App.editors.song.onApply(newLyrics => {
                    App.layerProcessor.song(components, newLyrics, handleLyrics);
                });
            }
                break;
            case 'mask': {
                if(editorCount.images == 0) {
                    App.editors.images.init();
                    App.editors.theme.init();
                }
                const editor = App.editors.images.newEditor();
                editorCount.images += 1;
                // debugger;
                editor.setLayer(layerData);
                layerData.editors = [editor];
                const handleSelectImage = () => {
                    const selected = !!layerData.objects[0];
                    if(!selected) {
                        App.editors.imageGallery.unbindAll();
                        App.editors.imageGallery.onImageSelect(file => {
                            editor.setImage(file.dataURL);
                            App.editors.imageGallery.close();
                            editor.launch();
                        });
                        App.editors.imageGallery.launch();
                    }
                    else {
                        editor.launch();
                    }
                };
                App.layerProcessor.mask(layerData, mask => {
                    layerData.objects[1] = mask;
                    App.canvas.refresh();
                    App.canvas.onClick(mask, handleSelectImage);
                });
                editor.onApply(() => {
                    layerData.value = editor.getCroppedDataURL() ?? layerData.value;
                    App.layerProcessor.image(layerData, img => {
                        layerData.objects[0] = img;
                        App.canvas.refresh();
                    });
                    editor.close();
                });
                editor.onChangeImage(() => {
                    editor.close();
                    App.editors.imageGallery.unbindAll();
                    App.editors.imageGallery.onImageSelect(file => {
                        editor.setImage(file.dataURL);
                        App.editors.imageGallery.close();
                        editor.launch();
                    });
                    App.editors.imageGallery.launch();
                });
            }
                break;
            case 'map-mask': {
                if(editorCount.maps == 0) {
                    App.editors.maps.init();
                }
                editorCount.maps += 1;
                const mapIdx = editorCount.maps - 1;
                const components = (() => {
                    const components = {};
                    components.mask = layerData;
                    for(const layer of App.layers) {
                        if(layer.type == 'marker' && layer.typeMetadata.id == layerData.typeMetadata.id) {
                            components.marker = layer;
                        }
                        if(layer.type == 'map') {
                            App.mapStyle = layer.typeMetadata.style_v2;
                        }
                    }
                    return components;
                })();
                const editor = App.editors.maps.newEditor(components);
                editor.setLayer(layerData);
                App.layerProcessor.mask(components.mask, mask => {
                    components.mask.objects[1] = mask;
                    App.canvas.refresh();
                    App.canvas.onClick(mask, () => {
                        App.editors.maps.setCollapse(false);
                        editor.modal.launch();
                    });
                    editor.modal.on('apply', () => {
                        App.editors.maps.__prevLocation = editor.map.getCenter();
                        editor.__lastLocation = editor.map.getCenter();
                        editor.export(exported => {
                            components.mask.value = exported.map;
                            App.layerProcessor.image(components.mask, img => {
                                components.mask.objects[0] = img;
                                App.canvas.refresh();
                                App.canvas.onClick(img, () => {
                                    App.editors.maps.setCollapse(false);
                                    editor.modal.launch();
                                });
                            });
                            editor.modal.close();
                        });
                    });
                });
            }
                break;
            case 'bgcolor': {
                App.layerProcessor.bgcolor(layerData, (backgrounds, objects) => {
                    if(objects.length > 1) {
                        App.editors.backgrounds.init();
                        editorCount.backgrounds = 1;
                    }
                    App.editors.backgrounds.fetch(backgrounds);
                    App.editors.backgrounds.onBackgroundSelect(idx => {
                        layerData.objects = [objects[idx]];
                        layerData.allObjects = objects;
                        App.canvas.refresh();
                    });
                    layerData.objects = [objects[0]];
                    layerData.allObjects = objects;
                    App.canvas.refresh();
                });
            }
                break;
        }
    }
    App.events.on('layerPropChange', ({layer, propName, propValue}) => {
        const mutations = {
            visible() {
                for(const object of layer.objects) {
                    object && object.set({
                        visible: propValue
                    });
                }
            },
            left() {
                for(const object of layer.objects) {
                    object && object.set({
                        left: propValue
                    });
                    if(layer.type == 'text') {
                        object && object.set({
                            left: layer.data.left + layer.config.width / 2
                        });
                    }
                }
            },
            top() {
                for(const object of layer.objects) {
                    object && object.set({
                        top: propValue
                    });
                    if(layer.type == 'text') {
                        object && object.set({
                            top: layer.data.top + (layer.config.height - object.height) / 2
                        });
                    }
                }
            },
            width() {
                for(const object of layer.objects) {
                    object && object.set({
                        width: propValue
                    });
                }
            },
            height() {
                for(const object of layer.objects) {
                    object && object.set({
                        height: propValue
                    });
                }
            },
            opacity() {
                for(const object of layer.objects) {
                    object && object.set({
                        opacity: propValue
                    });
                }
            },
            textValue() {
                for(const object of layer.objects) {
                    object && App.helper.setTextObjectValue(object, propValue, layer);
                }
            }
        };
        mutations[propName] && mutations[propName]();
    });
    App.events.on('spotifyInfoReady', ({title, artist, duration, duration_ms}) => {
        for(const layer of App.layers) {
            const type = layer.type;
            const module = layer.typeMetadata.module;
            const role = layer.typeMetadata.role;
            if(module == 'spotify' && role == 'title') {
                layer.data.textValue = title;
                App.canvas.refresh();
            }
            if(module == 'spotify' && role == 'artist') {
                layer.data.textValue = artist;
                App.canvas.refresh();
            }
            if(module == 'spotify' && role == 'duration') {
                layer.data.textValue = duration;
                App.canvas.refresh();
            }
            if(module == 'spotify' && role == 'passedTime') {
                const timePassed = duration_ms * (+layer.typeMetadata.percent) / 100;
                const formattedTime = App.helper.formatDuration(timePassed);
                layer.data.textValue = formattedTime;
                App.canvas.refresh();
            }
        }
    });
    App.editors.images.onOpenGallery(() => {
        App.editors.imageGallery.launch();
    });
    App.editors.imageGallery.onAutofill(() => {
        const allFiles = App.editors.imageGallery.getUploadedFiles();
        if(allFiles.length == 0) return;
        const chosenFiles = [];
        const isAllFilesChosen = () => chosenFiles.length == allFiles.length;
        const randomUnchosenFile = () => {
            const unchosenFiles = allFiles.filter(file => !chosenFiles.includes(file));
            const randomIdx = parseInt(Math.random() * unchosenFiles.length);
            chosenFiles.push(unchosenFiles[randomIdx]);
            return unchosenFiles[randomIdx];
        };
        const randomFile = () => {
            const randomIdx = parseInt(Math.random() * allFiles.length);
            return allFiles[randomIdx];
        };
        let cropped = 0;
        let needToCrop = 0;
        for(const layerData of App.layers) {
            if(layerData.type != 'mask') continue;
            needToCrop += 1;
            const ratio = layerData.data.width / layerData.data.height;
            const editor = layerData.editors[0];
            let file = isAllFilesChosen() ? randomFile() : randomUnchosenFile();
            editor.setImage(file.dataURL);
            App.helper.quickCrop(file.dataURL, ratio, croppedDataURL => {
                layerData.value = croppedDataURL;
                App.layerProcessor.image(layerData, img => {
                    layerData.objects[0] = img;
                    cropped += 1;
                    if(cropped == needToCrop) {
                        App.canvas.refresh();
                    }
                });
            });
        }
        App.editors.imageGallery.close();
    });
    App.canvas.refresh();

    for(const layer of App.layers) {
        const required = layer.typeMetadata.required == 'true';
        const module = layer.typeMetadata.module;
        const type = layer.type;
        const loadSpotifySong = (uri) => {
            const defaultSpotifyURI = uri ?? layer.config.data.defaultSpotifyURI;
            const svgBackgroundColor = layer.config.data.backgroundColor;
            const svgFillColor = layer.config.data.fillColor;
            const trackID = (defaultSpotifyURI.match(/(?<=track(s)?(\:|\/))[a-zA-Z0-9]+/g) ?? [''])[0];
            if(!trackID) return;
            const trackURI = `spotify:track:${trackID}`;
            $.ajax({
                url: `https://scannables.scdn.co/uri/plain/svg/000000/white/640/${trackURI}`,
                method: 'get',
            })
            .done(res => {
                res = new XMLSerializer().serializeToString(res);
                const svg = res.replaceAll('#000000', '#backgroundColor').replaceAll('#ffffff', '#fillColor')
                    .replaceAll('#backgroundColor', svgBackgroundColor)
                    .replaceAll('#fillColor', svgFillColor);
                App.helper.createFabricSVG(svg, object => {
                    object.scaleToHeight(layer.config.height);
                    const scaleFactor = layer.config.height / object.height
                    object.set({
                        left: layer.config.left + (layer.config.width - object.width * scaleFactor) / 2,
                        top: layer.config.top,
                    });
                    layer.objects = [object];
                });
            });
            $.ajax({
                url: `/design-upload/services/spotify-api/tracks/${trackID}`,
                method: 'get'
            })
            .done(res => {
                const durationMs = res.data.duration_ms;
                const duration = App.helper.formatDuration(durationMs);
                const title = res.data.name;
                const artist = res.data.artists[0].name;
                App.events.emit('spotifyInfoReady', {
                    title: title,
                    artist: artist,
                    duration: duration,
                    duration_ms: durationMs
                });
            })
            .fail(err => {
                Toastify({
                    text: `Failed to song! ID=${trackID}`,
                    duration: 3000,
                    close: true,
                    gravity: "top", // `top` or `bottom`
                    position: "left", // `left`, `center` or `right`
                    backgroundColor: "#bd2300",
                    stopOnFocus: true, // Prevents dismissing of toast on hover
                    onClick: function(){} // Callback after click
                }).showToast();
            });
        };
        if(type == "custom" && module == 'spotify') {
            App.editors.spotify.init();
            App.editors.spotify.child.setText(layer.config.data.defaultSpotifyURI);
            App.editors.spotify.child.onTextChange((newText, eventType) => {
                if(eventType != 'change') return;
                loadSpotifySong(newText);
            });
            loadSpotifySong();
            App.editors.spotify.child.onSearchInputChange(query => {
                $.ajax({
                    url: `/design-upload/services/spotify-api/search?q=${query}`,
                    method: 'get',
                })
                .done(res => {
                    if(!query) {
                        App.editors.spotify.child.setSearchResults({
                            tracks: {items: []}
                        });
                    }
                    App.editors.spotify.child.setSearchResults(res.data);
                });
            });
            App.editors.spotify.child.onSearchResultSelect(uri => {
                App.editors.spotify.child.setText(uri);
            });
        }
        if(type == "custom" && module == 'spiral') {
            App.editors.spiral.init();
            App.editors.spiral.child.setText(layer.config.spiral.text);
            App.editors.spiral.child.onTextChange((newText, eventType) => {
                if(eventType == 'change')
                    loadSpiral();
            });
            function loadSpiral() {
                const base64 = App.helper.getSpiralTextBase64({
                    width: layer.config.width,
                    height: layer.config.height,
                    text: App.editors.spiral.child.getValue(),
                    fontSize: layer.config.spiral.fontSize,
                    fontFamily: fontName,
                    letterSpacing: layer.config.spiral.letterSpacing
                });
                fabric.Image.fromURL(base64, img => {
                    img.set({
                        left: layer.data.left,
                        top: layer.data.top,
                        evented: false,
                    });
                    img.scaleToWidth(layer.config.width);
                    img.scaleToHeight(layer.config.height);
                    App.helper.lockObject(img);
                    layer.objects[0] = img;
                    App.canvas.refresh();
                }, { crossOrigin: 'anonymous' });
            }
            const fontUrl = `/design-upload/extract/${App.productName}/${layer.config.spiral.fontPath}`;
            const fontName = layer.config.spiral.fontFamily;
            App.helper.loadFont(fontName, fontUrl, () => {
                loadSpiral();
            });
        }
        if(type == "custom" && module == 'square') {
            App.editors.square.init();
            App.editors.square.child.setTitle(layer.typeMetadata.label ?? 'Text');
            App.editors.square.child.setText(layer.config.square.text);
            App.editors.square.child.onTextChange((newText, eventType) => {
                if(eventType == 'change')
                    loadSquare();
            });
            function loadSquare() {
                $.ajax({
                    url: SQUARE_GENERATOR_URL,
                    method: 'post',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        lyrics: App.editors.square.child.getValue().replaceAll(/[\[\]\(\)]/g, ''),
                        color: layer.config.square.color
                    }),
                    crossDomain:true,
                    crossOrigin:true
                })
                .done(res => {
                    const url = res.data;
                    fabric.Image.fromURL(url, img => {
                        img.set({
                            left: layer.data.left,
                            top: layer.data.top,
                            evented: false,
                        });
                        img.scaleToWidth(layer.config.width);
                        App.helper.lockObject(img);
                        layer.objects[0] = img;
                        App.canvas.refresh();
                    }, { crossOrigin: 'anonymous' });
                })
                .fail(err => {
                    Toastify({
                        text: `Failed to load image!`,
                        duration: 3000,
                        close: true,
                        gravity: "top", // `top` or `bottom`
                        position: "left", // `left`, `center` or `right`
                        backgroundColor: "#bd2300",
                        stopOnFocus: true, // Prevents dismissing of toast on hover
                        onClick: function(){} // Callback after click
                    }).showToast();
                });
            }
            loadSquare();
        }
    }

    if(App.editors.theme.initialized) {
        App.editors.theme.onChooseTheme(filter => {
            for(const layer of App.layers) {
                if(layer.type != 'mask') continue;
                const imgObj = layer.objects[0];
                if(!imgObj) continue;
                App.canvas.instance.remove(imgObj);
                if(!filter) {
                    imgObj.filters[0] = undefined;
                }
                else {
                    imgObj.filters[0] = new filter;
                }
                imgObj.applyFilters();
                App.canvas.refresh();
            }
        });
    }

    // UX
    // if(App.isMobile) {
    //     $('.tab-indicators span:not(.hidden):eq(0)').click();
    // }
    // else {
        $('.group:not(.hidden):eq(0):not(.active) .group-title').click();
    // }
};

const loadProduct = (pname) => {
    const urlParam = new URLSearchParams(window.location.search);
    const productName = pname ?? urlParam.get('product') ?? '';
    if(!productName) return;

    const preprocessLayers = (data) => {
        const parseLayerType = (layerType) => {
            const metaRaw = (layerType.match(/\[.*/g) ?? [''])[0];
            const meta = {};
            const result = {
                type: layerType.replace(metaRaw, ''),
                meta: meta
            };
            for (const fieldSet of [...metaRaw.matchAll(/[a-zA-Z0-9_-]+\:[\/$a-zA-Z0-9_-\s]+/g)]) {
                const [key, value] = fieldSet[0].split(':');
                meta[key] = value;
            }
            return result;
        };
        const layers = [];
        for (const [layerId, layer] of data.children.entries()) {
            const parsedLayerType = parseLayerType(layer.type);
            const layerData = {
                id: layerId,
                objects: [],
                editors: [],
                type: parsedLayerType.type,
                typeMetadata: parsedLayerType.meta,
                config: layer,
                value: null,
                data: {},
            };
            layers.push(layerData);
        }
        return layers;
    };
    $.ajax({
        url: `/design-upload/extract/${productName}/data.json`,
        type: 'get',
        dataType: 'json',
        cache: false
    })
    .done(res => {
        if(res.document) {
            App.document = res.document;
            App.init({ canvasWidth: res.document.width, canvasHeight: res.document.height, productName, layers: preprocessLayers(res) });
            ctrlMain();
        }
        else if(res.options) {
            let styleGroup = $(`.group.option-group`);
            // if(App.isMobile) {
            //     styleGroup = $('.tab-content[data-tab="select-style"]');
            //     $('.tab-indicators span[data-target="select-style"]').toggleClass('hidden', false);
            // }
            styleGroup.find('.select-group').html(res.options.map(option => `
                <div class="option">
                    <span data-slug="${option.slug}">${option.title}</span>
                </div>
            `).join(''));
            styleGroup.on('click', '.option', function() {
                loadProduct($(this).find('span').attr('data-slug'));
            });
            styleGroup.toggleClass('hidden', false);
            if(res.options[0]) styleGroup.find('.option:eq(0)').click();
        }
    })
    .fail(err => {
        console.log(err);
    });
};

// Entry point
(() => {
    const isMobile = $(window).width() <= 992;
    App.isMobile = isMobile;
    if(isMobile) {
        $('body').toggleClass('is-mobile', true);
    }
    else {
        $('body').toggleClass('is-desktop', true);
    }

    const eventEmitter = new EventEmitter;

    $(document).on('click', '.group-title', function () {
        const isActive = $(this).closest('.group').hasClass('active');
        const groupBody = $(this).closest('.group').find('.group-body');
        $('.group').toggleClass('active', false);
        $(this).closest('.group').toggleClass('active', !isActive);
        for (const el of $('.group .group-body.collapsible')) {
            setCollapse(el, true);
        }
        setCollapse(groupBody[0], isActive);
    });

    $(document).on('click', '.subgroup-title', function () {
        const isActive = $(this).closest('.subgroup').hasClass('active');
        const subgroupBody = $(this).closest('.subgroup').find('.subgroup-body');
        $('.subgroup').toggleClass('active', false);
        $(this).closest('.subgroup').toggleClass('active', !isActive);
        for (const el of $('.subgroup .subgroup-body.collapsible')) {
            setCollapse(el, true);
        }
        setCollapse(subgroupBody[0], isActive);
    });

    $(document).on('click', '.select-group .option', function () {
        const isActive = $(this).hasClass('selected');
        $('.select-group .option').toggleClass('selected', false);
        $(this).toggleClass('selected', true);
    });

    // if(App.isMobile) {
    //     $(document).on('click', '.tab-indicators span', function() {
    //         $('.tab-indicators span').toggleClass('active', false);
    //         $(this).toggleClass('active', true);
    //         const tabSlug = $(this).attr('data-target');
    //         eventEmitter.emit('tab_select', tabSlug);
    //     });

    //     eventEmitter.on('tab_select', tabSlug => {
    //         $('.tab-content').toggleClass('active', false);
    //         $(`.tab-content[data-tab=${JSON.stringify(tabSlug)}]`).toggleClass('active', true);
    //     });
    // }

    loadProduct();

    $('.finish-btn, .mobile-finish-btn').on('click', function() {
        $(this).toggleClass('loading', !$(this).hasClass('loading'));
        if($(this).hasClass('loading')) {
            const exportBase64 = App.canvas.exportBase64();
            if(App.debugMode) {
                return App.helper.downloadURI(exportBase64);
            }
            pipe(
                // create file
                () => new Promise(resolve => {
                    $.ajax({
                        url: '/design-upload/create-new-image',
                        method: 'post',
                        contentType: 'application/json',
                        data: JSON.stringify({
                            debug: App.debugMode
                        })
                    })
                    .then(res => {
                        resolve(res.filename);
                    })
                    .fail(console.log);
                }),
                promise => {
                    promise.then(filename => {
                        const chunkSize = 1048576; // 1MB
                        const nrChunks = Math.ceil(exportBase64.length / chunkSize);
                        const chunks = [];
                        for(let i = 0; i < nrChunks; ++i) {
                            chunks.push(exportBase64.slice(i * chunkSize, (i+1) * chunkSize));
                        }
                        const upload = (remainingChunks) => {
                            if(remainingChunks.length == 0) {
                                $.ajax({
                                    url: '/design-upload/build-image',
                                    method: 'post',
                                    contentType: 'application/json',
                                    data: JSON.stringify({
                                        filename: filename
                                    })
                                })
                                .then(res => {
                                    const url = 'https://customize.navitee.com/design-upload/base64/'+filename+'.png';
                                    if(App.debugMode) {
                                        window.open(url, '_blank');
                                    }
                                    parent.postMessage({
                                        name: "previewImg",
                                        data: {
                                            file: url,
                                        }
                                    }, "*");
                                })
                                .fail(console.error);
                                return;
                            };
                            $.ajax({
                                url: '/design-upload/append-image-data',
                                method: 'post',
                                contentType: 'application/json',
                                data: JSON.stringify({
                                    filename: filename,
                                    data: remainingChunks[0]
                                })
                            })
                            .then(res => upload(remainingChunks.slice(1)))
                            .fail(console.error);
                        };
                        upload(chunks);
                    });
                }
            )();
        }
    });
})();

// DEV ZONE
(() => {
})();
