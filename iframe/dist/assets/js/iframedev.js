
const App = {
    data: {},
    pipelines: {},
    events: new EventEmitter,
    canvas: null,
    helper: {
        getUrlParams() {
            let search = location.search.slice(1);
            return JSON.parse(
                '{"' + search.replace(/&/g, '","').replace(/=/g,'":"') + '"}', 
                (key, value) => (key==="" ? value : decodeURIComponent(value)) );
        },
        rgb2hex(r, g, b, a) {
            const red = ('0' + (+(r ?? 0)).toString(16)).slice(-2);
            const green = ('0' + (+(g ?? 0)).toString(16)).slice(-2);
            const blue = ('0' + (+(b ?? 0)).toString(16)).slice(-2);
            const alpha = ('0' + (+(a ?? 0)).toString(16)).slice(-2);
            if (a == undefined) {
                return '#' + red + green + blue;
            }
            return '#' + red + green + blue + alpha;
        },
        parseLayerType(layerType) {
            const metaRaw = (layerType.match(/\[.*/g) ?? [''])[0];
            const meta = {};
            const result = {
                type: layerType.replace(metaRaw, ''),
                meta: meta
            };
            for (const fieldSet of [...metaRaw.matchAll(/[a-zA-Z0-9_-]+\:[\/$a-zA-Z0-9_-\s]+/g)]) {
                const [key, value] = fieldSet[0].split(':');
                meta[key] = value;
            }
            return result;
        },
        loadFont(name, url, callback, errCallback) {
            const hash = (str) => {
                let result = '';
                for (const [cid, c] of [...str].entries()) {
                    result += c.charCodeAt(0) * (cid + 1);
                }
                return (+result) % 123456789101112;
            };
            const fontLoaderClass = `font-loader-${hash(name)}`;
            if ($(`.${fontLoaderClass}`).length == 0) {
                $('body').append(`
                <defs class="${fontLoaderClass}">
                    <style>
                        @font-face {
                            font-family: ${JSON.stringify(name)};
                            src: url(${JSON.stringify(url)});
                        }
                    </style>
                </defs>
                `);
            }
            const font = new FontFaceObserver(name);
            const promise = font.load(null, 10000);
            promise.catch(e => {
                Toastify({
                    text: `Failed to load font '${name}': ${e}`,
                    duration: 3000,
                    close: true,
                    gravity: "top", // `top` or `bottom`
                    position: "left", // `left`, `center` or `right`
                    backgroundColor: "#b1601b",
                    stopOnFocus: true, // Prevents dismissing of toast on hover
                    onClick: function(){} // Callback after click
                }).showToast();
                console.error(`Failed to load font '${name}': ${e}`);
            });
            callback && promise.then(callback);
            errCallback && promise.catch(errCallback);
        },
        lockObject(obj) {
            obj.set({
                lockMovementX: !0,
                lockMovementY: !0,
                hasControls: !1,
                hoverCursor: "pointer",
            });
        },
        loadSVG(path, callback) {
            $.ajax({
                url: path,
                type: 'get',
                dataType: 'html',
                success(res) {
                    fabric.loadSVGFromString(res, (objects, options) => {
                        const svg = fabric.util.groupSVGElements(objects, options);
                        callback(svg);
                    });
                },
                error(err) {
                    console.log('Failed to load SVG', err);
                }
            });
        }
    },
};

App.canvas = {
    instances: null,
    destroy() {
        if(this.instances != null) {
            this.instances.displayCanvas.dispose();
            this.instances.exportCanvas.dispose();
        }
    },
    deselectAll() {
        this.instances.displayCanvas.discardActiveObject().renderAll();
    },
    sort() {
        this.clear();
        for (const layer of App.data.layers) {
            layer.data.visible && layer.objects && layer.objects.forEach(obj => {
                if (obj ?? false) {
                    obj.set({
                        dirty: true
                    });
                    this.instances.displayCanvas.add(obj);
                }
            });
        }
    },
    refresh() {
        this.instances.displayCanvas.renderAll();
    },
    clear() {
        const canvas = this.instances.displayCanvas;
        canvas.remove(...canvas.getObjects());
    },
    onClick(obj, callback) {
        this.instances.displayCanvas.on('mouse:up', (e) => {
            const target = e.target;
            if (target == obj) {
                callback();
            }
        });
    },
    config: {width: 0, height: 0},
    init({width, height}) {
        this.destroy();
        this.config = { width, height };
        const displayCanvas = new fabric.Canvas('displayCanvas', {
            width, height,
            subTargetCheck: true,
            originX: "left",
            originY: "top",
            selection: false
        });
        const exportCanvas = new fabric.Canvas('exportCanvas', {
            width, height,
            originX: "left",
            originY: "top"
        });

        const autoCalib = () => {
            const width = App.canvas.config.width,
                height = App.canvas.config.height;
            const originalRatio = width / height;
            const containerWidth = window.innerWidth - $('.editor-wrapper').width();
            const containerHeight = window.innerHeight;
            const baseDimension = originalRatio >= (containerWidth / containerHeight) ? 'width' : 'height';

            const maxWidthRatio = 0.8; // css rule: max-width: 80%(desktop), 80%(mobile)
            const maxHeightRatio = 0.8; // css rule: max-height: 80vh(desktop), 80vh(mobile)
            if(baseDimension == 'width') {
                $('#displayCanvas').width(containerWidth * maxWidthRatio);
                $('#displayCanvas').height($('#displayCanvas').width() / width * height);
            }
            else {
                $('#displayCanvas').height(containerHeight * maxHeightRatio); 
                $('#displayCanvas').width($('#displayCanvas').height() / height * width);
            }
            $('#displayCanvas').closest('.canvas-container').find('canvas:not(#displayCanvas)').width($('#displayCanvas').width());
            $('#displayCanvas').closest('.canvas-container').find('canvas:not(#displayCanvas)').height($('#displayCanvas').height());

            $('#displayCanvas').closest('.canvas-container').width($('#displayCanvas').width());
            $('#displayCanvas').closest('.canvas-container').height($('#displayCanvas').height());
        };
        autoCalib();
        if(this.instances == null) {
            $(window).resize(autoCalib);
        }
        this.instances = {
            displayCanvas: displayCanvas,
            exportCanvas: exportCanvas
        };
        console.log(this.instances)
    }
};


// Entry point
(function setupDataRenderingPipelines() {
    /*
    Event naming rule:
        <pipelinename>_<eventname>
    */

    // local event emitter
    const eventEmitter = new EventEmitter;
    function loadProduct(sku) {
        $.ajax({
            url: `/design-upload/extract/${sku}/data.json`,
            type: 'get',
            dataType: 'json',
            cache: false
        })
        .done(res => {
            if(res.options) {
                App.data.parentJSON = res;
                eventEmitter.emit('_parentproductLoaded', res);
                return;
            }
            App.data.dataJSON = res;
            App.data.currentSKU = sku;
            App.events.emit('init_dataJSONReady', res);
        });
    }
    eventEmitter.on('_parentproductLoaded', function(parent) {
        loadProduct(parent.options[0].slug);
    });

    App.events.on('init_dataJSONReady', function processRawLayers() {
        App.data.layers = [];
        let layerIdx = 0;
        for(const layer of App.data.dataJSON.children) {
            const processedLayer = {
                index: layerIdx,
                type: App.helper.parseLayerType(layer.type).type,
                meta: App.helper.parseLayerType(layer.type).meta,
                objects: [],
                components: {},
                editors: {},
                data: {},
                status: {},
                raw: layer
            };
            layerIdx++;

            (function setupDefaultData() {
                let layerVisible = true;
                Object.defineProperty(processedLayer.data, 'visible', {
                    get() {
                        return layerVisible;
                    },
                    set(v) {
                        layerVisible = v;
                        App.events.emit('fabric_layerVisibleChange', processedLayer);
                    }
                });
            })();

            const layerPreprocessors = {
                fg() {
                    processedLayer.status.previewLoaded = false;
                    processedLayer.status.fullsizeLoaded = false;
                },
                text() {
                    processedLayer.status.fontLoaded = false;
                    let textColor = App.helper.rgb2hex(...layer.text.font.colors[0]);
                    let textValue = layer.text.value;
                    let textFontFamily = layer.text.font.name;
                    let textFontSize = layer.text.font.sizes[0];
                    let textHAlign = layer.text.font.alignment[0];
                    let textVAlign = processedLayer.meta.valign;
                    Object.defineProperty(processedLayer.data, 'color', {
                        get() {
                            return textColor;
                        },
                        set(v) {
                            textColor = v;
                            App.events.emit('fabric_textColorChange', processedLayer);
                        }
                    });
                    Object.defineProperty(processedLayer.data, 'value', {
                        get() {
                            return textValue;
                        },
                        set(v) {
                            textValue = v;
                            App.events.emit('fabric_textValueChange', processedLayer);
                        }
                    });
                    Object.defineProperty(processedLayer.data, 'fontFamily', {
                        get() {
                            return textFontFamily;
                        },
                        set(v) {
                            textFontFamily = v;
                            App.events.emit('fabric_textFontFamilyChange', processedLayer);
                        }
                    });
                    Object.defineProperty(processedLayer.data, 'fontSize', {
                        get() {
                            return textFontSize;
                        },
                        set(v) {
                            textFontSize = v;
                            App.events.emit('fabric_textFontSizeChange', processedLayer);
                        }
                    });
                    Object.defineProperty(processedLayer.data, 'hAlign', {
                        get() {
                            return textHAlign;
                        },
                        set(v) {
                            textHAlign = v;
                            App.events.emit('fabric_textHAlignChange', processedLayer);
                        }
                    });
                    Object.defineProperty(processedLayer.data, 'vAlign', {
                        get() {
                            return textVAlign;
                        },
                        set(v) {
                            textValign = v;
                            App.events.emit('fabric_textVAlignChange', processedLayer);
                        }
                    });
                },
                mapMask() {
                    processedLayer.components.mask = layer;
                    for(const layer of App.data.dataJSON.children) {
                        const parsedType = App.helper.parseLayerType(layer.type);
                        if(parsedType.type == 'marker' && parsedType.meta.id == processedLayer.meta.id) {
                            processedLayer.components.marker = layer;
                            break;
                        }
                    }
                },
                mask() {
                },
                bgcolor() {
                }
            };
            const toCamelCase = slug => {
                return slug.replaceAll(' ', '-').split('-').map((word, idx) => {
                    if(idx == 0) return word;
                    return word[0].toUpperCase() + word.slice(1);
                }).join('');
            };
            const layerPreprocessor = layerPreprocessors[toCamelCase(processedLayer.type)];
            layerPreprocessor && layerPreprocessor();
            layerPreprocessor && App.data.layers.push(processedLayer);
            App.events.emit('init_layerDataReady', processedLayer);
        }

        App.canvas.init({
            width: App.data.dataJSON.document.width,
            height: App.data.dataJSON.document.height
        });
        App.events.emit('init_allLayersReady');
    });

    App.events.on('init_layerDataReady', function(layer) {
        App.data.loadedFonts = {};

        const layerLoaders = {
            'text': function() {
                const fontURL = `/design-upload/extract/${App.data.currentSKU}/${layer.raw.text.font.path}`;
                const fontName = layer.data.fontFamily;
                App.helper.loadFont(fontName, fontURL, function() {
                    App.data.loadedFonts[fontName] = {
                        name: fontName,
                        path: fontURL
                    };
                    layer.status.fontLoaded = true;

                    const textObject = new fabric.Text(layer.data.value, {
                        left: layer.raw.left + layer.raw.width / 2,
                        top: layer.raw.top,
                        width: layer.raw.width,
                        height: layer.raw.height,
                        visible: layer.data.visible,
                        fill: layer.data.color,
                        // fill: '#000000',
                        fontFamily: layer.data.fontFamily,
                        fontSize: layer.data.fontSize,
                        splitByGrapheme: true,
                        textAlign: "center",
                    });
                    layer.objects = [textObject];
                    textObject.set({
                        originX: 'center',
                        originX: 'center',
                    });
                    App.helper.lockObject(textObject);
                    App.canvas.sort();
                    App.canvas.refresh();

                    App.events.emit('fabric_textFontReady', layer);
                });
            },
            'fg': function() {
                const imgUrl = `/design-upload/extract/${App.data.currentSKU}/${layer.raw.preview ?? layer.raw.image}`;
                const fullsizeImgUrl = `/design-upload/extract/${App.data.currentSKU}/${layer.raw.image}`;

                fabric.Image.fromURL(imgUrl, img => {
                    img.set({
                        left: layer.raw.left,
                        top: layer.raw.top,
                        evented: false,
                    });
                    img.scaleToWidth(App.data.dataJSON.document.width);
                    img.scaleToHeight(App.data.dataJSON.document.height);
                    layer.status.previewLoaded = true;

                    layer.objects[0] = img;
                    App.helper.lockObject(img);
                    App.canvas.sort();
                    App.canvas.refresh();

                    App.events.emit('fabric_fgPreviewReady', layer);
                }, { crossOrigin: 'anonymous' });
                fabric.Image.fromURL(fullsizeImgUrl, img => {
                    img.set({
                        left: layer.raw.left,
                        top: layer.raw.top,
                        evented: false,
                        visible: false,
                    });
                    img.scaleToWidth(layer.raw.width);
                    img.scaleToHeight(layer.raw.width);
                    layer.status.fullsizeLoaded = true;

                    layer.objects[1] = img;
                    App.helper.lockObject(img);
                    App.canvas.sort();
                    App.canvas.refresh();

                    App.events.emit('fabric_fgFullsizeReady', layer);
                }, { crossOrigin: 'anonymous' });
            },
            'mask': function() {
                const svgLocation = `/design-upload/extract/${App.data.currentSKU}/${layer.raw.image}`;
                App.helper.loadSVG(svgLocation, svg => {
                    svg.set({
                        left: layer.raw.left,
                        top: layer.raw.top,
                        fill: '#333d',
                        width: layer.raw.width,
                        height: layer.raw.height,
                        perPixelTargetFind: true
                    });

                    layer.objects[0] = svg;
                    App.helper.lockObject(svg);
                    App.canvas.sort();
                    App.canvas.refresh();
                    
                    App.events.emit('fabric_maskSVGReady', layer);
                });
            }
        };
        const layerLoader = layerLoaders[layer.type];
        layerLoader && layerLoader();
    });

    App.events.on('fabric_textFontReady', function(layer) {
    });

    const sku = App.helper.getUrlParams().product;
    App.data.sku = sku;
    loadProduct(sku);
})();

(function devZoneSetup() {

})();

