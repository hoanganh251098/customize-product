// define App;
const App = {
    // define App.datajson;
    datajson: {},
    // define App.activity;
    activity: {
        __logs: [],
        log(message, category='default') {
            this.__logs.push({
                message,
                category
            });
        },
        filter(categoryPattern) {
            return this.__logs.filter(log => log.category.match(categoryPattern ?? ''));
        },
        createReporter(category, postProcess) {
            const reporter = (message, ...extras) => {
                this.__logs.push({
                    message, category
                });
                postProcess instanceof Function && postProcess(message, ...extras);
            };
            return reporter;
        }
    },
    // define App.urlParams;
    get urlParams() {
        let search = location.search.slice(1);
        return JSON.parse(
            '{"' + search.replace(/&/g, '","').replace(/=/g,'":"') + '"}', 
            (key, value) => (key==="" ? value : decodeURIComponent(value)) );
    },
    // define App.layers;
    layers: [],
    // define App.canvas;
    canvas: {
        config: {width: 0, height: 0},
        instance: null,
        init({width, height}) {
            if(this.instance) {
                this.instance.displayCanvas.dispose();
                this.instance.exportCanvas.dispose();
            }
            this.config = { width, height };
            const component = new Shitonen({
                element: $('.main-content .canvas-wrapper')[0],
                shit: {},
                setup() {},
                init() {
                    const displayCanvas = new fabric.Canvas('displayCanvas', {
                        width, height,
                        subTargetCheck: true,
                        originX: "left",
                        originY: "top",
                        selection: false
                    });
                    const exportCanvas = new fabric.Canvas('exportCanvas', {
                        width, height,
                        originX: "left",
                        originY: "top"
                    });
                    Object.defineProperty(this, 'displayCanvas', {
                        get() { return displayCanvas; }
                    });
                    Object.defineProperty(this, 'exportCanvas', {
                        get() { return exportCanvas; }
                    });

                    const autoCalib = () => {
                        App.activity.log('canvas autoCalib');
                        const width = App.canvas.config.width,
                            height = App.canvas.config.height;
                        const originalRatio = width / height;
                        const distortedRatio = $('#displayCanvas').width() / $('#displayCanvas').height();
                        const windowWidth = window.innerWidth;
                        const windowHeight = window.innerHeight;
                        const baseDimension = originalRatio >= (windowWidth / windowHeight) ? 'width' : 'height';

                        const maxWidthRatio = App.isMobile ? 0.8 : 0.5; // css rule: max-width: 50vw(desktop), 80vw(mobile)
                        const maxHeightRatio = App.isMobile ? 0.9 : 0.8; // css rule: max-height: 80vh(desktop), 90vh(mobile)
                        if(baseDimension == 'width') {
                            $('#displayCanvas').width(windowWidth * maxWidthRatio);
                            $('#displayCanvas').height($('#displayCanvas').width() / width * height);
                        }
                        else {
                            console.log(windowHeight * maxHeightRatio);
                            $('#displayCanvas').height(windowHeight * maxHeightRatio); 
                            $('#displayCanvas').width($('#displayCanvas').height() / height * width);
                        }
                        $('#displayCanvas').closest('.canvas-container').find('canvas:not(#displayCanvas)').width($('#displayCanvas').width());
                        $('#displayCanvas').closest('.canvas-container').find('canvas:not(#displayCanvas)').height($('#displayCanvas').height());

                        $('#displayCanvas').closest('.canvas-container').width($('#displayCanvas').width());
                        $('#displayCanvas').closest('.canvas-container').height($('#displayCanvas').height());
                    };
                    autoCalib();
                    if(App.canvas.instance == null) {
                        App.activity.log('register canvas autoCalib on window resize');
                        $(window).resize(autoCalib);
                    }
                }
            });
            this.instance = component;
        }
    },
    // define App.editorGroups;
    editorGroups: {
        __groups: {},
        getGroup(name) {
            return this.__groups[name];
        },
        newGroup(name='anonymous', config={}) {
            const component = new Shitonen({
                html: `
                    <div class="group">
                        <div class="group-title">
                            <i class="fa fa-chevron-right"></i>
                            <span>{{title}}</span>
                        </div>
                        <div class="group-body collapsible" data-collapsed="true" style="height: 0px;">
                            {{body}}
                        </div>
                    </div>
                `,
                shit: {
                    title: config.title ?? 'Default group title',
                    body: config.body ?? '<p>Default group content</p>'
                },
                shitConfig: {
                    body: { escape: false }
                },
                init() {
                    const group = $(this.element);
                    this.query('.group-title').onclick = () => {
                        const isActive = group.hasClass('active');
                        const groupBody = group.find('.group-body');
                        $('.group').toggleClass('active', false);
                        group.toggleClass('active', !isActive);
                        for (const el of $('.group .group-body.collapsible')) {
                            setCollapse(el, true);
                        }
                        setCollapse(groupBody[0], isActive);
                    };
                }
            });
            this.__groups[name] = component;
            $('.editor-wrapper .editor').append(component.element);
            return component;
        },
        init() {
            $('.editor-wrapper .editor').html('');
            this.__groups = {};
        }
    }
};

// define Helper;
const Helper = {
    // define Helper.rgb2hex;
    rgb2hex(r, g, b, a) {
        const red = ('0' + (+(r ?? 0)).toString(16)).slice(-2);
        const green = ('0' + (+(g ?? 0)).toString(16)).slice(-2);
        const blue = ('0' + (+(b ?? 0)).toString(16)).slice(-2);
        const alpha = ('0' + (+(a ?? 0)).toString(16)).slice(-2);
        if (a == undefined) {
            return '#' + red + green + blue;
        }
        return '#' + red + green + blue + alpha;
    },
    // define Helper.setTextObjectValue;
    setTextObjectValue(textObject, newText, config) {
        const widthLimit = config.width;
        const heightLimit = config.height;
        const originalFontSize = config.text.font.sizes[0];
        textObject.set({fontSize: originalFontSize});
        newText = newText.replaceAll('\r', '\n');
        textObject.set('text', newText);
        const calculatedFontSize = {
            width: originalFontSize * widthLimit / textObject.width,
            height: originalFontSize * heightLimit / textObject.height
        };
        textObject.set('fontSize', Math.min(originalFontSize, calculatedFontSize.width, calculatedFontSize.height));
        textObject.set({
            top: config.top + (config.height - textObject.height) / 2
        });
    },
    // define Helper.createAutoResizeTextarea;
    createAutoResizeTextarea() {
        const scrollHiddenCSS = `
            overflow: hidden !important;
            -ms-overflow-style: none !important;
            scrollbar-width: none !important;
        `;
        if($('body > .autoresize-textarea-zone').length == 0) {
            $('body').append('<div class="autoresize-textarea-zone"></div>');
            $('body > .autoresize-textarea-zone').attr('style', `visibility:hidden;position:fixed;z-index:-9999999;`);
        }
        const hiddenTextarea = new Shitonen({
            html: `
                <textarea style="{{style}}" rows="1"></textarea>
            `,
            shit: {
                style: scrollHiddenCSS,
                value: ''
            },
            init() {
                this.shit.on('init change', 'value', _ => {
                    this.element.value = this.shit.value;
                });
                $('body > .autoresize-textarea-zone').append(this.element);
            }
        });
        const displayTextarea = new Shitonen({
            html: `<textarea></textarea>`,
            shit: {
                value: ''
            },
            init() {
                $(this.element).toggleClass('autoresize-textarea', true);
                CustomCSS.add('.autoresize-textarea', `
                    resize: none !important;
                    ${scrollHiddenCSS}
                `);
                const update = _ => {
                    this.shit.value = this.element.value;
                };
                this.element.oninput = update;
                this.element.onchange = update;
                const copyStyle = (name) => {
                    const css = $(this.element).css(name);
                    $(hiddenTextarea.element).css(name, css);
                };
                this.shit.on('init change', 'value', _ => {
                    hiddenTextarea.shit.value = this.shit.value;
                    $(this.element).val(this.shit.value);
                    copyStyle('width');
                    copyStyle('font-family');
                    copyStyle('font-size');
                    copyStyle('font-weight');
                    copyStyle('line-height');
                    const height = hiddenTextarea.element.scrollHeight;
                    $(this.element).css('height', height+'px');
                });
            }
        });
        return displayTextarea;
    }
};

function loadProduct(sku, isSubproduct=false) {
    App.activity.log(`Load ${isSubproduct?'sub':''}product "${sku}"`);
    $.ajax({
        url: `/design-upload/extract/${sku}/data.json`,
        type: 'get',
        dataType: 'json',
        cache: false
    })
    .done(res => {
        if(!isSubproduct)
            App.datajson = res;
        else {
            App.datajson.product = {
                sku,
                data: res
            };
        }
        const parseLayerType = (layerType) => {
            const metaRaw = (layerType.match(/\[.*/g) ?? [''])[0];
            const meta = {};
            const result = {
                type: layerType.replace(metaRaw, ''),
                meta: meta
            };
            for (const fieldSet of [...metaRaw.matchAll(/[a-zA-Z0-9_-]+\:[\/$a-zA-Z0-9_-\s]+/g)]) {
                const [key, value] = fieldSet[0].split(':');
                meta[key] = value;
            }
            return result;
        };
        if(res.document) {
            const report = App.activity.createReporter('LayerObjectManagerError', message => {
                console.error('LayerObjectManagerError: '+message);
            });
            App.canvas.init({
                width:  res.document.width,
                height: res.document.height
            });
            App.layers = [];
            for(const layer of res.children) {
                const processedLayer = {
                    id: uuidv4(),
                    get index() { return App.layers.indexOf(this); },
                    type: parseLayerType(layer.type).type,
                    meta: parseLayerType(layer.type).meta,
                    objects: {
                        __activeObjects: [],
                        __allObjects: {},
                        __config: {},
                        config(cfg) {
                            cfg ?? report('Empty config.');
                            cfg && Object.assign(this.__config, cfg);
                        },
                        enable(name) {
                            const object = this.__allObjects[name];
                            const objconfig = this.__config[name] ?? {};
                            const idx = objconfig.index ?? -1;
                            idx == -1 && this.__activeObjects.push(object);
                            idx != -1 && (this.__activeObjects[idx] = object);
                        },
                        disable(name) {
                            const object = this.__allObjects[name];
                            const idx = this.__activeObjects.indexOf(object);
                            idx != -1 && this.__activeObjects.splice(idx, 1);
                        },
                        setObject(name, object) {
                            name ?? report('Object name required.');
                            object ?? report('Object required.');
                            this.__allObjects[name] = object;
                        },
                        getObject(name) {
                            return this.__allObjects[name];
                        },
                        pushObject(object) {
                            const count = this.count;
                            this.setObject(count, object);
                            return count;
                        },
                        clear() {
                            this.__activeObjects = [];
                            this.__allObjects = {};
                            this.__config = {};
                        },
                        get count() {
                            return Object.keys(this.__allObjects).length;
                        }
                    },
                    components: {},
                    properties: {
                        __eventEmitter: new EventEmitter,
                        __keys: [],
                        define(name, config) {
                            Object.defineProperty(this, name, config);
                            if(!this.__keys.includes(name)) {
                                this.__keys.push(name);
                            }
                        },
                        on(eventTypes, propNames, callback) {
                            eventTypes = eventTypes.split(' ').filter(v => !!v);
                            propNames = propNames.split(' ').filter(v => !!v);
                            for(const eventType of eventTypes) {
                                for(const propName of propNames) {
                                    this.__eventEmitter.on(JSON.stringify([eventType, propName]), callback);
                                }
                            }
                        }
                    }
                };

                function defineProps(config) {
                    for(const [k, v] of Object.entries(config)) {
                        processedLayer.properties.define(k, {
                            get() {
                                return config[k];
                            },
                            set(v) {
                                config[k] = v;
                                processedLayer.properties.__eventEmitter.emit(JSON.stringify(['change', k]), v);
                            }
                        });
                    }
                }

                const layerPreprocessors = {
                    fg() {
                        // do nothing
                    },
                    text() {
                        defineProps({
                            required: processedLayer.meta.required == 'true',
                            value: '',
                            fontFamily: layer.text.font.name,
                            fontSize: layer.text.font.sizes[0],
                            color: Helper.rgb2hex(...layer.text.font.colors[0]),
                            halign: layer.text.font.alignment[0],
                            valign: processedLayer.meta.valign
                        });
                        processedLayer.properties.on('init change', 'value', function() {
                            const object = processedLayer.objects.getObject('text');
                            if(object == undefined) return;
                            // not implemented
                        });
                    },
                    songMask() {
                    },
                    mapMask() {
                        processedLayer.components.mask = layer;
                        for(const layer of res.children) {
                            const parsedType = parseLayerType(layer.type);
                            if(parsedType.type == 'marker' && parsedType.meta.id == processedLayer.meta.id) {
                                processedLayer.components.marker = layer;
                                break;
                            }
                        }
                    },
                    bgcolor() {
                    }
                };
                const layerTypeCamel = toCamelCase(processedLayer.type);
                if(layerPreprocessors[layerTypeCamel]) {
                    defineProps({
                        left:       layer.left,
                        top:        layer.top,
                        width:      layer.width,
                        height:     layer.height,
                        visible:    true,
                        opacity:    layer.opacity,
                        rotate:     layer.rotate ?? 0
                    });
                    layerPreprocessors[layerTypeCamel]();
                    App.layers.push(processedLayer);
                }
            }
        }
        else {
            loadProduct(res.options[0].slug, true);
        }
    });
}

// Entry point
(function() {
    const sku = App.urlParams.product;
    App.reportBug = App.activity.createReporter('Bug', (message, data) => {
        const payload = {
            message,
            data,
            created_at: ''+(new Date)
        };
        console.error(`Fake bug report payload: `, payload);
    });
    try {
        loadProduct(sku);

        /* DEV ZONE */

        // const x = Helper.createAutoResizeTextarea();
        // $('.main-content').html('');
        // $('body').append(x.element);
        // x.shit.value = `asdfasdfasdfasdfasdfsdfa`;
        // defineGlobal('x', x);

        /*
            Lifecycle:
                loading
                ready
        */
        const CanvasResource = classFactory({});

        const FabricCanvasManager = classFactory({
            init(canvas) {
                this.canvas = canvas;
            },
            createText() {},
            createImage(url) {},
            createMask() {},
            addToCanvas(object) {}
        });
    }
    catch(e) {
        App.reportBug(`Failed to load product "${sku}"`, e);
    }
})();

