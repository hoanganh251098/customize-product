(function() {
    const defineGlobal = (name, def) => {
        const globalScope = typeof window != 'undefined'
            ? window
            : global;
        if(globalScope[name] != undefined) return console.error(`Global name "${name}" conflict!`);
        globalScope[name] = def;
    };
    defineGlobal('defineGlobal', defineGlobal);
})();

(function core() {
    const lru_cache = (f) => {
        const cache = {};
        const wrapper = (...args) => {
            const cacheKey = JSON.stringify(args);
            if(cache[cacheKey] == undefined) {
                cache[cacheKey] = f(...args);
            }
            return cache[cacheKey];
        };
        return wrapper;
    };
    function uuidv4() {
        return (() => (typeof crypto == 'undefined' || crypto.getRandomValues == undefined)
            ? 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
                let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return r.toString(16); })
            : ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16))
            )();
    }
    const classFactory = (classConfig) => {
        const typeid = uuidv4();
        const shared = {};
        Object.assign(classConfig, {
            __shared: shared,
            __type: typeid,
        });

        class Replicated {
            constructor(...args) {
                for(const [k, v] of Object.entries(this.__proto__)) {
                    v instanceof Function && v.bind(this);
                }
                this.__id = uuidv4();
                this.init instanceof Function && this.init(...args);
            }

            toJSON() {
                return [{}, ...Object.entries(this)].reduce(
                    (acc, [k, v]) => (k.slice(0, 2) != '__' && Object.assign(acc, {[k]: v}), acc)
                );
            }
        };
        for(const k in classConfig) {
            const descriptor = Object.getOwnPropertyDescriptor(classConfig, k);
            if(descriptor.get || descriptor.set) {
                Object.defineProperty(Replicated.prototype, k, {
                    get: descriptor.get,
                    set: descriptor.set
                });
            }
            else {
                Object.assign(Replicated.prototype, {[k]: classConfig[k]});
            }
        }
        return Replicated;
    };
    const EventEmitter = classFactory({
        init() { this.listeners = {}; },
        on(name, callback) {
            (this.listeners[name] || (this.listeners[name] = [])) && this.listeners[name].push(callback); },
        emit(name, ...message) { (this.listeners[name] ?? []).map(v => (v(...message),v)); console.log(name, ...message); },
        unbind(name) { this.listeners[name] && (this.listeners[name] = null); },
        unbindAll() { this.listeners = {}; }
    });

    defineGlobal('lru_cache',       lru_cache);
    defineGlobal('uuidv4',          uuidv4);
    defineGlobal('classFactory',    classFactory);
    defineGlobal('EventEmitter',    EventEmitter);
})();

(function shitonen() {
    const globalPipes = {};
    const Shitonen = classFactory({ // Shit tier component
        on(...args) { this.__eventEmitter.on(...args); },
        helper: {
            htmlToElement(html) {
                const template = document.createElement('template');
                return template.innerHTML = html.trim() 
                    , template.content.firstChild;
            },
            toArray(obj) {
                const arr = [];
                for(let i = 0; i < obj.length; ++i) { arr.push(obj[i]); }
                return arr;
            },
            uuidv4() {
                return (() => (typeof crypto == 'undefined' || crypto.getRandomValues == undefined)
                    ? 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
                        let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                        return r.toString(16); })
                    : ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
                        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16))
                    )();
            },
            toRegex(str) {
                return str.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
            },
            generateDirective: lru_cache((match) => {
                return match.replaceAll('{{', '').replaceAll('}}', '').split('|').map(v=>v.trim());
            })
        },
        newShit(name) {
            const shit = this.shit;
            Object.defineProperty(shit, name, {
                get() { return shit.__data[name]; },
                set(val) {
                    shit.__data[name] = val;
                    shit.__eventEmitter.emit(JSON.stringify(['change', name]), val);
                },
                enumerable: true,
                configurable: true
            });
        },
        definePipe(name, processor) { this.__pipes[name] = processor; },
        getPipe(name) { return this.__pipes[name] ?? globalPipes[name]; },
        updateRefs() { this.__eventEmitter.emit('update_refs'); },
        query(query) { return this.element.querySelector(query) },
        queryAll(query) { return this.element.querySelectorAll(query); },
        defineProp(name, config) { Object.defineProperty(this, name, config); },
        init(config={}) {
            this.__eventEmitter = new EventEmitter;
            this.__pipes = {};
            const localPipes = config.pipes ?? {};
            Object.assign(this.__pipes, localPipes);
            const html = config.html ?? '';
            let element = config.element;
            try {
                element || (element = this.helper.htmlToElement(html));
            }
            catch(e) {
                console.error(`Failed to create component '${config.name ?? 'anonymous'}'. Error: ${e}`);
            }
            if(!element) return;
            this.element = element;
            this.config = config;
            this.shit = {
                __eventEmitter: new EventEmitter,
                __data: {},
                __element: element,
                __component: this,
                on(events, poopName, callback) {
                    events = events.split(' ').filter(v => !!v);
                    for(const event of events) {
                        this.__eventEmitter.on(JSON.stringify([event, poopName]), callback);
                    }
                }
            };

            const configShit = config.shit ?? {};
            const shitConfig = config.shitConfig ?? {};
            const getShitConfig = name => {
                return shitConfig[name] ?? {};
            };
            for(const [k, v] of Object.entries(configShit)) {
                const shit = this.shit;
                shit.__data[k] = v;
                this.newShit(k);
            }

            const parseElement = (element) => {
                const attributes = {};
                const updateRefs = () => {
                    for(const attr of this.helper.toArray(element.attributes ?? [])) {
                        attributes[attr.name] = {
                            get name() { return attr.name; },
                            get value() { return attr.value; },
                            set value(v) { attr.value = v; }
                        };
                    }
                    attributes['#innerHTML'] = {
                        get name() { return '#innerHTML'; },
                        get value() { return element.innerHTML; },
                        set value(v) {
                            element.innerHTML = v;
                        }
                    };
                    attributes['#value'] = {
                        get name() { return '#value'; },
                        get value() { return element.value; },
                        set value(v) { element.value = v; }
                    };
                };
                updateRefs();
                this.__eventEmitter.on('update_refs', () => updateRefs());
                
                const shit = this.shit;
                const shittyRegex = /\{\{[a-zA-Z0-9\|_ ]+\}\}/g;
                const renderChange = (config) => {
                    const replacements = {};
                    element && config.initialValue.match(shittyRegex).map(m => {
                        const directive = m.replaceAll('{{', '').replaceAll('}}', '').split('|').map(v=>v.trim());
                        let result = shit[directive[0]];
                        for(const pipeName of directive.slice(1)) {
                            const pipe = this.getPipe(pipeName);
                            pipe && (result = pipe(result));
                            !pipe && console.error(`Shitonen pipe '${pipeName}' not found!`);
                        }
                        replacements[m] = result;
                    });
                    let final = config.initialValue;
                    for(const [match, value] of Object.entries(replacements)) {
                        final = final.replaceAll(match, value);
                    }
                    if(!config.onApply) {
                        attributes[config.attrName].value = final;
                    }
                    else {
                        config.onApply(final);
                    }
                };
                Object.entries(attributes).map(([attrName, attr]) => `${attr.value}`.includes('{{') && `${attr.value}`.includes('}}') ? attr : null)
                    .filter(v => v != null)
                    .map(attr => (attr.value.match(shittyRegex) ?? []).map(m => (
                        (m = m.replaceAll('{{', '').replaceAll('}}', '').split('|').map(v=>v.trim())),
                        (() => {
                            const initialValue = attr.value;
                            if(attr.name != '#innerHTML') {
                                // normal attribute
                                shit.on('init change', m[0], () => {
                                    renderChange({
                                        'attrName': attr.name,
                                        'initialValue': initialValue
                                    });
                                });
                                return;
                            }
                            const children = this.helper.toArray(element.childNodes).filter(e => {
                                return e.nodeName == '#text'
                            });
                            for(const node of children) {
                                const match = node.textContent.match(shittyRegex);
                                if(!match) continue;
                                const initialValue = node.textContent;
                                shit.on('init change', m[0], () => {
                                    renderChange({
                                        'initialValue': initialValue,
                                        onApply(v) {
                                            // text content
                                            if(children.length != 1 || getShitConfig(m[0]).escape != false) {
                                                node.textContent = v;
                                                return;
                                            }
                                            const singleComponentEl = shit[m[0]].element || shit[m[0]].__element;
                                            // single element
                                            if(shit[m[0]] instanceof HTMLElement) {
                                                element.innerHTML = '';
                                                element.append(shit[m[0]]);
                                            }
                                            // single component
                                            else if(singleComponentEl) {
                                                element.innerHTML = '';
                                                element.append(singleComponentEl);
                                            }
                                            // multiple elements/components/html
                                            else if(shit[m[0]] instanceof Array) {
                                                element.innerHTML = '';
                                                for(const el of shit[m[0]]) {
                                                    if(el.element) {
                                                        element.appendChild(el.element)
                                                    }
                                                    else if(el.__element) {
                                                        element.appendChild(el.__element)
                                                    }
                                                    else {
                                                        el instanceof HTMLElement
                                                            ? element.appendChild(el)
                                                            : element.innerHTML += el
                                                    }
                                                }
                                            }
                                            // inner html
                                            else {
                                                element.innerHTML = v;
                                            }
                                        }
                                    });
                                });
                            }
                        })(), 'pee'))
                    );
            };
            const dfs = (element) => {
                parseElement(element);
                this.helper.toArray(element.childNodes ?? []).map(e => dfs(e));
            };
            config.setup && config.setup.call(this);
            dfs(element);
            for(const [k, v] of Object.entries(configShit)) {
                this.shit.__eventEmitter.emit(JSON.stringify(['init', k]));
            }
            config.init && config.init.call(this);
        },
    });
    Shitonen.definePipe = (name, processor) => { globalPipes[name] = processor; };
    Shitonen.getPipe = (name) => { return globalPipes[name]; };

    // DEFAULT SETUP
    Shitonen.definePipe('uppercase', v => (''+v).toUpperCase());
    Shitonen.definePipe('lowercase', v => (''+v).toLowerCase());
    Shitonen.definePipe('titlecase', v => v.split(' ').map(word => word[0].toUpperCase() + word.slice(1)).join(' '));
    Shitonen.definePipe('trim', v => (''+v).trim());
    Shitonen.definePipe('print', v => (console.log(v), v));

    // EXPORT
    defineGlobal('Shitonen', Shitonen);
    defineGlobal('Shit', function(config) {
        return (new Shitonen(config)).shit;
    });
})();
