const path = require("path");
const webpack = require("webpack");

module.exports = {
    mode: "production",
    entry: "./src/app.js",
    externals: {
        jquery: "$",
        fabric: "fabric",
        vue: "Vue",
        vuex: "Vuex"
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: [
                            [
                                "@babel/preset-env",
                                {
                                    targets: {
                                        chrome: "58",
                                        ie: "11",
                                    },
                                },
                            ],
                        ],
                        plugins: [
                            "@babel/plugin-transform-runtime",
                            "babel-plugin-transform-modern-regexp",
                        ],
                    },
                },
            },
            {
                test: /\.css$/i,
                include: path.resolve(__dirname, "src"),
                use: ["style-loader", "css-loader", "postcss-loader"],
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/i,
                type: "asset/resource",
            }
        ],
    },
    plugins: [
    ],
    resolve: {
        extensions: ["*", ".js", ".json"],
    },
    devServer: {
        contentBase: path.resolve(__dirname, "dist"),
        watchContentBase: true,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
            "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
        },
        compress: true,
        disableHostCheck: true,   // That solved it
    },
    target: "es5",
};
