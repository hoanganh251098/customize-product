import {
    App
} from './js';
// } from './js/main.js';
import Store from './js/store';
import Vue from 'vue';


import Modal from './js/components/modal.js';
import Collapsible from './js/components/collapsible.js';

// App debug
window.App = App;

const VueApp = new Vue({
    el: '#app',
    components: {
        modal: Modal,
        collapsible: Collapsible,
    },
    store: Store,
    data: {}
});

window.VueApp = VueApp;

