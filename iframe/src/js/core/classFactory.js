import UUIDv4Generator from './uuidv4.js';

const uuidv4 = UUIDv4Generator();

export default classConfig => {
    const typeid = uuidv4();
    const shared = {};
    Object.assign(classConfig, {
        __shared: shared,
        __type: typeid,
    });

    class Replicated {
        constructor(...args) {
            for(const [k, v] of Object.entries(this.__proto__)) {
                v instanceof Function && v.bind(this);
            }
            this.__id = uuidv4();
            this.init instanceof Function && this.init(...args);
        }

        toJSON() {
            return [{}, ...Object.entries(this)].reduce(
                (acc, [k, v]) => (k.slice(0, 2) != '__' && Object.assign(acc, {[k]: v}), acc)
            );
        }
    };
    for(const k in classConfig) {
        const descriptor = Object.getOwnPropertyDescriptor(classConfig, k);
        if(descriptor.get || descriptor.set) {
            Object.defineProperty(Replicated.prototype, k, {
                get: descriptor.get,
                set: descriptor.set
            });
        }
        else {
            Object.assign(Replicated.prototype, {[k]: classConfig[k]});
        }
    }
    return Replicated;
};

