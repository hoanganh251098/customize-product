function resize(base64, { width, height, scaleFactor, sizeCap=true }={}) {
    return new Promise((resolve, reject) => {
        if(!width && !height && !scaleFactor) return;

        const img = new Image();
        img.onload = function() {
            const canvas = document.createElement('canvas');

            let resizeWidth, resizeHeight;

            if(width && height) {
                resizeWidth     = width;
                resizeHeight    = height;
            }
            else if(width) {
                resizeWidth     = width;
                resizeHeight    = img.height / img.width * width;
            }
            else if(height) {
                resizeHeight    = height;
                resizeWidth     = img.width / img.height * height;
            }
            else if(scaleFactor) {
                resizeWidth     = img.width * scaleFactor;
                resizeHeight    = img.height * scaleFactor;
            }

            if(sizeCap) {
                resizeWidth     = Math.min(img.width, resizeWidth);
                resizeHeight    = Math.min(img.height, resizeHeight);
            }

            canvas.width    = resizeWidth;
            canvas.height   = resizeHeight;

            const ctx = canvas.getContext('2d');
            ctx.drawImage(img, 0, 0, resizeWidth, resizeHeight);

            resolve(canvas.toDataURL());
        }
        img.src = base64;
    });
}

function resizeToBlob(base64, { width, height, scaleFactor, sizeCap=true }={}) {
    return new Promise((resolve, reject) => {
        if(!width && !height && !scaleFactor) return;

        const img = new Image();
        img.onload = function() {
            const canvas = document.createElement('canvas');

            let resizeWidth, resizeHeight;

            if(width && height) {
                resizeWidth     = width;
                resizeHeight    = height;
            }
            else if(width) {
                resizeWidth     = width;
                resizeHeight    = img.height / img.width * width;
            }
            else if(height) {
                resizeHeight    = height;
                resizeWidth     = img.width / img.height * height;
            }
            else if(scaleFactor) {
                resizeWidth     = img.width * scaleFactor;
                resizeHeight    = img.height * scaleFactor;
            }

            if(sizeCap) {
                resizeWidth     = Math.min(img.width, resizeWidth);
                resizeHeight    = Math.min(img.height, resizeHeight);
            }

            canvas.width    = resizeWidth;
            canvas.height   = resizeHeight;

            const ctx = canvas.getContext('2d');
            ctx.drawImage(img, 0, 0, resizeWidth, resizeHeight);

            canvas.toBlob(blob => {
                resolve(blob);
            });
        }
        img.src = base64;
    });
}

function toBlob(dataURI) {
    'use strict'
    let byteString, 
        mimestring 

    if(dataURI.split(',')[0].indexOf('base64') !== -1 ) {
        byteString = atob(dataURI.split(',')[1])
    } else {
        byteString = decodeURI(dataURI.split(',')[1])
    }

    mimestring = dataURI.split(',')[0].split(':')[1].split(';')[0]

    let content = new Array();
    for (let i = 0; i < byteString.length; i++) {
        content[i] = byteString.charCodeAt(i)
    }

    return new Blob([new Uint8Array(content)], {type: mimestring});
}

export {
    resize,
    resizeToBlob,
    toBlob
};

