import $ from 'jquery';

function uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}
function Cropper({ element, imageSrc, path, viewBox, width, height }={}) {
    const width_ = width ?? 100;
    const height_ = height ?? 100;
    const defaultpath = `M 0 0 l ${width_},0 l 0,${height_} l -${width_},0 l 0,-${height_}`;
    const defaultviewBox = `0 0 ${width_} ${height_}`;
    if(!Cropper.cropData) {
        Cropper.cropData = {
            mouse: {
                startX: 0,
                startY: 0,
            },
            handle: {
                startX: 0,
                startY: 0,
            },
            data: {}
        };
        window.cropData = Cropper.cropData;
    }
    const cropData = Cropper.cropData;
    if(!Cropper.documentInitialized) {
        Cropper.documentInitialized = true;
        let el, cropper, wrapper, cropperData;
        function handleResizeStart(e) {
            el.mousemove(handleResizeMove);
            el.on('touchmove', handleResizeMove);
        }
        function handleResizeMove(e) {
            console.log(e);
        }
        function handleMouseMove(e) {
            e.preventDefault();
            const offsetX = (e.pageX ?? e.changedTouches[0].pageX) - cropData.mouse.startX;
            const offsetY = (e.pageY ?? e.changedTouches[0].pageY) - cropData.mouse.startY;
            let left = offsetX + cropData.handle.startX;
            let top = offsetY + cropData.handle.startY;
            cropperData.position = {
                x: left,
                y: top
            };
        }
        function handleStart(e) {
            if($(e.target).is('.control-dot')) {
                return handleResizeStart(e);
            }
            wrapper = $(this);
            cropper = wrapper.closest('.cropper');
            el = $(`[cropper-id=${cropper.attr('cropper-id')}]`);
            cropperData = cropData.data[cropper.attr('cropper-id')];
            cropData.mouse.startX = (e.pageX ?? e.changedTouches[0].pageX);
            cropData.mouse.startY = (e.pageY ?? e.changedTouches[0].pageY);
            cropData.handle.startX = cropperData.position.x;
            cropData.handle.startY = cropperData.position.y;
            el.on('mousemove', handleMouseMove);
            el.on('touchmove', handleMouseMove);
        }
        $(document).on('mousedown', '.mask-wrapper', handleStart);
        $(document).on('touchstart', '.mask-wrapper', handleStart);
        $(document).on('mouseup', function handler() {
            if(!el) return;
            el.off('mousemove', handleMouseMove);
            el.off('touchmove', handleMouseMove);
            el.off('mousemove', handleResizeMove);
            el.off('touchmove', handleResizeMove);
        });
    }
    if(!element) return;
    let el = $(element);
    if(el.is('[cropper-id]')) return cropData.data[el.attr('cropper-id')];
    const id = uuidv4();
    el.attr('cropper-id', id);
    const data = {
        __position: {
            x: 0,
            y: 0
        },
        __width: 100,
        __height: 100,
        __scaleX: 1,
        __scaleY: 1,
        __zoom: 1,
        __rotation: 0,
        get adjust() {
            return adjust;
        },
        get imageSrc() {
            return image.src;
        },
        set imageSrc(src) {
            image.src = src;
        },
        get imageBoundingRect() {
            return image.getBoundingClientRect();
        },
        get path() {
            return svg.find('path').attr('d');
        },
        set path(path) {
            const pathEl = svg.find('path');
            pathEl.css('transform', '');
            pathEl.attr('d', path);
            const pathBRect = pathEl[0].getBoundingClientRect();
            const pathScaleX = svg.width() / pathBRect.width;
            const pathScaleY = svg.height() / pathBRect.height;
            pathEl.css('transform', `scale(${pathScaleX}, ${pathScaleY})`);
        },
        get viewBox() {
            return svg.attr('viewBox');
        },
        set viewBox(v) {
            svg.attr('viewBox', v);
        },
        get width() {
            return this.__width;
        },
        set width(v) {
            this.__width = v;
            svg.attr('viewBox', `0 0 ${this.__width} ${this.__height}`);
            // svg.find('path').attr('d', `M 0 0 l ${this.__width},0 l 0,${this.__height} l -${this.__width},0 l 0,-${this.__height}`);
            adjust();
        },
        get height() {
            return this.__height;
        },
        set height(v) {
            this.__height = v;
            svg.attr('viewBox', `0 0 ${this.__width} ${this.__height}`);
            // svg.find('path').attr('d', `M 0 0 l ${this.__width},0 l 0,${this.__height} l -${this.__width},0 l 0,-${this.__height}`);
            adjust();
        },
        get cropX() {
            return this.__position.x;
        },
        set cropX(v) {
            this.__position.x = v;
            adjust();
        },
        get cropY() {
            return this.__position.y;
        },
        set cropY(v) {
            this.__position.y = v;
            adjust();
        },
        get cropSpanX() {
            const cropPos = this.__position;
            const imageBRect = image.getBoundingClientRect();
            const svgViewBox = svg.attr('viewBox').split(' ').map(v => +v);
            const svgWidth = svgViewBox[2] - svgViewBox[0];
            const svgHeight = svgViewBox[3] - svgViewBox[1];
            const svgRatio = svgWidth / svgHeight;
            const cropperRatio = imageBRect.width / imageBRect.height;
            let svgScale;
            if(svgRatio > cropperRatio) {
                svgScale = imageBRect.width / svgWidth;
            }
            else {
                svgScale = imageBRect.height / svgHeight;
            }
            const cropSpanX =  svgWidth * svgScale / this.zoom / imageBRect.width;
            return cropSpanX;
        },
        set cropSpanX(v) {
            const cropSpanX = this.cropSpanX;
            const scaleRatio = v / cropSpanX;
            this.zoom = this.zoom / scaleRatio;
            adjust();
        },
        get cropSpanY() {
            const cropPos = this.__position;
            const imageBRect = image.getBoundingClientRect();
            const svgViewBox = svg.attr('viewBox').split(' ').map(v => +v);
            const svgWidth = svgViewBox[2] - svgViewBox[0];
            const svgHeight = svgViewBox[3] - svgViewBox[1];
            const svgRatio = svgWidth / svgHeight;
            const cropperRatio = imageBRect.width / imageBRect.height;
            let svgScale;
            if(svgRatio > cropperRatio) {
                svgScale = imageBRect.width / svgWidth;
            }
            else {
                svgScale = imageBRect.height / svgHeight;
            }
            const cropSpanY =  svgHeight * svgScale / this.zoom / imageBRect.height;
            return cropSpanY;
        },
        set cropSpanY(v) {
            const cropSpanY = this.cropSpanY;
            const scaleRatio = v / cropSpanY;
            this.zoom = this.zoom / scaleRatio;
            adjust();
        },
        get scaleX() {
            return this.__scaleX;
        },
        set scaleX(v) {
            this.__scaleX = v;
            adjust();
        },
        get scaleY() {
            return this.__scaleY;
        },
        set scaleY(v) {
            this.__scaleY = v;
            adjust();
        },
        get position() {
            const imageBRect = image.getBoundingClientRect();
            return {
                x: this.__position.x * imageBRect.width,
                y: this.__position.y * imageBRect.height
            };
        },
        set position({ x, y }={}) {
            const imageBRect = image.getBoundingClientRect();
            if(x != undefined) {
                let left = Math.min(imageBRect.width - wrapper.width(), x);
                left = Math.max(0, left);
                this.__position.x = left / imageBRect.width;
            }
            if(y != undefined) {
                let top = Math.min(imageBRect.height - wrapper.height(), y);
                top = Math.max(0, top);
                this.__position.y = top / imageBRect.height;
            }
            wrapper.css('left', this.__position.x * imageBRect.width + 'px');
            wrapper.css('top', this.__position.y * imageBRect.height + 'px');
        },
        get zoom() {
            return this.__zoom;
        },
        set zoom(v) {
            this.__zoom = v;
            adjust();
        },
        get rotation() {
            return this.__rotation;
        },
        set rotation(v) {
            this.__rotation = v;
            adjust();
            adjust();
        },
        onLoad(callback) {
            $(image).on('load', callback);
        },
        offLoad(handler) {
            $(image).off('load', handler);
        },
        export() {
            const cropPos = this.__position;
            const imageBRect = image.getBoundingClientRect();
            const svgViewBox = svg.attr('viewBox').split(' ').map(v => +v);
            const svgWidth = svgViewBox[2] - svgViewBox[0];
            const svgHeight = svgViewBox[3] - svgViewBox[1];
            const svgRatio = svgWidth / svgHeight;
            const cropperRatio = imageBRect.width / imageBRect.height;
            let svgScale;
            if(svgRatio > cropperRatio) {
                svgScale = imageBRect.width / svgWidth;
            }
            else {
                svgScale = imageBRect.height / svgHeight;
            }
            return {
                imageSrc: this.imageSrc,
                cropX: cropPos.x,
                cropY: cropPos.y,
                cropSpanX: svgWidth * svgScale / this.zoom / imageBRect.width,
                cropSpanY: svgHeight * svgScale / this.zoom / imageBRect.height,
                scaleX: this.scaleX,
                scaleY: this.scaleY,
                rotation: this.rotation,
                zoom: this.zoom
            }
        },
        import(data, callback) {
            return new Promise(resolve => {
                const {
                    imageSrc,
                    cropX,
                    cropY,
                    cropSpanX,
                    cropSpanY,
                    scaleX,
                    scaleY,
                    rotation,
                    zoom
                } = data;
                if(imageSrc != this.imageSrc) {
                    this.imageSrc = imageSrc;
                    const handler = () => {
                        this.cropX = cropX;
                        this.cropY = cropY;
                        this.zoom = zoom;
                        this.scaleX = scaleX;
                        this.scaleY = scaleY;
                        this.rotation = rotation;
                        adjust();
                        this.offLoad(handler);
                        resolve();
                        callback && callback();
                    };
                    this.onLoad(handler);
                }
                else {
                    this.cropX = cropX;
                    this.cropY = cropY;
                    this.zoom = zoom;
                    this.scaleX = scaleX;
                    this.scaleY = scaleY;
                    this.rotation = rotation;
                    adjust();
                    resolve();
                    callback && callback();
                }
            });
        }
    };
    cropData.data[id] = data;
    el.html(`
        <div class="img-wrapper">
            <img class="img-edit">
        </div>
        <div class="mask-wrapper">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="${viewBox ?? defaultviewBox}" version="1.1">
                <defs xmlns="http://www.w3.org/2000/svg">
                    <mask id="masked_${id}">
                        <rect width="100%" height="100%" fill="white"></rect>
                        <path d="${path ?? defaultpath}" stroke="none" fill="black"></path>
                    </mask>
                </defs>
                <rect width="100%" height="100%" fill="rgba(2, 2, 2, .8)" mask="url(#masked_${id})"></rect>
            </svg>
        </div>
    `);
    const wrapper = el.find('.mask-wrapper');
    const svg = el.find('svg');
    const image = el.find('img')[0];
    const adjust = () => {
        const imageBRect = image.getBoundingClientRect();
        const elBRect = el[0].getBoundingClientRect();
        const svgViewBox = svg.attr('viewBox').split(' ').map(v => +v);
        const svgWidth = svgViewBox[2] - svgViewBox[0];
        const svgHeight = svgViewBox[3] - svgViewBox[1];
        const svgRatio = svgWidth / svgHeight;
        const cropperRatio = imageBRect.width / imageBRect.height;
        const boxShadowSpread = Math.max($(window).width(), $(window).height());
        let svgScale;
        if(svgRatio > cropperRatio) {
            svgScale = imageBRect.width / svgWidth;
        }
        else {
            svgScale = imageBRect.height / svgHeight;
        }
        svg.attr('width', svgWidth * svgScale / data.zoom + 'px');
        svg.attr('height', svgHeight * svgScale / data.zoom + 'px');
        el.find('.mask-wrapper').css('box-shadow', `0 0 0 ${boxShadowSpread}px rgb(2 2 2 / 80%)`);
        data.position = {
            x: data.position.x,
            y: data.position.y
        };
        $(image).css('transform', `scale(${data.scaleX}, ${data.scaleY}) rotate(${data.rotation}deg)`);
        el.find('.img-wrapper').css('width', imageBRect.width + 'px');
        el.find('.img-wrapper').css('height', imageBRect.height + 'px');
        el.css('width', imageBRect.width + 'px');
        el.css('height', imageBRect.height + 'px');
        $(image).css('position', 'relative');
        const imageLeft = +$(image).css('left').replace('px', '');
        const imageTop = +$(image).css('top').replace('px', '');
        $(image).css('left', imageLeft - (imageBRect.left - elBRect.left) + 'px');
        $(image).css('top', imageTop - (imageBRect.top - elBRect.top) + 'px');
    };
    image.draggable = false;
    image.onload = adjust;
    if(imageSrc) {
        image.src = imageSrc;
    }
    el.on('adjust', adjust);
    return data;
}
export default Cropper;

