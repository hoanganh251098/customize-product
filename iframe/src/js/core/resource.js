function Resource(data) {
    const self = {
        __parent: null,
        __children: [],
        addChild(child) {
            this.__children.push(child);
            child.__parent = this;
        },
        dealloc() {
            data.destroy && data.destroy();
            delete data.__resourceInstance;
            if(this.__parent) {
                const idx = this.__parent.__children.indexOf(this);
                if(idx != -1) {
                    this.__parent.__children.splice(idx, 1);
                }
                this.__parent = null;
            }
            for(const child of this.__children) {
                child.dealloc();
            }
        },
        get data() {
            return data;
        }
    };
    data.__resourceInstance = self;
    return self;
}

export default Resource;
