import EnvConfig from "../config/env.js";
import $ from 'jquery';

const origin = EnvConfig.mode == 'development'
    ? 'https://dev.navitee.com'
    : ''
    ;

const endpoints = {
    imageUpload     : origin + '/Plugin/upload-images/upload.php',
    oldProducts     : "https://customize.navitee.com/design-upload/products",
};

function rGET(url) {
    return $.ajax({
        url,
        method: 'GET'
    });
}

function rPOST(url, body) {
    let contentType;
    let data;
    if(body) {
        contentType = 'application/json';
        data        = JSON.stringify(body)
    }
    return $.ajax({
        url,
        method: 'POST',
        contentType,
        data
    });
}

export {
    endpoints
};
