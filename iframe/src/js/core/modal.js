import $ from 'jquery';

function uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}
function Modal(element) {
    if(!$('.ha-modal-region').length) {
        $(document.body).append('<div class="ha-modal-region"></div>');
    }
    const modalID = uuidv4();
    const el = $(`
        <div class="ha-modal"></div>
    `);
    if(element) {
        el.append($(element));
    }
    $('.ha-modal-region').append(el);
    return {
        launch() {
            el.toggleClass('show', true);
            $('.ha-modal-region').toggleClass('show', true);
            $(document.body).toggleClass('no-scroll', true);
        },
        close() {
            el.toggleClass('show', false);
            $('.ha-modal-region').toggleClass('show', !!$('.ha-modal.show').length);
            $(document.body).toggleClass('no-scroll', !!$('.ha-modal.show').length);
        },
        get element() {
            return el;
        },
        destroy() {
            el.remove();
        }
    };
}

export default Modal;
