import { EventEmitter } from '../core/eventSystem';
import Vuex from 'vuex';
import Vue from 'vue';

Vue.use(Vuex);

const urlParam = new URLSearchParams(window.location.search);

export default new Vuex.Store({
    // DEBUG
    strict: urlParam.get('debug') == 'true',

    state: {
        productInfo: {
            sku: '',
            subproductSKU: '',
            product: {},
            subproduct: {},
        },
        error: {
            show: false,
            message: '',
        },
        isFontError: false,
        eventEmitter: new EventEmitter,
        debugMode: urlParam.get('debug') == 'true',
        editors: {
            gallery: {
                show: false,
            },
            theme: {
                show: false,
                expand: false,
            },
            options: {
                show: false,
                expand: false,
            },
            photos: {
                show: false,
                expand: false,
            },
            texts: {
                show: false,
                expand: false,
            },
            maps: {
                show: false,
                expand: false,
            },
            starmap: {
                show: false,
                expand: false,
            },
            moon: {
                show: false,
                expand: false
            }
        },
        document: {
            width: 0,
            height: 0,
        },
        layers: [],
        pageLoader: {
            show: false,
            message: "Please wait . . ."
        },
        canvasLoader: {
            show: false,
            message: "Please wait . . ."
        },
        canvasLoaderMessages: {
        },
    },

    getters: {
        debugMode: state => {
            return state.debugMode;
        },
        isSubproduct: state => {
            return state.productInfo.sku != state.productInfo.subproductSKU;
        },
        hasMaskLayer: state => {
            return state.layers.map(layer => layer.type).includes('mask');
        },
        hasTextLayer: state => {
            return state.layers.map(layer => layer.type).includes('text');
        },
        hasMapLayer: state => {
            return state.layers.map(layer => layer.type).includes('map-mask');
        },
        hasSpotifyLayer: state => {
            return state.layers.map(layer => layer.typeMetadata.module).includes('spotify');
        },
        hasSpiralLayer: state => {
            return state.layers.map(layer => layer.typeMetadata.module).includes('spiral');
        },
        hasSquareLayer: state => {
            return state.layers.map(layer => layer.typeMetadata.module).includes('square');
        },
        hasSongLayer: state => {
            return state.layers.map(layer => layer.typeMetadata.module).includes('lyrics');
        },
        hasBackgroundTextLayer: state => {
            return state.layers.map(layer => layer.type + layer.typeMetadata.module).includes('textbackground');
        },
        hasClipartTextLayer: state => {
            return state.layers.map(layer => layer.type).includes('clipart-text');
        },
        hasBackgroundLayer: state => {
            return state.layers.map(layer => layer.type).includes('bgcolor');
        },
        editorVisibility: state => name => {
            return state.editors[name].show;
        },
        editorExpansion: state => name => {
            return state.editors[name].expand;
        },
        loaderVisibility: state => {
            return state.pageLoader.show;
        },
        pageLoaderMessage: state => {
            return state.pageLoader.message;
        },
        imageEditModals: state => {},
        canvasLoader: state => {
            const messages = [];
            for(const [k, message] of Object.entries(state.canvasLoaderMessages)) {
                if(!messages.includes(message)) {
                    messages.push(message);
                }
            }
            return {
                show: messages.length > 0,
                messages
            };
        }
    },

    actions: {
        loadProduct({ commit, state }) { return 'hehe'; },
        refreshCanvas({ commit, state }) {},
    },

    mutations: {
        setDebugMode(state, v) {
            state.debugMode = v;
        },
        showEditor(state, name) {
            state.editors[name].show = true;
        },
        hideEditor(state, name) {
            state.editors[name].show = false;
        },
        expandEditor(state, name) {
            state.editors[name].expand = true;
        },
        collapseEditor(state, name) {
            state.editors[name].expand = false;
        },
        showPageLoader(state, message) {
            if(message != undefined) state.pageLoader.message = message;
            state.pageLoader.show = true;
        },
        hidePageLoader(state) {
            state.pageLoader.show = false;
        },
        showCanvasLoader(state, message) {
            if(message != undefined) state.canvasLoader.message = message;
            state.canvasLoader.show = true;
        },
        hideCanvasLoader(state) {
            for(const messageID of Object.keys(state.canvasLoaderMessages)) {
                Vue.delete(state.canvasLoaderMessages, messageID);
            }
        },
        setLayers(state, layers) {
            state.layers = layers;
        },
        setProduct(state, product) {
            state.productInfo.product = product;
        },
        setSubproduct(state, subproduct) {
            state.productInfo.subproduct = subproduct;
        },
        setProductSKU(state, v) {
            state.productInfo.sku = v;
        },
        setSubproductSKU(state, v) {
            state.productInfo.subproductSKU = v;
        },
        setCanvasLoaderMessage(state, [messageID, message]) {
            if(message != undefined) {
                Vue.set(state.canvasLoaderMessages, messageID, message);
            }
            else {
                Vue.delete(state.canvasLoaderMessages, messageID);
            }
        },
        setFontError(state, v) {
            state.isFontError = v;
        }
    },

});

