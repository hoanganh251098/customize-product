import Vue from 'vue';

const template = `
<transition name="modal" @after-enter="$emit('shown')" @after-leave="$emit('hidden')">
    <div class="modal-mask scroll-hidden" v-show="show">
        <div class="modal-wrapper">
            <div class="modal-container">
                <div class="modal-header">
                    <span class="modal-title">
                        <slot name="title"></slot>
                    </span>
                    <button class="close" @click="close()">
                        <span class="fa fa-times"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <slot name="body"></slot>
                </div>
                <div class="modal-footer">
                    <slot name="footer"></slot>
                </div>
            </div>
        </div>
    </div>
</transition>
`;

export default {
    template,
    props: {
        show: {
            type: Boolean,
            default: false
        }
    },
    methods: {
        close() {
            this.show = false;
        },
        open() {
            this.show = true;
        }
    }
};
