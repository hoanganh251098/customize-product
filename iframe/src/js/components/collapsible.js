import UUIDv4Generator from '../core/uuidv4.js';
import Vue from 'vue';

const uuidv4 = UUIDv4Generator();

const template = `
<div class="collapsible" :class="[ show ? 'expanded' : 'collapsed' ]">
    <div class="collapsible-header">
        <div class="collapsible-title" @click="$emit('titleClick')">
            <slot name="title"></slot>
        </div>
        <div class="collapsible-toggle" @click="toggle()">
            <slot name="toggle"></slot>
        </div>
    </div>
    <transition name="collapsible" @after-enter="$emit('expanded')" @after-leave="$emit('collapsed')">
        <div class="collapsible-body" v-show="show">
            <slot name="body"></slot>
        </div>
    </transition>
</div>
`;

export default {
    template,
    props: {
        show: {
            type: Boolean,
            default: true
        }
    },
    methods: {
        expand() {
            this.show = true;
        },
        collapse() {
            this.show = false;
        },
        toggle() {
            this.show = !this.show;
        }
    }
};
