import '../css/style.css';
import '../css/cropper.css';
import '../css/modal.css';
import {
    MAPBOX_API,
    MAPBOX_STYLES
} from './config/mapbox.js';
import {
    HEART_GENERATOR_URL,
    SQUARE_GENERATOR_URL
} from './config/services.js';
import {
    EventEmitter,
    MultiPromise
} from './core/eventSystem.js';
import UUIDv4Generator  from './core/uuidv4.js';
import classFactory     from './core/classFactory.js';
import {
    resize as imgResize,
    resizeToBlob as imgResizeToBlob,
    toBlob
} from './core/img.js';
import {
    endpoints as ApiEndpoints
} from './core/api.js';
import MoonPhase from 'moonphase-js';
import moment, { localeData } from 'moment';
import Cropper from './core/cropper';
import Modal from './core/modal';
import Resource from './core/resource';

window.Resource = Resource;

const uuidv4 = UUIDv4Generator();

Math.logBase = (n, base) => Math.log(n) / Math.log(base);

const App = {
    get productRoot() {
        if(App.mode('design')) {
            return 'http://localhost:3000/product-root';
        }
        return 'https://d1vyizgcxsj7up.cloudfront.net/products-data';
    },
    mode(...args) {
        const urlParam = new URLSearchParams(window.location.search);
        const mode = urlParam.get('mode') ?? '';
        for(const arg of args) {
            if(mode.includes(arg)) {
                return true;
            }
        }
        return false;
    },
    productName: null,
    mapStyle: null,
    events: new EventEmitter(),
    canvas: {
        instance: null,
        exportInstance: null,
        deselectAll() {
            this.instance.discardActiveObject().renderAll();
        },
        render() {
            this.instance.renderAll.bind(this.instance);
        },
        refresh() {
            if(App.mode('debug')) {
                console.log('CanvasDebugLog:: refreshed');
            }
            this.clear();
            for (const layer of App.layers) {
                layer.objects && layer.objects.forEach(obj => {
                    if (obj ?? false) {
                        obj.set({
                            dirty: true
                        });
                        this.instance.add(obj);
                    }
                });
            }
            this.instance.renderAll();
        },
        clear() {
            const canvas = this.instance;
            canvas.remove(...canvas.getObjects());
        },
        onClick(obj, callback) {
            App.events.on('fakeFabricMouseUp', target => {
                if(target == obj) {
                    callback();
                }
            });
            this.instance.on('mouse:up', (e) => {
                const target = e.target;
                if (target == obj) {
                    callback();
                }
            });
        },
        async exportBase64() {
            if(App.mode('debug')) {
                const refreshExportCanvas = () => {
                    const canvas = this.exportInstance;
                    canvas.remove(...canvas.getObjects());
                    for (const layer of App.layers) {
                        if(!layer.data.visible) continue;
                        if(layer.typeMetadata.export == 'false') continue;
                        layer.objects && layer.objects.forEach(obj => {
                            if (obj ?? false) {
                                obj.set({
                                    dirty: true
                                });
                                canvas.add(obj);
                            }
                        });
                    }
                    canvas.renderAll();
                };

                for(const layer of App.layers) {
                    if(layer.type == 'fg') {
                        layer.objects = [layer.allObjects[1]];
                    }
                }

                refreshExportCanvas();

                const base64 = this.exportInstance.toDataURL();

                for(const layer of App.layers) {
                    if(layer.type == 'fg') {
                        layer.objects = [layer.allObjects[0]];
                    }
                }

                App.canvas.refresh();

                return base64;
            }

            await (new Promise(async(resolve, reject) => {
                App.canvas.instance.setOverlayImage(
                    await App.helper.createWatermark('Preview'),
                    () => {
                        App.canvas.instance.renderAll();
                        resolve();
                    },
                    {
                        crossOrigin: 'anonymous'
                    }
                );
            }));

            const dataURL = this.instance.toDataURL({
                format: 'jpg',
                quality: 1,
                multiplier: 2
            });

            App.canvas.instance.setOverlayImage(null, () => {
                App.canvas.instance.renderAll();
            });

            return dataURL;
        },
        export: async function() {
            const exportData = {
                productName: App.productName,
                timestamp: (new Date).getTime(),
                layers: []
            };

            const exportPromises = [];

            for(const layer of App.layers) {
                const elayer = {
                    type: layer.type,
                    typeMetadata: layer.typeMetadata,
                    userInput: {
                        background: {
                            selected: null
                        },
                        text: '',
                        image: {
                            url: null,
                            cropbox: {
                                x       : null,
                                y       : null,
                                spanX   : null,
                                spanY   : null,
                                rotation: null,
                                scaleX  : null,
                                scaleY  : null,
                            },
                            filters: [],
                        },
                        photoText: {
                            characters: []
                        },
                        theme: '',
                        map: {
                            url     : null,
                            placeName: null,
                            lat     : null,
                            lng     : null,
                            zoom    : null,
                            pitch   : null,
                            bearing : null,
                            marker: {
                                lng: null,
                                lat: null,
                                ...layer.data.marker,
                                enabled: false,
                                x: layer.data.marker?.x ?? null,
                                y: layer.data.marker?.y ?? null,
                            },
                        },
                        spotify: {
                            uri: null,
                        },
                        starmap: {
                            placeName: null,
                            lat     : null,
                            lng     : null,
                            date    : null,
                            time    : null,
                        },
                        moon: {
                            date    : null
                        },
                        clipartText: {
                            characters: []
                        },
                        lyrics: {}
                    }
                };

                exportData.layers.push(elayer);

                const exportPromise = new Promise(async(resolve, reject) => {
                    if(layer.type == 'bgcolor') {
                        elayer.userInput.background.selected = $('.splide__slide.is-active').index();
                    }
                    if(layer.type == 'text') {
                        elayer.userInput.text = layer.data.textValue;
                    }
                    if(layer.type == 'mask') {
                        const imgObj = layer.objects[0];
                        const maskObj = layer.objects[1];

                        if(imgObj) {
                            const defaultCropboxData = App.helper.calcDefaultCropboxData(imgObj.width, imgObj.height, layer.config.width / layer.config.height);
                            elayer.userInput.image.cropbox.x = layer.data.cropboxX ?? defaultCropboxData.x;
                            elayer.userInput.image.cropbox.y = layer.data.cropboxY ?? defaultCropboxData.y;
                            elayer.userInput.image.cropbox.spanX = layer.data.cropboxSpanX ?? defaultCropboxData.spanX;
                            elayer.userInput.image.cropbox.spanY = layer.data.cropboxSpanY ?? defaultCropboxData.spanY;
                            elayer.userInput.image.cropbox.rotation = layer.data.cropboxRotation ?? defaultCropboxData.rotation;
                            elayer.userInput.image.cropbox.scaleX = layer.data.cropboxScaleX ?? defaultCropboxData.scaleX;
                            elayer.userInput.image.cropbox.scaleY = layer.data.cropboxScaleY ?? defaultCropboxData.scaleY;

                            const maxWidth      = 2048;
                            const scaleDownRatio = 1;
                            const cropboxSpanX  = elayer.userInput.image.cropbox.spanX;
                            const scaledWidth   = maskObj.width / cropboxSpanX;
                            const resizeWidth   = Math.min(maxWidth, scaledWidth) * scaleDownRatio;
                            const approxResizeWidth = Math.min(
                                maxWidth,
                                Math.pow(2, Math.ceil(Math.log2(resizeWidth)))
                            );

                            if(!layer.file['finalURL_' + approxResizeWidth]) {
                                layer.file['finalURL_' + approxResizeWidth] = new Promise(async(resolve, reject) => {
                                    const resized = await App.helper.imgResize(layer.file.dataURL, { width: approxResizeWidth });
                                    const blob = App.helper.toBlob(resized);
                                    const res = await App.helper.uploadBlobAWS(blob);
                                    const imgUrl = res.data;
                                    resolve(imgUrl);
                                });
                            }
                            elayer.userInput.image.url  = (await layer.file['finalURL_' + approxResizeWidth]) ?? null;
                        }
                        elayer.userInput.image.filters = [App.editors.theme.selectedFilterName];
                    }
                    if(layer.type == 'photo-text') {
                        for(let i = 0; i < layer.objects.length / 2; ++i) {
                            const imgObj = layer.objects[i * 2];
                            const maskObj = layer.objects[i * 2 + 1];

                            const charExportData = {
                                idx: i,
                                char: layer.value.characters[i].char,
                                config: layer.value.characters[i].config,
                                image: {
                                    url: null,
                                    cropbox: {
                                        x       : null,
                                        y       : null,
                                        spanX   : null,
                                        spanY   : null,
                                        rotation: null,
                                        scaleX  : null,
                                        scaleY  : null
                                    },
                                    filters: [],
                                }
                            };

                            if(imgObj) {
                                const defaultCropboxData = App.helper.calcDefaultCropboxData(imgObj.width, imgObj.height, layer.config.width / layer.config.height);
                                charExportData.image.cropbox.x = layer.data['cropboxX_'+i] ?? defaultCropboxData.x;
                                charExportData.image.cropbox.y = layer.data['cropboxY_'+i] ?? defaultCropboxData.y;
                                charExportData.image.cropbox.spanX = layer.data['cropboxSpanX_'+i] ?? defaultCropboxData.spanX;
                                charExportData.image.cropbox.spanY = layer.data['cropboxSpanY_'+i] ?? defaultCropboxData.spanY;
                                charExportData.image.cropbox.rotation = layer.data['cropboxRotation_'+i] ?? defaultCropboxData.rotation;
                                charExportData.image.cropbox.scaleX = layer.data['cropboxScaleX_'+i] ?? defaultCropboxData.scaleX;
                                charExportData.image.cropbox.scaleY = layer.data['cropboxScaleY_'+i] ?? defaultCropboxData.scaleY;

                                const maxWidth      = 2048;
                                const scaleDownRatio = 1;
                                const cropboxSpanX  = charExportData.image.cropbox.spanX;
                                const scaledWidth   = maskObj.width / cropboxSpanX;
                                const resizeWidth   = Math.min(maxWidth, scaledWidth) * scaleDownRatio;
                                const approxResizeWidth = Math.min(
                                    maxWidth,
                                    Math.pow(2, Math.ceil(Math.log2(resizeWidth)))
                                );

                                if(!layer.files[i]['finalURL_' + approxResizeWidth]) {
                                    layer.files[i]['finalURL_' + approxResizeWidth] = new Promise(async(resolve, reject) => {
                                        const resized = await App.helper.imgResize(layer.files[i].dataURL, { width: approxResizeWidth });
                                        const blob = App.helper.toBlob(resized);
                                        const res = await App.helper.uploadBlobAWS(blob);
                                        const imgUrl = res.data;
                                        resolve(imgUrl);
                                    });
                                }
                                charExportData.image.url  = (await layer.files[i]['finalURL_' + approxResizeWidth]) ?? null;
                            }
                            charExportData.image.filters = [App.editors.theme.selectedFilterName];
                            elayer.userInput.photoText.characters.push(charExportData);
                        }
                    }
                    if(layer.type == 'map-mask') {
                        const map = layer.editors[0].map;
                        if(map && layer.value) {
                            elayer.userInput.map.lat    = map.getCenter().lat;
                            elayer.userInput.map.lng    = map.getCenter().lng;
                            elayer.userInput.map.zoom   = map.getZoom();
                            elayer.userInput.map.pitch  = map.getPitch();
                            elayer.userInput.map.bearing = map.getBearing();
                            const r = await App.helper.uploadBlobAWS(App.helper.toBlob(layer.value));
                            elayer.userInput.map.url = r.data;
                            elayer.userInput.map.marker.lng = layer.editors[0].marker.getLngLat().lng;
                            elayer.userInput.map.marker.lat = layer.editors[0].marker.getLngLat().lat;
                        }
                    }
                    if(layer.type == 'custom' && layer.typeMetadata.module == 'spotify') {
                        elayer.userInput.spotify.uri = layer.spotifyURI ?? null;
                    }
                    if(layer.type == 'custom' && layer.typeMetadata.module == 'starmap') {
                        const place = layer.editors[0].modal.inputInfo.place;
                        elayer.userInput.starmap.lat = place
                            ? layer.editors[0].modal.inputInfo.place.geometry.location.lat()
                            : null
                            ;
                        elayer.userInput.starmap.lng = place
                            ? layer.editors[0].modal.inputInfo.place.geometry.location.lng()
                            : null
                            ;
                        elayer.userInput.starmap.date = layer.editors[0].modal.inputInfo.date;
                        elayer.userInput.starmap.time = layer.editors[0].modal.inputInfo.time;
                    }
                    if(layer.type == 'custom' && layer.typeMetadata.module == 'moonphase') {
                        const date = layer.editors[0].modal.inputInfo.date;
                        elayer.userInput.moon.date = date;
                    }
                    if(layer.type == 'clipart-text') {
                        elayer.userInput.clipartText.characters = layer.value;
                    }
                    if(layer.type == 'custom' && layer.typeMetadata.module == 'spiral') {
                        elayer.userInput.lyrics = layer.value;
                        if(layer.value) {
                            elayer.userInput.lyrics.text = $('.song-edit-container textarea').val();
                        }
                    }
                    if(layer.type == 'song-mask') {
                        elayer.userInput.lyrics = layer.value;
                        if(layer.value) {
                            elayer.userInput.lyrics.text = $('.song-edit-container textarea').val();
                        }
                    }
                    resolve();
                });
                exportPromises.push(exportPromise);
            }
            await Promise.all(exportPromises);
            return exportData;
        },
        import: async function(config, { resume=false }={}) {
            const layers = config.layers;
            await (async function fetchImages() {
                const blobMap = {};
                for(const [layerIdx, layer] of layers.entries()) {
                    if(layer.type != 'mask') continue;
                    const filter = layer.userInput.image.filters[0];
                    if(filter) {
                        const filterIndex = ['BlackWhite', 'Grayscale', 'Vintage', 'Bright'].indexOf(filter) + 1;
                        $(`.theme-option:eq(${filterIndex})`).click();
                    }
                    else {
                        $(`.theme-option:eq(0)`).click();
                    }
                    if(layer.userInput.image.url) {
                        const imgURL = layer.userInput.image.url.replaceAll('preview/image_preview', 'original/image_original');
                        if(!blobMap[imgURL]) {
                            const blob = await fetch(imgURL).then(r => r.blob());
                            blobMap[imgURL] = blob;
                            App.dropzone.instance.addFile(blob);
                        }
                        App.layers[layerIdx].file = blobMap[imgURL];
                    }
                }
            })();

            if(App.mode('debug')) {
                await new Promise(resolve => {
                    const intervalID = setInterval(() => {
                        let pass = true;
                        for(const layer of App.layers) {
                            if(layer.type == 'fg' && !layer.allObjects[1]) {
                                pass = false;
                                break;
                            }
                        }
                        if(pass) {
                            clearInterval(intervalID);
                            resolve();
                        }
                    }, 200);
                });
            }
            
            await (function fetchSpotify() {
                return new Promise((resolve, reject) => {
                    App.events.one('spotifyInfoReady', () => {
                        resolve();
                    }, { timeout: 10000 });
                    let foundSpotify = false;
                    for(const [layerIdx, layer] of layers.entries()) {
                        if(layer.type != 'custom' || layer.typeMetadata.module != 'spotify') continue;
                        App.editors.spotify.child.setText(layer.userInput.spotify.uri);
                        foundSpotify = true;
                        break;
                    }
                    if(!foundSpotify) resolve();
                });
            })();

            async function fetchLayer(layerIdx, layer) {
                return await new Promise(async resolve => {
                    const layerData = App.layers[layerIdx];
                    if(layer.type == 'mask') {
                        const editor = layerData.editors[0];
                        const file = layerData.file;
                        file && file.onThumbnailReady(() => {
                            editor.setImage(file.displayURL);
                            App.layerProcessor.image(layerData, img => {
                                const layerImageRatio = layerData.config.width / layerData.config.height;
                                const imgRatio = img.width / img.height;
                                let scaleRatio;
                                if(imgRatio > layerImageRatio) {
                                    scaleRatio = layerData.config.height / img.height;
                                    img.scaleToHeight(layerData.config.height);
                                }
                                else {
                                    scaleRatio = layerData.config.width / img.width;
                                    img.scaleToWidth(layerData.config.width);
                                }
                                if(layerData.config.keepRatio == false) {
                                    App.helper.scaleTo(img, {
                                        width: layerData.config.width,
                                        height: layerData.config.height
                                    });
                                }
                                img.set({
                                    left: layerData.config.left + layerData.config.width / 2 - img.width * scaleRatio / 2,
                                    top: layerData.config.top + layerData.config.height / 2 - img.height * scaleRatio / 2
                                });
                                layerData.objects[0] = img;

                                layerData.data.cropboxX = layer.userInput.image.cropbox.x;
                                layerData.data.cropboxY = layer.userInput.image.cropbox.y;
                                layerData.data.cropboxSpanX = layer.userInput.image.cropbox.spanX;
                                layerData.data.cropboxSpanY = layer.userInput.image.cropbox.spanY;
                                layerData.data.cropboxRotation = layer.userInput.image.cropbox.rotation;
                                layerData.data.cropboxScaleX = layer.userInput.image.cropbox.scaleX;
                                layerData.data.cropboxScaleY = layer.userInput.image.cropbox.scaleY;

                                App.helper.applyCropboxData(layerData);
                                App.canvas.refresh();
                                resolve();
                            });
                        });
                        return;
                    }
                    if(layer.type == 'text') {
                        layerData.data.textValue = layer.userInput.text;
                        resolve();
                        return;
                    }
                    if(layer.type == 'map-mask') {
                        layerData.editors[0].modal.__eventEmitter.one('initialized', () => {
                            if(!layer.userInput.map.zoom) return;
                            const map = layerData.editors[0].map;
                            const marker = layerData.editors[0].marker;
                            console.log('modal shown', layer.userInput.map);
                            map.setCenter({
                                lng: layer.userInput.map.lng,
                                lat: layer.userInput.map.lat,
                            });
                            map.setPitch(layer.userInput.map.pitch);
                            map.setBearing(layer.userInput.map.bearing);
                            map.setZoom(layer.userInput.map.zoom);
                            marker.setLngLat({
                                lng: layer.userInput.map.marker.lng,
                                lat: layer.userInput.map.marker.lat,
                            });
                        });
                        if(layer.userInput.map.url) {
                            const fetchPromise = new MultiPromise;
                            fetchPromise.register('map');
                            fetchPromise.register('marker');
                            fetchPromise.start().then(result => {
                                resolve();
                            });
                            layerData.value = layer.userInput.map.url;
                            App.layerProcessor.image(layerData, img => {
                                layerData.objects[0] = img;
                                App.canvas.refresh();
                                fetchPromise.fulfill('map');
                            });
                            const exported = layer.userInput.map;
                            fabric.Image.fromURL(layer.userInput.map.marker.url, img => {
                                const mask = layerData.objects[1];
                                mask.set('fill', '#e6a51201');
                                const clipPath = new fabric.Path(mask.path, {
                                    left: layerData.data.left,
                                    top: layerData.data.top,
                                    absolutePositioned: true,
                                    fill: "transparent",
                                    perPixelTargetFind: true
                                });
                                img.set({
                                    dirty: true,
                                    clipPath: clipPath,
                                    left: layerData.config.left + exported.marker.left * layerData.config.width,
                                    top: layerData.config.top + exported.marker.top * layerData.config.height,
                                    evented: false,
                                });
                                img.scaleToWidth(exported.marker.width * layerData.config.width);
                                if(layerData.config.keepRatio == false) {
                                    App.helper.scaleTo(img, {
                                        width: layerData.config.width,
                                        height: layerData.config.height
                                    });
                                }
                                App.helper.lockObject(img);
                                layerData.objects[2] = img;
                                App.canvas.refresh();
                                fetchPromise.fulfill('marker');
                            }, { crossOrigin: 'anonymous' });
                        }
                        else {
                            resolve();
                        }
                        return;
                    }
                    if(layer.type == 'custom' && layer.typeMetadata.module == 'starmap') {
                        layerData.importData = layer.userInput.starmap;
                        const listener = _layerData => {
                            if(_layerData != layerData) return;
                            App.events.remove('starmapLoaded', listener);
                            resolve();
                        };
                        App.events.one('starmapLoaded', () => {
                            App.events.on('starmapLoaded', listener);
                            App.events.emit('importStarmap', layerData);
                        }, { timeTravel: true });
                        return;
                    }
                    if(layer.type == 'song-mask') {
                        App.events.one('lyricsLoaded', () => {
                            $('.song-edit-container textarea').val(layer.userInput.lyrics.text);
                            $('.song-edit-container .apply-btn').click();
                        }, {timeTravel: true});
                        resolve();
                        return;
                    }
                    if(layer.type == 'custom' && layer.typeMetadata.module == 'spiral') {
                        App.events.one('lyricsLoaded', () => {
                            $('.song-edit-container textarea').val(layer.userInput.lyrics.text);
                            $('.song-edit-container .apply-btn').click();
                        }, {timeTravel: true});
                        resolve();
                        return;
                    }
                    if(layer.type == 'bgcolor') {
                        if(layer.userInput.background == undefined) return resolve();
                        const selected = layer.userInput.background.selected;
                        App.events.one('bgcolorReady', () => {
                            if(selected != null && selected > -1) {
                                App.events.emit('selectBgcolor', selected);
                            }
                            resolve();
                        }, { timeTravel: true });
                        return;
                    }
                    if(layer.type == 'clipart-text') {
                        const text = layer.userInput.clipartText.characters.map(c => c?.char ?? ' ').join('');
                        $('.clipart-text-group input').val(text);
                        $('.clipart-text-group .apply-btn').click();
                        function runClicker() {
                            for(const [objIdx, object] of layerData.objects.entries()) {
                                const char = layer.userInput.clipartText.characters[objIdx]
                                    ? layer.userInput.clipartText.characters[objIdx].char
                                    : ' '
                                    ;
                                if(char >= 'A' && char <= 'Z') {
                                    App.events.emit('setClipartLetterIndex', {
                                        position: objIdx,
                                        index: layer.userInput.clipartText.characters[objIdx].idx
                                    });
                                }
                            }
                            resolve();
                        }
                        const intervalID = setInterval(() => {
                            if(layerData.objects.filter(obj => !!obj).length == text.length) {
                                clearInterval(intervalID);
                                runClicker();
                            }
                        }, 500);
                        return;
                    }
                    resolve();
                });
            }

            const layerLoaders = [];
            for(const [layerIdx, layer] of layers.entries()) {
                layerLoaders.push(fetchLayer(layerIdx, layer));
            }
            await Promise.all(layerLoaders);
        }
    },
    layers: [],
    dropzone: {
        get instance() {
            return Dropzone.forElement('#myAwesomeDropzone');
        },
        init() {
            Dropzone.autoDiscover = false;
            $('#myAwesomeDropzone').dropzone({
                url: ApiEndpoints.imageUpload,
                autoProcessQueue: false,
                addRemoveLinks: true,
                paramName: "file",
                timeout: 180000,
                acceptedFiles: "image/*",
                init() {
                    const self = this;
                    $('#myAwesomeDropzone').toggleClass('dropzone', true);
                    Dropzone.forElement('#myAwesomeDropzone');
                    this.on("addedfile", async function(file) {
                        file.messageID = uuidv4();
                        file.thumbnailReady = false;
                        file.onThumbnailReady = function(callback) {
                            const intervalID = setInterval(() => {
                                if(file.displayURL) {
                                    clearInterval(intervalID);
                                    callback(file);
                                }
                            }, 50);
                        };
                        Object.defineProperty(file, 'imagesURLs', {
                            async get() {
                                if(!file.dataURL) return null;
                                if(file.__imagesURLs) return file.__imagesURLs;

                                VueApp.$store.commit('setCanvasLoaderMessage', [file.messageID, 'Processing images ...']);

                                if(!file.awsUpload) {
                                    file.awsUpload = App.helper.uploadBlobAWS(App.helper.toBlob(file.displayURL));
                                }
                                const res = await file.awsUpload;
                                const imgURL = res.data;
                                const filterOrigin = 'https://dev.goodsmize.com/Plugin/upload-url/upload.php';
                                const images = {
                                    preview: imgURL,
                                    original: imgURL,
                                    filter: {
                                        get BlackWhite() {
                                            return (async() => {
                                                const url = `${filterOrigin}?url=${imgURL}&greyscale=1&brightness=0&contrast=50&gamma=0.03&colorize=0,0,0`;
                                                if(!this.__BlackWhite) {
                                                    this.__BlackWhite = App.helper.createBlobURL(url);
                                                }
                                                return await this.__BlackWhite;
                                            })();
                                        },
                                        get Grayscale() {
                                            return (async() => {
                                                const url = `${filterOrigin}?url=${imgURL}&greyscale=1&brightness=0&contrast=0&gamma=1&colorize=0,0,0`;
                                                if(!this.__Grayscale) {
                                                    this.__Grayscale = App.helper.createBlobURL(url);
                                                }
                                                return await this.__Grayscale;
                                            })();
                                        },
                                        get Vintage() {
                                            return (async() => {
                                                const url = `${filterOrigin}?url=${imgURL}&brightness=0&contrast=0&gamma=0.72&colorize=16,7,0`;
                                                if(!this.__Vintage) {
                                                    this.__Vintage = App.helper.createBlobURL(url);
                                                }
                                                return await this.__Vintage;
                                            })();
                                        },
                                        get Bright() {
                                            return (async() => {
                                                const url = `${filterOrigin}?url=${imgURL}&brightness=12&contrast=0&gamma=1&colorize=-13,-14,-31`;
                                                if(!this.__Bright) {
                                                    this.__Bright = App.helper.createBlobURL(url);
                                                }
                                                return await this.__Bright;
                                            })();
                                        },
                                    }
                                };

                                VueApp.$store.commit('setCanvasLoaderMessage', [file.messageID]);

                                file.__imagesURLs = images;

                                return images;
                            }
                        });
                    });
                    this.on("thumbnail", async function(file) {
                        try {
                            file.displayURL = await App.helper.imgResize(file.dataURL, { width: Math.min(window.screen.width, 1024) });
                        }
                        catch(e) {
                            VueApp.$store.commit('setCanvasLoaderMessage', [file.messageID]);
                            console.error(e);
                        }
                    });
                }
            });
        }
    },
    editors: {
        backgrounds: {
            __eventEmitter: null,
            get __groupEl() {
                return $(`.group.background-group`);
            },
            onBackgroundSelect(callback) {
                this.__eventEmitter.on('background_select', () => {
                    callback(+$('.splide__slide.is-active > :eq(0)').attr('data-index'));
                });
            },
            fetch(backgrounds) {
                this.__eventEmitter = new EventEmitter;
                this.__eventEmitter.register('background_select');
                const html = backgrounds.map((v, idx) => v.type == 'color'
                    ? `<div class="bg" style="background-color: ${v.value};" data-type="color" data-index="${idx}"></div>`
                    : `<img class="bg" src="${v.value}" data-type="image" data-index="${idx}">`).map(v => `<li class="splide__slide">${v}</li>`).join('\n');
                this.__groupEl.find('.splide__list').html(html);
                const splide = new Splide( '.editor-wrapper .splide', {
                    perPage: 1,
                    keyboard: false
                }).mount();
                splide.on('moved', () => {
                    this.__eventEmitter.emit('background_select');
                });
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);
            }
        },
        imageGallery: {
            __popoverKiller: null,
            __eventEmitter: null,
            get events() {
                return this.__eventEmitter;
            },
            launch() {
                // $('#imageGalleryModal').modal('show');
                this.modal.launch();
            },
            close() {
                // $('#imageGalleryModal').modal('hide');
                this.modal.close();
            },
            getUploadedFiles() {
                return App.dropzone.instance.files;
            },
            openUpload() {
                $("#myAwesomeDropzone").click();
            },
            onImageSelect(callback) {
                $(document).on('click', '#imageGalleryModal .dz-image', function () {
                    const itemIdx = $(this).closest('.dz-preview').index() - 1;
                    callback(App.dropzone.instance.files[itemIdx]);
                });
            },
            onAutofill(callback) {
                const othis = this;
                this.__eventEmitter.on('autofill', () => {
                    callback.call(this);
                });
            },
            unbindAll() {
                $(document).off('click', '#imageGalleryModal .dz-image');
            },
            init() {
                const othis = this;
                this.__eventEmitter = new EventEmitter;
                this.__eventEmitter.register('autofill');
                App.events.on('beforeProductLoad', () => {
                    this.__eventEmitter.unbindAll();
                });

                if(!this.modal) {
                    const modal = Modal($('#imageGalleryModal'));
                    this.modal = modal;
                    $(this.modal.element).on('click', '.close-btn', () => {
                        this.modal.close();
                    });
                }

                $('#imageGalleryModal .autofill-btn').popover({
                    content: '<i class="fa fa-exclamation-triangle" style="margin-right: 1em; color: #ff9a72;"></i>No images to autofill!',
                    container: '#imageGalleryModal',
                    placement: 'top',
                    html: true,
                });
                $('#imageGalleryModal .autofill-btn').unbind();
                $('#imageGalleryModal .autofill-btn').on('click', function() {
                    $(this).popover('hide');
                    $(this).popover('disable');
                    if(othis.getUploadedFiles().length == 0) {
                        $(this).popover('enable');
                        $(this).popover('show');
                        if(othis.__popoverKiller) {
                            clearTimeout(othis.__popoverKiller);
                        }
                        othis.__popoverKiller = setTimeout(() => {
                            $(this).popover('hide');
                        }, 2000);
                    }
                    othis.__eventEmitter.emit('autofill');
                });
                $(document).on('click', '#imageGalleryModal .dz-image', function () {
                    othis.close();
                });
            }
        },
        images: {
            // new
            __prototype: {
                __imgData: null,
                __layer: {},
                __eventEmitter: null,
                onApply(callback) {
                    this.__eventEmitter.on('apply', () => {
                        callback.call(this);
                    });
                    return this;
                },
                onChangeImage(callback) {
                    this.__eventEmitter.on('change_image', () => {
                        callback.call(this);
                    });
                },
                apply() {
                    this.__cropData = this.cropper.export();
                    this.__eventEmitter.emit('apply');
                },
                getCropboxData() {
                    return this.__cropData;
                },
                setLayer(layer) {
                    this.__layer = layer;
                    return this;
                },
                setImage(b64) {
                    this.__imgData = b64;
                    return this;
                },
                launch() {
                    const othis = this;
                    if(!$(`#editorImage_${this.__id}`).length) {
                        const modal = Modal($(`
                        <div id="editorImage_${this.__id}" style="user-select:none;">
                            <div class="modal-header">
                                <h5 class="modal-title">Image Editor</h5>
                                <button type="button" class="close close-btn" style="outline:none; color:#000000;">
                                    <span aria-hidden="true" class="fa fa-times"></span>
                                </button>
                            </div>
                            <div class="d-flex justify-content-center flex-wrap">
                                <div class="cropper-container" style="display:flex;justify-content:center;align-items:center;padding:2rem;">
                                    <div class="cropper"></div>
                                </div>
                                <div style="width:100%;padding:0 2rem;">
                                    <input class="zoom-input" type="range" min="0" max="100" value="0">
                                </div>
                                <div style="width:100%;padding:0 2rem;display:flex;justify-content:space-between;flex-wrap:wrap;">
                                    <button class="btn mt-3 rotate-left-btn">
                                        <i class="fa fa-undo"></i>
                                    </button>
                                    <button class="btn mt-3 rotate-right-btn">
                                        <i class="fa fa-redo"></i>
                                    </button>
                                    <button class="btn mt-3 flip-x-btn">Flip X</button>
                                    <button class="btn mt-3 flip-y-btn">Flip Y</button>
                                </div>
                            </div>
                            <div class="d-flex" style="margin: 1rem">
                                <div style="flex:1;padding:1rem">
                                    <button class="btn btn-secondary change-image-btn width-100">Change image</button>
                                </div>
                                <div style="flex:1;padding:1rem">
                                    <button class="btn btn-primary apply-btn width-100">Apply</button>
                                </div>
                            </div>
                        </div>
                        `));
                        this.modal = modal;
                        $(document).on('click', `#editorImage_${this.__id} .close-btn`, function() {
                            othis.close();
                        });
                        $(document).on('click', `#editorImage_${this.__id} .apply-btn`, function() {
                            othis.apply();
                        });
                        $(document).on('click', `#editorImage_${this.__id} .change-image-btn`, function() {
                            othis.__eventEmitter.emit('change_image');
                        });
                        $(document).on('input', `#editorImage_${this.__id} .zoom-input`, function() {
                            if(!othis.cropper) return;
                            setTimeout(() => {
                                othis.cropper.zoom = Math.pow(1.1, Math.max(0, 0 + (+$(this).val()) / 10));
                            });
                        });
                        $(document).on('click', `#editorImage_${this.__id} .rotate-left-btn`, function() {
                            if(!othis.cropper) return;
                            othis.cropper.rotation = (360 + othis.cropper.rotation - 90 * othis.cropper.scaleX * othis.cropper.scaleY) % 360;
                            othis.cropper.adjust();
                        });
                        $(document).on('click', `#editorImage_${this.__id} .rotate-right-btn`, function() {
                            if(!othis.cropper) return;
                            othis.cropper.rotation = (360 + othis.cropper.rotation + 90 * othis.cropper.scaleX * othis.cropper.scaleY) % 360;
                            othis.cropper.adjust();
                        });
                        $(document).on('click', `#editorImage_${this.__id} .flip-x-btn`, function() {
                            if(!othis.cropper) return;
                            othis.cropper.scaleX = -othis.cropper.scaleX;
                            othis.cropper.adjust();
                        });
                        $(document).on('click', `#editorImage_${this.__id} .flip-y-btn`, function() {
                            if(!othis.cropper) return;
                            othis.cropper.scaleY = -othis.cropper.scaleY;
                            othis.cropper.adjust();
                        });
                    }
                    if(!this.cropper) {
                        this.cropper = Cropper({
                            element: $(`#editorImage_${this.__id} .cropper`)
                        });
                    }
                    this.cropper.imageSrc = this.__imgData;
                    this.modal.launch();
                    setTimeout(function() {
                        const layer = othis.__layer;
                        const ratio = layer.config.width / layer.config.height;
                        othis.cropper.width = ratio * 100;
                        const imageWidth = othis.cropper.imageBoundingRect.width;
                        const imageHeight = othis.cropper.imageBoundingRect.height;
                        const defaultCropboxData = App.helper.calcDefaultCropboxData(
                            imageWidth, imageHeight, ratio
                        );
                        const restoreData = othis.cropper.export();
                        restoreData.cropX = layer.data['cropboxX'] ?? defaultCropboxData.x;
                        restoreData.cropY = layer.data['cropboxY'] ?? defaultCropboxData.y;
                        othis.cropper.import(restoreData, () => {
                            othis.cropper.adjust();
                            if(othis.cropper.path != othis.__layer['svgPath']) {
                                othis.cropper.path = othis.__layer['svgPath'];
                            }
                        });
                    }, 200)
                    return this;
                },
                close() {
                    this.modal.close();
                    return this;
                },
                initEditButton() {
                    if(!$('#image-edit-style').length) {
                        $(document.body).append(`
                        <div id="image-edit-style">
                            <style>
                                .photo-edit-btn {
                                    outline: none !important;
                                    border: none;
                                    border-radius: 50%;
                                    width: 3rem;
                                    height: 3rem;
                                    font-size: 1.5rem;
                                    background-color: #ffffff88;
                                    display: flex;
                                    justify-content: center;
                                    align-items: center;
                                    transform: translate(-50%, -50%);
                                    pointer-events: auto;
                                }
                                .photo-edit-btn:hover {
                                    background-color: white;
                                }
                                .photo-edit-btn:active {
                                    background-color: #ffffff88;
                                }
                            </style>
                        </div>
                        `);
                    }
                    const layer = this.__layer;
                    if(!layer) return;
                    const imageObj = layer.objects[1];
                    if(!imageObj) return;
                    const editBtn = $(`
                        <div style="position:absolute;z-index:999;top:0;left:0;pointer-events:none;">
                            <button class="photo-edit-btn">
                                <i class="fa fa-image"></i>
                            </button>
                        </div>
                    `);
                    this.editBtn = editBtn;
                    $(document.body).append(editBtn);
                    editBtn.on('click', () => {
                        const selected = layer.objects[0];
                        if(!selected) {
                            App.editors.imageGallery.unbindAll();
                            App.editors.imageGallery.onImageSelect(file => {
                                file.onThumbnailReady(() => {
                                    layer.file = file;
                                    this.setImage(file.displayURL);
                                    App.editors.imageGallery.close();
                                    this.launch();
                                });
                            });
                            App.editors.imageGallery.launch();
                        }
                        else {
                            this.launch();
                        }
                    });
                    App.events.on('canvasPositionChange', () => {
                        this.adjustEditButton();
                    });
                    this.adjustEditButton();
                },
                adjustEditButton() {
                    const layer = this.__layer;
                    const editBtn = this.editBtn;
                    if(layer.config.showEditBtn == false) {
                        editBtn.hide();
                        return;
                    }
                    if(!editBtn) return;
                    editBtn.show();
                    const canvasEl = App.canvas.instance.wrapperEl.children[0];
                    const canvasBRect = canvasEl.getBoundingClientRect();
                    const bodyBRect = document.body.getBoundingClientRect();
                    const imageObj = layer.objects[1];
                    const textObjBRect = imageObj.getBoundingRect();
                    const imageObjScreenLeft = (textObjBRect.left + textObjBRect.width / 2) / App.canvas.instance.width * canvasBRect.width + canvasBRect.left - bodyBRect.left;
                    const imageObjScreenTop = (textObjBRect.top + textObjBRect.height / 2) / App.canvas.instance.height * canvasBRect.height + canvasBRect.top - bodyBRect.top;
                    editBtn.css('left', imageObjScreenLeft + 'px');
                    editBtn.css('top',  imageObjScreenTop   + 'px');
                    if(layer.objects[0]) {
                        editBtn.hide();
                    }
                    else {
                        editBtn.show();
                    }
                },
                init() {
                    this.__eventEmitter = new EventEmitter;
                    this.__eventEmitter.register('apply');
                    this.__eventEmitter.register('change_image');
                    App.events.on('beforeProductLoad', () => {
                        this.__eventEmitter.unbindAll();
                    });
                    return this;
                },
                clone() {
                    const newObj = {};
                    Object.assign(newObj, this);
                    newObj.init();
                    return newObj;
                }
            },
            __children: [],
            get __groupEl() {
                return $(`.group.gallery-group`);
            },
            newEditor() {
                const imgEditor = this.__prototype.clone();
                this.__children.push(imgEditor);
                imgEditor.__id = this.__children.length;
                return imgEditor;
            },
            onOpenGallery(callback) {
                this.__groupEl.on('click', '.open-gallery-btn', function() {
                    callback();
                });
            },
            setCollapse(value) {
                $('.group').toggleClass('active', false);
                this.__groupEl.toggleClass('active', !value);
                for (const group of $('.group:not(.active) .group-body.collapsible')) {
                    setCollapse(group, true);
                }
                setCollapse(this.__groupEl.find('.group-body')[0], value);
            },
            get(idx) {
                return this.__children[idx];
            },
            init() {
                // VueApp.$store.commit('showEditor', 'photos');
            }
        },
        texts: {
            __prototype: {
                __el: null,
                __layer: {},
                getIndex() {
                    return this.__el.index() - 3;
                },
                onTextChange(callback) {
                    const othis = this;
                    this.__el.on('change', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'change');
                    });
                    this.__el.on('keyup', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'keyup');
                    });
                },
                getTitle() {
                    return this.__el.find('label').text().replaceAll('*', '');
                },
                setTitle(title) {
                    this.__el.find('label').html(title);
                    return this;
                },
                setLayer(layer) {
                    this.__layer = layer;
                    return this;
                },
                setText(value) {
                    this.__el.find('textarea').val(value);
                    this.__el.find('textarea').trigger('change');
                    return this;
                },
                setPlaceholder(value) {
                    this.__el.find('textarea').attr('placeholder', value);
                    return this;
                },
                getValue() {
                    return this.__el.find('textarea').val();
                },
                focus() {
                    const index = this.__el.index() - 3;
                    App.editors.texts.setActive(index);
                    this.__el.find('textarea').focus();

                    App.editors.texts.launch();
                },
                adjustEditButton() {
                    const idx = this.getIndex();
                    const layer = App.layers.filter(l => l.type == 'text' && l.objects.length)[idx];
                    if(!layer) return;
                    const editBtn = this.editBtn;
                    if(layer.config.showEditBtn == false) {
                        editBtn.hide();
                        return;
                    }
                    if(!editBtn) return;
                    editBtn.show();
                    const canvasEl = App.canvas.instance.wrapperEl.children[0];
                    const canvasBRect = canvasEl.getBoundingClientRect();
                    const bodyBRect = document.body.getBoundingClientRect();
                    const textObj = layer.objects[0];
                    const textObjBRect = textObj.getBoundingRect();
                    const textObjScreenRight = (textObjBRect.left + textObjBRect.width) / App.canvas.instance.width * canvasBRect.width + canvasBRect.left - bodyBRect.left;
                    const textObjScreenTop = textObjBRect.top / App.canvas.instance.height * canvasBRect.height + canvasBRect.top - bodyBRect.top;
                    editBtn.css('left', textObjScreenRight + 'px');
                    editBtn.css('top',  textObjScreenTop   + 'px');
                },
                initEditButton() {
                    const idx = this.getIndex();
                    const editBtn = $(`
                        <div style="position:absolute;z-index:999;top:0;left:0;">
                            <button class="text-edit-btn">
                                <i class="fa fa-pen"></i>
                            </button>
                        </div>
                    `);
                    this.editBtn = editBtn;
                    $(document.body).append(editBtn);
                    editBtn.on('click', () => {
                        const layer = App.layers.filter(l => l.type == 'text')[idx];
                        if(layer.typeMetadata.module == 'song_title') {
                            App.editors.song.setCollapse(false);
                            // App.editors.song.__groupEl.find('input.song-title').focus();
                            App.editors.song.launch();
                        }
                        else {
                            App.editors.texts.setCollapse(false);
                            App.editors.texts.setActive(idx);
                            App.editors.texts.get(idx).focus();
                            App.editors.texts.launch();
                        }
                    });
                    App.events.on('canvasPositionChange', () => {
                        this.adjustEditButton();
                    });
                    this.adjustEditButton();
                },
                init() {
                    const html = `
                    <div class="text-form-group form-group flex-column">
                        <label style="padding-left:0!important;" class="old-text-label"></label>
                        <textarea class="form-control" rows="3" spellcheck="false"></textarea>
                    </div>`;
                    this.__el = $($.parseHTML(html));
                },
                clone() {
                    const newObj = {};
                    Object.assign(newObj, this);
                    newObj.init();
                    return newObj;
                }
            },
            __children: [],
            get __groupEl() {
                return $(`.group.text-group`);
            },
            newEditor() {
                const editor = this.__prototype.clone();
                this.__children.push(editor);
                this.__groupEl.find('.group-body').append(editor.__el);
                return editor;
            },
            setCollapse(value) {
                $('.group').toggleClass('active', false);
                this.__groupEl.toggleClass('active', !value);
                for (const group of $('.group:not(.active) .group-body')) {
                    setCollapse(group, true);
                }
                setCollapse(this.__groupEl.find('.group-body')[0], value);
            },
            calib() {
                $('textarea').trigger('change');
            },
            get(idx) {
                return this.__children[idx];
            },
            setActive(idx) {
                const editor = this.get(idx);
                const layer = App.layers.filter(l => l.type == 'text')[idx];
                if(layer && layer.typeMetadata.module == 'song_title') {
                    const nextIdx = (idx + 1) % this.__children.length;
                    if(nextIdx != idx) {
                        return this.setActive(nextIdx);
                    }
                    else {
                        VueApp.$store.commit('hideEditor', 'texts');
                    }
                }
                this.__groupEl.find('.text-edit-label .text-label').text(editor.getTitle());
                this.__groupEl.find('.text-form-group').toggleClass('active', false);
                this.__groupEl.find(`.text-form-group:eq(${idx})`).toggleClass('active', true);
                $('.text-switch-left').prop('disabled', false);
                $('.text-switch-right').prop('disabled', false);
                const self = this;
                function checkLeft() {
                    let leftIdx = idx - 1;
                    while(leftIdx >= 0) {
                        const layer = App.layers.filter(l => l.type == 'text')[leftIdx];
                        if(layer && layer.typeMetadata.module != 'song_title') return true;
                        leftIdx--;
                    }
                    return false;
                }
                function checkRight() {
                    let rightIdx = idx + 1;
                    while(rightIdx < self.__children.length) {
                        const layer = App.layers.filter(l => l.type == 'text')[rightIdx];
                        if(layer && layer.typeMetadata.module != 'song_title') return true;
                        rightIdx++;
                    }
                    return false;
                }
                if(!checkLeft()) {
                    $('.text-switch-left').prop('disabled', true);
                }
                if(!checkRight()) {
                    $('.text-switch-right').prop('disabled', true);
                }
                if(editor.__layer?.typeMetadata.required == 'true') {
                    this.__groupEl.find('.text-edit-label .required-star').show();
                }
                else {
                    this.__groupEl.find('.text-edit-label .required-star').hide();
                }
            },
            switchLeft() {
                const currentIdx = +this.__groupEl.find('.text-form-group.active').index() - 3;
                const leftIdx = (currentIdx + this.__children.length - 1) % this.__children.length
                const leftLayer = App.layers.filter(l => l.type == 'text' && l.objects.length)[leftIdx];
                if(leftLayer.typeMetadata.module != 'song_title') {
                    this.setActive(leftIdx);
                }
                else {
                    this.setActive(leftIdx - 1);
                }
            },
            switchRight() {
                const currentIdx = +this.__groupEl.find('.text-form-group.active').index() - 3;
                const rightIdx = (currentIdx + 1) % this.__children.length;
                const rightLayer = App.layers.filter(l => l.type == 'text' && l.objects.length)[rightIdx];
                if(rightLayer.typeMetadata.module != 'song_title') {
                    this.setActive(rightIdx);
                }
                else {
                    this.setActive(rightIdx + 1);
                }
                this.setActive(rightIdx);
            },
            initEditButtonsStyle() {
                if(!$('#edit-btn-style').length) {
                    $(document.body).append(`
                    <div id="edit-btn-style">
                        <style>
                            .text-edit-btn {
                                outline: none !important;
                                border: none;
                                border-radius: 50%;
                                width: 2rem;
                                height: 2rem;
                                background-color: #ffffff88;
                                display: flex;
                                justify-content: center;
                                align-items: center;
                            }
                            .text-edit-btn:hover {
                                background-color: white;
                            }
                            .text-edit-btn:active {
                                background-color: #ffffff88;
                            }
                        </style>
                    </div>
                    `);
                }
                $('.text-edit-btn').remove();
            },
            fetch() {
                if(this.__children.length) {
                    this.setActive(0);
                }
            },
            launch() {
                $('.text-edit-modal .clone-area').empty();
                const tabContainer = $('.text-group .text-tab-container').clone();
                tabContainer.toggleClass('clone', true);
                tabContainer.appendTo('.text-edit-modal .clone-area');
                tabContainer.find('.text-switch-left')
                    .toggleClass('text-switch-left', false)
                    .toggleClass('text-switch-left-clone', true);
                tabContainer.find('.text-switch-right')
                    .toggleClass('text-switch-right', false)
                    .toggleClass('text-switch-right-clone', true);
                tabContainer.find('.text-edit-label')
                    .toggleClass('text-edit-label', false)
                    .toggleClass('text-edit-label-clone', true);
                const activeTextarea = () => {
                    return $('.text-group .text-form-group.active textarea');
                };
                const updateTabContainer = () => {
                    tabContainer.find('.text-switch-left-clone')
                        .attr('disabled', $('.text-group .text-tab-container .text-switch-left').attr('disabled') == 'disabled');
                    tabContainer.find('.text-switch-right-clone')
                        .attr('disabled', $('.text-group .text-tab-container .text-switch-right').attr('disabled') == 'disabled');
                    tabContainer.find('.text-edit-label-clone').empty();
                    $('.text-group .text-edit-label').children().clone().appendTo(tabContainer.find('.text-edit-label-clone'));
                    $('.text-edit-modal textarea').val(activeTextarea().val());
                    $('.text-edit-modal textarea').attr('placeholder', activeTextarea().attr('placeholder') ?? '');
                };
                tabContainer.on('click', '.text-switch-right-clone', () => {
                    $('.text-group .text-tab-container .text-switch-right').click();
                    updateTabContainer();
                });
                tabContainer.on('click', '.text-switch-left-clone', () => {
                    $('.text-group .text-tab-container .text-switch-left').click();
                    updateTabContainer();
                });
                updateTabContainer();
                this.modal.launch();
            },
            close() {
                this.modal.close();
            },
            initModalUI() {
                $(document.body).append(`
                <style>
                    .text-tab-container {
                        display: none !important;
                    }
                    .text-tab-container.clone {
                        display: flex !important;
                    }
                    .text-form-group.active {
                        display: none !important;
                    }
                    .text-edit-label-clone {
                        display: flex;
                        align-items: center;
                        padding: 1rem;
                    }
                </style>
                `);
            },
            adjustDescription() {
                const canvasEl = App.canvas.instance.wrapperEl.children[0];
                const canvasBRect = canvasEl.getBoundingClientRect();
                const bodyBRect = document.body.getBoundingClientRect();
                const imageObj = App.layers[0].objects.filter(obj => !!obj)[0];
                if(!imageObj) return;
                const textObjBRect = imageObj.getBoundingRect();
                const imageObjScreenLeft = textObjBRect.left / App.canvas.instance.width * canvasBRect.width + canvasBRect.left - bodyBRect.left;
                const imageObjScreenTop = textObjBRect.top / App.canvas.instance.height * canvasBRect.height + canvasBRect.top - bodyBRect.top;
                const description = $('.edit-text-description');
                description.css('left', imageObjScreenLeft + 'px');
                description.css('top', imageObjScreenTop + textObjBRect.height / App.canvas.instance.height * canvasBRect.height + 20 + 'px');
                description.css('width', canvasBRect.width + 'px');
            },
            init() {
                if(!this.modal) {
                    this.modal = Modal($('.text-edit-modal'));
                    const activeTextarea = () => {
                        return $('.text-group .text-form-group.active textarea');
                    }
                    $('.text-edit-modal .apply-btn').on('click', () => {
                        App.editors.texts.close();
                    });
                    $('.text-edit-modal textarea').on('change', () => {
                        activeTextarea().val($('.text-edit-modal textarea').val());
                        activeTextarea().trigger('change');
                    });
                    $(this.modal.element).on('click', '.close-btn', () => this.modal.close());
                }
                if($('.edit-text-description').length == 0) {
                    $(document.body).append(`
                    <div class="edit-text-description">
                        <span>Click the pen icon to edit</span>
                    </div>
                    <style>
                        .edit-text-description {
                            position: absolute;
                            display: flex;
                            justify-content: center;
                        }
                    </style>
                    `);
                    setInterval(() => {
                        App.editors.texts.adjustDescription();
                    }, 300);
                }
                // VueApp.$store.commit('showEditor', 'texts');
                this.__groupEl.find('.group-body').empty();
                this.__groupEl.find('.group-body').append(`
                    <style>
                        .text-tab-container {
                            display:flex;
                            justify-content: space-between;
                            align-items:stretch;
                            height:3rem;
                            padding: 0 1rem;
                        }
                        .text-edit-label {
                            display: flex;
                            align-items: center;
                            padding: 0 .5rem;
                        }
                        .text-edit-label .text-label {
                            color: #4c596a;
                            vertical-align: middle;
                            max-width: 100%;
                            user-select: none;
                            white-space: nowrap;
                            overflow-x: hidden;
                            text-overflow: ellipsis;
                        }
                        .text-switch-btn {
                            outline: none !important;
                            border: none;
                            border-radius: .5rem;
                            background-color: #768d67;
                            color: white;
                            cursor: pointer;
                            padding: 0 1rem;
                        }
                        .text-switch-btn:active {
                            background-color: #666;
                        }
                        .text-switch-btn:disabled {
                            background-color: #aaa;
                        }
                        .old-text-label {
                            display: none !important;
                        }
                        .text-form-group {
                            display: none !important;
                        }
                        .text-form-group.active {
                            display: flex !important;
                        }
                    </style>
                    <div class="text-tab-container my-2">
                        <button class="text-switch-btn text-switch-left">
                            <i class="fa fa-chevron-left mr-1"></i> Prev
                        </button>
                        <div class="text-edit-label">
                            <span class="text-label"></span>
                            <span class="required-star" style="color:#e6a512;margin-left:.25rem;">*</span>
                        </div>
                        <button class="text-switch-btn text-switch-right">
                            Next <i class="fa fa-chevron-right ml-1"></i>
                        </button>
                    </div>
                    <button class="btn btn-secondary width-100 my-2 edit-text-btn" style="padding:1rem;">Edit text</button>
                `);
                this.__groupEl.find('.text-switch-left').on('click', () => {
                    this.switchLeft();
                });
                this.__groupEl.find('.text-switch-right').on('click', () => {
                    this.switchRight();
                });
                this.__groupEl.find('.edit-text-btn').on('click', () => {
                    this.launch();
                });
                this.initModalUI();
                App.events.on('beforeProductLoad', () => {
                    this.__children = [];
                });
            }
        },
        spiral: {
            child: {
                __el: null,
                onTextChange(callback) {
                    const othis = this;
                    this.__el.on('change', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'change');
                    });
                    this.__el.on('keyup', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'keyup');
                    });
                },
                setTitle(title) {
                    this.__el.find('label').html(title);
                    return this;
                },
                setLayer(layer) {
                    this.__layer = layer;
                    return this;
                },
                setText(value) {
                    this.__el.find('textarea').val(value);
                    this.__el.find('textarea').trigger('change');
                    return this;
                },
                setPlaceholder(value) {
                    this.__el.find('textarea').attr('placeholder', value);
                    return this;
                },
                getValue() {
                    return this.__el.find('textarea').val();
                },
                focus() {
                    this.__el.find('textarea').focus();
                },
                init() {
                    const html = `
                    <div class="form-group">
                        <label></label>
                        <textarea class="form-control" rows="1" spellcheck="false"></textarea>

                        <div class="song-search d-block mb-3">
                            <input type="text" class="form-control search-input" placeholder="Search song by name" spellcheck="false">
                            <div class="search-results collapsible mt-3"></div>
                        </div>
                        <label class="form-item-label">Lyrics</label>
                        <textarea class="form-control" rows="6"></textarea>
                        <div class="d-flex justify-content-end">
                            <button class="btn btn-secondary apply-btn">Apply</button>
                        </div>
                    </div>`;
                    this.__el = $(App.editors.spiral.__groupEl.find('.form-group'));
                    const rowsCalib = (el) => {
                        const protoEl = $('#fontSizeCalculationArea textarea:eq(0)');
                        protoEl.width(el.width());
                        protoEl.val(el.val());
                        el.height(protoEl[0].scrollHeight);
                    };
                    this.__el.on('change', 'textarea', function () {
                        rowsCalib($(this));
                    });
                    if(!App.isMobile) {
                        this.__el.on('input', 'textarea', function () {
                            rowsCalib($(this));
                        });
                    }
                }
            },
            get __groupEl() {
                return $(`.group.spiral-group`);
            },
            setCollapse(value) {
                $('.group').toggleClass('active', false);
                this.__groupEl.toggleClass('active', !value);
                for (const group of $('.group:not(.active) .group-body')) {
                    setCollapse(group, true);
                }
                setCollapse(this.__groupEl.find('.group-body')[0], value);
            },
            calib() {
                $('textarea').trigger('change');
            },
            init() {
                // this.__groupEl.toggleClass('hidden', false);
                this.child.init();
            }
        },
        square: {
            child: {
                __el: null,
                onTextChange(callback) {
                    const othis = this;
                    this.__el.on('change', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'change');
                    });
                    this.__el.on('keyup', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'keyup');
                    });
                },
                setTitle(title) {
                    this.__el.find('label').html(title);
                    return this;
                },
                setLayer(layer) {
                    this.__layer = layer;
                    return this;
                },
                setText(value) {
                    this.__el.find('textarea').val(value);
                    this.__el.find('textarea').trigger('change');
                    return this;
                },
                setPlaceholder(value) {
                    this.__el.find('textarea').attr('placeholder', value);
                    return this;
                },
                getValue() {
                    return this.__el.find('textarea').val();
                },
                focus() {
                    this.__el.find('textarea').focus();
                },
                init() {
                    const html = `
                    <div class="form-group">
                        <label></label>
                        <textarea class="form-control" rows="1" spellcheck="false"></textarea>
                    </div>`;
                    this.__el = $(App.editors.square.__groupEl.find('.form-group'));
                    const rowsCalib = (el) => {
                        const protoEl = $('#fontSizeCalculationArea textarea:eq(0)');
                        protoEl.width(el.width());
                        protoEl.val(el.val());
                        el.height(protoEl[0].scrollHeight);
                    };
                    this.__el.on('change', 'textarea', function () {
                        rowsCalib($(this));
                    });
                    if(!App.isMobile) {
                        this.__el.on('input', 'textarea', function () {
                            rowsCalib($(this));
                        });
                    }
                }
            },
            get __groupEl() {
                return $(`.group.square-group`);
            },
            setCollapse(value) {
                $('.group').toggleClass('active', false);
                this.__groupEl.toggleClass('active', !value);
                for (const group of $('.group:not(.active) .group-body')) {
                    setCollapse(group, true);
                }
                setCollapse(this.__groupEl.find('.group-body')[0], value);
            },
            calib() {
                $('textarea').trigger('change');
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);
                this.child.init();
            }
        },
        clipartText: {
            __layer: null,
            get __groupEl() {
                return $('.group.clipart-text-group');
            },
            onApply(callback) {
                this.__groupEl.on('click', '.apply-btn', () => {
                    const text = this.__groupEl.find('input').val();
                    callback && callback(text);
                });
            },
            onFocusOut(callback) {
                this.__groupEl.on('focusout', 'input', () => {
                    const text = this.__groupEl.find('input').val();
                    callback && callback(text);
                });
            },
            setPlacehoder(placeholder) {
                this.__groupEl.find('input').attr('placeholder', placeholder);
            },
            setText(text) {
                this.__groupEl.find('input').val(text);
            },
            setLayer(layer) {
                this.__layer = layer;
            },
            getText() {
                const rawText = this.__groupEl.find('input').val();
                // return rawText.split('').filter(char => {
                //     return !!this.__layer.config.images[char.toUpperCase()] || char == ' '
                // }).join('');
                return rawText.toUpperCase();
            },
            displayError(err) {
                if(!err) {
                    this.__groupEl.find('.error-message-wrapper').toggleClass('hidden', true);
                }
                else {
                    this.__groupEl.find('.error-message-wrapper').toggleClass('hidden', false);
                }
                this.__groupEl.find('.error-message').text(err);
            },
            adjustDescription() {
                const canvasEl = App.canvas.instance.wrapperEl.children[0];
                const canvasBRect = canvasEl.getBoundingClientRect();
                const bodyBRect = document.body.getBoundingClientRect();
                const imageObj = App.layers[0].objects.filter(obj => !!obj)[0];
                if(!imageObj) return;
                const textObjBRect = imageObj.getBoundingRect();
                const imageObjScreenLeft = textObjBRect.left / App.canvas.instance.width * canvasBRect.width + canvasBRect.left - bodyBRect.left;
                const imageObjScreenTop = textObjBRect.top / App.canvas.instance.height * canvasBRect.height + canvasBRect.top - bodyBRect.top;
                const description = $('.clipart-text-description');
                description.css('left', imageObjScreenLeft + 'px');
                description.css('top', imageObjScreenTop + textObjBRect.height / App.canvas.instance.height * canvasBRect.height + 20 + 'px');
                description.css('width', canvasBRect.width + 'px');
            },
            init() {
                if($('.clipart-text-description').length == 0) {
                    $(document.body).append(`
                    <div class="clipart-text-description">
                        <span>Click on any letter to change the letter image</span>
                    </div>
                    <style>
                        .clipart-text-description {
                            position: absolute;
                            display: flex;
                            justify-content: center;
                        }
                    </style>
                    `);
                    setInterval(() => {
                        App.editors.clipartText.adjustDescription();
                    }, 300);
                }
                this.__groupEl.toggleClass('hidden', false);
            }
        },
        photoText: {
            __prototype: {
                __imgData: null,
                __layer: {},
                __eventEmitter: null,
                onApply(callback) {
                    this.__eventEmitter.on('apply', () => {
                        callback.call(this);
                    });
                    return this;
                },
                onChangeImage(callback) {
                    this.__eventEmitter.on('change_image', () => {
                        callback.call(this);
                    });
                },
                apply() {
                    this.__cropData = this.cropper.export();
                    this.__eventEmitter.emit('apply');
                },
                getCropboxData() {
                    return this.__cropData;
                },
                setLayer(layer) {
                    this.__layer = layer;
                    return this;
                },
                setIndex(index) {
                    this.__index = index;
                },
                setImage(b64) {
                    this.__imgData = b64;
                    return this;
                },
                launch() {
                    const othis = this;
                    if(!$(`#editorImage_${this.__id}`).length) {
                        const modal = Modal($(`
                        <div id="editorImage_${this.__id}" style="user-select:none;">
                            <div class="modal-header">
                                <h5 class="modal-title">Image Editor</h5>
                                <button type="button" class="close close-btn" style="outline:none; color:#000000;">
                                    <span aria-hidden="true" class="fa fa-times"></span>
                                </button>
                            </div>
                            <div class="d-flex justify-content-center flex-wrap">
                                <div class="cropper-container" style="display:flex;justify-content:center;align-items:center;padding:2rem;">
                                    <div class="cropper"></div>
                                </div>
                                <div style="width:100%;padding:0 2rem;">
                                    <input class="zoom-input" type="range" min="0" max="100" value="0">
                                </div>
                                <div style="width:100%;padding:0 2rem;display:flex;justify-content:space-between;flex-wrap:wrap;">
                                    <button class="btn mt-3 rotate-left-btn">
                                        <i class="fa fa-undo"></i>
                                    </button>
                                    <button class="btn mt-3 rotate-right-btn">
                                        <i class="fa fa-redo"></i>
                                    </button>
                                    <button class="btn mt-3 flip-x-btn">Flip X</button>
                                    <button class="btn mt-3 flip-y-btn">Flip Y</button>
                                </div>
                            </div>
                            <div class="d-flex" style="margin: 1rem">
                                <div style="flex:1;padding:1rem">
                                    <button class="btn btn-secondary change-image-btn width-100">Change image</button>
                                </div>
                                <div style="flex:1;padding:1rem">
                                    <button class="btn btn-primary apply-btn width-100">Apply</button>
                                </div>
                            </div>
                        </div>
                        `));
                        this.modal = modal;
                        $(document).on('click', `#editorImage_${this.__id} .close-btn`, function() {
                            othis.close();
                        });
                        $(document).on('click', `#editorImage_${this.__id} .apply-btn`, function() {
                            othis.apply();
                        });
                        $(document).on('click', `#editorImage_${this.__id} .change-image-btn`, function() {
                            othis.__eventEmitter.emit('change_image');
                        });
                        $(document).on('input', `#editorImage_${this.__id} .zoom-input`, function() {
                            if(!othis.cropper) return;
                            setTimeout(() => {
                                othis.cropper.zoom = Math.pow(1.1, Math.max(0, 0 + (+$(this).val()) / 10));
                            });
                        });
                        $(document).on('click', `#editorImage_${this.__id} .rotate-left-btn`, function() {
                            if(!othis.cropper) return;
                            othis.cropper.rotation = (360 + othis.cropper.rotation - 90 * othis.cropper.scaleX * othis.cropper.scaleY) % 360;
                            othis.cropper.adjust();
                        });
                        $(document).on('click', `#editorImage_${this.__id} .rotate-right-btn`, function() {
                            if(!othis.cropper) return;
                            othis.cropper.rotation = (360 + othis.cropper.rotation + 90 * othis.cropper.scaleX * othis.cropper.scaleY) % 360;
                            othis.cropper.adjust();
                        });
                        $(document).on('click', `#editorImage_${this.__id} .flip-x-btn`, function() {
                            if(!othis.cropper) return;
                            othis.cropper.scaleX = -othis.cropper.scaleX;
                            othis.cropper.adjust();
                        });
                        $(document).on('click', `#editorImage_${this.__id} .flip-y-btn`, function() {
                            if(!othis.cropper) return;
                            othis.cropper.scaleY = -othis.cropper.scaleY;
                            othis.cropper.adjust();
                        });
                    }
                    if(!this.cropper) {
                        this.cropper = Cropper({
                            element: $(`#editorImage_${this.__id} .cropper`)
                        });
                    }
                    this.cropper.imageSrc = this.__imgData;
                    this.modal.launch();
                    setTimeout(function() {
                        const idx = othis.__index;
                        const layer = othis.__layer;
                        const charConfig = othis.__layer.value.characters[idx].config;
                        const ratio = charConfig.width / charConfig.height;
                        othis.cropper.width = ratio * 100;
                        const imageWidth = othis.cropper.imageBoundingRect.width;
                        const imageHeight = othis.cropper.imageBoundingRect.height;
                        const defaultCropboxData = App.helper.calcDefaultCropboxData(
                            imageWidth, imageHeight, ratio
                        );
                        const restoreData = othis.cropper.export();
                        restoreData.cropX = layer.data['cropboxX_'+idx] ?? defaultCropboxData.x;
                        restoreData.cropY = layer.data['cropboxY_'+idx] ?? defaultCropboxData.y;
                        othis.cropper.import(restoreData, () => {
                            othis.cropper.adjust();
                            if(othis.cropper.path != othis.__layer['svgPath_'+othis.__index]) {
                                othis.cropper.path = othis.__layer['svgPath_'+othis.__index];
                            }
                        });
                    }, 200);
                    return this;
                },
                close() {
                    this.modal.close();
                    return this;
                },
                destroy() {
                    this.modal && this.modal.destroy();
                },
                initEditButton() {
                    if(!$('#image-edit-style').length) {
                        $(document.body).append(`
                        <div id="image-edit-style">
                            <style>
                                .photo-edit-btn {
                                    outline: none !important;
                                    border: none;
                                    border-radius: 50%;
                                    width: 3rem;
                                    height: 3rem;
                                    font-size: 1.5rem;
                                    background-color: #ffffff88;
                                    display: flex;
                                    justify-content: center;
                                    align-items: center;
                                    transform: translate(-50%, -50%);
                                    pointer-events: auto;
                                }
                                .photo-edit-btn:hover {
                                    background-color: white;
                                }
                                .photo-edit-btn:active {
                                    background-color: #ffffff88;
                                }
                            </style>
                        </div>
                        `);
                    }
                    const layer = this.__layer;
                    if(!layer) return;
                    const imageObj = layer.objects[this.__index * 2 + 1];
                    if(!imageObj) return;
                    const editBtn = $(`
                        <div style="position:absolute;z-index:999;top:0;left:0;pointer-events:none;">
                            <button class="photo-text-fn photo-edit-btn">
                                <i class="fa fa-image"></i>
                            </button>
                        </div>
                    `);
                    this.editBtn = editBtn;
                    $(document.body).append(editBtn);
                    editBtn.on('click', () => {
                        const selected = !!layer.objects[this.__index * 2];
                        if(!selected) {
                            App.editors.imageGallery.unbindAll();
                            App.editors.imageGallery.onImageSelect(file => {
                                file.onThumbnailReady(() => {
                                    layer.files[this.__index] = file;
                                    this.setImage(file.displayURL);
                                    App.editors.imageGallery.close();
                                    this.launch();
                                });
                            });
                            App.editors.imageGallery.launch();
                        }
                        else {
                            this.launch();
                        }
                    });
                    App.events.on('canvasPositionChange', () => {
                        this.adjustEditButton();
                    });
                    this.adjustEditButton();
                },
                adjustEditButton() {
                    const layer = this.__layer;
                    const editBtn = this.editBtn;
                    if(layer.config.showEditBtn == false) {
                        editBtn.hide();
                        return;
                    }
                    if(!editBtn) return;
                    editBtn.show();
                    const canvasEl = App.canvas.instance.wrapperEl.children[0];
                    const canvasBRect = canvasEl.getBoundingClientRect();
                    const bodyBRect = document.body.getBoundingClientRect();
                    const imageObj = layer.objects[this.__index * 2 + 1];
                    if(!imageObj) return;
                    const textObjBRect = imageObj.getBoundingRect();
                    const imageObjScreenLeft = (textObjBRect.left + textObjBRect.width / 2) / App.canvas.instance.width * canvasBRect.width + canvasBRect.left - bodyBRect.left;
                    const imageObjScreenTop = (textObjBRect.top + textObjBRect.height / 2) / App.canvas.instance.height * canvasBRect.height + canvasBRect.top - bodyBRect.top;
                    editBtn.css('left', imageObjScreenLeft + 'px');
                    editBtn.css('top',  imageObjScreenTop   + 'px');
                    if(layer.objects[this.__index * 2]) {
                        editBtn.hide();
                    }
                    else {
                        editBtn.show();
                    }
                },
                init() {
                    this.__eventEmitter = new EventEmitter;
                    this.__eventEmitter.register('apply');
                    this.__eventEmitter.register('change_image');
                    App.events.on('beforeProductLoad', () => {
                        this.__eventEmitter.unbindAll();
                    });
                    return this;
                },
                clone() {
                    const newObj = {};
                    Object.assign(newObj, this);
                    newObj.init();
                    return newObj;
                }
            },
            __children: [],
            initEditor(i) {
                const imgEditor = this.__prototype.clone();
                this.__children[i] = imgEditor;
                imgEditor.__id = uuidv4();
                return imgEditor;
            },
            setCollapse(value) {
                $('.group').toggleClass('active', false);
                this.__groupEl.toggleClass('active', !value);
                for (const group of $('.group:not(.active) .group-body.collapsible')) {
                    setCollapse(group, true);
                }
                setCollapse(this.__groupEl.find('.group-body')[0], value);
            },
            get(idx) {
                return this.__children[idx];
            },
            __layer: null,
            get __groupEl() {
                return $('.group.photo-text-group');
            },
            onApply(callback) {
                this.__groupEl.on('click', '.apply-btn', () => {
                    const text = this.__groupEl.find('input').val();
                    callback && callback(text);
                });
            },
            setPlacehoder(placeholder) {
                this.__groupEl.find('input').attr('placeholder', placeholder);
            },
            setText(text) {
                this.__groupEl.find('input').val(text);
            },
            setLayer(layer) {
                this.__layer = layer;
            },
            getText() {
                const rawText = this.__groupEl.find('input').val();
                // return rawText.split('').filter(char => {
                //     return !!this.__layer.config.images[char.toUpperCase()] || char == ' '
                // }).join('');
                return rawText.toUpperCase();
            },
            displayError(err) {
                if(!err) {
                    this.__groupEl.find('.error-message-wrapper').toggleClass('hidden', true);
                }
                else {
                    this.__groupEl.find('.error-message-wrapper').toggleClass('hidden', false);
                }
                this.__groupEl.find('.error-message').text(err);
            },
            reset() {
                this.displayError('');
                for(const child of this.__children) {
                    child && child.destroy();
                }
                this.__children = [];
                $('.photo-text-fn.photo-edit-btn').remove();
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);
            }
        },
        backgroundText: {
            get __groupEl() {
                return $(`.group.background-text-group`);
            },
            onTextChange(callback) {
                const othis = this;
                this.__groupEl.on('change', 'textarea', function () {
                    callback.call(othis, $(this).val(), 'change');
                });
                if(!App.isMobile) {
                    this.__groupEl.on('input', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'input');
                    });
                }
            },
            setTitle(title) {
                this.__groupEl.find('label').html(title);
                return this;
            },
            setText(value) {
                this.__groupEl.find('textarea').val(value);
                this.__groupEl.find('textarea').trigger('change');
                return this;
            },
            setPlaceholder(value) {
                this.__groupEl.find('textarea').attr('placeholder', value);
                return this;
            },
            setCollapse(value) {
                $('.group').toggleClass('active', false);
                this.__groupEl.toggleClass('active', !value);
                for (const group of $('.group:not(.active) .group-body')) {
                    setCollapse(group, true);
                }
                setCollapse(this.__groupEl.find('.group-body')[0], value);
            },
            focus() {
                this.__groupEl.find('textarea').focus();
            },
            calib() {
                $('textarea').trigger('input');
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);
                const rowsCalib = (el) => {
                    const protoEl = $('#fontSizeCalculationArea textarea:eq(0)');
                    protoEl.width(el.width());
                    protoEl.val(el.val());
                    el.height(protoEl[0].scrollHeight);
                };
                this.__groupEl.on('change', 'textarea', function () {
                    // rowsCalib($(this));
                });
                if(!App.isMobile) {
                    this.__groupEl.on('input', 'textarea', function () {
                        // rowsCalib($(this));
                    });
                }
            }
        },
        maps: {
            __prototype2: classFactory({
                get groupEl() {
                    return $(`.group.map-group`);
                },
                setLayer(layerData) {
                    this.__layer = layerData;
                },
                export(callback) {
                    const maskInfo = this.data.mask.config;
                    const maskRatio = maskInfo.width / maskInfo.height;
                    const actualPixelRatio = window.devicePixelRatio;
                    // const dpi = 4 * App.document.resolution * App.document.width / 4096
                        // * (this.__layer.config.width / App.document.width);
                    // const dpi = actualPixelRatio * 3 * 96;
                    const dpi = actualPixelRatio * 96;
                    Object.defineProperty(window, 'devicePixelRatio', {
                        get: function() {return dpi / 96}
                    });
                    let width = this.modal.el.find('.editor-map').width();
                    if(width <= 0) {
                        width = window.screen.width / 2
                    }
                    const height = width / maskRatio;
                    var hidden = document.createElement('div');
                    hidden.className = 'hidden-map';
                    document.body.appendChild(hidden);
                    var container = document.createElement('div');
                    container.style.width   = width + 'px';
                    container.style.height  = height + 'px';
                    hidden.appendChild(container);
                    const map = this.map;
                    var zoom = map.getZoom();
                    var center = map.getCenter();
                    var bearing = map.getBearing();
                    var pitch = map.getPitch();
                    const mapStyle = App.helper.getMapStyleInfo();
                    var renderMap = new mapboxgl.Map({
                        container: container,
                        center: center,
                        zoom: zoom,
                        style: mapStyle.styleUrl,
                        bearing: bearing,
                        pitch: pitch,
                        interactive: false,
                        preserveDrawingBuffer: true,
                        fadeDuration: 0,
                        attributionControl: false
                    });

                    renderMap.once('idle', () => {
                        const base64 = renderMap.getCanvas().toDataURL();
                        const m = $(this.marker.getElement());
                        const editor = m.closest('.editor-map');
                        const markerAlignment = {
                            left    : m.position().left / editor.width(),
                            top     : m.position().top  / editor.height(),
                            width   : m.width()         / editor.width(),
                            height  : m.height()        / editor.height()
                        };

                        renderMap.remove();
                        hidden.parentNode.removeChild(hidden);
                        Object.defineProperty(window, 'devicePixelRatio', {
                            get: function() {return actualPixelRatio}
                        });

                        const lastSelectedPosition = JSON.parse(this.geocoder.lastSelected);

                        callback({
                            'map': base64,
                            'location': lastSelectedPosition ? lastSelectedPosition.text : 'Kansas City',
                            'marker': markerAlignment
                        });
                    });
                },
                initMap() {
                    const editor = this;
                    const maskInfo = this.data.mask.config;
                    const markerInfo = this.data.marker.config;
                    const markerIconURL = `${App.productRoot}/${App.productName}/` + markerInfo.image;
                    const el = editor.modal.el.find('.editor-map');
                    const config = {
                        "mapOptions": {
                            "zoom": 10,
                            "styles": "ckjmh73zj1vdp19o2782cxn27",
                            "places": null
                        },
                        "markerLocation": {
                            "lat": null,
                            "lng": null
                        }
                    };

                    // map container calibration
                    const maskRatio = maskInfo.width / maskInfo.height;
                    el.height(el.width() / maskRatio);

                    const mapStyle = App.helper.getMapStyleInfo();
                    mapboxgl.accessToken = mapStyle.accessToken;
                    const map = new mapboxgl.Map({
                        container: el[0],
                        style: mapStyle.styleUrl,
                        center: [ // starting position
                            App.editors.maps.__prevLocation.lng,
                            App.editors.maps.__prevLocation.lat,
                        ],
                        zoom: 9,                    // starting zoom
                        bearing: 0,
                        pitch: 0,
                        preserveDrawingBuffer: true
                    });
                    this.map = map;

                    const geocoder = new MapboxGeocoder({ // Initialize the geocoder
                        accessToken: mapboxgl.accessToken, // Set the access token
                        mapboxgl: mapboxgl, // Set the mapbox-gl instance
                        marker: false, // Do not use the default marker style,
                        flyTo: { duration: 0 }
                    });
                      
                    // Add the geocoder to the map
                    map.addControl(geocoder);
                    editor.geocoder = geocoder;

                    map.addControl(new mapboxgl.NavigationControl());

                    // const input = editor.modal.el.find('.place-input')[0];
                    // const autocomplete = new google.maps.places.Autocomplete(input);
                    // autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
                    // autocomplete.addListener("place_changed", () => {
                    //     const place = autocomplete.getPlace();
                    //     map.setCenter({
                    //         lng: place.geometry.location.lng(),
                    //         lat: place.geometry.location.lat()
                    //     });
                    // });

                    editor.modal.on('shown', () => {
                        setTimeout(() => {
                            map.resize();
                        }, 200);
                    });

                    editor.map = map;

                    const markerEl = $(document.createElement('div'));            
                    const markerWidthRatio  = markerInfo.width  / maskInfo.width;
                    const markerHeightRatio = markerInfo.height / maskInfo.height;
                    const markerWidth   = el.width()    * markerWidthRatio;
                    const markerHeight  = el.height()   * markerHeightRatio;
                    console.log(markerWidth);

                    markerEl.css('background-image', `url('${encodeURI(markerIconURL)}')`);
                    console.log(markerEl);
                    markerEl.css('background-size', '100%');
                    markerEl.css('width', markerWidth+'px');
                    markerEl.css('height', markerHeight+'px');
                    const markerOptions = {
                        offset: [
                            el.width() * ((markerInfo.left + markerInfo.width) / App.document.width - 0.5),
                            el.height() * ((markerInfo.top + markerInfo.height * 1.5) / App.document.height - 0.5),
                        ],
                        anchor: 'center',
                        draggable: true
                    }
                    if(this.data.marker.typeMetadata.customPosition != 'true') {
                        delete markerOptions.offset;
                    }
                    const marker = new mapboxgl.Marker(markerEl[0], markerOptions);

                    marker.setLngLat([
                        config.markerLocation.lng, config.markerLocation.lat
                    ])
                    .addTo(map);

                    editor.marker = marker;
                    marker.setLngLat(map.getCenter());

                    map.on('moveend', function() {
                        const editorEl = $(marker._element).closest('.editor-map');
                        const markerPos = marker._pos;
                        if(markerPos.x < 0 || markerPos.x > editorEl.width()
                        || markerPos.y < 0 || markerPos.y > editorEl.height() )
                            marker.setLngLat(map.getCenter());
                    });

                    $('.mapboxgl-ctrl-bottom-left').remove();
                    $('.mapboxgl-ctrl-bottom-right').remove();
                },
                init(data) {
                    this.data = data;
                    const editor = this;
                    const self = this;
                    this.openButton = $($.parseHTML(`
                    <button class="btn btn-secondary open-map-btn">Edit map ${$('.open-map-btn').length + 1}</button>`));
                    this.openButton.css('width', '100%');

                    App.editors.maps.__groupEl.find('.group-body').append(this.openButton);
                    if(this.openButton.index() > 0) {
                        this.openButton.css('margin-top', '1em');
                    }
                    this.modal = new (classFactory({
                        get el() {
                            return $(`#rep-modal-${this.__id}`);
                        },
                        launch() {
                            this.modal.launch();
                            this.__eventEmitter.emit('show');
                            this.__eventEmitter.emit('shown');
                        },
                        close() {
                            this.modal.close();
                            this.__eventEmitter.emit('hide');
                            this.__eventEmitter.emit('hidden');
                        },
                        on(eventName, callback) {
                            this.__eventEmitter.on(eventName, callback);
                        },
                        init(data) {
                            this.__eventEmitter = new EventEmitter;
                            this.__eventEmitter.register('initialized');
                            this.__eventEmitter.register('show');
                            this.__eventEmitter.register('shown');
                            this.__eventEmitter.register('hide');
                            this.__eventEmitter.register('hidden');
                            this.__eventEmitter.register('apply');
                            App.events.on('beforeProductLoad', () => {
                                this.__eventEmitter.unbindAll();
                            });
                            let mapMaskOverlayHTML = `
                                <div class="map-mask-overlay" style="position:absolute;z-index:1">
                                    <svg viewBox="-10 -10 120 120" style="max-width: 100%;max-height: 100%;">
                                        <defs>
                                            <mask id="myMask">
                                                <!-- Everything under a white pixel will be visible -->
                                                <rect x="0" y="0" width="100" height="100" fill="white" />

                                                <!-- Everything under a black pixel will be invisible -->
                                                <path  fill="black"
                                            d="M1584.811,0.113 C1589.748,2.380 1592.020,11.212 1594.853,15.866 C1600.573,25.268 1606.310,34.966 1612.260,44.634 C1638.859,87.869 1664.451,131.947 1691.260,175.462 C1768.727,301.199 1844.684,428.835 1921.567,554.929 C1945.341,593.923 1967.252,633.195 1991.194,672.058 C2073.070,804.958 2152.217,939.749 2233.552,1073.444 C2388.355,1327.905 2541.788,1584.239 2696.842,1838.544 C2733.672,1898.947 2768.535,1959.623 2805.301,2020.058 C2869.034,2124.820 2932.946,2230.449 2996.776,2335.140 C3022.359,2377.099 3046.000,2419.354 3071.760,2461.172 C3100.205,2507.347 3127.747,2554.243 3156.117,2600.904 C3163.648,2613.290 3171.148,2625.859 3178.880,2638.576 C3182.909,2645.205 3189.626,2652.230 3192.269,2659.810 C2128.324,2659.810 1064.059,2659.810 0.114,2659.810 C0.114,2659.354 0.114,2658.897 0.114,2658.441 C6.284,2653.673 9.304,2643.348 13.503,2636.522 C23.028,2621.037 32.002,2605.091 41.622,2589.259 C73.675,2536.508 104.412,2482.723 136.691,2429.664 C223.253,2287.374 307.077,2142.390 393.777,2000.194 C418.711,1959.301 441.191,1917.821 466.083,1876.901 C660.910,1556.636 848.040,1230.484 1043.188,910.424 C1053.601,893.346 1062.254,876.135 1072.645,859.052 C1117.845,784.750 1161.192,709.047 1206.546,634.385 C1270.390,529.280 1332.673,422.231 1396.682,317.248 C1418.436,281.568 1437.922,245.423 1459.615,209.710 C1488.484,162.180 1516.216,113.713 1545.310,65.868 C1558.058,44.903 1574.219,22.454 1584.811,0.113 Z" />
                                            </mask>
                                        </defs>

                                        <rect x="0" y="0" width="100" height="100" fill="white" mask="url(#myMask)" />
                                    </svg>
                                </div>
                            `;
                            mapMaskOverlayHTML = '';
                            const modal = Modal($(`
                            <div class="editor-map-modal" id="rep-modal-${this.__id}">
                                <div class="modal-header">
                                    <h5 class="modal-title">Map Editor</h5>
                                    <button type="button" class="close close-btn" style="outline:none; color:#000000;">
                                        <span aria-hidden="true" class="fa fa-times"></span>
                                    </button>
                                </div>
                                <div class="d-flex justify-content-center flex-wrap" style="padding: 2rem;">
                                    <div class="form-group">
                                        <!-- <input type="text" class="form-control place-input" placeholder="Input your place"> -->
                                    </div>
                                    <div class="d-flex justify-content-center map-container">
                                        <div class="editor-map">
                                            ${mapMaskOverlayHTML}
                                        </div>
                                    </div>
                                    <button class="apply-btn btn btn-primary width-100 mt-2" 
                                        style="padding:1rem 0;">
                                        <i class="fa fa-check"></i> Apply
                                    </button>
                                </div>
                            </div>
                            `));
                            this.modal = modal;
                            $(this.modal.element).on('click', '.close-btn', () => this.close());
                            $(document).on('click', `#rep-modal-${this.__id} .apply-btn`, () => {
                                this.__eventEmitter.emit('apply');
                            });
                        }
                    }));
                    this.openButton.on('click', () => {
                        this.modal.launch();
                    });

                    if(true) {
                        const maskInfo = data.mask.config;

                        // aka init for the first time
                        const afterModalShown = () => {
                            if(editor.modal.el.find('.mapboxgl-canvas').length) return;
                            self.initMap();
                            self.modal.__eventEmitter.emit('initialized');
                        };

                        editor.modal.on('shown', () => {
                            const el = editor.modal.el.find('.editor-map');
                            el.height(el.width() / maskInfo.width * maskInfo.height);
                            console.log(el);
                            const maxHeight = $(window).height() * 0.7;
                            if(el.height() > maxHeight) {
                                const scaleDownRatio = maxHeight / el.height();
                                el.height(el.height()   * scaleDownRatio);
                                el.width(el.width()     * scaleDownRatio);
                            }
                            afterModalShown();
                            if(!editor.__lastLocation) { editor.__lastLocation = editor.map.getCenter(); }
                            editor.map.setCenter(editor.__lastLocation);
                        });
                    }
                }
            }),
            __children: [],
            __prevLocation: {
                lng: -94.58703058563479,
                lat: 39.13043540145199
            },
            get __groupEl() {
                return $(`.group.map-group`);
            },
            newEditor(data) {
                const editor = new this.__prototype2(data);
                this.__children.push(editor);
                this.__groupEl.find('.group-body').append(editor.__el);
                return editor;
            },
            setCollapse(value) {
                $('.group').toggleClass('active', false);
                this.__groupEl.toggleClass('active', !value);
                for (const group of $('.group:not(.active) .group-body')) {
                    setCollapse(group, true);
                }
                setCollapse(this.__groupEl.find('.group-body')[0], value);
            },
            get(idx) {
                return this.__children[idx];
            },
            init() {
                this.__children = [];
                VueApp.$store.commit('showEditor', 'maps');
            }
        },
        starmaps: {
            __prototype: classFactory({
                get groupEl() {
                    return $(`.group.map-group`);
                },
                setLayer(layerData) {
                    this.__layer = layerData;
                },
                init() {
                    const editor = this;
                    VueApp.$store.commit('showEditor', 'starmap');

                    this.modal = new (classFactory({
                        get el() {
                            return $(`#rep-modal-${this.__id}`);
                        },
                        get inputInfo() {
                            const date = this.el.find('.date-input').val();
                            let formattedDate = '2021/05/17';
                            if(date) {
                                const dateTimestamp = new Date(date);
                                const prefix0 = function(str) {
                                    return ('0' + str).slice(-2);
                                };
                                const dd = prefix0(dateTimestamp.getDate());
                                const mm = prefix0(dateTimestamp.getMonth() + 1);
                                const yyyy = dateTimestamp.getYear() + 1900;
                                formattedDate = `${yyyy}/${mm}/${dd}`;
                            }
                            const time = this.el.find('.time-input').val() + ':00';
                            const place = this.autocomplete.getPlace();
                            return {
                                place,
                                date: formattedDate,
                                time,
                                timestamp: + new Date(date)
                            };
                        },
                        set inputInfo({lat, lng, date, time}={}) {
                            const splittedDate = date.split('/');
                            const yyyy = splittedDate[0];
                            const mm = splittedDate[1];
                            const dd = splittedDate[2];
                            const d = new Date;
                            d.setDate(+dd);
                            d.setMonth(+mm-1);
                            d.setYear(+yyyy);
                            this.el.find('.time-input').val(time.slice(0,-3));
                            // incomplete
                        },
                        launch() {
                            this.setErrorMessage('');
                            this.modal.launch();
                            this.__eventEmitter.emit('show');
                            this.__eventEmitter.emit('shown');
                        },
                        close() {
                            this.modal.close();
                            this.__eventEmitter.emit('hide');
                            this.__eventEmitter.emit('hidden');
                        },
                        on(eventName, callback) {
                            this.__eventEmitter.on(eventName, callback);
                        },
                        onApply(callback) {
                            this.__eventEmitter.on('apply', () => {
                                if(!this.el.find('.place-input').val()) {
                                    this.setErrorMessage('Place is required');
                                }
                                else if(!this.el.find('.date-input').val()) {
                                    this.setErrorMessage('Please fill Date & time');
                                }
                                else {
                                    this.setErrorMessage('');
                                    callback(this.inputInfo);
                                }
                            });
                        },
                        setErrorMessage(msg) {
                            if(!msg) {
                                this.el.find('.error-message-wrapper').toggleClass('d-inline-block', false);
                                this.el.find('.error-message-wrapper').toggleClass('d-none', true);
                            }
                            else {
                                this.el.find('.error-message-wrapper').toggleClass('d-inline-block', true);
                                this.el.find('.error-message-wrapper').toggleClass('d-none', false);
                                this.el.find('.error-message').text(msg);
                            }
                        },
                        init() {
                            this.__eventEmitter = new EventEmitter;
                            this.__eventEmitter.register('apply');
                            this.__eventEmitter.register('show');
                            this.__eventEmitter.register('shown');
                            this.__eventEmitter.register('hide');
                            this.__eventEmitter.register('hidden');
                            const modalID = `rep-modal-${this.__id}`;
                            const modal = Modal($(`
                            <div class="editor-starmap-modal" id="${modalID}">
                                <div class="modal-header">
                                    <h5 class="modal-title">Starmap Editor</h5>
                                    <button type="button" class="close close-btn" style="outline:none; color:#000000;">
                                        <span aria-hidden="true" class="fa fa-times"></span>
                                    </button>
                                </div>
                                <div style="padding: 2rem;">
                                    <div class="form-group">
                                        <label>Place</label>
                                        <input type="text" class="form-control place-input">
                                    </div>
                                    <div class="form-group">
                                        <label>Date & time</label>
                                        <div class="row">
                                            <div class="col-md-6 mb-3">
                                                <input type="date" class="form-control date-input">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="time" class="form-control time-input" value="17:00">
                                            </div>
                                        </div>
                                    </div>
                                    <span class="d-inline-block text-danger error-message-wrapper mb-3">
                                        <i class="fa fa-exclamation-triangle"></i>
                                        &nbsp;
                                        <span class="error-message"></span>
                                    </span>
                                    <button class="btn btn-primary width-100 apply-btn">Apply</button>
                                </div>
                            </div>
                            `));
                            this.modal = modal;
                            $(this.modal.element).on('click', '.close-btn', () => this.close());
                            $(document).on('click', `#rep-modal-${this.__id} .apply-btn`, () => {
                                this.__eventEmitter.emit('apply');
                            });

                            this.eventEmitter = new EventEmitter;
                            const input = $(`#rep-modal-${this.__id} .place-input`)[0];
                            const autocomplete = new google.maps.places.Autocomplete(input);
                            this.autocomplete = autocomplete;
                            autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
                            autocomplete.addListener("place_changed", () => {
                                const place = autocomplete.getPlace();
                                this.eventEmitter.emit('placeChanged', place);
                            });
                            this.setErrorMessage('');
                        }
                    }));
                }
            }),
            __children: [],
            get __groupEl() {
                return $(`.group.starmap-group`);
            },
            onOpenStarmap(name, callback) {
                this.__groupEl.on('click', '.open-starmap-btn', function() {
                    if($(this).attr('data-attr').trim() == name) {
                        callback($(this).attr('data-attr'));
                    }
                });
            },
            generateAction(data){
                $('.group.starmap-group').find('.group-body').append(`
                    <button class="btn btn-secondary width-100 mt-2 open-starmap-btn" data-attr="${data}" style="padding:1rem;">
                        Edit Starmap ${$('.open-starmap-btn').length + +1}
                    </button>`);
            },
            newEditor() {
                const editor = new this.__prototype();
                this.__children.push(editor);
                this.__groupEl.find('.group-body').append(editor.__el);
                return editor;
            },
            setCollapse(value) {
                $('.group').toggleClass('active', false);
                this.__groupEl.toggleClass('active', !value);
                for (const group of $('.group:not(.active) .group-body')) {
                    setCollapse(group, true);
                }
                setCollapse(this.__groupEl.find('.group-body')[0], value);
            },
            get(idx) {
                return this.__children[idx];
            },
            reset() {
                this.__children = [];
                $('.open-starmap-btn').remove();
                $('.editor-starmap-modal').remove();
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);
            }
        },
        moonphases: {
            __prototype: classFactory({
                get groupEl() {
                    return $(`.group.moon-group`);
                },
                setLayer(layerData) {
                    this.__layer = layerData;
                },
                init() {
                    const editor = this;
                    VueApp.$store.commit('showEditor', 'moon');

                    this.modal = new (classFactory({
                        get el() {
                            return $(`#rep-modal-${this.__id}`);
                        },
                        get moonphase() {
                            const date = this.el.find('.date-input').val();
                            const d = date ? new Date(date) : new Date();
                            d.setHours(19, 0, 0, 0);
                            const m = new MoonPhase(d);
                            return m.phase;
                        },
                        get inputInfo() {
                            const date = this.el.find('.date-input').val();
                            let formattedDate = '2021/05/17';
                            if(date) {
                                const dateTimestamp = date ? new Date(date) : new Date();
                                const prefix0 = function(str) {
                                    return ('0' + str).slice(-2);
                                };
                                const dd = prefix0(dateTimestamp.getDate());
                                const mm = prefix0(dateTimestamp.getMonth() + 1);
                                const yyyy = dateTimestamp.getYear() + 1900;
                                formattedDate = `${yyyy}/${mm}/${dd}`;
                            }
                            return {
                                date: formattedDate,
                            };
                        },
                        launch() {
                            this.modal.launch();
                            this.__eventEmitter.emit('show');
                            this.__eventEmitter.emit('shown');
                        },
                        close() {
                            this.modal.close();
                            this.__eventEmitter.emit('hide');
                            this.__eventEmitter.emit('hidden');
                        },
                        on(eventName, callback) {
                            this.__eventEmitter.on(eventName, callback);
                        },
                        onApply(callback) {
                            this.__eventEmitter.on('apply', () => {
                                callback(this.moonphase);
                            });
                        },
                        init() {
                            this.__eventEmitter = new EventEmitter;
                            this.__eventEmitter.register('apply');
                            this.__eventEmitter.register('show');
                            this.__eventEmitter.register('shown');
                            this.__eventEmitter.register('hide');
                            this.__eventEmitter.register('hidden');
                            const modalID = `rep-modal-${this.__id}`;
                            this.modal = Modal($(`
                            <div class="editor-moon-modal" id="${modalID}" style="min-width:50vw">
                                <div class="modal-header">
                                    <h5 class="modal-title">Moon Editor</h5>
                                    <button type="button" class="close close-btn" style="outline:none; color:#000000;">
                                        <span aria-hidden="true" class="fa fa-times"></span>
                                    </button>
                                </div>
                                <div style="padding: 2rem;">
                                    <div class="form-group mb-3">
                                        <label>Date</label>
                                        <input type="date" class="form-control date-input">
                                    </div>
                                    <button class="btn btn-primary width-100 apply-btn">Apply</button>
                                </div>
                            </div>
                            `));
                            $(this.modal.element).on('click', '.close-btn', () => this.close());
                            $(document).on('click', `#rep-modal-${this.__id} .apply-btn`, () => {
                                this.__eventEmitter.emit('apply');
                            });
                        }
                    }));
                }
            }),
            __children: [],
            get __groupEl() {
                return $(`.group.moon-group`);
            },
            onOpenMoon(name, callback) {
                this.__groupEl.on('click', '.open-moon-btn', function() {
                    if($(this).attr('data-attr').trim() == name) {
                        callback($(this).attr('data-attr'));
                    }
                });
            },
            generateAction(data) {
                $('.group.moon-group').find('.group-body').append(`
                    <button class="btn btn-secondary width-100 mt-2 open-moon-btn" data-attr="${data}" style="padding:1rem;">
                        Edit Moon ${$('.open-moon-btn').length + +1}
                    </button>`);
            },
            newEditor() {
                const editor = new this.__prototype();
                this.__children.push(editor);
                this.__groupEl.find('.group-body').append(editor.__el);
                return editor;
            },
            setCollapse(value) {
                $('.group').toggleClass('active', false);
                this.__groupEl.toggleClass('active', !value);
                for (const group of $('.group:not(.active) .group-body')) {
                    setCollapse(group, true);
                }
                setCollapse(this.__groupEl.find('.group-body')[0], value);
            },
            get(idx) {
                return this.__children[idx];
            },
            reset() {
                this.__children = [];
                $('.open-moon-btn').remove();
                $('.editor-moon-modal').remove();
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);
            }
        },
        song: {
            __el: null,
            onSearchInputChange(callback) {
                let lastChanged = 0;
                const self = this;
                const groupEl = App.editors.song.__groupEl;
                groupEl.on('change input', '.search-input', function () {
                    lastChanged = (new Date).getTime();
                    setTimeout(() => {
                        if((new Date).getTime() - lastChanged < 500) return;
                        callback(
                            groupEl.find('.search-input').val()
                        );
                    }, 500);
                });
            },
            onSearchResultSelect(callback) {
                App.events.one('importLyrics', ({uri, title, artist}) => {
                    callback({uri, title, artist});
                }, { timeTravel: true });
                const groupEl = App.editors.song.__groupEl;
                groupEl.on('click', '.select-song-btn', function() {
                    const selected = $('.song-edit-modal .search-result.selected');
                    if(!selected.length) return;
                    const data = {
                        uri: selected.attr('data-uri'),
                        title: selected.find('.song-name').text(),
                        artist: selected.find('.artist').text()
                    };
                    callback(data);
                });
            },
            onTitleChange(callback) {
                const othis = this;
                this.__groupEl.find('input.song-title').on('change', function() {
                    callback.call(othis, $(this).val(), 'change');
                });
                this.__groupEl.find('input.song-title').on('input', function() {
                    callback.call(othis, $(this).val(), 'input');
                });
            },
            onTextChange(callback) {
                const othis = this;
                this.__el.on('change', '.song-edit-container textarea', function () {
                    callback.call(othis, $(this).val(), 'change');
                });
                this.__el.on('keyup', '.song-edit-container textarea', function () {
                    callback.call(othis, $(this).val(), 'keyup');
                });
            },
            switchSearchMode() {
                const groupEl = this.__groupEl;
                groupEl.find('.back-btn').css('display', 'inline');
                groupEl.find('.search-lyrics-btn').css('display', 'none');
                groupEl.find('.song-search').css('display', 'block');
                groupEl.find('.song-edit-container').css('display', 'none');
                $('.song-edit-modal .search-lyrics-btn-wrapper').css('display', 'none');
            },
            switchEditMode() {
                const groupEl = this.__groupEl;
                groupEl.find('.back-btn').css('display', 'none');
                groupEl.find('.search-lyrics-btn').css('display', 'inline');
                groupEl.find('.song-search').css('display', 'none');
                groupEl.find('.song-edit-container').css('display', 'block');
                $('.song-edit-modal .search-lyrics-btn-wrapper').css('display', 'block');
            },
            setSongTitle(text) {
                const groupEl = this.__groupEl;
                groupEl.find('input.song-title').val(text);
            },
            setSearchResults(data) {
                const groupEl = this.__groupEl;
                groupEl.find('.search-results').html(data.map(item => `
                    <div class="search-result" data-uri="${item.uri}">
                        <span class="artist">${item.artist}</span><br>
                        <span class="song-name">${item.title}</span>
                    </div>
                `).join(''));
                this.events && this.events.emit('setSearchResults');
            },
            get __groupEl() {
                return $(`.group.song-group`);
            },
            setCollapse(value) {
                $('.group').toggleClass('active', false);
                this.__groupEl.toggleClass('active', !value);
                for (const group of $('.group:not(.active) .group-body')) {
                    setCollapse(group, true);
                }
                setCollapse(this.__groupEl.find('.group-body')[0], value);
            },
            onApply(callback) {
                this.__groupEl.on('click', '.apply-btn', () => {
                    callback(this.__groupEl.find('textarea').val());
                });
            },
            getLyrics() {
                return this.__groupEl.find('textarea').val();
            },
            setLyrics(text) {
                this.__groupEl.find('textarea').val(text);
                this.events && this.events.emit('setLyrics');
            },
            focus() {
                this.__groupEl.find('textarea').focus();
            },
            launch() {
                const updateModal = () => {
                    $('.song-edit-modal .song-title').val($('.song-group .song-title').val());
                    $('.song-edit-modal textarea').val($('.song-group textarea').val());
                    $('.song-edit-modal .back-btn').css('display', $('.song-group .back-btn').css('display') ?? '');
                    $('.song-edit-modal .search-lyrics-btn').css('display', $('.song-group .search-lyrics-btn').css('display') ?? '');
                    $('.song-edit-modal .song-search').css('display', $('.song-group .song-search').css('display') ?? '');
                    $('.song-edit-modal .song-edit-container').css('display', $('.song-group .song-edit-container').css('display') ?? '');
                };
                if(!$('.song-edit-modal').length) {
                    this.events = new EventEmitter;
                    this.events.register('setSearchResults');
                    this.events.register('setLyrics');
                    this.modal = Modal($(`
                    <div class="song-edit-modal" style="min-width:50vw">
                        <div class="modal-header">
                            <h5 class="modal-title">Edit song</h5>
                            <button type="button" class="close close-btn" style="outline:none; color:#000000;">
                                <span aria-hidden="true" class="fa fa-times"></span>
                            </button>
                        </div>
                        <div style="padding: 2rem;">
                            <div class="mb-3" style="display: flex; justify-content: space-between;">
                                <div>
                                    <a class="back-btn" href="javascript:;" style="display:none; color: #4d88a6;">
                                        <i class="fa fa-chevron-left"></i> Back
                                    </a>
                                </div>
                                <div class="search-lyrics-btn-wrapper" style="background: #94a4aa;padding: 1rem;">
                                    <a  class="search-lyrics-btn" href="javascript:;" style="color: #4d88a6; color: white;">Search lyrics</a>
                                </div>
                            </div>
                            <div class="song-search mb-3" style="display: none;">
                                <input type="text" class="form-control search-input" placeholder="Search song by name" spellcheck="false">
                                <div class="search-results collapsible mt-3"></div>
                                <button class="btn btn-primary select-song-btn width-100 mt-3">Select song</button>
                            </div>
                            <div class="song-edit-container">
                                <label class="form-item-label">Title</label>
                                <input type="text" class="form-control song-title mb-2" spellcheck="false">
                                <label class="form-item-label">Lyrics</label>
                                <textarea class="form-control" rows="6"></textarea>
                                <div class="d-flex justify-content-end mt-3 width-100">
                                    <button class="btn btn-primary apply-btn width-100">
                                        <i class="fa fa-check"></i> Preview
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    `));
                    $(this.modal.element).on('click', '.close-btn', () => this.modal.close());
                    $('.song-edit-modal .back-btn').on('click', () => {
                        $('.song-group .back-btn').click();
                        setTimeout(() => {
                            updateModal();
                        });
                    });
                    $('.song-edit-modal .search-lyrics-btn').on('click', () => {
                        $('.song-group .search-lyrics-btn').click();
                        setTimeout(() => {
                            updateModal();
                        });
                    });
                    $('.song-edit-modal .search-input').on('input', () => {
                        $('.song-group .search-input').val($('.song-edit-modal .search-input').val());
                        $('.song-group .search-input').trigger('input');
                    });
                    this.events.on('setSearchResults', () => {
                        $('.song-edit-modal .search-results').empty();
                        $('.song-group .search-results').children().clone().appendTo('.song-edit-modal .search-results');
                    });
                    $('.song-edit-modal').on('click', '.search-result', e => {
                        const idx = $(e.target).index();
                        $(`.song-group .search-results .search-result:eq(${idx})`).click();
                        setTimeout(() => { updateModal(); })
                    });
                    $('.song-edit-modal').on('input', '.song-title', e => {
                        $('.song-group .song-title').val($('.song-edit-modal .song-title').val());
                        $('.song-group .song-title').trigger('input');
                    });
                    $('.song-edit-modal').on('input', 'textarea', e => {
                        $('.song-group textarea').val($('.song-edit-modal textarea').val());
                    });
                    $('.song-edit-modal').on('click', '.apply-btn', e => {
                        $('.song-group .apply-btn').click();
                        this.close();
                    });
                    $('.song-edit-modal').on('click', '.search-result', function() {
                        $('.search-result').toggleClass('selected', false);
                        $(this).toggleClass('selected', true);
                    });
                    $('.song-edit-modal').on('click', '.select-song-btn', function() {
                        $('.song-group .select-song-btn').click();
                    });
                    this.events.on('setLyrics', () => {
                        updateModal();
                    });
                }
                updateModal();
                this.modal.launch();
            },
            close() {
                this.modal.close();
            },
            init() {
                // this.__groupEl.toggleClass('hidden', false);
                this.__el = this.__groupEl;
                if(!this.initialized) {
                    this.initialized = true;
                    this.__el.on('click', '.search-lyrics-btn', () => {
                        this.switchSearchMode();
                    });
                    this.__el.on('click', '.back-btn', () => {
                        this.switchEditMode();
                    });
                }
            }
        },
        spotify: {
            child: {
                __el: null,
                onSearchInputChange(callback) {
                    let lastChanged = 0;
                    const self = this;
                    const groupEl = App.editors.spotify.__groupEl;
                    groupEl.on('change input', '.search-input', function () {
                        lastChanged = (new Date).getTime();
                        setTimeout(() => {
                            if((new Date).getTime() - lastChanged < 500) return;
                            callback(
                                groupEl.find('.search-input').val()
                            );
                        }, 500);
                    });
                },
                onSearchResultSelect(callback) {
                    const groupEl = App.editors.spotify.__groupEl;
                    groupEl.on('click', '.search-result', function() {
                        callback($(this).attr('data-uri'));
                        $('.search-result').toggleClass('selected', false);
                        $(this).toggleClass('selected', true);
                    });
                },
                onTextChange(callback) {
                    const othis = this;
                    this.__el.on('change', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'change');
                    });
                    this.__el.on('keyup', 'textarea', function () {
                        callback.call(othis, $(this).val(), 'keyup');
                    });
                },
                setSearchResults(data) {
                    const groupEl = App.editors.spotify.__groupEl;
                    groupEl.find('.search-results').html(data.tracks.items.map(item => `
                        <div class="search-result" data-uri="${item.uri}">
                            <span class="artist">${item.artists[0].name}</span><br>
                            <span class="song-name">${item.name}</span>
                        </div>
                    `).join(''));
                },
                setTitle(title) {
                    this.__el.find('label').html(title);
                    return this;
                },
                setLayer(layer) {
                    this.__layer = layer;
                    return this;
                },
                setText(value) {
                    this.__el.find('textarea').val(value);
                    this.__el.find('textarea').trigger('change');
                    return this;
                },
                setPlaceholder(value) {
                    this.__el.find('textarea').attr('placeholder', value);
                    return this;
                },
                getValue() {
                    return this.__el.find('textarea').val();
                },
                focus() {
                    App.editors.spotify.__groupEl.find('.search-input').focus();
                },
                init() {
                    const html = `
                    <div class="form-group">
                        <label></label>
                        <textarea class="form-control" rows="1" spellcheck="false"></textarea>
                    </div>`;
                    this.__el = $(App.editors.spotify.__groupEl.find('.form-group'));
                    const rowsCalib = (el) => {
                        const protoEl = $('#fontSizeCalculationArea textarea:eq(0)');
                        protoEl.width(el.width());
                        protoEl.val(el.val());
                        el.height(protoEl[0].scrollHeight);
                    };
                    this.__el.on('change', 'textarea', function () {
                        rowsCalib($(this));
                    });
                    if(!App.isMobile) {
                        this.__el.on('input', 'textarea', function () {
                            rowsCalib($(this));
                        });
                    }
                    if(App.mode('debug')) {
                        $('.spotify-uri').toggleClass('d-none', false);
                    }
                }
            },
            get __groupEl() {
                return $(`.group.spotify-group`);
            },
            setCollapse(value) {
                $('.group').toggleClass('active', false);
                this.__groupEl.toggleClass('active', !value);
                for (const group of $('.group:not(.active) .group-body')) {
                    setCollapse(group, true);
                }
                setCollapse(this.__groupEl.find('.group-body')[0], value);
            },
            calib() {
                $('textarea').trigger('change');
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);
                this.child.init();
            }
        },
        theme: {
            onChooseTheme(callback) {
                this.eventEmitter.on('themeChoose', callback);
            },
            get groupEl() {
                return $('.group.theme-group');
            },
            get selectedID() {
                const selectedOption = this.groupEl.find('.theme-option.selected');
                if(selectedOption.length == 0) return -1;
                const id = selectedOption.closest('.theme-option-wrapper').index();
                return id;
            },
            get selectedFilter() {
                const themeID = this.selectedID;
                if(themeID == -1) return null;
                return this.filters[themeID];
            },
            get selectedFilterName() {
                const filterNames = [
                    '',
                    'BlackWhite',
                    'Grayscale',
                    'Vintage',
                    'Bright',
                    'Technicolor',
                    'Polaroid',
                    'Brownie',
                ];
                const themeID = this.selectedID;
                if(themeID == -1) return null;
                return filterNames[themeID];
            },
            init() {
                const filters = [
                    null,
                    fabric.Image.filters.BlackWhite,
                    fabric.Image.filters.Grayscale,
                    fabric.Image.filters.Vintage,
                    fabric.Image.filters.Kodachrome,
                    fabric.Image.filters.Technicolor,
                    fabric.Image.filters.Polaroid,
                    fabric.Image.filters.Brownie,
                ];
                this.filters = filters;
                const self = this;
                this.eventEmitter = new EventEmitter;
                this.eventEmitter.register('themeChoose');
                App.events.on('beforeProductLoad', () => {
                    this.eventEmitter.unbindAll();
                });
                this.groupEl.on('click', '.theme-option', function() {
                    $('.theme-group .theme-option').toggleClass('selected', false);
                    $(this).toggleClass('selected', true);
                    self.eventEmitter.emit('themeChoose', self.selectedFilterName);
                });
                this.initialized = true;
                // VueApp.$store.commit('showEditor', 'theme');
            }
        },
        __devProto: classFactory({
            get __groupEl() {
                return $(`.group.dev-group`).closest('.group');
            },
            init() {
                this.__groupEl.toggleClass('hidden', false);

                const layout = new Shitonen({
                    html: `
                        <div>
                            <div class="layers">
                                {{ layers }}
                            </div>
                            <div class="width-100 mt-3 pt-2 d-flex" style="border-top: 1px solid #aaa;">
                                <button class="upload-btn btn btn-secondary font-weight-normal">
                                    <i class="fa fa-upload mr-2"></i>
                                    Upload
                                </button>
                                <input type="text" class="form-control" value="{{result}}" style="flex:1;line-height:1.5;display:inline-block;height:4rem;box-shadow:none;">
                            </div>
                        </div>
                        `,
                    shit: {
                        layers: [],
                        result: ''
                    },
                    shitConfig: {
                        layers: { escape: false }
                    },
                    init() {
                        this.query('.upload-btn').onclick = async() => {
                            this.shit.result = 'Uploading, please wait hehe...';
                            const r = await App.helper.uploadBlobAWS(App.helper.toBlob(await App.canvas.exportBase64()));
                            this.shit.result = r.data;
                        };
                    }
                });
                const layers = App.layers.map(layer => {
                    if(['map'].includes(layer.type)) return null;
                    return new Shitonen({
                        html: `
                            <div class="subgroup">
                                <div class="subgroup-title">
                                    <i class="fa fa-{{icon}}"></i>
                                    <span class="font-weight-normal">{{title}} </span>
                                    <span class="font-weight-normal">{{status}}</span>
                                </div>
                                <div class="subgroup-body collapsible" data-collapsed="true" style="height: 0px;">
                                    <div class="content-wrapper" style="margin: .25rem 1.5rem;">
                                        <div class="d-flex align-items-stretch mt-2">
                                            <button class="btn btn-secondary visible-toggle" style="border:1px solid #4c596a33 !important;border-right: none !important;width:4rem;">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                            <div class="opacity-wrapper d-flex align-items-center" style="flex: 1;background-color: #ffffffe6;border: 1px solid #4c596a33;border-left: none;padding-right:1.25em;">
                                                <span class="opacity-value-text mr-2" style="width: 2rem">${(layer.data.opacity ?? layer.config.opacity) * 100}</span>
                                                <input class="ranger" type="range" min="0" max="100" value="${(layer.data.opacity ?? layer.config.opacity) * 100}">
                                            </div>
                                        </div>
                                        <div class="d-flex align-items-stretch mt-2">
                                            <button class="btn btn-secondary" style="line-height:1;border:1px solid #4c596a33 !important;border-right: none !important;width:4rem;">
                                                X
                                            </button>
                                            <div class="left-wrapper d-flex align-items-center" style="flex: 1;background-color: #ffffffe6;border: 1px solid #4c596a33;border-left: none;padding-right:1.25em;">
                                                <span class="mr-2" style="width: 2rem">--</span>
                                                <input class="ranger" type="range" min="-20" max="120" value="${(layer.data.left ?? layer.config.left) / App.document.width * 100}">
                                            </div>
                                        </div>
                                        <div class="d-flex align-items-stretch mt-2">
                                            <button class="btn btn-secondary" style="line-height:1;border:1px solid #4c596a33 !important;border-right: none !important;width:4rem;">
                                                Y
                                            </button>
                                            <div class="top-wrapper d-flex align-items-center" style="flex: 1;background-color: #ffffffe6;border: 1px solid #4c596a33;border-left: none;padding-right:1.25em;">
                                                <span class="mr-2" style="width: 2rem">--</span>
                                                <input class="ranger" type="range" min="-20" max="110" value="${(layer.data.top ?? layer.config.top) / App.document.height * 100}">
                                            </div>
                                        </div>
                                        <div class="d-flex align-items-stretch mt-2">
                                            <button class="btn btn-secondary" style="text-transform:none;line-height:1;border:1px solid #4c596a33 !important;border-right: none !important;width:4rem;">
                                                Aa
                                            </button>
                                            <div class="font-size-wrapper d-flex align-items-center" style="flex: 1;background-color: #ffffffe6;border: 1px solid #4c596a33;border-left: none;padding-right:1.25em;">
                                                <span class="mr-2" style="width: 2rem">--</span>
                                                <input class="ranger" type="range" min="0" max="100" value="{{initialFontSizeLevel}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`,
                        shit: {
                            data: layer,
                            icon: 'question',
                            status: '',
                            title: '',
                            initialFontSizeLevel: 100,
                        },
                        init() {
                            const iconmap = {
                                'fg'        : 'image',
                                'text'      : 'font',
                                'mask'      : 'mask',
                                'map-mask'  : 'map',
                                'marker'    : 'map-marker-alt',
                                'bgcolor'   : 'palette',
                            };
                            this.shit.icon = iconmap[layer.type] ?? 'question';
                            this.shit.title = `#${layer.id} ${layer.type}`;
                            if(layer.type == 'text') {
                                this.shit.title += ' - ' + layer.typeMetadata.label;
                            }
                            this.query('.visible-toggle').onclick = e => {
                                const el = $(this.query('.visible-toggle'));
                                layer.data.visible = !layer.data.visible;
                                App.canvas.refresh();
                                if(!layer.data.visible) {
                                    el.find('i').attr('class', 'fa fa-eye-slash');
                                    this.shit.status = '(hidden)';
                                }
                                else {
                                    el.find('i').attr('class', 'fa fa-eye');
                                    this.shit.status = '';
                                }
                            };
                            const updateOpacity = e => {
                                layer.data.opacity = (+this.query('.opacity-wrapper input').value) / 100;
                                this.query('.opacity-wrapper .opacity-value-text').textContent = this.query('.opacity-wrapper input').value;
                            };
                            const updateLeft = e => {
                                layer.data.left = (+this.query('.left-wrapper input').value) / 100 * App.document.width;
                                App.canvas.refresh();
                            };
                            const updateTop = e => {
                                layer.data.top = (+this.query('.top-wrapper input').value) / 100 * App.document.height;
                                App.canvas.refresh();
                            };
                            this.query('.opacity-wrapper input').onchange = updateOpacity;
                            this.query('.opacity-wrapper input').oninput = updateOpacity;
                            this.query('.left-wrapper input').onchange = updateLeft;
                            this.query('.left-wrapper input').oninput = updateLeft;
                            this.query('.top-wrapper input').onchange = updateTop;
                            this.query('.top-wrapper input').oninput = updateTop;
                            if(layer.type == 'text') {
                            console.log((layer.data.left ?? layer.config.left) / App.document.width * 100);
                                const baseFontSize = App.document.width / 200;
                                const updateFontSize = e => {
                                    const el = $(this.query('.font-size-wrapper input'));
                                    layer.data.fontSize = baseFontSize * Math.pow(1.04, +el.val());
                                    layer.data.textValue = layer.data.textValue;
                                    App.canvas.refresh();
                                };
                                this.query('.font-size-wrapper input').onchange = updateFontSize;
                                this.query('.font-size-wrapper input').oninput = updateFontSize;
                                this.shit.initialFontSizeLevel = Math.logBase(layer.config.text.font.sizes[0] / baseFontSize, 1.04);
                            }
                            else {
                                $(this.query('.font-size-wrapper')).parent().toggleClass('d-flex', false);
                                $(this.query('.font-size-wrapper')).parent().toggleClass('d-none', true);
                            }
                        }
                    });
                }).filter(v => (v ?? null) != null);
                layout.shit.layers = layers;

                $('#dev-tools-wrapper').html('');
                $('#dev-tools-wrapper').append(layout.element);
            }
        }),
        init() {
            this.imageGallery.init();
            if(App.mode('debug')) this.dev = new this.__devProto;
        }
    },
    helper: {
        __private: {},
        async resume() {
            const messageID = uuidv4();
            VueApp.$store.commit('setCanvasLoaderMessage', [messageID, 'Restoring your data ...']);
            const resumeData = JSON.parse(window.localStorage.getItem('resume_' + App.productName));
            await App.canvas.import(resumeData, { resume: true });
            VueApp.$store.commit('setCanvasLoaderMessage', [messageID]);
        },
        rng: {
            sfc32(a, b, c, d) {
                return function() {
                  a >>>= 0; b >>>= 0; c >>>= 0; d >>>= 0; 
                  var t = (a + b) | 0;
                  a = b ^ b >>> 9;
                  b = c + (c << 3) | 0;
                  c = (c << 21 | c >>> 11);
                  d = d + 1 | 0;
                  t = t + d | 0;
                  c = c + t | 0;
                  return (t >>> 0) / 4294967296;
                }
            },
            mulberry32(a) {
                return function() {
                  var t = a += 0x6D2B79F5;
                  t = Math.imul(t ^ t >>> 15, t | 1);
                  t ^= t + Math.imul(t ^ t >>> 7, t | 61);
                  return ((t ^ t >>> 14) >>> 0) / 4294967296;
                }
            },
            xoshiro128ss(a, b, c, d) {
                return function() {
                    var t = b << 9, r = a * 5; r = (r << 7 | r >>> 25) * 9;
                    c ^= a; d ^= b;
                    b ^= c; a ^= d; c ^= t;
                    d = d << 11 | d >>> 21;
                    return (r >>> 0) / 4294967296;
                }
            }
        },
        imgResize,
        imgResizeToBlob,
        toBlob,
        async fulfill(data) {
            await App.canvas.import(data);
            return await App.helper.uploadBlobAWS(App.helper.toBlob(await App.canvas.exportBase64()));
        },
        async createWatermark(text) {
            const origCanvas = App.document;
            const wmCanvas = document.createElement("canvas");
            wmCanvas.width = origCanvas.width;
            wmCanvas.height = origCanvas.height;

            const wmContext=wmCanvas.getContext('2d');
            wmContext.globalAlpha=1;

            const fontSize = origCanvas.width / 5;

            wmContext.font = `${fontSize}px Arial`;
            wmContext.strokeStyle = "#2c2c2c11";
            wmContext.fillStyle = "#ffffff33";
            wmContext.lineWidth = fontSize / 40;

            const metrics = wmContext.measureText(text);
            const width = metrics.width;
            const height = 72;

            wmContext.translate(origCanvas.width/2, origCanvas.height/2);
            wmContext.rotate(-Math.atan(origCanvas.height/origCanvas.width));
            wmContext.strokeText(text,-width/2,height/2);
            wmContext.fillText(text,-width/2,height/2);

            return wmCanvas.toDataURL();
        },
        async createBlobURL(url) {
            if(url.indexOf('blob') == 0) return url;
            return URL.createObjectURL(await fetch(url).then(r => r.blob()))
        },
        checkEmptyText() {
            let ok = true;
            for(const layer of App.layers) {
                if(layer.typeMetadata.required == 'true' && layer.type == 'text' && !layer.data.textChanged) {
                    const editor = layer.editors[0];
                    if(!editor) continue;
                    if(editor.getValue && editor.getValue()) continue;
                    ok = false;
                    Toastify({
                        text: `Please fill required fields.`,
                        duration: 3000,
                        close: true,
                        gravity: "top", // `top` or `bottom`
                        position: "left", // `left`, `center` or `right`
                        backgroundColor: "#bd2300",
                        stopOnFocus: true, // Prevents dismissing of toast on hover
                        onClick: function(){} // Callback after click
                    }).showToast();
                    App.editors.texts.setCollapse(false);
                    layer.editors[0].focus();
                    break;
                }
            }
            return ok;
        },
        applyCropboxData(layer) {
            const img = layer.objects[0];
            const mask = layer.objects[1];
            const maskRatio = (mask.width * mask.scaleX) / (mask.height * mask.scaleY);
            const defaultCropboxData = App.helper.calcDefaultCropboxData(img.width, img.height, maskRatio);
            const maskWidth = mask.width * mask.scaleX;
            const maskHeight = mask.height * mask.scaleY;
            const cropX = layer.data['cropboxX'] ?? defaultCropboxData.x;
            const cropY = layer.data['cropboxY'] ?? defaultCropboxData.y;
            const cropSpanX = layer.data['cropboxSpanX'] ?? defaultCropboxData.spanX;
            const cropSpanY = layer.data['cropboxSpanY'] ?? defaultCropboxData.spanY;
            const cropScaleX = layer.data['cropboxScaleX'] ?? defaultCropboxData.scaleX;
            const cropScaleY = layer.data['cropboxScaleY'] ?? defaultCropboxData.scaleY;
            const cropRotation = layer.data['cropboxRotation'] ?? defaultCropboxData.rotation;
            const rotatedAngle = (360 + cropRotation) % 360;
            const isRotatedRightAngle = rotatedAngle == 90 || rotatedAngle == 270;
            const scaledImgWidth = isRotatedRightAngle ? maskHeight / cropSpanY : maskWidth / cropSpanX;
            const scaledImgHeight = isRotatedRightAngle ? maskWidth / cropSpanX : maskHeight / cropSpanY;
            img.rotate(rotatedAngle * cropScaleX * cropScaleY);
            img.set({
                flipX: cropScaleX == -1,
                flipY: cropScaleY == -1,
                scaleX: scaledImgWidth / img.width,
                scaleY: scaledImgHeight / img.height
            });
            const imgBRect = img.getBoundingRect(false, true);
            const imgOffsetX = - cropX * maskWidth / cropSpanX - imgBRect.left;
            const imgOffsetY = - cropY * maskHeight / cropSpanY - imgBRect.top;
            img.set({
                left: mask.left + img.left + imgOffsetX,
                top: mask.top + img.top + imgOffsetY,
            });
        },
        applyCropboxData_forPhotoText(layer, idx) {
            const img = layer.objects[idx * 2];
            const mask = layer.objects[idx * 2 + 1];
            const maskRatio = (mask.width * mask.scaleX) / (mask.height * mask.scaleY);
            const defaultCropboxData = App.helper.calcDefaultCropboxData(img.width, img.height, maskRatio);
            const maskWidth = mask.width * mask.scaleX;
            const maskHeight = mask.height * mask.scaleY;
            const cropX = layer.data['cropboxX_'+idx] ?? defaultCropboxData.x;
            const cropY = layer.data['cropboxY_'+idx] ?? defaultCropboxData.y;
            const cropSpanX = layer.data['cropboxSpanX_'+idx] ?? defaultCropboxData.spanX;
            const cropSpanY = layer.data['cropboxSpanY_'+idx] ?? defaultCropboxData.spanY;
            const cropScaleX = layer.data['cropboxScaleX_'+idx] ?? defaultCropboxData.scaleX;
            const cropScaleY = layer.data['cropboxScaleY_'+idx] ?? defaultCropboxData.scaleY;
            const cropRotation = layer.data['cropboxRotation_'+idx] ?? defaultCropboxData.rotation;
            const rotatedAngle = (360 + cropRotation) % 360;
            const isRotatedRightAngle = rotatedAngle == 90 || rotatedAngle == 270;
            const scaledImgWidth = isRotatedRightAngle ? maskHeight / cropSpanY : maskWidth / cropSpanX;
            const scaledImgHeight = isRotatedRightAngle ? maskWidth / cropSpanX : maskHeight / cropSpanY;
            img.rotate(rotatedAngle * cropScaleX * cropScaleY);
            img.set({
                flipX: cropScaleX == -1,
                flipY: cropScaleY == -1,
                scaleX: scaledImgWidth / img.width,
                scaleY: scaledImgHeight / img.height
            });
            const imgBRect = img.getBoundingRect(false, true);
            const imgOffsetX = - cropX * maskWidth / cropSpanX - imgBRect.left;
            const imgOffsetY = - cropY * maskHeight / cropSpanY - imgBRect.top;
            img.set({
                left: mask.left + img.left + imgOffsetX,
                top: mask.top + img.top + imgOffsetY,
            });
        },
        calcDefaultCropboxData(imageWidth, imageHeight, ratio) {
            const result = {
                x: null,
                y: null,
                spanX: null,
                spanY: null,
                scaleX: 1,
                scaleY: 1,
                rotation: 0
            };

            const imageRatio = imageWidth / imageHeight;
            if(imageRatio >= ratio) {
                result.y        = 0;
                result.spanY    = 1;
                result.spanX    = imageHeight   * ratio / imageWidth;
                result.x        = 0.5 - result.spanX / 2;
            }
            else {
                result.x        = 0;
                result.spanX    = 1;
                result.spanY    = imageWidth    / ratio / imageHeight;
                result.y        = 0.5 - result.spanY / 2;
            }

            return result;
        },
        scrollTop() {
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        },
        layerQuery(selector) {
            const result = [];
            for(const layer of App.layers) {
                const matchType = selector.type != undefined
                    ? layer.type == selector.type
                    : true
                    ;
                if(!matchType) continue;
                let matchMeta = true;
                for(const [metaName, metaValue] of Object.entries(selector.meta ?? {})) {
                    if(layer.typeMetadata[metaName] != metaValue) {
                        matchMeta = false;
                        break;
                    }
                }
                if(matchMeta) result.push(layer);
            }
            return result;
        },
        layerAssert(layer, condition, callback) {
            const matchType = condition.type != undefined
                ? layer.type == condition.type
                : true
                ;
            if(!matchType) return false;
            let matchMeta = true;
            for(const [metaName, metaValue] of Object.entries(condition.meta ?? {})) {
                if(layer.typeMetadata[metaName] != metaValue) {
                    matchMeta = false;
                    break;
                }
            }
            if(!matchMeta) return false;
            callback && callback();
            return true;
        },
        getMapStyleInfo() {
            const styleAlias = App.mapStyle;
            if(MAPBOX_STYLES[styleAlias]) {
                return MAPBOX_STYLES[styleAlias];
            }
            return MAPBOX_STYLES.default;
        },
        loadFont(name, url, callback) {
            const hash = (str) => {
                let result = '';
                for (const [cid, c] of [...str].entries()) {
                    result += c.charCodeAt(0) * (cid + 1);
                }
                return (+result) % 123456789101112;
            };
            const fontLoaderClass = `font-loader-${hash(name)}`;
            if ($(`.${fontLoaderClass}`).length == 0) {
                $('body').append(`
                <div class="${fontLoaderClass}">
                    <style>
                        @font-face {
                            font-family: ${JSON.stringify(name)};
                            src: url(${JSON.stringify(url)});
                        }
                    </style>
                </div>
                `);
            }
            const font = new FontFaceObserver(name);
            if(!this.__private.fontloader) this.__private.fontloader = {};
            const messageID = uuidv4();
            VueApp.$store.commit('setCanvasLoaderMessage', [messageID, 'Loading font ...']);
            const promise = font.load(null, 10000);
            promise.then(() => {
                VueApp.$store.commit('setCanvasLoaderMessage', [messageID]);
            }).catch(() => {
                VueApp.$store.commit('setCanvasLoaderMessage', [messageID]);
            });
            if(!this.__private.fontloader[name]) {
                promise.catch(e => {
                    VueApp.$store.commit('setFontError', true);
                    Toastify({
                        text: `Failed to load font '${name}': ${e}`,
                        duration: 3000,
                        close: true,
                        gravity: "top",         // `top` or `bottom`
                        position: "left",       // `left`, `center` or `right`
                        backgroundColor: "#bd2300",
                        stopOnFocus: true,      // Prevents dismissing of toast on hover
                        onClick: function(){}   // Callback after click
                    }).showToast();
                    console.error(`Failed to load font '${name}': ${e}`);
                });
                this.__private.fontloader[name] = true;
            }
            callback && promise.then(callback);
        },
        initCropper(el, ratio, readyCallback) {
            return $(el).cropper({
                // initialAspectRatio: ratio ?? 16 / 9,
                aspectRatio: ratio ?? 16 / 9,
                restore: false,
                viewMode: 1,
                crop: function (event) {
                },
                zoom: function (e) {
                    e.preventDefault(); // Prevent zoom
                },
                ready: readyCallback
            });
        },
        lockObject(obj) {
            obj.set({
                lockMovementX: true,
                lockMovementY: true,
                hasControls: false,
                hoverCursor: "pointer",
            });
        },
        scaleTo(obj, {width, height}) {
            const scaleWidth = width / obj.width;
            const scaleHeight = height / obj.height;
            obj.set({
                scaleX: scaleWidth,
                scaleY: scaleHeight
            });
        },
        rgbToHex(r, g, b, a) {
            const red = ('0' + (+(r ?? 0)).toString(16)).slice(-2);
            const green = ('0' + (+(g ?? 0)).toString(16)).slice(-2);
            const blue = ('0' + (+(b ?? 0)).toString(16)).slice(-2);
            const alpha = ('0' + (+(a ?? 0)).toString(16)).slice(-2);
            if (a == undefined) {
                return '#' + red + green + blue;
            }
            return '#' + red + green + blue + alpha;
        },
        setTextObjectValue(textObject, newText, layerData) {
            const widthLimit = layerData.config.width;
            const heightLimit = layerData.config.height;
            const originalFontSize = layerData.data.fontSize || layerData.config.text.font.sizes[0];
            textObject.set({
                fontSize: originalFontSize,
                angle: 0
            });
            newText = newText.replaceAll('\r', '\n');
            textObject.set('text', newText);
            const calculatedFontSize = {
                width: originalFontSize * widthLimit / textObject.width,
                height: originalFontSize * heightLimit / textObject.height * 1.4
            };
            textObject.set('fontSize', Math.min(originalFontSize, calculatedFontSize.width, calculatedFontSize.height));
            // textObject.set('fontSize', Math.min(originalFontSize, calculatedFontSize.width));
            textObject.set({
                top: layerData.data.top + (layerData.config.height - textObject.height) / 2,
                angle: layerData.config.rotate,
                opacity: layerData.data.opacity ?? 1
            });
        },
        loadSVG(path, callback) {
            $.ajax({
                url: path,
                type: 'get',
                dataType: 'html',
                success(res) {
                    fabric.loadSVGFromString(res, (objects, options) => {
                        const svg = fabric.util.groupSVGElements(objects, options);
                        callback(svg);
                    });
                },
                error(err) {
                    console.log('Failed to load SVG', err);
                }
            });
        },
        downloadURI(uri) {
            var link = document.createElement("a");
            link.download = undefined;
            link.href = uri;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        },
        openBase64InNewTab(b64) {
            var image = new Image();
            image.src = b64;

            var w = window.open("");
            w.document.write(image.outerHTML);
        },
        openInNewTab(url) {
            window.open(url, '_blank');
        },
        asyncLoop(callback, interval = 50) {
            let lock = false;
            const intervalID = setInterval(() => {
                if (!lock) {
                    lock = true;
                    const breaker = () => { clearInterval(intervalID) };
                    callback(breaker);
                    lock = false;
                }
            }, interval);
        },
        setLayerProp(layer, name, value) {
            if(layer.data.hasOwnProperty(name)) {
                layer.data[name] = value;
            }
            else {
                Object.defineProperty(layer.data, name, {
                    get() {
                        return value;
                    },
                    set(v) {
                        const __silent = (v ?? {}).__silent;
                        if(__silent) {
                            value = v.value;
                            return;
                        }
                        const oldValue = value;
                        value = v;
                        App.events.emit('layerPropChange', {
                            layer: layer,
                            propName: name,
                            propValue: v,
                            oldValue: oldValue
                        });
                    }
                });
            }
        },
        formatDuration(duration_ms) {
            const h = parseInt(duration_ms / 3600000);
            const m = parseInt((duration_ms % 3600000) / 60000);
            const s = parseInt((duration_ms % 60000) / 1000);
            // console.log(res);
            const prefix = function(n) {
                n = ''+n;
                if(n.length <= 2) return ('0' + n).slice(-2);
                return n;
            };
            const duration = (h > 0 ? prefix(h) + ':' : '') + prefix(m) + ':' + prefix(s);
            return duration;
        },
        createAutoResizeTextarea() {
            const scrollHiddenCSS = `
                overflow: hidden !important;
                -ms-overflow-style: none !important;
                scrollbar-width: none !important;
            `;
            if($('body > .autoresize-textarea-zone').length == 0) {
                $('body').append('<div class="autoresize-textarea-zone"></div>');
                $('body > .autoresize-textarea-zone').attr('style', `visibility:hidden;position:fixed;z-index:-9999999;`);
            }
            const hiddenTextarea = new Shitonen({
                html: `
                    <textarea style="{{style}}" rows="1"></textarea>
                `,
                shit: {
                    style: scrollHiddenCSS,
                    value: ''
                },
                init() {
                    this.shit.on('init change', 'value', _ => {
                        this.element.value = this.shit.value;
                    });
                    $('body > .autoresize-textarea-zone').append(this.element);
                }
            });
            const displayTextarea = new Shitonen({
                html: `<textarea class="form-control"></textarea>`,
                shit: {
                    value: ''
                },
                init() {
                    $(this.element).toggleClass('autoresize-textarea', true);
                    CustomCSS.add('.autoresize-textarea', `
                        resize: none !important;
                        ${scrollHiddenCSS}
                    `);
                    const update = _ => {
                        this.shit.value = this.element.value;
                    };
                    this.element.oninput = update;
                    this.element.onchange = update;
                    const copyStyle = (name) => {
                        const css = $(this.element).css(name);
                        $(hiddenTextarea.element).css(name, css);
                    };
                    this.shit.on('init change', 'value', _ => {
                        hiddenTextarea.shit.value = this.shit.value;
                        $(this.element).val(this.shit.value);
                        copyStyle('width');
                        copyStyle('font-family');
                        copyStyle('font-size');
                        copyStyle('font-weight');
                        copyStyle('line-height');
                        const height = hiddenTextarea.element.scrollHeight;
                        $(this.element).css('height', height+'px');
                    });
                }
            });
            return displayTextarea;
        },
        createFabricSVG(svg, cb) {
            fabric.loadSVGFromString(svg, function(objects, options) {
                var svgGroup = fabric.util.groupSVGElements(objects, options);
                svgGroup.set({
                    lockMovementX: !0,
                    lockMovementY: !0,
                    hasControls: !1,
                    hoverCursor: "pointer"
                });
                cb && cb(svgGroup);
            });
        },
        getSpiralTextBase64({
            width=100,
            height=100,
            lineHeight=1,
            start=600,
            end=18000,
            text='',
            letterSpacing=2,
            color='#000',
            fontSize=20,
            fontFamily='Georgia'
        }={}) {
            if($('#spiralCanvas').length == 0) {
                $(document.body).append('<canvas id="spiralCanvas" class="d-none"></canvas>');
            }

            const path = [];
            const trueLineHeight = fontSize / 4 * lineHeight;
            for (let i = 0; i < 7200; i++) {
                const angle = 0.1 * i;
                const deg = angle / Math.PI * 180;
                if(deg >= start && deg <= end) {
                    const x = (trueLineHeight + trueLineHeight * angle) * Math.cos(angle);
                    const y = (trueLineHeight + trueLineHeight * angle) * Math.sin(angle);
                    path.push(x);
                    path.push(y);
                }
            }

            const canvas = document.getElementById("spiralCanvas");
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext("2d");
            function drawPath(path) {
                ctx.save();
                ctx.strokeStyle = "#00000000";
                ctx.lineWidth = 1;
                ctx.beginPath();
                ctx.moveTo(path[0], path[1]);
                for (var i = 2; i < path.length; i += 2) {
                    ctx.lineTo(path[i], path[i + 1]);
                }
                ctx.stroke();
                ctx.restore();
            }

            ctx.font = `${fontSize}px ${fontFamily}`;
            ctx.textBaseline = "middle";
            ctx.fillStyle = color;
            ctx.lineWidth = letterSpacing;
            ctx.strokeStyle = "#00000000";

            ctx.translate(width / 2, height / 2);
            drawPath(path);
            ctx.textPath(text, path);

            return canvas.toDataURL();
        },
        showConfirmPopup({title, confirmText, denyText}={}, callback) {
            Swal.fire({
                title: title ?? 'The action is irreversible!<br>Are you sure?',
                showCancelButton: true,
                confirmButtonText: confirmText ?? `Yes, I'm happy with the design.`,
                cancelButtonText: denyText ?? `Cancel`,
                allowOutsideClick: false
            }).then((result) => {
                callback(result.isConfirmed);
            });
        },
        uploadBlob(blob, callback) {
            return new Promise((resolve, reject) => {
                const fd = new FormData();
                fd.append('blob', 'true');
                fd.append('file', blob);
                $.ajax({
                    type: 'POST',
                    url: 'https://dev.goodsmize.com/Plugin/upload-images/upload.php',
                    data: fd,
                    processData: false,
                    contentType: false
                }).done(function(data) {
                    callback && callback(data);
                    resolve(data);
                }).fail(function(error) {
                    callback && callback(undefined, error);
                    reject(error);
                });
            });
        },
        uploadBlobAWS(blob, callback) {
            return new Promise((resolve, reject) => {
                const fd = new FormData();
                fd.append('blob', 'true');
                fd.append('file', blob);
                $.ajax({
                    type: 'POST',
                    url: 'https://dev.goodsmize.com/uploads3do',
                    data: fd,
                    processData: false,
                    contentType: false
                }).done(function(data) {
                    callback && callback(data);
                    resolve(data);
                }).fail(function(error) {
                    callback && callback(undefined, error);
                    reject(error);
                });
            });
        }
    },
    layerProcessor: {
        fg(layerData, callback, options={}) {
            const finished = !!options.finished;
            const imgUrl            = `${App.productRoot}/${App.productName}/${layerData.config.preview ?? layerData.config.image}`;
            const fullsizeImgUrl    = `${App.productRoot}/${App.productName}/${layerData.config.image}`;
            let data = {
                __loaded: 0,
                __threshold: 2,
                objects: [],
                get loaded() { return this.__loaded; },
                set loaded(v) { this.__loaded = v; }
            };
            fabric.Image.fromURL(imgUrl, img => {
                img.set({
                    left: layerData.data.left,
                    top: layerData.data.top,
                    evented: false,
                });
                img.scaleToWidth(layerData.config.width);
                img.scaleToHeight(layerData.config.height);
                if(layerData.config.keepRatio == false) {
                    App.helper.scaleTo(img, {
                        width: layerData.config.width,
                        height: layerData.config.height
                    });
                }
                App.helper.lockObject(img);
                data.objects[0] = img;
                data.loaded += 1;
                callback(data.objects);
            }, { crossOrigin: 'anonymous' });
            if(App.mode('debug')) {
                fabric.Image.fromURL(fullsizeImgUrl, img => {
                    img.set({
                        left: layerData.data.left,
                        top: layerData.data.top,
                        evented: false,
                    });
                    img.scaleToWidth(layerData.config.width);
                    img.scaleToHeight(layerData.config.height);
                    if(layerData.config.keepRatio == false) {
                        App.helper.scaleTo(img, {
                            width: layerData.config.width,
                            height: layerData.config.height
                        });
                    }
                    App.helper.lockObject(img);
                    data.objects[1] = img;
                    data.loaded += 1;
                }, { crossOrigin: 'anonymous' });
            }
        },
        text(layerData, callback) {
            const textValue = layerData.data.textValue ?? layerData.config.text.value;
            const fontConfig = layerData.config.text.font;
            const fontName = fontConfig.name;
            const fontUrl = `${App.productRoot}/${App.productName}/${fontConfig.path}`;
            App.helper.loadFont(fontName, fontUrl, () => {
                const textObject = new fabric.IText(textValue, {
                    left: layerData.data.left,
                    top: layerData.data.top,
                    angle: layerData.config.rotate,
                    fill: App.helper.rgbToHex(...fontConfig.colors[0]),
                    fontFamily: fontName,
                    fontSize: fontConfig.sizes[0],
                    splitByGrapheme: true,
                    textAlign: "center"
                });
                App.canvas.instance.on('text:changed', e => {
                    if(e.target == textObject) {
                        layerData.data.textValue = {
                            __silent: true,
                            value: e.target.text
                        };
                        layerData.editors[0].setText(e.target.text);
                    }
                });
                if(fontConfig.alignment[0] == 'center') {
                    textObject.set({
                        left: layerData.data.left + layerData.config.width / 2
                    });
                }
                if(fontConfig.alignment[0] == 'right') {
                    textObject.set({
                        left: layerData.data.left + layerData.config.width
                    });
                }
                /* STROKE EFFECT */
                if (fontConfig.stroke) {
                    textObject.set({
                        stroke: fontConfig.stroke.color,
                        strokeWidth: fontConfig.stroke.width,
                        strokeUniform: true,
                        paintFirst: "stroke",
                    });
                }
                App.helper.lockObject(textObject);
                textObject.set({
                    originX: fontConfig.alignment[0],
                });
                callback(textObject);
            });
        },
        backgroundText(layerData, callback) {
            const rawTextValue = (layerData.value ? layerData.value : layerData.config.text.value).replaceAll('\n', ' ').replaceAll('\r', ' ');
            const textValue = rawTextValue.split(' ').filter(v => !!v).join(' ');
            const words = textValue.split(' ').filter(v => !!v);
            const fontConfig = layerData.config.text.font;
            const fontName = fontConfig.name;
            const fontUrl = `${App.productRoot}/${App.productName}/${fontConfig.path}`;
            App.helper.loadFont(fontName, fontUrl, () => {
                const textConfig = {
                    fontSize: fontConfig.sizes[0],
                    fill: App.helper.rgbToHex(...fontConfig.colors[0]),
                    fontFamily: fontName,
                    splitByGrapheme: true
                };
                const exec = () => {
                    const generateLine = (text, lineIdx, lineLength, options) => {
                        const words = text.split(' ');
                        const selectableWordsIdx = words.map((v, idx) => !!v ? idx : -1).filter(v => v != -1);
                        if(selectableWordsIdx.length == 0 || typeof lineLength != 'number') {
                            return '';
                        }
                        let line = '';
                        let selectedCounter = 0;
                        const startIdx = selectableWordsIdx[(lineIdx) % selectableWordsIdx.length] ?? 0;
                        for(let i = 0; i < 999; i++) {
                            const trueIdx = (i + startIdx) % words.length;
                            const word = words[trueIdx]
                            line += word;
                            if(!!word) {
                                selectedCounter += 1;
                                if(selectedCounter >= lineLength) {
                                    break;
                                }
                            }
                            line += ' '
                        }
                        return line;
                    };
                    const sum = (arr) => [0, ...arr].reduce((acc, v) => acc + v);
                    const generateDummyText = (value) => {
                        return new fabric.Text(value, textConfig);
                    };
                    const measure = lru_cache((text) => {
                        const words = text.split(' ');
                        const obj = generateDummyText(text);
                        const data = {
                            width: obj.width,
                            height: obj.height,
                            value: text
                        };
                        if(words.length > 1) {
                            data.words = words.map(w => measure(w));
                            data.nrSpaces = words.length - 1;
                        } else {
                            data.words = [data];
                        }
                        return data;
                    });
                    const nrLines = Math.floor(layerData.config.height / measure(textValue).height);
                    const heightOfLine = layerData.config.height / nrLines;
                    const nrWords = Math.max(1, Math.floor(layerData.config.width / measure(textValue).width * textValue.split(' ').length));
                    const calculateLine = lru_cache((text, width) => {
                        const textMeasure = measure(text);
                        const oneSpaceWidth = measure(' ').width;
                        let minSpaceWidth = oneSpaceWidth * 2.5;
                        let words = [...textMeasure.words];
                        const calcSpaceWidth = () => ((width - sum(words.map(w => w.width))) / (words.length - 1));
                        while(calcSpaceWidth() < minSpaceWidth && words.length > 1) {
                            words.pop();
                        }
                        const spaceWidth = calcSpaceWidth();
                        let accumulatedOffset = 0;
                        return words.map(w => {
                            const data = {
                                offset: accumulatedOffset,
                                width: w.width,
                                value: w.value,
                            };
                            accumulatedOffset += w.width + spaceWidth;
                            return data;
                        });
                    });

                    const objects = [];
                    for(let i = 0; i < nrLines; i++) {
                        const lineText = generateLine(textValue, i % 2, nrWords);
                        const lineStrat = calculateLine(lineText, layerData.config.width);
                        for(const [wordIdx, wordStrat] of lineStrat.entries()) {
                            const textObj = new fabric.Text(wordStrat.value, textConfig);
                            textObj.set({
                                left: layerData.config.left + wordStrat.offset,
                                top: i * heightOfLine,
                            });
                            App.helper.lockObject(textObj);
                            objects.push(textObj);
                        }
                    }

                    return objects;
                };

                callback(exec());
            });
        },
        mask(layerData, callback) {
            const svgLocation = `${App.productRoot}/${App.productName}/${layerData.config.image}`;
            App.helper.loadSVG(svgLocation, svg => {
                svg.set({
                    left: layerData.data.left,
                    top: layerData.data.top,
                    fill: '#333d',
                    width: layerData.config.width,
                    height: layerData.config.height,
                    perPixelTargetFind: true
                });
                App.helper.lockObject(svg);
                layerData['svgPath'] = svg.d;
                callback(svg);
            });
        },
        async image(layerData, callback) {
            const filterName = App.editors.theme.selectedFilterName;
            let filteredImageURL = layerData.value;
            if(layerData.file) {
                filteredImageURL = layerData.file.displayURL;
                if(filterName && !['BlackWhite', 'Grayscale', 'Vintage', 'Bright'].includes(filterName)) {
                    console.error(`Filter not yet supported: ${filterName}`);
                }
                else if(filterName) {
                    const images = await layerData.file.imagesURLs;
                    filteredImageURL = await images.filter[filterName];
                }
            }
            else if(layerData.config.placeholder) {
                const placeholder = layerData.config.placeholder.includes('http')
                    ? layerData.config.placeholder
                    : `${App.productRoot}/${App.productName}/${layerData.config.placeholder}`
                    ;
                filteredImageURL = placeholder;
            }
            fabric.Image.fromURL(filteredImageURL, img => {
                const mask = layerData.objects[1];
                mask.set('fill', '#e6a51201');
                const clipPath = new fabric.Path(mask.path, {
                    left: layerData.data.left,
                    top: layerData.data.top,
                    absolutePositioned: true,
                    fill: "transparent",
                    perPixelTargetFind: true
                });
                img.set({
                    dirty: true,
                    clipPath: clipPath,
                    left: layerData.data.left,
                    top: layerData.data.top,
                    evented: false
                });
                if(mask.width / mask.height > img.width / img.height) {
                    img.scaleToWidth(mask.width);
                }
                else {
                    img.scaleToHeight(mask.height);
                }
                if(layerData.config.keepRatio == false) {
                    App.helper.scaleTo(img, {
                        width: layerData.config.width,
                        height: layerData.config.height
                    });
                }
                App.helper.lockObject(img);
                callback(img);
            }, { crossOrigin: 'anonymous' });
        },
        async photoText(layerData, callback, { index }={}) {
            if(index == undefined) return;
            const filterName = App.editors.theme.selectedFilterName;
            let filteredImageURL = null;
            if(layerData.files[index]) {
                filteredImageURL = layerData.files[index].displayURL;
                if(filterName && !['BlackWhite', 'Grayscale', 'Vintage', 'Bright'].includes(filterName)) {
                    console.error(`Filter not yet supported: ${filterName}`);
                }
                else if(filterName) {
                    const images = await layerData.files[index].imagesURLs;
                    filteredImageURL = await images.filter[filterName];
                }
            }
            else return;
            fabric.Image.fromURL(filteredImageURL, img => {
                const mask = layerData.objects[index * 2 + 1];
                mask.set('fill', '#e6a51201');
                const clipPath = new fabric.Path(mask.path, {
                    left: mask.left,
                    top: mask.top,
                    absolutePositioned: true,
                    fill: "#00000001",
                    perPixelTargetFind: true
                });
                App.helper.scaleTo(clipPath, {
                    width: mask.width * mask.scaleX,
                    height: mask.height * mask.scaleY
                });
                img.set({
                    dirty: true,
                    clipPath: clipPath,
                    left: mask.left,
                    top: mask.top,
                    evented: false,
                });
                if(mask.width / mask.height > img.width / img.height) {
                    img.scaleToWidth(mask.width * mask.scaleX);
                }
                else {
                    img.scaleToHeight(mask.height * mask.scaleY);
                }
                if(layerData.config.keepRatio == false) {
                    App.helper.scaleTo(img, {
                        width: mask.width * mask.scaleX,
                        height: mask.height * mask.scaleY
                    });
                }
                App.helper.lockObject(img);
                callback(img);
            }, { crossOrigin: 'anonymous' });
        },
        bgcolor(layerData, callback) {
            const customBackgrounds = layerData.typeMetadata.changecolor.split('/');
            const backgrounds = [];
            const objects = [];
            let resolved = 0;
            const addObj = (idx, obj) => {
                if(obj) {
                    obj.set({evented: false});
                    obj.scaleToWidth(layerData.config.width);
                    obj.scaleToHeight(layerData.config.height);
                    if(layerData.config.keepRatio == false) {
                        App.helper.scaleTo(obj, {
                            width: layerData.config.width,
                            height: layerData.config.height
                        });
                    }
                    App.helper.lockObject(obj);
                }
                objects[idx] = obj;
                resolved += 1;
                if(resolved == customBackgrounds.length) {
                    callback(backgrounds, objects);
                    App.events.emit('bgcolorReady');
                }
            };
            layerData.urls = [];
            for(const customBg of customBackgrounds) {
                const idx = backgrounds.length;
                // color
                if(customBg[0] != '$') {
                    const obj = new fabric.Rect({
                        left: 0,
                        top: 0,
                        width: layerData.config.width,
                        height: layerData.config.height,
                        fill: '#' + customBg
                    });
                    backgrounds.push({
                        type: 'color',
                        value: '#' + customBg
                    });
                    addObj(idx, obj);
                }
                // image
                else {
                    let bgURL = `${App.productRoot}/${App.productName}/` + layerData.config.images[customBg.slice(1)].match(/images\/.*$/g)[0];
                    let fullsizeBgURL = bgURL.replace('preview-background', 'background');
                    layerData.urls[idx] = {
                        bgURL, fullsizeBgURL
                    };
                    backgrounds.push({
                        type: 'image',
                        value: bgURL
                    });
                    addObj(idx, null);
                    // const messageID = uuidv4();
                    // VueApp.$store.commit('setCanvasLoaderMessage', [messageID, 'Loading background ...']);
                    // fabric.Image.fromURL(bgURL, img => {
                    //     img.set({
                    //         left: 0,
                    //         top: 0
                    //     });
                    //     addObj(idx, img);
                    //     VueApp.$store.commit('setCanvasLoaderMessage', [messageID]);
                    // }, { crossOrigin: 'anonymous' });
                }
            }
        },
        song(components, lyrics, callback) {
            const fontPath = components.text.config.text.font.path.includes('http')
                ? components.text.config.text.font.path
                : `${App.productRoot}/${App.productName}/${components.text.config.text.font.path}`
                ;
            $.ajax({
                // url: HEART_GENERATOR_URL,
                url: 'https://dev.goodsmize.com/text-shape/generate',
                type: 'post',
                // data: JSON.stringify({
                //     contents: lyrics,
                //     color: '000000'
                // }),
                data: JSON.stringify({
                    lyrics,
                    color: App.helper.rgbToHex(...components.text.config.text.font.colors[0]),
                    font: fontPath,
                    shape: components.text.typeMetadata.shape ?? 'heart2',
                    lineHeight: components.text.config.text.font.lineHeight ?? 1,
                    base64: true,
                    export: App.mode('debug'),
                    width: components.mask.config.width
                }),
                dataType: 'json',
                contentType: 'application/json'
            })
            .done(res => {
                // console.log(res);
                const url = res.data;
                fabric.Image.fromURL(
                    url,
                    img => {
                        var maskConfig = components.mask.config;
                        img.set({
                            left: maskConfig.left,
                            top: maskConfig.top,
                        });
                        img.scaleToWidth(maskConfig.width);
                        img.scaleToHeight(maskConfig.height);
                        if(maskConfig.keepRatio == false) {
                            App.helper.scaleTo(img, {
                                width: maskConfig.width,
                                height: maskConfig.height
                            });
                        }
                        App.helper.lockObject(img);
                        callback(img);
                    },
                    { crossOrigin: "anonymous" }
                );
            })
            .fail(err => {
                console.log(err);
            });
        }
    },
    test() {
        const textobj = new fabric.Text('MyText', {
            width: 1000,
            top: 5,
            left: 5,
            fontSize: 450,
            textAlign: 'center',
        });
        for(let i = 1; i < 11; i++) {
            textobj.set({fontSize: i * 50});
            console.log(textobj.width / i);
        }
    },
    init({ productName, canvasWidth, canvasHeight, layers }) {
        const firstTimeLoad = !App.initialized;
        App.initialized = true;

        // $('.group:not(.option-group)').toggleClass('hidden', true);
        $(`.group.text-group`).find('.group-body').html('');
        $(`.group.map-group`).find('.group-body').html('');
        $(`.group.background-group`).find('.group-body .splide__list').html('');

        if(App.canvas.instance) {
            App.canvas.instance.dispose();
        }

        const canvas = new fabric.Canvas('canvas', {
            // subTargetCheck: true,
            originX: "left",
            originY: "top",
            renderOnAddRemove: false,
            preserveObjectStacking: true
        });
        var disableScroll = function(){
            canvas.allowTouchScrolling = false;
        };

        var enableScroll = function(){
            canvas.allowTouchScrolling = true;
        };

        canvas.on('object:moving', disableScroll);
        canvas.on('object:scaling', disableScroll);
        canvas.on('object:rotating', disableScroll);
        canvas.on('mouse:up', enableScroll);

        canvas.selection = false;
        if(App.mode('debug')) {
            canvas.on('mouse:up', (e) => {
                const target = e.target;
                for (const layer of layers) {
                    for (const [objId, obj] of layer.objects.entries()) {
                        if (obj == target) {
                            console.log(layer, 'LayerID: ' + layer.id);
                            layer.onClick && layer.onClick();
                        }
                    }
                }
            });
        }
        canvas.set({
            scaleDownRatio: Math.min(window.screen.width, 1920) / canvasWidth
        });
        App.scaleDownRatio = canvas.scaleDownRatio;
        canvas.setWidth(canvasWidth * canvas.scaleDownRatio);
        canvas.setHeight(canvasHeight * canvas.scaleDownRatio);
        canvas.setZoom(canvas.scaleDownRatio);

        App.canvas.zoomRatio = canvas.scaleDownRatio;

        if(App.mode('debug')) {
            // $('.page-loader').toggleClass('d-none', false);
            const exportCanvas = new fabric.Canvas('exportCanvas', {
                originX: "left",
                originY: "top",
            });
            exportCanvas.setWidth(canvasWidth);
            exportCanvas.setHeight(canvasHeight);
            const dimensionLimit = 20480;
            const exportZoomRatio = canvasWidth >= canvasHeight ? Math.min(dimensionLimit, canvasWidth) / canvasWidth : Math.min(dimensionLimit, canvasHeight) / canvasHeight;
            exportCanvas.setDimensions({ width: canvasWidth * exportZoomRatio, height: canvasHeight * exportZoomRatio });
            exportCanvas.setZoom(exportZoomRatio);
            this.canvas.exportInstance = exportCanvas;
        }
        else {
            $('#exportCanvas').remove();
        }

        const autoCalib = () => {  
            const isMobile = $(window).width() <= 992;          
            App.isMobile = isMobile;

            const maxWidthRatio = isMobile ? 0.9 : 0.8;
            const maxHeightRatio = isMobile ? 1 : 0.8;

            const editorWrapper = $('.editor-wrapper');
            const containerWidth = !isMobile
                ? window.innerWidth - editorWrapper.width()
                : window.innerWidth
                ;
            const containerHeight = !isMobile
                ? window.innerHeight
                : window.innerHeight - 112
                ;

            const frameRatio = containerWidth / containerHeight * maxWidthRatio / maxHeightRatio;
            const canvasRatio = canvasWidth / canvasHeight;

            const container = $('#canvas').closest('.canvas-container');

            if (canvasRatio >= frameRatio) {
                $('#canvas').closest('.canvas-container').find('canvas').width(containerWidth * maxWidthRatio);
                $('#canvas').closest('.canvas-container').find('canvas').height(containerWidth * maxWidthRatio / canvasRatio);
            }
            else {
                $('#canvas').closest('.canvas-container').find('canvas').height(containerHeight * maxHeightRatio);
                $('#canvas').closest('.canvas-container').find('canvas').width(containerHeight * maxHeightRatio * canvasRatio);
            }
            $('#canvas').closest('.canvas-container').width($('#canvas').width());
            $('#canvas').closest('.canvas-container').height($('#canvas').height());
        };
        autoCalib();
        $(window).resize(autoCalib);

        if(firstTimeLoad) {
            const lastWindowSize = {
                width: $(window).width(),
                height: $(window).height()
            };
            setInterval(() => {
                const changed = $(window).width() != lastWindowSize.width || $(window).height() != lastWindowSize.height;
                if(changed) {
                    lastWindowSize.width = $(window).width();
                    lastWindowSize.height = $(window).height();
                    $(window).trigger('resize');
                }
            }, 500);

            let lastCanvasPosition = {};
            setInterval(() => {
                const canvasEl = App.canvas.instance.wrapperEl.children[0];
                const canvasBRect = canvasEl.getBoundingClientRect();
                const bodyBRect = document.body.getBoundingClientRect();
                if(canvasBRect.left == undefined) {}
                else if(
                    canvasBRect.left - bodyBRect.left != lastCanvasPosition.left ||
                    canvasBRect.top - bodyBRect.top != lastCanvasPosition.top ||
                    canvasBRect.width != lastCanvasPosition.width ||
                    canvasBRect.height != lastCanvasPosition.height
                ) {
                    App.events.emit('canvasPositionChange', canvasBRect);
                }
                lastCanvasPosition = {
                    left: canvasBRect.left - bodyBRect.left,
                    top: canvasBRect.top - bodyBRect.top,
                    width: canvasBRect.width,
                    height: canvasBRect.height
                };
            }, 200);

            setInterval(() => {
                App.events.emit('canvasPositionChange');
            }, 2000);

            // on click outside of canvas
            $(document).on('click', 'body > .main-content', e => {
                if(!$('.canvas-container')[0].contains(e.target)) {
                    App.canvas.deselectAll();
                }
            });
        }

        this.canvas.instance = canvas;
        this.productName = productName;
        this.layers = layers;
        if(firstTimeLoad) {
            this.dropzone.init();
        }
        this.editors.init();
        this.events.unbindAll();
    }
};

const ctrlMain = async () => {
    /*
    Initialize:
        step 1: create, align & lock object
        step 2: create connections between data, object and editor
    On Finish:
        step 1: export to preview image and full size image
        step 2: send finished result to parent
    */
    
    if(App.mode('debug')) {
        // Animation debug
        window.onanimationiteration = console.log;
    }

    let editorCount = {
        images: 0,
        texts: 0,
        maps: 0,
        backgrounds: 0,
    };

    let spotifyInfo = {};
    for (const layerData of App.layers) {
        const required = layerData.typeMetadata.required == 'true';
        const module = layerData.typeMetadata.module;
        const type = layerData.type;
        App.helper.setLayerProp(layerData, 'visible', true);
        App.helper.setLayerProp(layerData, 'left', layerData.config.left);
        App.helper.setLayerProp(layerData, 'top', layerData.config.top);
        App.helper.setLayerProp(layerData, 'width', layerData.config.width);
        App.helper.setLayerProp(layerData, 'height', layerData.config.height);
        App.helper.setLayerProp(layerData, 'opacity', layerData.config.opacity ?? 1);
        switch (layerData.type) {
            case 'fg': {
                App.layerProcessor.fg(layerData, images => {
                    layerData.objects = [images[0]];
                    layerData.allObjects = images;
                    App.canvas.refresh();
                });
            }
                break;
            case 'text': {
                const requiredStar = '<span style="color: #e6a512;"> *</span>';
                if(editorCount.texts == 0) {
                    if(layerData.typeMetadata.module != 'background') {
                        App.editors.texts.init();
                    }
                    else if(layerData.typeMetadata.module == 'background') {
                        App.editors.backgroundText.init();
                    }
                }
                if(!['background', 'song_lyrics'].includes(layerData.typeMetadata.module)) {
                    const editor = App.editors.texts.newEditor();
                    layerData.editors[0] = editor;
                    if(layerData.typeMetadata.module == 'song_title') {
                        const songTitle = layerData.config.text.value;
                        App.editors.song.setSongTitle(songTitle);
                        App.editors.song.onTitleChange(newTitle => {
                            editor.setText(newTitle);
                        });
                    }

                    const defaultTextValue = layerData.config.text.value;

                    App.helper.setLayerProp(layerData, 'textValue', layerData.config.text.value);
                    App.helper.setLayerProp(layerData, 'textChanged', false);
                    editorCount.texts += 1;
                    editor.setTitle(layerData.typeMetadata.label+(required ? requiredStar : ''))
                        .setPlaceholder(layerData.config.text.value)
                        .setLayer(layerData);
                    if(layerData.typeMetadata.module == 'song_title') {
                        editor.setTitle('Song title');
                    }
                    if(!required)
                        editor.setText(layerData.config.text.value);
                    layerData.editors = [editor];
                    App.layerProcessor.text(layerData, text => {
                        layerData.objects = [text];
                        App.helper.setTextObjectValue(text, layerData.config.text.value, layerData);
                        App.canvas.refresh();
                        editor.initEditButton();
                        layerData.editors[0]?.adjustEditButton && layerData.editors[0].adjustEditButton();
                        App.canvas.onClick(text, () => {
                            if(layerData.typeMetadata.module == 'song_title') {
                                App.editors.song.setCollapse(false);
                                // App.editors.song.__groupEl.find('input.song-title').focus();
                                App.editors.song.launch();
                            }
                            else {
                                App.editors.texts.setCollapse(false);
                                editor.focus();
                            }
                        });
                        App.events.emit('textLayerReady', layerData);
                    });
                    App.events.on('layerPropChange', ({layer, propName, propValue}) => {
                        if(propName == 'textValue' && layer == layerData) {
                            editor.setText(propValue);
                        }
                    });
                    if(layerData.typeMetadata.module == 'starmap_date') {
                        App.events.on('starmapApply', ({id, data}) => {
                            if(id == layerData.typeMetadata.id) {
                                const defaultFormat = 'MM-DD-yyyy';
                                const formattedDate = moment(data.timestamp).format(layerData.config.format ?? defaultFormat);
                                editor.setText(formattedDate);
                            }
                        });
                    }
                    if(layerData.typeMetadata.module == 'starmap_text' && layerData.config.formula) {
                        App.events.on('starmapApply', ({id, data}) => {
                            if(id == layerData.typeMetadata.id) {
                                const defaultDateFormat = 'MM-DD-yyyy';
                                const defaultTimeFormat = 'hh:mm A';
                                const formattedDate = moment(data.timestamp).format(layerData.config.dateFormat ?? defaultDateFormat);
                                const formattedTime = moment(data.time, 'HH:mm:ss').format(layerData.config.timeFormat ?? defaultTimeFormat);
                                const formula = layerData.config.formula;
                                const location = data.place
                                    ? data.place.address_components.slice(-2).filter(l => l).map(l => l.long_name).join(', ')
                                    : 'Location'
                                    ;
                                editor.setText(formula.replaceAll('{location}', location).replaceAll('{date}', formattedDate).replaceAll('{time}', formattedTime));
                            }
                        })
                    }
                    if(layerData.typeMetadata.module == 'map_location') {
                        App.events.on('mapApply', ({id, data}) => {
                            if(id == layerData.typeMetadata.id) {
                                editor.setText(data.location);
                            }
                        });
                    }
                    editor.onTextChange((newText, eventType) => {
                        if(layerData.typeMetadata.module == 'song_title') {
                            App.editors.song.setSongTitle(newText);
                        }
                        if(newText != defaultTextValue) {
                            layerData.data.textChanged = true;
                        }
                        if (!newText.trim() && required) {
                            App.helper.setTextObjectValue(layerData.objects[0], '', layerData);
                        }
                        if(newText != layerData.data.textValue) {
                            App.helper.setTextObjectValue(layerData.objects[0], newText, layerData);
                            layerData.data.textValue = newText;
                        }
                        // App.canvas.refresh();
                    });
                    App.canvas.instance.on('text:editing:entered', e => {
                        setTimeout(() => {
                            if(e.target == layerData.objects[0]) {
                                layerData.objects[0].setSelectionStart(layerData.objects[0].text.length);
                                layerData.objects[0].setSelectionEnd(layerData.objects[0].text.length);
                            }
                        }, 100);
                    });
                    App.canvas.instance.on('text:editing:exited', e => {
                        if(e.target == layerData.objects[0]) {
                            App.helper.setTextObjectValue(layerData.objects[0], layerData.objects[0].text, layerData);
                        }
                    });
                }
                else if(layerData.typeMetadata.module == 'background') {
                    const editor = App.editors.backgroundText;
                    editor.setTitle(layerData.typeMetadata.label+(required ? requiredStar : ''))
                        .setText(layerData.config.text.value)
                        .setPlaceholder(layerData.config.text.value);
                    layerData.editors = [editor];

                    const generateBgText = () => {
                        App.layerProcessor.backgroundText(layerData, objects => {
                            layerData.objects = objects;
                            App.canvas.refresh();
                            for(const obj of objects) {
                                App.canvas.onClick(obj, () => {
                                    editor.setCollapse(false);
                                    editor.focus();
                                });
                            }
                        });
                    };
                    generateBgText();
                    let timeoutID = null;
                    let lastApply = (new Date).getTime();
                    editor.onTextChange((newText, eventType) => {
                        clearTimeout(timeoutID);
                        timeoutID = setTimeout(() => {
                            if (!newText.trim() && eventType == 'change') {
                                newText = layerData.config.text.value;
                                editor.setText(newText);
                            }
                            layerData.value = newText;
                            generateBgText();
                            App.canvas.clear();
                            App.canvas.refresh();
                        }, 500);
                        lastApply = (new Date).getTime();
                    });
                }
            }
                break;
            case 'song-mask': {
                App.editors.song.init();
                const components = (() => {
                    const components = {
                        mask: layerData
                    };
                    for(const layer of App.layers) {
                        if(layer.type == 'text' && layer.typeMetadata.module == 'song_lyrics') {
                            components.text = layer;
                        }
                    }
                    return components;
                })();
                const lyrics = components.text.config.text.value;
                App.editors.song.setLyrics(lyrics);

                let searchVersion = 0;
                App.editors.song.onSearchInputChange(query => {
                    searchVersion++;
                    const version = searchVersion;
                    if(query) {
                        $.ajax({
                            url: `https://dev.goodsmize.com/genius-lyrics/search?q=${encodeURI(query)}`,
                            method: 'get'
                        })
                        .then(response => {
                            if(version == searchVersion) {
                                App.editors.song.setSearchResults(response.data.map(item => ({
                                    uri: item.result.url,
                                    artist: item.result.primary_artist.name,
                                    title: item.result.title
                                })));
                            }
                        });
                    }
                    else {
                        App.editors.song.setSearchResults([]);
                    }
                });
                let lyricsVersion = 0;
                App.editors.song.onSearchResultSelect(({uri, artist, title}) => {
                    layerData.value = {uri, artist, title};
                    const songTitleLayer = App.layers.filter(l => l.typeMetadata.module == 'song_title')[0];
                    if(songTitleLayer) {
                        songTitleLayer.data.textValue = title;
                    }
                    lyricsVersion++;
                    const version = lyricsVersion;
                    const messageID = uuidv4();
                    VueApp.$store.commit('setCanvasLoaderMessage', [messageID, 'Getting lyrics ...']);
                    App.editors.song.switchEditMode();
                    App.editors.song.close();
                    $.ajax({
                        url: `https://dev.goodsmize.com/genius-lyrics/lyrics?url=${uri}`,
                        method: 'get'
                    })
                    .done(response => {
                        if(version == lyricsVersion) {
                            const lyrics = response.data;
                            App.editors.song.setLyrics(lyrics);
                            $('.song-group .apply-btn').trigger('click');
                        }
                    })
                    .always(() => {
                        VueApp.$store.commit('setCanvasLoaderMessage', [messageID]);
                    })
                });
                
                function processSong(lyrics) {
                    const letterSpacing = components.text.config.text.font.letterSpacing ?? 1;
                    lyrics = lyrics.replaceAll(/\[.*\]/ig, '').replaceAll('\n', ' ').replaceAll(/[\[\]]/ig, '').replaceAll(' ', ' '.repeat(letterSpacing)).trim();
                    const messageID = uuidv4();
                    const handleLyrics = img => {
                        layerData.objects = [img];
                        App.canvas.onClick(img, () => {
                            App.editors.song.setCollapse(false);
                            // App.editors.song.focus();
                            App.editors.song.launch();
                        });
                        App.canvas.refresh();
                        VueApp.$store.commit('setCanvasLoaderMessage', [messageID]);
                        App.events.emit('lyricsLoaded');
                    };
                    VueApp.$store.commit('setCanvasLoaderMessage', [messageID, 'Loading song ...']);
                    App.layerProcessor.song(components, lyrics, handleLyrics);
                }
                processSong(lyrics);
                App.editors.song.onApply(newLyrics => {
                    processSong(newLyrics);
                });
            }
                break;
            case 'mask': {
                if(editorCount.images == 0) {
                    App.editors.images.init();
                    App.editors.theme.init();
                }
                const editor = App.editors.images.newEditor();
                editorCount.images += 1;
                editor.setLayer(layerData);
                layerData.editors = [editor];
                
                App.helper.setLayerProp(layerData, 'imageExt',      undefined)
                App.helper.setLayerProp(layerData, 'imageHash',     undefined);
                App.helper.setLayerProp(layerData, 'imageFilter',   undefined);
                App.helper.setLayerProp(layerData, 'cropboxX',      undefined);
                App.helper.setLayerProp(layerData, 'cropboxY',      undefined);
                App.helper.setLayerProp(layerData, 'cropboxSpanX',  undefined);
                App.helper.setLayerProp(layerData, 'cropboxSpanY',  undefined);
                App.helper.setLayerProp(layerData, `cropboxRotation`, undefined);

                const handleSelectImage = () => {
                    const selected = !!layerData.objects[0];
                    if(!selected) {
                        App.editors.imageGallery.unbindAll();
                        App.editors.imageGallery.onImageSelect(file => {
                            file.onThumbnailReady(() => {
                                layerData.file = file;
                                editor.setImage(file.displayURL);
                                App.editors.imageGallery.close();
                                editor.launch();
                            });
                        });
                        App.editors.imageGallery.launch();
                    }
                    else {
                        editor.launch();
                    }
                };
                if(layerData.config.placeholder == 'default' || layerData.typeMetadata.placeholder == 'default') {
                    App.helper.createFabricSVG(`<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="image" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-image fa-w-16 fa-7x"><path fill="#ffffffaa" d="M464 448H48c-26.51 0-48-21.49-48-48V112c0-26.51 21.49-48 48-48h416c26.51 0 48 21.49 48 48v288c0 26.51-21.49 48-48 48zM112 120c-30.928 0-56 25.072-56 56s25.072 56 56 56 56-25.072 56-56-25.072-56-56-56zM64 384h384V272l-87.515-87.515c-4.686-4.686-12.284-4.686-16.971 0L208 320l-55.515-55.515c-4.686-4.686-12.284-4.686-16.971 0L64 336v48z" class=""></path></svg>`, imageSVG => {
                        layerData.objects[2] = imageSVG;
                        imageSVG.set({
                            top: layerData.config.top + layerData.config.height / 2 - imageSVG.height / 2,
                            left: layerData.config.left + layerData.config.width / 2 - imageSVG.width / 2,
                            evented: false
                        });
                    });
                }
                else if(layerData.config.placeholder) {
                    const placeholder = layerData.config.placeholder.includes('http')
                        ? layerData.config.placeholder
                        : `${App.productRoot}/${App.productName}/${layerData.config.placeholder}`
                        ;

                    editor.setImage(layerData.config.placeholder);
                    App.layerProcessor.image(layerData, img => {
                        const layerImageRatio = layerData.config.width / layerData.config.height;
                        const imgRatio = img.width / img.height;
                        let scaleRatio;
                        if(imgRatio > layerImageRatio) {
                            scaleRatio = layerData.config.height / img.height;
                            img.scaleToHeight(layerData.config.height);
                        }
                        else {
                            scaleRatio = layerData.config.width / img.width;
                            img.scaleToWidth(layerData.config.width);
                        }
                        if(layerData.config.keepRatio == false) {
                            App.helper.scaleTo(img, {
                                width: layerData.config.width,
                                height: layerData.config.height
                            });
                        }
                        img.set({
                            left: layerData.config.left + layerData.config.width / 2 - img.width * scaleRatio / 2,
                            top: layerData.config.top + layerData.config.height / 2 - img.height * scaleRatio / 2
                        });
                        layerData.objects[0] = img;
                        layerData.objects.splice(2, 1);
    
                        layerData.data.cropboxX = null;
                        layerData.data.cropboxY = null;
                        layerData.data.cropboxSpanX = null;
                        layerData.data.cropboxSpanY = null;
    
                        App.helper.applyCropboxData(layerData);
                        App.canvas.refresh();
                    });
                }
                App.layerProcessor.mask(layerData, mask => {
                    layerData.objects[1] = mask;
                    App.canvas.refresh();
                    App.canvas.onClick(mask, handleSelectImage);
                    editor.initEditButton();
                });
                editor.onApply(() => {
                    const cropboxData = editor.getCropboxData();
                    App.layerProcessor.image(layerData, img => {
                        layerData.objects[0] = img;
                        layerData.data['cropboxX']     = cropboxData.cropX;
                        layerData.data['cropboxY']     = cropboxData.cropY;
                        layerData.data['cropboxSpanX'] = cropboxData.cropSpanX;
                        layerData.data['cropboxSpanY'] = cropboxData.cropSpanY;
                        layerData.data['cropboxScaleX'] = cropboxData.scaleX;
                        layerData.data['cropboxScaleY'] = cropboxData.scaleY;
                        layerData.data['cropboxRotation'] = cropboxData.rotation;
                        App.canvas.refresh();
                    });
                    editor.close();
                });
                editor.onChangeImage(() => {
                    editor.close();
                    App.editors.imageGallery.unbindAll();
                    App.editors.imageGallery.onImageSelect(file => {
                        file.onThumbnailReady(() => {
                            layerData.data.cropboxX = null;
                            layerData.data.cropboxY = null;
                            layerData.data.cropboxSpanX = null;
                            layerData.data.cropboxSpanY = null;
                            layerData.file = file;
                            editor.setImage(file.displayURL);
                            App.editors.imageGallery.close();
                            editor.launch();
                        });
                    });
                    App.editors.imageGallery.launch();
                });
            }
                break;
            case 'map-mask': {
                if(editorCount.maps == 0) {
                    App.editors.maps.init();
                }
                editorCount.maps += 1;
                const mapIdx = editorCount.maps - 1;
                const components = (() => {
                    const components = {};
                    components.mask = layerData;
                    for(const layer of App.layers) {
                        if(layer.type == 'marker' && layer.typeMetadata.id == layerData.typeMetadata.id) {
                            components.marker = layer;
                        }
                        if(layer.type == 'map') {
                            App.mapStyle = layer.typeMetadata.style_v2;
                        }
                    }
                    return components;
                })();
                const editor = App.editors.maps.newEditor(components);
                editor.setLayer(layerData);
                layerData.editors.push(editor);
                App.layerProcessor.mask(components.mask, mask => {
                    components.mask.objects[1] = mask;
                    let mapLayer = App.layers.filter(l => l.typeMetadata.id == layerData.typeMetadata.id && l.type == 'map')[0];
                    if(!mapLayer) mapLayer = App.layers.filter(l => l.type == 'map')[0];
                    const mapStyle = MAPBOX_STYLES[mapLayer.typeMetadata.style_v2] ?? MAPBOX_STYLES.default;
                    if(mapStyle.placeholder) {
                        components.mask.value = mapStyle.placeholder;
                        App.layerProcessor.image(components.mask, img => {
                            components.mask.objects[0] = img;
                            components.mask.objects.splice(2, 1);
                            App.canvas.refresh();
                            App.canvas.onClick(img, () => {
                                App.editors.maps.setCollapse(false);
                                editor.modal.launch();
                            });
                        });
                    }
                    App.canvas.refresh();
                    App.canvas.onClick(mask, () => {
                        App.editors.maps.setCollapse(false);
                        editor.modal.launch();
                    });
                    editor.modal.on('apply', () => {
                        App.editors.maps.__prevLocation = editor.map.getCenter();
                        editor.__lastLocation = editor.map.getCenter();
                        editor.export(exported => {
                            App.events.emit('mapApply', {
                                id: components.mask.typeMetadata.id,
                                data: exported
                            });
                            const markerURL = `${App.productRoot}/${App.productName}/${components.marker.config.image}`;
                            fabric.Image.fromURL(markerURL, img => {
                                const mask = components.mask.objects[1];
                                mask.set('fill', '#e6a51201');
                                const clipPath = new fabric.Path(mask.path, {
                                    left: components.mask.data.left,
                                    top: components.mask.data.top,
                                    absolutePositioned: true,
                                    fill: "transparent",
                                    perPixelTargetFind: true
                                });
                                img.set({
                                    dirty: true,
                                    clipPath: clipPath,
                                    left: components.mask.config.left + exported.marker.left * components.mask.config.width,
                                    top: components.mask.config.top + exported.marker.top * components.mask.config.height,
                                    evented: false,
                                });
                                components.mask.data.marker = {
                                    ...exported.marker,
                                    x: exported.marker.left,
                                    y: exported.marker.top,
                                    url: markerURL
                                };
                                img.scaleToWidth(exported.marker.width * components.mask.config.width);
                                App.helper.lockObject(img);
                                components.mask.objects[2] = img;
                                App.canvas.refresh();
                            }, { crossOrigin: 'anonymous' });
                            components.mask.value = exported.map;
                            components.mask.marker = exported.marker;
                            App.layerProcessor.image(components.mask, img => {
                                components.mask.objects[0] = img;
                                // components.mask.objects.splice(2, 1);
                                App.canvas.refresh();
                                App.canvas.onClick(img, () => {
                                    App.editors.maps.setCollapse(false);
                                    editor.modal.launch();
                                });
                            });
                            editor.modal.close();
                        });
                    });
                });
            }
                break;
            case 'bgcolor': {
                App.layerProcessor.bgcolor(layerData, (backgrounds, objects) => {
                    if(layerData.urls.length > 1) {
                        App.editors.backgrounds.init();
                        editorCount.backgrounds = 1;
                    }
                    App.editors.backgrounds.fetch(backgrounds);
                    function loadBg(idx) {
                        layerData.allObjects = objects;
                        const messageID = uuidv4();
                        VueApp.$store.commit('setCanvasLoaderMessage', [messageID, 'Loading background ...']);
                        if(App.mode('debug') && layerData.urls[idx]) {
                            const bgURL = layerData.urls[idx].fullsizeBgURL;
                            fabric.Image.fromURL(bgURL, img => {
                                img.set({
                                    left: layerData.config.left,
                                    top: layerData.config.top,
                                    evented: false,
                                });
                                img.scaleToWidth(layerData.config.width);
                                img.scaleToHeight(layerData.config.height);
                                if(layerData.config.keepRatio == false) {
                                    App.helper.scaleTo(img, {
                                        width: layerData.config.width,
                                        height: layerData.config.height
                                    });
                                }
                                layerData.objects = [img];
                                App.helper.lockObject(img);
                                App.canvas.refresh();
                                VueApp.$store.commit('setCanvasLoaderMessage', [messageID]);
                            }, { crossOrigin: 'anonymous' });
                        }
                        else {
                            const bgURL = layerData.urls[idx]?.bgURL;
                            if(!layerData.allObjects[idx] && bgURL) {
                                fabric.Image.fromURL(bgURL, img => {
                                    img.set({
                                        left: layerData.config.left,
                                        top: layerData.config.top,
                                        evented: false,
                                    });
                                    img.scaleToWidth(layerData.config.width);
                                    img.scaleToHeight(layerData.config.height);
                                    if(layerData.config.keepRatio == false) {
                                        App.helper.scaleTo(img, {
                                            width: layerData.config.width,
                                            height: layerData.config.height
                                        });
                                    }
                                    layerData.objects = [img];
                                    layerData.allObjects[idx] = img;
                                    App.helper.lockObject(img);
                                    App.canvas.refresh();
                                    VueApp.$store.commit('setCanvasLoaderMessage', [messageID]);
                                }, { crossOrigin: 'anonymous' });
                            }
                            else {
                                layerData.objects = [layerData.allObjects[idx]];
                                layerData.objects = [objects[idx]];
                                VueApp.$store.commit('setCanvasLoaderMessage', [messageID]);
                                App.canvas.refresh();
                            }
                        }
                    }
                    App.editors.backgrounds.onBackgroundSelect(idx => {
                        loadBg(idx);
                    });
                    loadBg(0);
                    if(App.mode('debug')) {
                        App.events.on('selectBgcolor', idx => {
                            loadBg(idx);
                        });
                    }
                });
            }
                break;
            case 'clipart-text': {
                const editor = App.editors.clipartText;
                editor.init();
                editor.setPlacehoder(layerData.typeMetadata.label);
                editor.setText(layerData.config.defaultText ?? 'abc');
                editor.setLayer(layerData);
                const localEventTrigger = {
                    loadImageAt: null,
                };
                const renderText = text => {
                    layerData.value = [];
                    const imagesCalib = () => {
                        const spacing = +(layerData.config.letterSpacing ?? 20);
                        let totalWidth = 0;
                        let totalChars = 0;
                        for(const i in text) {
                            const obj = layerData.objects[i];
                            const char = text[i];
                            if(obj) {
                                totalWidth += obj.width / obj.height * layerData.config.height;
                                totalChars++;
                            }
                            else if(char == ' ') {
                                totalWidth += layerData.config.spaceWidth;
                                totalChars++;
                            }
                        }
                        totalWidth += spacing * (totalChars - 1);
                        let scaleRatio = 1;
                        if(totalWidth > layerData.config.width) {
                            scaleRatio = layerData.config.width / totalWidth;
                        }
                        const startX = layerData.config.left + (layerData.config.width - totalWidth * scaleRatio) / 2;
                        const startY = layerData.config.top + layerData.config.height * (1 - scaleRatio) / 2;
                        let currentStartX = startX;
                        for(const i in text) {
                            const obj = layerData.objects[i];
                            const char = text[i];
                            if(obj) {
                                const scale = layerData.config.height * scaleRatio / obj.height;
                                obj.set({
                                    left: currentStartX,
                                    top: startY,
                                    scaleX: scale,
                                    scaleY: scale
                                });
                                currentStartX += (obj.width / obj.height * layerData.config.height + spacing) * scaleRatio;
                            }
                            else if(char == ' ') {
                                currentStartX += (layerData.config.spaceWidth + spacing) * scaleRatio;
                            }
                        }
                    };
                    while(layerData.objects.length) {
                        layerData.objects.pop();
                    }
                    const loadImageAt = (i, imageIdx) => {
                        const char = text[i].toUpperCase();
                        if(char > 'Z' || char < 'A') return;
                        const rawImgURL = layerData.config.images[char][imageIdx];
                        const imgURL = `${App.productRoot}/${App.productName}/${rawImgURL}`;
                        layerData.value[i] = {
                            char: char,
                            idx: imageIdx,
                            path: rawImgURL
                        };
                        const messageID = uuidv4();
                        VueApp.$store.commit('setCanvasLoaderMessage', [messageID, 'Loading images ...']);
                        fabric.Image.fromURL(imgURL, img => {
                            img.set({
                                shadow: new fabric.Shadow({
                                    ...layerData.config.shadow,
                                    color: layerData.config.shadow.color,
                                    blur: layerData.config.shadow.blur
                                })
                            });
                            App.helper.lockObject(img);
                            layerData.objects[i] = img;
                            App.canvas.onClick(img, () => {
                                const availableImages = layerData.config.images[char];
                                if(availableImages.length <= 1) return;
                                let newIdx = (imageIdx + 1) % availableImages.length;
                                loadImageAt(i, newIdx);
                            });
                            imagesCalib();
                            App.canvas.refresh();
                            VueApp.$store.commit('setCanvasLoaderMessage', [messageID]);
                        }, { crossOrigin: 'anonymous' });
                    };
                    localEventTrigger.loadImageAt = loadImageAt;
                    let history = {};
                    for(let i = 0; i < text.length; ++i) {
                        const char = text[i].toUpperCase();
                        if(char > 'Z' || char < 'A') continue;
                        if(history[char] == undefined) {
                            history[char] = 0;
                        }
                        const availableImages = layerData.config.images[char];
                        loadImageAt(i, history[char]);
                        history[char] = (history[char] + 1) % availableImages.length;
                    }
                };
                editor.displayError('');
                renderText(layerData.config.defaultText ?? 'abc');

                let currentText = editor.getText();
                const handleApply = () => {
                    const text = editor.getText();
                    for(const c of text) {
                        const C = c.toUpperCase();
                        if(C != ' ' && layerData.config.images[C] == undefined) {
                            return editor.displayError(`Not allowed character: ${c}`);
                        }
                    }
                    if(text.length < layerData.config.minCharacters) {
                        editor.displayError('Text too short');
                    }
                    else if(text.length > layerData.config.maxCharacters) {
                        editor.displayError('Text too long');
                    }
                    else {
                        editor.displayError('');
                        renderText(text);
                        currentText = text;
                    }
                };

                editor.onFocusOut(() => {
                    const text = editor.getText();
                    if(text != currentText) handleApply();
                });
                editor.onApply(handleApply);
                App.events.on('setClipartLetterIndex', ({position, index}) => {
                    localEventTrigger.loadImageAt(position, index);
                });
            }
                break;
            case 'photo-text': {
                App.editors.imageGallery.init();
                const editor = App.editors.photoText;
                editor.init();
                editor.setPlacehoder(layerData.typeMetadata.label);
                editor.setText(layerData.config.defaultText ?? 'abc');
                editor.setLayer(layerData);
                const localEventHandlers = {};
                const resetLayer = () => {
                    App.editors.photoText.reset();
                    layerData.files = [];
                    layerData.value = {
                        text: '',
                        characters: []
                    };
                    while(layerData.objects.length) {
                        layerData.objects.pop();
                    }
                    for(let i = 0; i < 32; ++i) {
                        App.helper.setLayerProp(layerData, `imageExt_${i}`,      undefined)
                        App.helper.setLayerProp(layerData, `imageHash_${i}`,     undefined);
                        App.helper.setLayerProp(layerData, `imageFilter_${i}`,   undefined);
                        App.helper.setLayerProp(layerData, `cropboxX_${i}`,      undefined);
                        App.helper.setLayerProp(layerData, `cropboxY_${i}`,      undefined);
                        App.helper.setLayerProp(layerData, `cropboxSpanX_${i}`,  undefined);
                        App.helper.setLayerProp(layerData, `cropboxSpanY_${i}`,  undefined);
                        App.helper.setLayerProp(layerData, `cropboxRotation_${i}`, undefined);
                    }
                };
                resetLayer();
                const renderText = text => {
                    resetLayer();
                    App.editors.photoText.reset();
                    const imagesCalib = () => {
                        const spacing = +(layerData.config.letterSpacing ?? 20);
                        let totalWidth = 0;
                        let totalChars = 0;
                        for(const i in text) {
                            const obj = layerData.objects[i * 2 + 1];
                            const char = text[i];
                            if(obj) {
                                totalWidth += obj.width / obj.height * layerData.config.height;
                                totalChars++;
                            }
                            else if(char == ' ') {
                                totalWidth += layerData.config.spaceWidth;
                                totalChars++;
                            }
                        }
                        totalWidth += spacing * (totalChars - 1);
                        let scaleRatio = 1;
                        if(totalWidth > layerData.config.width) {
                            scaleRatio = layerData.config.width / totalWidth;
                        }
                        const startX = layerData.config.left + (layerData.config.width - totalWidth * scaleRatio) / 2;
                        const startY = layerData.config.top + layerData.config.height * (1 - scaleRatio) / 2;
                        let currentStartX = startX;
                        for(const i in text) {
                            const obj = layerData.objects[i * 2 + 1];
                            const char = text[i];
                            if(obj) {
                                const scale = layerData.config.height * scaleRatio / obj.height;
                                obj.set({
                                    left: currentStartX,
                                    top: startY,
                                    scaleX: scale,
                                    scaleY: scale
                                });
                                layerData.value.characters[i] = {
                                    char: text[i],
                                    config: {
                                        left: currentStartX,
                                        top: startY,
                                        width: obj.width * scale,
                                        height: obj.height * scale
                                    },
                                    imgURL: null
                                };
                                currentStartX += (obj.width / obj.height * layerData.config.height + spacing) * scaleRatio;
                            }
                            else if(char == ' ') {
                                const width = (layerData.config.spaceWidth + spacing) * scaleRatio;
                                layerData.value.characters[i] = {
                                    char: ' ',
                                    config: {
                                        left: currentStartX,
                                        top: startY,
                                        width: width,
                                        height: 0
                                    },
                                    imgURL: null
                                };
                                currentStartX += width;
                            }
                        }
                    };
                    const loadImageAt = (i) => {
                        const char = text[i].toUpperCase();
                        App.editors.photoText.initEditor(i);
                        App.editors.photoText.get(i).setLayer(layerData);
                        App.editors.photoText.get(i).setIndex(i);
                        if(char > 'Z' || char < 'A') return;
                        const rawImgURL = layerData.config.images[char];
                        const imgURL = `${App.productRoot}/${App.productName}/${rawImgURL}`;
                        const messageID = uuidv4();
                        VueApp.$store.commit('setCanvasLoaderMessage', [messageID, 'Loading images ...']);
                        const handleSelectImage = () => {
                            const selected = !!layerData.objects[i * 2];
                            const editor = App.editors.photoText.get(i);
                            if(!selected) {
                                App.editors.imageGallery.unbindAll();
                                App.editors.imageGallery.onImageSelect(file => {
                                    file.onThumbnailReady(() => {
                                        layerData.files[i] = file;
                                        editor.setImage(file.displayURL);
                                        App.editors.imageGallery.close();
                                        editor.launch();
                                    });
                                });
                                App.editors.imageGallery.launch();
                            }
                            else {
                                editor.launch();
                            }
                        };
                        App.helper.loadSVG(imgURL, svg => {
                            svg.set({
                                fill: '#333d',
                                perPixelTargetFind: true,
                                stroke: layerData.config.strokeColor ?? '#000000',
                                strokeWidth: layerData.config.strokeWidth ?? 5,
                                strokeUniform: true,
                                paintFirst: "stroke",
                            });
                            App.helper.lockObject(svg);
                            layerData['svgPath_'+i] = svg.d;
                            layerData.objects[i * 2 + 1] = svg;
                            App.canvas.onClick(svg, () => {
                                handleSelectImage();
                            });
                            App.editors.photoText.get(i).initEditButton();
                            imagesCalib();
                            App.canvas.refresh();
                            VueApp.$store.commit('setCanvasLoaderMessage', [messageID]);
                        });
                    };
                    const handleAutofill = () => {
                        /*
                        Get all files
                        */
                        const allFiles = App.editors.imageGallery.getUploadedFiles();
                        if(allFiles.length == 0) return;
                        const chosenFiles = [];
                        const isAllFilesChosen = () => chosenFiles.length == allFiles.length;
                        const randomUnchosenFile = () => {
                            const unchosenFiles = allFiles.filter(file => !chosenFiles.includes(file));
                            const randomIdx = parseInt(Math.random() * unchosenFiles.length);
                            chosenFiles.push(unchosenFiles[randomIdx]);
                            return unchosenFiles[randomIdx];
                        };
                        const randomFile = () => {
                            const randomIdx = parseInt(Math.random() * allFiles.length);
                            return allFiles[randomIdx];
                        };
                        let cropped     = 0;
                        let needToCrop  = 0;
                        for(let i = 0; i < layerData.objects.length / 2; ++i) {
                            const e = App.editors.photoText.get(i);
                            if(!layerData.objects[i * 2 + 1]) continue;
                            needToCrop      += 1;
                            const editor    = App.editors.photoText.get(i);
                            let file        = isAllFilesChosen() ? randomFile() : randomUnchosenFile();
                            layerData.files[i] = file;

                            file.onThumbnailReady(() => {
                                editor.setImage(file.displayURL);
                                App.layerProcessor.photoText(layerData, img => {
                                    layerData.objects[i * 2] = img;

                                    layerData.data['cropboxX_'+i] = null;
                                    layerData.data['cropboxY_'+i] = null;
                                    layerData.data['cropboxSpanX_'+i] = null;
                                    layerData.data['cropboxSpanY_'+i] = null;

                                    App.helper.applyCropboxData_forPhotoText(layerData, i);
                                    App.canvas.refresh();
                                    cropped += 1;
                                    if(cropped == needToCrop) {
                                        App.helper.scrollTop();
                                    }
                                }, {
                                    index: i
                                });
                            });
                        }
                    };
                    localEventHandlers.loadImageAt = loadImageAt;
                    localEventHandlers.handleAutofill = handleAutofill;
                    for(let i = 0; i < text.length; ++i) {
                        const char = text[i].toUpperCase();
                        if(char > 'Z' || char < 'A') continue;
                        loadImageAt(i);
                        const e = App.editors.photoText.get(i);
                        if(e) {
                            e.onApply(() => {
                                const cropboxData = e.getCropboxData();
                                App.layerProcessor.photoText(layerData, img => {
                                    layerData.objects[i * 2] = img;
                                    layerData.data['cropboxX_'+i]     = cropboxData.cropX;
                                    layerData.data['cropboxY_'+i]     = cropboxData.cropY;
                                    layerData.data['cropboxSpanX_'+i] = cropboxData.cropSpanX;
                                    layerData.data['cropboxSpanY_'+i] = cropboxData.cropSpanY;
                                    layerData.data['cropboxScaleX_'+i] = cropboxData.scaleX;
                                    layerData.data['cropboxScaleY_'+i] = cropboxData.scaleY;
                                    layerData.data['cropboxRotation_'+i] = cropboxData.rotation;
                                    App.helper.applyCropboxData_forPhotoText(layerData, i);
                                    App.canvas.refresh();
                                }, {index: i});
                                e.close();
                            });
                            e.onChangeImage(() => {
                                e.close();
                                App.editors.imageGallery.unbindAll();
                                App.editors.imageGallery.onImageSelect(file => {
                                    file.onThumbnailReady(() => {
                                        layerData.files[i] = file;
                                        e.setImage(file.displayURL);
                                        layerData.data['cropboxX_'+i] = null;
                                        layerData.data['cropboxY_'+i] = null;
                                        layerData.data['cropboxSpanX_'+i] = null;
                                        layerData.data['cropboxSpanY_'+i] = null;
                                        layerData.data['cropboxScaleX_'+i] = null;
                                        layerData.data['cropboxScaleY_'+i] = null;
                                        layerData.data['cropboxRotation_'+i] = null;
                                        App.editors.imageGallery.close();
                                        e.launch();
                                    });
                                });
                                App.editors.imageGallery.launch();
                            });
                        }
                    }
                };
                editor.displayError('');
                renderText(layerData.config.defaultText ?? 'abc');
                editor.onApply(() => {
                    const text = editor.getText();
                    for(const c of text) {
                        const C = c.toUpperCase();
                        if(C != ' ' && layerData.config.images[C] == undefined) {
                            return editor.displayError(`Not allowed character: ${c}`);
                        }
                    }
                    if(text.length < layerData.config.minCharacters) {
                        editor.displayError('Text too short');
                    }
                    else if(text.length > layerData.config.maxCharacters) {
                        editor.displayError('Text too long');
                    }
                    else {
                        editor.displayError('');
                        renderText(text);
                    }
                });
                App.events.on('setClipartLetterIndex', ({position, index}) => {
                    localEventHandlers.loadImageAt(position, index);
                });
                App.editors.imageGallery.onAutofill(() => {
                    localEventHandlers.handleAutofill && localEventHandlers.handleAutofill();
                });
            }
                break;
        }
    }
    (function textReadyChecker() {
        let nrTexts = App.layers.filter(l => l.type == 'text' && l.typeMetadata.module != 'song_lyrics').length,
            nrTextReady = 0;
        const textReadyHandler = () => {
            nrTextReady++;
            if(nrTextReady >= nrTexts) {
                App.events.emit('allTextsReady');
            }
        };
        App.events.on('textLayerReady', textReadyHandler, { timeTravel: true });
        App.events.one('beforeProductLoad', () => { App.events.remove('textLayerReady', textReadyHandler)});
        if(!nrTexts) {
            textReadyHandler();
        }
    })();
    App.events.on('layerPropChange', ({layer, propName, propValue}) => {
        const cropboxMutation = () => {
            if(propValue == undefined) return;
            if(layer.type == 'photo-text') {
                const idx = +propName.split('_')[1];
                if(isNaN(idx)) return;
                App.helper.applyCropboxData_forPhotoText(layer, idx);
            }
            else {
                App.helper.applyCropboxData(layer);
            }
        };
        const mutations = {
            visible() {
                for(const object of layer.objects) {
                    object && object.set({
                        visible: propValue
                    });
                }
            },
            left() {
                for(const object of layer.objects) {
                    object && object.set({
                        left: propValue
                    });
                    if(layer.type == 'text') {
                        object && object.set({
                            left: layer.data.left + layer.config.width / 2
                        });
                    }
                }
            },
            top() {
                for(const object of layer.objects) {
                    object && object.set({
                        top: propValue
                    });
                    if(layer.type == 'text') {
                        object && object.set({
                            top: layer.data.top + (layer.config.height - object.height) / 2
                        });
                    }
                }
            },
            width() {
                for(const object of layer.objects) {
                    object && object.set({
                        width: propValue
                    });
                }
            },
            height() {
                for(const object of layer.objects) {
                    object && object.set({
                        height: propValue
                    });
                }
            },
            opacity() {
                for(const object of layer.objects) {
                    object && object.set({
                        opacity: propValue
                    });
                }
                App.canvas.refresh();
            },
            textValue() {
                for(const object of layer.objects) {
                    object && App.helper.setTextObjectValue(object, propValue, layer);
                }
                App.canvas.refresh();
                layer.editors[0]?.adjustEditButton && layer.editors[0].adjustEditButton();
            }
        };
        mutations[propName] && mutations[propName]();
        if(propName.includes('cropbox')) {
            cropboxMutation();
        }
    });
    App.events.one('allTextsReady', () => {
        App.events.on('spotifyInfoReady', ({title, artist, duration, duration_ms}) => {
            for(const layer of App.layers) {
                const type = layer.type;
                const module = layer.typeMetadata.module;
                const role = layer.typeMetadata.role;
                if(module == 'spotify' && role == 'title') {
                    layer.data.textValue = title;
                    App.canvas.refresh();
                    layer.editors[0]?.adjustEditButton && layer.editors[0].adjustEditButton();
                }
                if(module == 'spotify' && role == 'artist') {
                    layer.data.textValue = artist;
                    App.canvas.refresh();
                    layer.editors[0]?.adjustEditButton && layer.editors[0].adjustEditButton();
                }
                if(module == 'spotify' && role == 'duration') {
                    layer.data.textValue = duration;
                    App.canvas.refresh();
                    layer.editors[0]?.adjustEditButton && layer.editors[0].adjustEditButton();
                }
                if(module == 'spotify' && role == 'passedTime') {
                    const timePassed = duration_ms * (+layer.typeMetadata.percent) / 100;
                    const formattedTime = App.helper.formatDuration(timePassed);
                    layer.data.textValue = formattedTime;
                    App.canvas.refresh();
                    layer.editors[0]?.adjustEditButton && layer.editors[0].adjustEditButton();
                }
            }
            App.helper.scrollTop();
        }, { timeTravel: true });
    }, { timeTravel: true });
    App.editors.images.onOpenGallery(() => {
        App.editors.imageGallery.launch();
    });
    App.editors.imageGallery.onAutofill(() => {
        const allFiles = App.editors.imageGallery.getUploadedFiles();
        if(allFiles.length == 0) return;
        const chosenFiles = [];
        const isAllFilesChosen = () => chosenFiles.length == allFiles.length;
        const randomUnchosenFile = () => {
            const unchosenFiles = allFiles.filter(file => !chosenFiles.includes(file));
            const randomIdx = parseInt(Math.random() * unchosenFiles.length);
            chosenFiles.push(unchosenFiles[randomIdx]);
            return unchosenFiles[randomIdx];
        };
        const randomFile = () => {
            const randomIdx = parseInt(Math.random() * allFiles.length);
            return allFiles[randomIdx];
        };
        let cropped     = 0;
        let needToCrop  = 0;
        for(const layerData of App.layers) {
            if(layerData.type != 'mask') continue;
            needToCrop      += 1;
            const ratio     = layerData.data.width / layerData.data.height;
            const editor    = layerData.editors[0];
            let file        = isAllFilesChosen() ? randomFile() : randomUnchosenFile();
            layerData.file  = file;
            
            file.onThumbnailReady(() => {
                editor.setImage(file.displayURL);
                App.layerProcessor.image(layerData, img => {
                    const layerImageRatio = layerData.config.width / layerData.config.height;
                    const imgRatio = img.width / img.height;
                    let scaleRatio;
                    if(imgRatio > layerImageRatio) {
                        scaleRatio = layerData.config.height / img.height;
                        img.scaleToHeight(layerData.config.height);
                    }
                    else {
                        scaleRatio = layerData.config.width / img.width;
                        img.scaleToWidth(layerData.config.width);
                    }
                    if(layerData.config.keepRatio == false) {
                        App.helper.scaleTo(img, {
                            width: layerData.config.width,
                            height: layerData.config.height
                        });
                    }
                    img.set({
                        left: layerData.config.left + layerData.config.width / 2 - img.width * scaleRatio / 2,
                        top: layerData.config.top + layerData.config.height / 2 - img.height * scaleRatio / 2
                    });
                    layerData.objects[0] = img;
                    layerData.objects.splice(2, 1);

                    layerData.data.cropboxX = null;
                    layerData.data.cropboxY = null;
                    layerData.data.cropboxSpanX = null;
                    layerData.data.cropboxSpanY = null;

                    App.helper.applyCropboxData(layerData);
                    App.canvas.refresh();
                    cropped += 1;
                    if(cropped == needToCrop) {
                        App.helper.scrollTop();
                    }
                });
            });
        }
        App.editors.imageGallery.close();
    });
    App.canvas.refresh();
    // end function
    for(const layer of App.layers) {
        const required = layer.typeMetadata.required == 'true';
        const module = layer.typeMetadata.module;
        const type = layer.type;
        const loadSpotifySong = (uri) => {
            const defaultSpotifyURI = uri ?? layer.config.data.defaultSpotifyURI;
            const svgBackgroundColor = layer.config.data.backgroundColor;
            const svgFillColor = layer.config.data.fillColor;
            const trackID = (defaultSpotifyURI.match(/track(s)?(\:|\/)[a-zA-Z0-9]+/g) ?? [''])[0].replace(/track(s)?(\:|\/)/g, '');
            if(!trackID) return;
            const trackURI = `spotify:track:${trackID}`;
            layer.spotifyURI = trackURI;
            const codeMessageID = uuidv4();
            VueApp.$store.commit('setCanvasLoaderMessage', [codeMessageID, 'Loading spotify code ...']);
            $.ajax({
                url: `https://scannables.scdn.co/uri/plain/svg/000000/white/640/${trackURI}`,
                method: 'get',
            })
            .done(res => {
                res = new XMLSerializer().serializeToString(res);
                const svg = res.replaceAll('#000000', '#backgroundColor').replaceAll('#ffffff', '#fillColor')
                    .replaceAll('#backgroundColor', svgBackgroundColor)
                    .replaceAll('#fillColor', svgFillColor);
                App.helper.createFabricSVG(svg, object => {
                    object.scaleToHeight(layer.config.height);
                    const scaleFactor = layer.config.height / object.height
                    object.set({
                        left: layer.config.left + (layer.config.width - object.width * scaleFactor) / 2,
                        top: layer.config.top,
                    });
                    App.canvas.onClick(object, () => {
                        App.editors.spotify.setCollapse(false);
                        setTimeout(() => {
                            App.editors.spotify.child.focus();
                        }, 100);
                    });
                    layer.objects = [object];
                });
            })
            .always(() => {
                VueApp.$store.commit('setCanvasLoaderMessage', [codeMessageID]);
            });
            const infoMessageID = uuidv4();
            VueApp.$store.commit('setCanvasLoaderMessage', [infoMessageID, 'Loading spotify track info ...']);
            $.ajax({
                url: `https://dev.goodsmize.com/design-upload/services/spotify-api/tracks/${trackID}`,
                method: 'get'
            })
            .done(res => {
                const durationMs = res.data.duration_ms;
                const duration = App.helper.formatDuration(durationMs);
                const title = res.data.name;
                const artist = res.data.artists[0].name;
                App.events.emit('spotifyInfoReady', {
                    title: title,
                    artist: artist,
                    duration: duration,
                    duration_ms: durationMs
                });
            })
            .fail(err => {
                Toastify({
                    text: `Failed to load song! ID=${trackID}`,
                    duration: 3000,
                    close: true,
                    gravity: "top", // `top` or `bottom`
                    position: "left", // `left`, `center` or `right`
                    backgroundColor: "#bd2300",
                    stopOnFocus: true, // Prevents dismissing of toast on hover
                    onClick: function(){} // Callback after click
                }).showToast();
            })
            .always(() => {
                VueApp.$store.commit('setCanvasLoaderMessage', [infoMessageID]);
            });
        };
        if(type == "custom" && module == 'spotify') {
            App.editors.spotify.init();
            App.editors.spotify.child.setText(layer.config.data.defaultSpotifyURI);
            App.editors.spotify.child.onTextChange((newText, eventType) => {
                if(eventType != 'change') return;
                loadSpotifySong(newText);
            });
            loadSpotifySong();
            App.editors.spotify.child.onSearchInputChange(query => {
                $.ajax({
                    url: `https://dev.goodsmize.com/design-upload/services/spotify-api/search?q=${query}`,
                    method: 'get',
                })
                .done(res => {
                    if(!query) {
                        App.editors.spotify.child.setSearchResults({
                            tracks: {items: []}
                        });
                    }
                    App.editors.spotify.child.setSearchResults(res.data);
                });
            });
            App.editors.spotify.child.onSearchResultSelect(uri => {
                App.editors.spotify.child.setText(uri);
            });
        }
        if(type == "custom" && module == 'spiral') {
            App.editors.song.init();
            App.editors.spiral.init();

            let searchVersion = 0;
            App.editors.song.onSearchInputChange(query => {
                searchVersion++;
                const version = searchVersion;
                if(query) {
                    $.ajax({
                        url: `https://dev.goodsmize.com/genius-lyrics/search?q=${encodeURI(query)}`,
                        method: 'get'
                    })
                    .then(response => {
                        if(version == searchVersion) {
                            App.editors.song.setSearchResults(response.data.map(item => ({
                                uri: item.result.url,
                                artist: item.result.primary_artist.name,
                                title: item.result.title
                            })));
                        }
                    });
                }
                else {
                    App.editors.song.setSearchResults([]);
                }
            });
            let lyricsVersion = 0;
            App.editors.song.onSearchResultSelect(({uri, artist, title}) => {
                layer.value = {uri, artist, title};
                const songTitleLayer = App.layers.filter(l => l.typeMetadata.module == 'song_title')[0];
                if(songTitleLayer) {
                    songTitleLayer.data.textValue = title;
                }
                lyricsVersion++;
                const version = lyricsVersion;
                const messageID = uuidv4();
                VueApp.$store.commit('setCanvasLoaderMessage', [messageID, 'Getting lyrics ...']);
                App.editors.song.switchEditMode();
                $.ajax({
                    url: `https://dev.goodsmize.com/genius-lyrics/lyrics?url=${uri}`,
                    method: 'get'
                })
                .done(response => {
                    if(version == lyricsVersion) {
                        const lyrics = response.data;
                        App.editors.song.setLyrics(lyrics);
                        $('.song-group .apply-btn').trigger('click');
                    }
                })
                .always(() => {
                    VueApp.$store.commit('setCanvasLoaderMessage', [messageID]);
                })
            });

            const setText = lyrics => {
                const characterLimit = layer.config.spiral.characterLimit;
                if(characterLimit && lyrics.length > characterLimit) {
                    App.editors.spiral.child.setText(lyrics.slice(0, characterLimit) + '...');
                }
                else {
                    App.editors.spiral.child.setText(lyrics);
                }
                App.editors.song.setLyrics(lyrics);
            }

            App.editors.song.onApply(lyrics => {
                setText(lyrics);
            });

            setText(layer.config.spiral.text);
            App.editors.spiral.child.onTextChange((newText, eventType) => {
                if(eventType == 'change')
                    loadSpiral();
            });
            function loadSpiral() {
                const base64 = App.helper.getSpiralTextBase64({
                    width: layer.config.width,
                    height: layer.config.height,
                    text: App.editors.spiral.child.getValue(),
                    fontSize: layer.config.spiral.fontSize,
                    fontFamily: fontName,
                    letterSpacing: layer.config.spiral.letterSpacing,
                    start: layer.config.spiral.startRadius,
                    color: layer.config.spiral.color ?? '#000000'
                });
                fabric.Image.fromURL(base64, img => {
                    img.set({
                        left: layer.data.left,
                        top: layer.data.top
                    });
                    img.scaleToWidth(layer.config.width);
                    img.scaleToHeight(layer.config.height);
                    if(layer.config.keepRatio == false) {
                        App.helper.scaleTo(img, {
                            width: layer.config.width,
                            height: layer.config.height
                        });
                    }
                    App.canvas.onClick(img, () => {
                        App.editors.song.setCollapse(false);
                        // App.editors.song.__groupEl.find('input').focus()
                        App.editors.song.launch();
                    });
                    App.helper.lockObject(img);
                    layer.objects[0] = img;
                    App.canvas.refresh();
                    App.events.emit('lyricsLoaded', layer);
                }, { crossOrigin: 'anonymous' });
            }
            const fontUrl = `${App.productRoot}/${App.productName}/${layer.config.spiral.fontPath}`;
            const fontName = layer.config.spiral.fontFamily;
            App.helper.loadFont(fontName, fontUrl, () => {
                loadSpiral();
            });
        }
        if(type == "custom" && module == 'square') {
            App.editors.square.init();
            App.editors.square.child.setTitle(layer.typeMetadata.label ?? 'Text');
            App.editors.square.child.setText(layer.config.square.text);
            App.editors.square.child.onTextChange((newText, eventType) => {
                if(eventType == 'change')
                    loadSquare();
            });
            function loadSquare() {
                $.ajax({
                    url: SQUARE_GENERATOR_URL,
                    method: 'post',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        lyrics: App.editors.square.child.getValue().replaceAll(/[\[\]\(\)]/g, ''),
                        color: layer.config.square.color
                    }),
                    crossDomain:true,
                    crossOrigin:true
                })
                .done(res => {
                    const url = res.data;
                    fabric.Image.fromURL(url, img => {
                        img.set({
                            left: layer.data.left,
                            top: layer.data.top,
                            evented: false,
                        });
                        img.scaleToWidth(layer.config.width);
                        if(layer.config.keepRatio == false) {
                            App.helper.scaleTo(img, {
                                width: layer.config.width,
                                height: layer.config.height
                            });
                        }
                        App.helper.lockObject(img);
                        layer.objects[0] = img;
                        App.canvas.refresh();
                    }, { crossOrigin: 'anonymous' });
                })
                .fail(err => {
                    Toastify({
                        text: `Failed to load image!`,
                        duration: 3000,
                        close: true,
                        gravity: "top", // `top` or `bottom`
                        position: "left", // `left`, `center` or `right`
                        backgroundColor: "#bd2300",
                        stopOnFocus: true, // Prevents dismissing of toast on hover
                        onClick: function(){} // Callback after click
                    }).showToast();
                });
            }
            loadSquare();
        }
        if(type == 'custom' && module == 'starmap') {
            App.editors.starmaps.init();
            const iframeID = uuidv4();
            $('#starmap-region').append(`<iframe id="iframe-${iframeID}" src="" frameborder="0"></iframe>`);
            const iframe = $(`#iframe-${iframeID}`);
            const editor = App.editors.starmaps.newEditor();
            layer.editors = [editor];
            layer.objects[1] = new fabric.Circle({
                left: layer.config.left,
                top: layer.config.top,                
                radius: Math.max(layer.config.width, layer.config.height) / 2,
                stroke: '#00000000',
                fill: '#00000001'
            });
            App.helper.lockObject(layer.objects[1]);
            App.canvas.refresh();
            App.canvas.onClick(layer.objects[1], () => {
                editor.modal.launch();
            });
            editor.modal.onApply(inputInfo => {
                App.events.emit('starmapApply', {
                    id: layer.typeMetadata.id,
                    data: inputInfo
                });
                layer.objects[0] = undefined;
                loadStarmap(inputInfo);
                editor.modal.close();
            });

            // generate Button Editor
            App.editors.starmaps.generateAction(`iframe-${iframeID}`);
            App.editors.starmaps.onOpenStarmap(`iframe-${iframeID}`, (data) => {
                editor.modal.launch();
            });

            // App.editors.starmaps
            function loadStarmap(inputInfo, importData) {
                const lat = importData?.lat ??
                    (inputInfo.place ? inputInfo.place.geometry.location.lat() : 50);
                const lng = importData?.lng ??
                    (inputInfo.place ? inputInfo.place.geometry.location.lng() : 10);
                const date = importData?.date ?? (inputInfo.date);
                const time = importData?.time ?? (inputInfo.time);
                const isDateValid = date.match(/^[0-9]{4}\/[0-9]{1,2}\/[0-9]{1,2}$/g);
                const isTimeValid = time.match(/^[0-9]{2}\:[0-9]{2}\:[0-9]{2}$/g);
                if(!isDateValid) {
                    Toastify({
                        text: `Invalid date: ${date}`,
                        duration: 3000,
                        close: true,
                        gravity: "top", // `top` or `bottom`
                        position: "left", // `left`, `center` or `right`
                        backgroundColor: "#bd2300",
                        stopOnFocus: true, // Prevents dismissing of toast on hover
                        onClick: function(){} // Callback after click
                    }).showToast();
                    return;
                }
                if(!isTimeValid) {
                    Toastify({
                        text: `Invalid time: ${time}`,
                        duration: 3000,
                        close: true,
                        gravity: "top", // `top` or `bottom`
                        position: "left", // `left`, `center` or `right`
                        backgroundColor: "#bd2300",
                        stopOnFocus: true, // Prevents dismissing of toast on hover
                        onClick: function(){} // Callback after click
                    }).showToast();
                    return;
                }
                const url = `https://storage.navitee.com/starapi/?lat=${lat}&long=${lng}&date=${date}&time=${time}&iframe_id=${iframeID}`;
                iframe.attr('src', url);
                VueApp.$store.commit('setCanvasLoaderMessage', [iframeID, 'Loading starmap ...']);
            }
            $(window).on('message', event => {
                const data = event.originalEvent.data;
                if(event.originalEvent.origin == 'https://storage.navitee.com' && data.name == 'preview_star' 
                    && data.data.iframe_id == iframeID
                    ) {
                    const imgUrl = data.data.file;
                    fabric.Image.fromURL(imgUrl, img => {
                        img.set({
                            left: layer.data.left,
                            top: layer.data.top,
                            evented: false,
                        });
                        img.scaleToWidth(layer.config.width);
                        img.scaleToHeight(layer.config.height);
                        if(layer.config.keepRatio == false) {
                            App.helper.scaleTo(img, {
                                width: layer.config.width,
                                height: layer.config.height
                            });
                        }
                        App.helper.lockObject(img);
                        layer.objects[0] = img;
                        VueApp.$store.commit('setCanvasLoaderMessage', [iframeID]);
                        App.canvas.refresh();
                        App.events.emit('starmapLoaded', layer);
                    }, { crossOrigin: 'anonymous' });
                }
            });
            loadStarmap(editor.modal.inputInfo);
            App.events.on('importStarmap', layer => {
                loadStarmap(editor.modal.inputInfo, layer.importData);
            });
        }
        if(type == 'custom' && module == 'moonphase') {
            App.editors.moonphases.init();
            const editorID = uuidv4();
            const editor = App.editors.moonphases.newEditor();
            layer.editors = [editor];
            layer.objects[1] = new fabric.Rect({
                left: layer.config.left,
                top: layer.config.top,
                width: layer.config.width,
                height: layer.config.height,
                stroke: '#00000000',
                fill: '#00000001'
            });
            App.helper.lockObject(layer.objects[1]);
            App.canvas.refresh();
            App.canvas.onClick(layer.objects[1], () => {
                editor.modal.launch();
            });
            const phasesImageURLs = layer.config.moonphase.images;
            const nrPhases = phasesImageURLs.length;
            const phaseLength = 1 / nrPhases;
            function loadPhase(phaseNumber) {
                const phaseImageURL = `${App.productRoot}/${App.productName}/${phasesImageURLs[phaseNumber]}`;
                const messageID = uuidv4();
                VueApp.$store.commit('setCanvasLoaderMessage', [messageID, 'Loading moon ...']);
                fabric.Image.fromURL(phaseImageURL, img => {
                    img.set({
                        left: layer.data.left,
                        top: layer.data.top,
                        evented: false,
                    });
                    img.scaleToWidth(layer.config.width);
                    img.scaleToHeight(layer.config.height);
                    if(layer.config.keepRatio == false) {
                        App.helper.scaleTo(img, {
                            width: layer.config.width,
                            height: layer.config.height
                        });
                    }
                    App.helper.lockObject(img);
                    layer.objects[0] = img;
                    App.canvas.refresh();
                    VueApp.$store.commit('setCanvasLoaderMessage', [messageID]);
                }, { crossOrigin: 'anonymous' });
            }
            loadPhase(
                (editor.modal.moonphase / phaseLength + 0.5 >>> 0) % nrPhases   
            )
            editor.modal.onApply(phase => {
                layer.objects[0] = undefined;
                const phaseNumber = (phase / phaseLength + 0.5 >>> 0) % nrPhases;
                loadPhase(phaseNumber);
                editor.modal.close();
            });

            // generate Button Editor
            App.editors.moonphases.generateAction(`editor-${editorID}`);
            App.editors.moonphases.onOpenMoon(`editor-${editorID}`, (data) => {
                editor.modal.launch();
            });
        }
    }

    if(App.editors.theme.initialized) {
        App.editors.theme.onChooseTheme(filterName => {
            for(const layer of App.layers) {
                if(layer.type != 'mask') continue;
                const imgObj = layer.objects[0];
                if(!imgObj) continue;
                if(!layer.file) continue;
                const messageID = uuidv4();
                
                if(App.editors.theme.selectedFilter) {
                    VueApp.$store.commit('setCanvasLoaderMessage', [messageID, 'Applying filter ...']);
                }
                App.layerProcessor.image(layer, img => {
                    if(layer.objects[0]) {
                        App.canvas.instance.remove(layer.objects[0]);
                    }
                    layer.objects[0] = img;
                    App.helper.applyCropboxData(layer);
                    VueApp.$store.commit('setCanvasLoaderMessage', [messageID]);
                    App.canvas.refresh();
                });
            }
        });
    }

    (function heartShapedPostprocess() {
        if(App.mode('debug')) return;
        // const songTitleLayer = App.layers.filter(l => l.typeMetadata.module == 'song_title')[0];
        // if(songTitleLayer) {
        //     songTitleLayer.editors[0].__el.hide();
        // }

        // $($('.song-group .song-search').parent().children()[1]).hide();
        // $($('.song-group .song-search').parent().children()[2]).hide();
        // $($('.song-group .song-search').parent().children()[3]).hide();
        // $($('.song-group .song-search').parent().children()[3]).find('button').hide();
    })();

    App.editors.texts.fetch();

    // resume
    try {
        App.events.one('allTextsReady', () => {
            const resumeData = JSON.parse(window.localStorage.getItem('resume_' + App.productName));
            const hasTextLayer      = !!App.layers.filter(l => l.type == 'text').length;
            const hasMaskLayer      = !!App.layers.filter(l => l.type == 'mask').length;
            const hasSpotifyLayer   = !!App.layers.filter(l => l.typeMetadata.module == 'spotify').length;
            const hasMapLayer       = !!App.layers.filter(l => l.type == 'map-mask').length;
            const hasBgColorLayer   = !!App.layers.filter(l => l.type == 'bgcolor').length;
            const isExpired         = !!resumeData
                ? (new Date).getTime() - (resumeData.timestamp ?? 0) > 3600000
                : true
                ;
            (!!resumeData) && (hasTextLayer || hasMaskLayer || hasSpotifyLayer || hasMapLayer || hasBgColorLayer) && !isExpired
                && App.helper.showConfirmPopup({
                    title: 'We\'ve found an autosaved from your last session!<br>Would you like to continue editing this project?',
                    confirmText: 'Continue editing',
                    denyText: 'Start a new one'
                }, isConfirmed => {
                    if(isConfirmed) App.helper.resume();
                    else {
                        window.localStorage.removeItem('resume_' + App.productName);
                    }
                });
        }, { timeTravel: true });
    }
    catch(e) {
        console.error(e);
    }

    // UX
    // if(!App.isMobile) {
        setTimeout(() => {
            $('.group:visible:eq(0):not(.active) .group-title').click();
        }, 500);
    // }
};

const loadProduct = (pname) => {
    App.events.emit('beforeProductLoad');
    const urlParam = new URLSearchParams(window.location.search);
    const productName = pname ?? (urlParam.get('product') ?? '');
    if(!productName) return;

    const messageID = uuidv4();
    typeof VueApp != 'undefined' && VueApp.$store.commit('setCanvasLoaderMessage', [messageID, 'Loading subproduct ...']);

    App.editors.texts.initEditButtonsStyle();

    const preprocessLayers = (data) => {
        const parseLayerType = (layerType) => {
            const metaRaw = (layerType.match(/\[.*/g) ?? [''])[0];
            const meta = {};
            const result = {
                type: layerType.replace(metaRaw, ''),
                meta: meta
            };
            for (const fieldSet of [...metaRaw.matchAll(/[a-zA-Z0-9_-]+\:[\/$a-zA-Z0-9_-\s]+/g)]) {
                const [key, value] = fieldSet[0].split(':');
                meta[key] = value;
            }
            return result;
        };
        const layers = [];
        for (const [layerId, layer] of data.children.entries()) {
            const parsedLayerType = parseLayerType(layer.type);
            const layerData = {
                id: layerId,
                objects: [],
                editors: [],
                type: parsedLayerType.type,
                typeMetadata: parsedLayerType.meta,
                config: layer,
                value: null,
                data: {},
            };
            layers.push(layerData);
        }
        return layers;
    };
    const productHost = App.mode('design')
        ? 'http://localhost:3000/product-root'
        : 'https://dev.goodsmize.com/design-upload-v2/products'
        ;
    $.ajax({
        url: `${productHost}/${productName}/data.json`,
        type: 'get',
        dataType: 'json',
        cache: false
    })
    .done(res => {
        if(res.document) {
            App.document = res.document;
            App.init({ canvasWidth: res.document.width, canvasHeight: res.document.height, productName, layers: preprocessLayers(res) });
            typeof VueApp != 'undefined' && VueApp.$store.commit('setCanvasLoaderMessage', [messageID]);
            ctrlMain();
        }
        else if(res.options) {
            let styleGroup = $(`.group.option-group`);
            styleGroup.find('.select-group').html(res.options.map(option => `
                <div class="option">
                    <span data-slug="${option.slug}">${option.title}</span>
                </div>
            `).join(''));
            styleGroup.on('click', '.option', function() {
                loadProduct($(this).find('span').attr('data-slug'));
            });
            styleGroup.toggleClass('hidden', false);
            VueApp.$store.commit('showEditor', 'options');
            if(res.options[0]) styleGroup.find('.option:eq(0)').click();
        }
    })
    .fail(err => {
        console.log(err);
    });
};

// Entry point
(() => {
    $(document.body).css('--viewport-width', document.documentElement.clientWidth + 'px');
    $(document.body).css('--viewport-height', document.documentElement.clientHeight + 'px');

    App.events.register('beforeProductLoad');
    App.events.register('layerPropChange');
    App.events.register('spotifyInfoReady');
    App.events.register('textLayerReady');
    App.events.register('allTextsReady');
    App.events.register('bgcolorReady');
    App.events.register('canvasPositionChange');
    App.events.register('mapApply');
    App.events.register('starmapApply');
    App.events.register('starmapLoaded');
    App.events.register('importStarmap');
    App.events.register('importLyrics');
    App.events.register('lyricsLoaded');
    App.events.register('importClipartText');
    App.events.register('fakeFabricMouseUp');
    App.events.register('setClipartLetterIndex');
    App.events.register('selectBgcolor');

    if(!App.mode('debug') && !App.mode('production')) {
        console.log     = (...args) => {};
        console.error   = (...args) => {};
        console.warn    = (...args) => {};
        console.debug   = (...args) => {};
    }
    const isMobile = $(window).width() <= 992;
    App.isMobile = isMobile;
    if(isMobile) {
        $('body').toggleClass('is-mobile', true);
    }
    else {
        $('body').toggleClass('is-desktop', true);
    }

    const eventEmitter = new EventEmitter;
    App.events.on('beforeProductLoad', () => {
        eventEmitter.unbindAll();
    });
    App.events.on('beforeProductLoad', () => {
        App.editors.starmaps.reset();
    });

    $(document).on('click', '.group-title', function () {
        const isActive = $(this).closest('.group').hasClass('active');
        const groupBody = $(this).closest('.group').find('.group-body');
        $('.group').toggleClass('active', false);
        $(this).closest('.group').toggleClass('active', !isActive);
        for (const el of $('.group .group-body.collapsible')) {
            setCollapse(el, true);
        }
        setCollapse(groupBody[0], isActive);
    });

    $(document).on('click', '.subgroup-title', function () {
        const isActive = $(this).closest('.subgroup').hasClass('active');
        const subgroupBody = $(this).closest('.subgroup').find('.subgroup-body');
        $('.subgroup').toggleClass('active', false);
        $(this).closest('.subgroup').toggleClass('active', !isActive);
        for (const el of $('.subgroup .subgroup-body.collapsible')) {
            setCollapse(el, true);
        }
        setCollapse(subgroupBody[0], isActive);
    });

    $(document).on('click', '.select-group .option', function () {
        const isActive = $(this).hasClass('selected');
        $('.select-group .option').toggleClass('selected', false);
        $(this).toggleClass('selected', true);
    });

    $(document).on('click', '.alternative-dz-upload-btn', function () {
        $("#myAwesomeDropzone").click();
    });

    loadProduct();

    $(document).on('click', '.finish-btn', function() {
        if(!App.helper.checkEmptyText()) return;
        App.helper.showConfirmPopup(undefined, async (confirm) => {
            if(!confirm) return;
            VueApp.$store.commit('showPageLoader', 'Generating your image, please wait . . .');
            if(App.mode('design')) {
                VueApp.$store.commit('hidePageLoader');
                return App.helper.downloadURI(await App.canvas.exportBase64(true));
            }

            const exportedBase64    = await App.canvas.exportBase64();
            const blob              = await App.helper.imgResizeToBlob(exportedBase64, { width: 1200 });
            const uploadBase64Promise = App.helper.uploadBlobAWS(blob);
            const exportPromise     = App.canvas.export();

            const res               = await uploadBase64Promise;
            const exportedJSON      = JSON.stringify(await exportPromise);

            window.localStorage.setItem('resume_' + App.productName, exportedJSON);

            const imgUrl = res.data;

            $.ajax({
                url: 'https://dev.goodsmize.com/design-upload-v2/item-exports',
                method: 'post',
                contentType: 'application/json',
                data: JSON.stringify({
                    filename: res.hash,
                    data: JSON.parse(exportedJSON)
                })
            })
            .then((res) => {
                // $.ajax({
                //     url: `https://dev.goodsmize.com/customize-export?previewURL=${imgUrl}`,
                //     type: 'get'
                // })
                // .always(() => {
                //     parent.postMessage({
                //         name: "previewImg",
                //         data: {
                //             file: imgUrl,
                //         }
                //     }, "*");
                // });
                parent.postMessage({
                    name: "previewImg",
                    data: {
                        file: imgUrl,
                    }
                }, "*");
            })
            .fail((e) => {
                console.log(e);
            })
            .always(() => {
                VueApp.$store.commit('hidePageLoader');
            });
        });
    });
})();

// DEV ZONE
(() => {
})();

export {
    App
};
