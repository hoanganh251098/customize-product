const MAPBOX_API = {
    username: 'nafchan00',
    accessToken: 'pk.eyJ1IjoibmFmY2hhbjAwIiwiYSI6ImNraHpxMXdwejB2Z3gyeWtiNzZ2ZWFvYmcifQ.8dfBdxpPoYKv32GEQGeBMQ',
    style: 'mapbox://styles/mapbox/emerald-v8'
};

const MAPBOX_STYLES = {
    default: {
        styleUrl: "mapbox://styles/mapbox/emerald-v8",
        username: "nafchan00",
        accessToken:
            "pk.eyJ1IjoibmFmY2hhbjAwIiwiYSI6ImNraHpxMXdwejB2Z3gyeWtiNzZ2ZWFvYmcifQ.8dfBdxpPoYKv32GEQGeBMQ",
        placeholder: "https://dev.goodsmize.com/design-upload-v2/resources/mapbox-placeholder/default.png"
    },
    white: {
        styleUrl: "mapbox://styles/hfnuser0000/ckqg2ppnu0emo17vmqbqn03k4",
        username: "hfnuser0000",
        accessToken:
            "pk.eyJ1IjoiaGZudXNlcjAwMDAiLCJhIjoiY2tuNnJmeXA3MGdycTJ3bWw2M2l6eXVyNSJ9.ABZHs1qooTswkFSLQqk-8Q",
        placeholder: "https://dev.goodsmize.com/design-upload-v2/resources/mapbox-placeholder/white.png"
    },
    street: {
        styleUrl: "mapbox://styles/mapbox/streets-v11",
        username: "nafchan00",
        accessToken:
            "pk.eyJ1IjoibmFmY2hhbjAwIiwiYSI6ImNraHpxMXdwejB2Z3gyeWtiNzZ2ZWFvYmcifQ.8dfBdxpPoYKv32GEQGeBMQ",
        placeholder: "https://dev.goodsmize.com/design-upload-v2/resources/mapbox-placeholder/street.png"
    },
    vintage: {
        styleUrl: "mapbox://styles/hfnuser0000/ckqixxt9204ax17pn30h1bem0",
        username: "hfnuser0000",
        accessToken:
            "pk.eyJ1IjoiaGZudXNlcjAwMDAiLCJhIjoiY2tuNnJmeXA3MGdycTJ3bWw2M2l6eXVyNSJ9.ABZHs1qooTswkFSLQqk-8Q",
        placeholder: "https://dev.goodsmize.com/design-upload-v2/resources/mapbox-placeholder/vintage.png"
    },
    worth_britt: {
        styleUrl: "mapbox://styles/hfnuser0000/ckqiy89jn06qu18oogi4bjjmc",
        username: "hfnuser0000",
        accessToken:
            "pk.eyJ1IjoiaGZudXNlcjAwMDAiLCJhIjoiY2tuNnJmeXA3MGdycTJ3bWw2M2l6eXVyNSJ9.ABZHs1qooTswkFSLQqk-8Q",
        placeholder: "https://dev.goodsmize.com/design-upload-v2/resources/mapbox-placeholder/worth_britt.png"
    },
    north_star: {
        styleUrl: "mapbox://styles/hfnuser0000/ckqgdo2ag09d718peyb2dukes",
        username: "hfnuser0000",
        accessToken:
            "pk.eyJ1IjoiaGZudXNlcjAwMDAiLCJhIjoiY2tuNnJmeXA3MGdycTJ3bWw2M2l6eXVyNSJ9.ABZHs1qooTswkFSLQqk-8Q",
        placeholder: "https://dev.goodsmize.com/design-upload-v2/resources/mapbox-placeholder/north_star.png"
    }
};

export {
    MAPBOX_API,
    MAPBOX_STYLES
};


