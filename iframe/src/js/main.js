import '../css/style-dev.css';
import Store from './store';
import $ from 'jquery';

const eventEmitter = Store.state.eventEmitter;

Store.watch(
    state => state.eventEmitter.listenerCount,
    function() {
        console.log(`Global event emitter nr listeners: ${eventEmitter.listenerCount}`);
    }
);

// On SKU change -- load parent product
Store.watch(
    state => state.productInfo.sku,
    function() {
        const sku = Store.state.productInfo.sku;
        $.ajax({
            url: `https://customize.navitee.com/design-upload/products/${sku}/data.json`,
            type: 'get',
            dataType: 'json',
            cache: false
        })
        .done(res => {
            Store.commit('setProduct', res);
        });
    }
);

// On parent product change
Store.watch(
    state => state.productInfo.product,
    function() {
        const sku = Store.state.productInfo.sku;
        const productData = Store.state.productInfo.product;
        if(productData.document) {
            Store.commit('setSubproductSKU', sku);
            return;
        }
        const options = productData.options;
        if(options.length == 0) return;
        Store.commit('setSubproductSKU', options[0].slug);
    }
);

// On subproduct SKU change
Store.watch(
    state => state.productInfo.subproductSKU,
    function() {
        const parentSKU = Store.state.productInfo.sku;
        const sku = Store.state.productInfo.subproductSKU;
        if(sku != parentSKU) {
            $.ajax({
                url: `https://customize.navitee.com/design-upload/products/${sku}/data.json`,
                type: 'get',
                dataType: 'json',
                cache: false
            })
            .done(res => {
                Store.commit('setSubproduct', res);
            });
            return;
        }
        Store.commit('setSubproduct', Store.state.productInfo.product);
    }
);

// On subproduct loaded
Store.watch(
    state => state.productInfo.subproduct,
    function() {
        const preprocessLayers = (data) => {
            const parseLayerType = (layerType) => {
                const metaRaw = (layerType.match(/\[.*/g) ?? [''])[0];
                const meta = {};
                const result = {
                    type: layerType.replace(metaRaw, ''),
                    meta: meta
                };
                for (const fieldSet of [...metaRaw.matchAll(/[a-zA-Z0-9_-]+\:[\/$a-zA-Z0-9_-\s]+/g)]) {
                    const [key, value] = fieldSet[0].split(':');
                    meta[key] = value;
                }
                return result;
            };
            const layers = [];
            for (const [layerId, layer] of data.children.entries()) {
                const parsedLayerType = parseLayerType(layer.type);
                const layerData = {
                    id: layerId,
                    objects: [],
                    editors: [],
                    type: parsedLayerType.type,
                    typeMetadata: parsedLayerType.meta,
                    config: layer,
                    value: null,
                    data: {},
                };
                layers.push(layerData);
            }
            return layers;
        };
        const subproductData = Store.state.productInfo.subproduct;
        const processedLayers = preprocessLayers(subproductData);
        Store.commit('setLayers', processedLayers);
    }
);

Store.watch(
    state => Store.getters.canvasLoader,
    function(value) {
        console.log(value)
    }
);


function main() {
    const urlParam = new URLSearchParams(window.location.search);
    const productSKU = urlParam.get('product');
    Store.commit('setProductSKU', productSKU);
}

main();
