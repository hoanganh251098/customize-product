<?php 
require 'vendor/autoload.php';
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic as Image;
Image::configure(array('driver' => 'imagick'));

$url        = $_GET['url'];

scaleImage($url);

function scaleImage($url) {
    
    $width      = isset($_GET['width']) ? isset($_GET['width']) : getimagesize($url)[0];
    
    $image = Image::make($url)->resize($width, null, function ($constraint) {
        $constraint->aspectRatio();
    });

    if (isset($_GET['greyscale'])) {
        $image->greyscale();
    } 
    if (isset($_GET['contrast'])) {
        $image->contrast($_GET['contrast']);
    } 
    if (isset($_GET['gamma'])) {
        $image->gamma($_GET['gamma']);
    } 
    if (isset($_GET['brightness'])) {
        $image->brightness($_GET['brightness']);
    } 
    if (isset($_GET['colorize'])) {
        $red    = explode(",", $_GET['colorize'])[0];
        $green  = explode(",", $_GET['colorize'])[1];
        $blue   = explode(",", $_GET['colorize'])[2];
        $image->colorize($red, $green, $blue);
    } 

    header("Content-Type: image/jpg");
    header("Access-Control-Allow-Origin: *");
    echo $image->response('jpg');

    $image->destroy();
}


 ?>