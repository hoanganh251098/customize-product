<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<div class="wrapper">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 align-justify-center h-100">
				<img class="image_output" src="" alt="">
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 align-justify-center h-100">
				<div class="w-100">
					<div class="form-group">
						<label for="">Image</label>
						<input type="text" id="image" class="form-control" name="image" value="https://tat-customize.s3-accelerate.amazonaws.com/185219933300941582657195842295383719223.jpeg">
					</div>
					<div class="form-group">
						<label for="">contrast <span class="contrast"></span></label>
						<input type="range" id="contrast" name="contrast" value="0" min="0" max="100">
					</div>
					<div class="form-group">
						<label for="">brightness <span class="brightness"></span></label>
						<input type="range" id="brightness" name="brightness" value="0" min="-100" max="100">
					</div>
					<div class="form-group">
						<label for="">gamma <span class="gamma"></span></label>
						<input type="range" id="gamma" name="gamma" value="100" min="0" max="500">
					</div>
					<div class="form-group">
						<label for="">greyscale</label>
						<label class="switch">
						  	<input type="checkbox" name="greyscale" id="greyscale">
						  	<span class="slider round"></span>
						</label>
					</div>
					<div class="form-group">
						<label for="">colorize R-G-B  <span class="colorize"></span></label>
						<input type="range" id="colorize_red" name="colorize_red" value="0" min="-100" max="100">
						<input type="range" id="colorize_green" name="colorize_green" value="0" min="-100" max="100">
						<input type="range" id="colorize_blue" name="colorize_blue" value="0" min="-100" max="100">
					</div>
					
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">

var url 	= 'upload.php?';
var scale 	= 'width=' + 1200 + '&url=';
// var image 	= 'https://images3.alphacoders.com/823/thumb-1920-82317.jpg';
setURL ()


contrast.oninput = function() {
	$('.contrast').html(contrast.value)
} 
brightness.oninput = function() {
	$('.brightness').html(brightness.value)
} 
gamma.oninput = function() {
	$('.gamma').html(gamma.value)
} 
colorize_red.oninput = function() {
	$('.colorize').html(`( ${colorize_red.value}, ${colorize_green.value}, ${colorize_blue.value} ) `)
} 
colorize_green.oninput = function() {
	$('.colorize').html(`( ${colorize_red.value}, ${colorize_green.value}, ${colorize_blue.value} ) `)
} 
colorize_blue.oninput = function() {
	$('.colorize').html(`( ${colorize_red.value}, ${colorize_green.value}, ${colorize_blue.value} ) `)
} 


contrast.onmouseup = function() {
	setURL()
} 
gamma.onmouseup = function() {
	setURL()
} 
brightness.onmouseup = function() {
	setURL()
} 
colorize_red.onmouseup = function() {
	setURL()
} 
colorize_green.onmouseup = function() {
	setURL()
} 
colorize_blue.onmouseup = function() {
	setURL()
} 



$('#greyscale').on('change', function(){
	setURL()
})
$('#image').on('change', function(){
	setURL()
})

function setURL (){
	image = $('#image').val() == '' ? 'https://tat-customize.s3-accelerate.amazonaws.com/185219933300941582657195842295383719223.jpeg' : $('#image').val()
	var urlReturn = url + scale + image;

	if ($('#greyscale').is(":checked")) urlReturn += `&greyscale=1`;
	if ($('#brightness').val()) urlReturn += `&brightness=${$('#brightness').val()}`;
	if ($('#contrast').val()) urlReturn += `&contrast=${$('#contrast').val()}`;
	if ($('#gamma').val()) urlReturn += `&gamma=${$('#gamma').val() / 100}`;
	
	urlReturn += `&colorize=${$('#colorize_red').val()},${$('#colorize_green').val()},${$('#colorize_blue').val()}`;

	$('.image_output').attr('src', urlReturn);
	// console.log(urlReturn)
}
</script>
</html>
