<?php

$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 5000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if(isset($_POST["submit"])) {
        if(is_array($_FILES)) {
            $file = $_FILES['fileToUpload']['tmp_name']; 
            $sourceProperties = getimagesize($file);
            $imageName      = basename($_FILES["fileToUpload"]["name"]);
            $fileNewName    = $imageName . time();

            // create folder
            mkdir($fileNewName, 0700);
            mkdir($fileNewName.'/resize', 0700);
            mkdir($fileNewName.'/full', 0700);
            mkdir($fileNewName.'/filter', 0700);

            //
            $folderPathResize   = $fileNewName."/resize/";
            $folderPathFull     = $fileNewName."/full/";
            $folderPathFIlter   = $fileNewName."/filter/";


            // / tạo resize
            $ext = pathinfo($_FILES['fileToUpload']['name'], PATHINFO_EXTENSION);
            $imageType = $sourceProperties[2];
            switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageResourceId = imagecreatefrompng($file); 
                    $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagepng($targetLayer,$folderPathResize. $fileNewName. "_thump.". $ext);
                    break;
                case IMAGETYPE_GIF:
                    $imageResourceId = imagecreatefromgif($file); 
                    $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagegif($targetLayer,$folderPathResize. $fileNewName. "_thump.". $ext);
                    break;
                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($file); 
                    $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagejpeg($targetLayer,$folderPathResize. $fileNewName. "_thump.". $ext);
                    break;
                default:
                    echo "Invalid Image type.";
                    exit;
                    break;
            }

            // tạo ảnh full
            move_uploaded_file($file, $folderPathFull. $fileNewName. ".". $ext);


            // tạo filter
            $im = imagecreatefrompng($folderPathFull. $fileNewName. ".". $ext);
            if($im && imagefilter($im, IMG_FILTER_COLORIZE, 201, 131, 119)) {
                imagepng($im, $folderPathFIlter. $fileNewName. ".". $ext);
            } else {
                echo 'Conversion to grayscale failed.';
            }
            imagedestroy($im);
            echo "Image Resize Successfully.";
        }
    }

}


function imageResize($imageResourceId,$width,$height) {
    $targetWidth = $width / 10;
    $targetHeight = $height / 10;
    $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
    imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0, $targetWidth, $targetHeight, $width, $height);
    return $targetLayer;
}

?>
