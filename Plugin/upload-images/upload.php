<?php

require_once 'vendor_image/autoload.php';
use imagemanipulation\ImageBuilder;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic as Image;
Image::configure(array('driver' => 'imagick'));


// set header json
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header('Content-Type: application/json');


// enable error message
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


function generate_filename() {
    $strings = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $stringLength = strlen($strings);
    $newStrings = '';
    for ($i = 0; $i < 20; $i++) {
        $newStrings .= $strings[rand(0, $stringLength - 1)];
    }
    return $newStrings;
}


$target_dir = "upload-images-user/";


if (isset($_POST) && !empty($_FILES['file']) && !empty($_POST['blob'])) {
    $file = $_FILES['file'];
    $file_name      = generate_filename().time();
    $path_folder_file    = $target_dir.$file_name;

    $ext = str_replace('image/','' , $file['type']);

    mkdir($path_folder_file, 0755);
    mkdir($path_folder_file.'/preview', 0755);
    mkdir($path_folder_file.'/original', 0755);
    mkdir($path_folder_file.'/filter', 0755);


    $path_folder_preview    = $path_folder_file."/preview/";
    $path_folder_original   = $path_folder_file."/original/";
    $path_folder_filter     = $path_folder_file."/filter/";
    $image = Image::make($file['tmp_name']);
    $image->orientate();
    $image->save($path_folder_original . "image_original.". $ext);

    $image = Image::make($path_folder_original . "image_original.". $ext);
    $image->orientate();

    $image_preview = $image->resize(500, null, function ($constraint) {
        $constraint->aspectRatio();
    });
    $image_preview->save($path_folder_preview."image_preview.". $ext);


    $ib1            = new ImageBuilder(new \SplFileInfo($path_folder_original . "image_original.". $ext));
    $ib2            = new ImageBuilder(new \SplFileInfo($path_folder_original . "image_original.". $ext));
    $ib3            = new ImageBuilder(new \SplFileInfo($path_folder_original . "image_original.". $ext));

    $ib1->grayscale()
        ->save( new \SplFileInfo( $path_folder_filter . "_filter_gray.png" ) );
        
    $ib2 ->grayscale()
        ->contrast(50)
        ->save( new \SplFileInfo( $path_folder_filter . "_filter_blackwhite.png" ) );
    $ib3
        ->sepia()
        ->darken(5)
        ->contrast(-5)
        ->save( new \SplFileInfo( $path_folder_filter . "_filter_vintage.png" ) );

    header('Content-Type: application/json');
    echo json_encode([
        'code'          => 201, 
        "message"       => "Image upload successfully", 
        "hash"          => $file_name, 
        "ext"           => $ext,
        "dimentions"    => [$image->width(),$image->height()],
        "data"          => [
            "gray"          => $path_folder_filter . "_filter_gray.png",
            "blackwhite"    => $path_folder_filter . "_filter_blackwhite.png",
            "vintage"       => $path_folder_filter . "_filter_vintage.png",
        ]
    ]);
    $image->destroy();
    die();
}


if (isset($_POST) && !empty($_FILES['file'])) {
    $target_file = $target_dir . basename($_FILES["file"]["name"]);
    $check_upload = 1;
    $image_file_type = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    
    if(isset($_FILES["file"])) {
        $check = getimagesize($_FILES["file"]["tmp_name"]);
        if($check !== false) {
            $check_upload = 1;
        } else {
            echo json_encode(["code" => 302, "message" => "File is not an image."]);
            die();
            $check_upload = 0;
        }
    }


    // Check file size
    if ($_FILES["file"]["size"] > 50000000) {
        echo json_encode(['code' => 302, "message" => "Sorry, your file is too large."]);
        die();
        $check_upload = 0;
    }

    
    // Check if $check_upload is set to 0 by an error
    if ($check_upload == 0) {
        echo json_encode(['code' => 302, "message" => "Sorry, your file was not uploaded."]);
        die();
    } else {
        if(is_array($_FILES)) {
            $file = $_FILES["file"]['tmp_name']; 
            $source_properties = getimagesize($file);
            $file_name      = generate_filename().time();
            $path_folder_file    = $target_dir.$file_name;

            $image = Image::make($file);
            $image->orientate();

            // create folder
            mkdir($path_folder_file, 0755);
            mkdir($path_folder_file.'/preview', 0755);
            mkdir($path_folder_file.'/original', 0755);
            mkdir($path_folder_file.'/filter', 0755);

            $path_folder_preview    = $path_folder_file."/preview/";
            $path_folder_original   = $path_folder_file."/original/";
            $path_folder_filter     = $path_folder_file."/filter/";


            // create image preview resize
            $ext = pathinfo($_FILES["file"]['name'], PATHINFO_EXTENSION);

            // save image original
            $image->save($path_folder_original . "image_original.". $ext);

            $image_preview = $image->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            
            $image_preview->save($path_folder_preview."image_preview.". $ext);

            $ib1            = new ImageBuilder(new \SplFileInfo(__DIR__ . DIRECTORY_SEPARATOR . $path_folder_original . "image_original.". $ext));
            $ib2            = new ImageBuilder(new \SplFileInfo(__DIR__ . DIRECTORY_SEPARATOR . $path_folder_original . "image_original.". $ext));
            $ib3            = new ImageBuilder(new \SplFileInfo(__DIR__ . DIRECTORY_SEPARATOR . $path_folder_original . "image_original.". $ext));
            // create filter
            $ib1->grayscale()
                ->save( new \SplFileInfo( $path_folder_filter . "_filter_gray.png" ) );
                
            $ib2 ->grayscale()
                ->contrast(50)
                ->save( new \SplFileInfo( $path_folder_filter . "_filter_blackwhite.png" ) );
            $ib3
                ->sepia()
                ->darken(5)
                ->contrast(-5)
                ->save( new \SplFileInfo( $path_folder_filter . "_filter_vintage.png" ) );

            header('Content-Type: application/json');
            echo json_encode([
                'code'          => 201, 
                "message"       => "Image upload successfully", 
                "hash"          => $file_name, 
                "ext"           => $ext, 
                "dimentions"    => [$source_properties[0],$source_properties[1]],
                "data"          => [
                    "gray"          => $path_folder_filter . "_filter_gray.png",
                    "blackwhite"    => $path_folder_filter . "_filter_blackwhite.png",
                    "vintage"       => $path_folder_filter . "_filter_vintage.png",
                ]
            ]);
            $image->destroy();
        }
    }
}
?>