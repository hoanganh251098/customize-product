<?php
require_once 'vendor_image/autoload.php';
use imagemanipulation\ImageBuilder;

function render($path_folder_original, $path_folder_filter, $ext) {
    $ib1            = new ImageBuilder(new \SplFileInfo(__DIR__ . DIRECTORY_SEPARATOR . $path_folder_original . "image_original.". $ext));
    $ib2            = new ImageBuilder(new \SplFileInfo(__DIR__ . DIRECTORY_SEPARATOR . $path_folder_original . "image_original.". $ext));
    $ib3            = new ImageBuilder(new \SplFileInfo(__DIR__ . DIRECTORY_SEPARATOR . $path_folder_original . "image_original.". $ext));
    $image_global   = new ImageBuilder(new \SplFileInfo(__DIR__ . DIRECTORY_SEPARATOR . $path_folder_original . "image_original.". $ext));

    $a = $ib1->grayscale()
        ->save(new \SplFileInfo($path_folder_filter . "_filter_gray.png"));
        
    $b = $ib2->grayscale()
        ->contrast(50)
        ->save(new \SplFileInfo($path_folder_filter . "_filter_blackwhite.png"));

    $c = $ib3->sepia()
        ->darken(5)
        ->contrast(-5)
        ->save(new \SplFileInfo($path_folder_filter . "_filter_vintage.png"));

    $d = $image_global->save(new \SplFileInfo( $path_folder_original . "image_global.png"));
    return "";
}