var sepia 		= document.getElementById("sepia");
var brightness 	= document.getElementById("brightness");
var contrast 	= document.getElementById("contrast");
var duotone 	= '';
var colorize 	= '';

var url 	= 'filter.php';

var suburl 	= [
	'sepia',
	'brightness',
]
suburl.map(v => console.log(v))

 
sepia.oninput = function() {
	$('.sepia-output').html(sepia.value)
} 
brightness.oninput = function() {
	$('.brightness-output').html(brightness.value)
} 
contrast.oninput = function() {
	$('.contrast-output').html(contrast.value)
} 


// Update the current slider value (each time you drag the slider handle)
sepia.onmouseup = function() {
	setURL()
	// doFilter()
} 
brightness.onmouseup = function() {
	setURL()
	// doFilter()
} 
contrast.onmouseup = function() {
	setURL()
	// doFilter()
} 
$('#duotone').on('change', function(){
	duotone 	= $('#duotone').val().split('#')[1]
	red 	= parseInt(duotone.slice(0, 2), 16)
	green 	= parseInt(duotone.slice(2, 4), 16)
	blue 	= parseInt(duotone.slice(4, 6), 16)
	duotone 	= `${red},${green},${blue}`
	setURL()
})

$('#color').on('change', function(){
	colorize 	= $('#color').val().split('#')[1] + ',' + $('#color_range').val()
	setURL()
})

$('#color_range').on('change', function(){
	colorize 	= $('#color').val().split('#')[1] + ',' + $('#color_range').val()
	setURL()
})

$('.create').on('click', function(){
	dataReturn = ``
	if (sepia.value != -1) {
		dataReturn +=
		`$ib->sepia(${sepia.value}) 
`;
	}
	if (brightness.value != 0) {
		dataReturn += `$ib->brightness(${brightness.value}) 
`;
	}
	if (contrast.value != 0) {
		dataReturn += `$ib->contrast(${contrast.value}) 
`;
	}
	if (duotone != '') {
		dataReturn += `$ib->duotone(${duotone}) 
`;
	}
	if (colorize != '') {
		dataReturn += `$ib->colorize(${colorize}) 
`;
	}

	$('.output').val(dataReturn)
})


function doFilter(){
	console.log(sepia.value)
	$('.image').css({
		'filter' : `sepia(${sepia.value}%)`
	})
}

function setURL (){
	var urlReturn = '';

	// config value 
	var data = [
		{'sepia': sepia.value},
		{'brightness': brightness.value},
		{'contrast': contrast.value},
		{'duotone': duotone},
		{'colorize': colorize},
		{'link': link}
	];
	data.map(v => {
		Object.entries(v).map(obj => {
			urlReturn += `${obj[0]}=${obj[1]}&`
		})
		
	})
	pushURL(urlReturn)
	console.log(urlReturn)
}
function pushURL(urlReturn){
	$('iframe').attr('src', url + '?' + urlReturn);
	var x = document.getElementsByTagName("iframe")[0].contentWindow;
	setTimeout(()=>{
		x.document.getElementsByTagName('img')[0].setAttribute('style', 'width: 100%')
	}, 800)
}

var x = document.getElementsByTagName("iframe")[0].contentWindow;
setTimeout(()=>{
	x.document.getElementsByTagName('img')[0].setAttribute('style', 'width: 100%')
}, 500)