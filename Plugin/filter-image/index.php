<?php 
if (isset($_GET['link'])) {
	$val = $_GET['link'];
} else {
	$val = 'review2.jpg';
}

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="style.css" />
</head>
<body>
	<div class="review-wrapper col-md-6">
		<iframe src="filter.php?link=<?php echo $val; ?>" style="">
			
		</iframe>
		<!-- <img src="review.jpg" alt="" width="700" class="image"> -->
	</div>
	<div class="range-wrapper slidecontainer col-md-6">
		<div class="row">
			<form class="col-md-12" method="post" action="index.php" enctype="multipart/form-data">
				<input type="file" name="file" value="" class="file_upload">
				<button type="submit" name="OK">Upload</button>
			</form>
			<div class="col-md-12">
				<div class="form-group">
					<label for="">sepia <span class="sepia-output"></span></label>
					<input type="range" min="-1" max="255" value="-1" class="slider form-control" id="sepia">
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label for="">brightness <span class="brightness-output"></span></label>
					<input type="range" min="-255" max="255" value="0" class="slider form-control" id="brightness">
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label for="">contrast <span class="contrast-output"></span></label>
					<input type="range" min="-100" max="100" value="0" class="slider form-control" id="contrast">
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label for="">duotone</label>
					<input type="color"class="form-control" id="duotone">
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label for="">color</label>
					<input type="color"class="form-control color" id="color">
					<label for="">opacity</label>
					<input type="range" min="0" max="127" value="0" class="slider form-control" id="color_range">
				</div>
			</div>
		</div>
		<div class="form-group">
			<textarea name="" class="output form-control" rows="10"></textarea>
			<button type="" class="create">Create filter</button>
		</div>
	</div>
</body>
<script>
	var link = '<?php echo $val; ?>';
</script>
<script src="script.js" type="text/javascript" charset="utf-8" async defer></script>
</html>

<?php 
	if (isset($_POST['OK'])) {
		$file = $_FILES['file'];
		move_uploaded_file($file['tmp_name'], 'upload/'. time(). $file['name']);
		header('location: index.php?link='.'upload/'. time(). $file['name']);
	}
 ?>