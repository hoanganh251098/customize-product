<?php
require_once 'vendor/autoload.php';
use imagemanipulation\ImageBuilder;

$sepia = 0;
$brightness = 0;
$contrast = 0;

if (isset($_GET['link'])) {
	$ib = new ImageBuilder(new \SplFileInfo(__DIR__ . DIRECTORY_SEPARATOR . $_GET['link']));
} else {
	$ib = new ImageBuilder(new \SplFileInfo(__DIR__ . DIRECTORY_SEPARATOR . 'review2.jpg'));
}


if (isset($_GET['sepia'])) {
	$sepia 		= $_GET['sepia'];
	if ($sepia != -1) {
		$ib->sepia($sepia);
	}
}
if (isset($_GET['brightness'])) {
	$brightness 		= $_GET['brightness'];
	if ($brightness != 0) {
		$ib->brightness($brightness);
	}
}
if (isset($_GET['contrast'])) {
	$contrast 		= $_GET['contrast'];
	if ($contrast != 0) {
		$ib->contrast($contrast);
	}
}
if (isset($_GET['duotone'])) {
	$duotone 		= $_GET['duotone'];
	if ($duotone != '') {
		$red 	= explode(",", $duotone)[0];
		$green 	= explode(",", $duotone)[1];
		$blue 	= explode(",", $duotone)[2];
		$ib->duotone($red, $green, $blue);
	}
}
if (isset($_GET['colorize'])) {
	$colorize 		= $_GET['colorize'];
	if ($colorize != '') {
		$hex 		= explode(",", $colorize)[0];
		$opacity 	= explode(",", $colorize)[1];
		$ib->colorize($hex, $opacity);
	}
}

$ib->render(100);