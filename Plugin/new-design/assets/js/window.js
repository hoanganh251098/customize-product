// Multiple images preview in browser
var imagesPreview = function(input, placeToInsertImagePreview) {
    if (input.files) {
        var filesAmount = input.files.length;
        for (i = 0; i < filesAmount; i++) {
            var reader = new FileReader();
            reader.onload = function(event) {
                $(placeToInsertImagePreview).append(`
                    <div class="image-upload-item align-justify-center">
                        <img src="${event.target.result}" alt="image small" />
                    </div>
                `); 
            }
            reader.readAsDataURL(input.files[i]);
        }
    }
};
$('#LoadImage').on('change', function() {
    imagesPreview(this, '.image-upload-wrapper');
});






