from flask import Flask, jsonify, request, Response
import os
import json
import traceback
import importlib
import requests
import logging

LOG_FILENAME = './log.txt'
logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG)


app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

@app.after_request
def after_request(res):
    res.headers['Access-Control-Allow-Credentials'] = 'true'
    res.headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS, PUT, DELETE'
    res.headers['Access-Control-Allow-Headers'] = 'Content-Type'
    return res

def list_folder():
    def f_filter(v):
        return os.path.isdir(v)
    folders = [*filter(f_filter, os.listdir('.'))]
    return folders

# clear log
open('log.txt', 'w', encoding='utf-8').write('')

status = {}
for service_name in list_folder():
    files = os.listdir(service_name)
    if 'blueprint.py' in files:
        try:
            app.register_blueprint(importlib.import_module(f'{service_name}.blueprint').router, url_prefix='/'+service_name)
            status[service_name] = 'OK'
        except Exception as e:
            status[service_name] = str(e)
            logging.error(e, exc_info=True)

@app.route('/status')
def get_status():
    return jsonify(status)

@app.route('/request_info', defaults={'path': '/'}, methods=['GET', 'POST', 'PUT', 'DELETE'])
@app.route('/request_info/', defaults={'path': '/'}, methods=['GET', 'POST', 'PUT', 'DELETE'])
@app.route('/request_info/<path:path>', methods=['GET', 'POST', 'PUT', 'DELETE'])
def request_info(path):
    return jsonify({
        'path': path,
        'method': request.method,
        'args': request.args,
        'data': request.data,
        'json': request.json,
        'form': request.form,
        'files': request.files,
        'values': request.values
    })

@app.route('/help', methods = ['GET'])
def help():
    prefix = request.args.get('prefix', '')
    func_list = {}
    for rule in app.url_map.iter_rules():
        if rule.rule.startswith(prefix):
            func_list[rule.rule] = {
                'methods': json.loads(str(rule.methods).replace('{', '[').replace('}', ']').replace("'", '"')),
                'doc': app.view_functions[rule.endpoint].__doc__
            }
            func_list[rule.rule]['methods'].remove('OPTIONS')
            func_list[rule.rule]['methods'].remove('HEAD')
    return jsonify(func_list)

app.run(host='0.0.0.0', port='8000', debug=True)
