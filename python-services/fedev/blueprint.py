from flask import Blueprint, render_template, abort, send_from_directory, make_response, request, redirect, flash, jsonify
import traceback
import requests
import time
import json
import jwt
import os

router = Blueprint('fedev', __name__, template_folder="./pages", static_folder="./assets")

@router.route('/')
def test():
    return jsonify({
        'message': 'ok'
    })

@router.route('/<page>')
def serve_page(page):
    return render_template(page+'.html')

@router.route('/assets/<path:path>')
def serve_asset(path):
    return send_from_directory(router.static_folder, path)