from flask import Blueprint, render_template, abort, send_from_directory, make_response, request, redirect, flash, jsonify
import traceback
import requests
import urllib.parse
import time
import json
import jwt
import os

router = Blueprint('spotify-api', __name__)

class SpotifyAPI:
    __last_token = None

    @classmethod
    def get_token(cls):
        if cls.__last_token == None or (time.time() - cls.__last_token['created_at']) > cls.__last_token['expires_in'] * 0.9:
            url = f'http://www.spotifycodes.com/getToken.php'
            res = requests.get(url)
            data = res.json()
            data['created_at'] = time.time()
            cls.__last_token = data
            return data
        return cls.__last_token

@router.route('/search')
def serve_search():
    q = request.args.get('q', '')
    if not q:
        return jsonify({
            'data': []
        })
    page = int(request.args.get('page', '1'))
    offset = (page - 1) * 20
    encoded_query = urllib.parse.quote_plus(q)
    access_token = SpotifyAPI.get_token()['access_token']
    url = f'http://api.spotify.com/v1/search?query={encoded_query}&type=track&offset={offset}&limit=20&access_token={access_token}'
    res = requests.get(url)
    data = res.json()
    return jsonify({
        'data': data
    }), res.status_code

@router.route('/tracks/<trackid>')
def serve_home(trackid):
    access_token = SpotifyAPI.get_token()['access_token']
    url = f'http://api.spotify.com/v1/tracks/{trackid}?access_token={access_token}'
    res = requests.get(url)
    data = res.json()
    return jsonify({
        'data': data
    }), res.status_code

@router.route('/access-token')
def serve_token():
    return jsonify({
        'data': SpotifyAPI.get_token()
    })
