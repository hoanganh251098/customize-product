from apidev.sqlite_wrapper import Schema, ForeignKey

import time
import datetime

DATABASE_LOCATION = './apidev/database.db'

def get_timestamp():
    return int(time.time())

def from_timestamp(timestamp):
    return datetime.datetime.fromtimestamp(int(timestamp))

class Product(Schema):
    sku = 'varchar(255)'
    created_at = 'varchar(255)'
    modified_at = 'varchar(255)'

class Store(Schema):
    domain = 'varchar(255)'
    shopify_name = 'varchar(255)'
    access_key = 'varchar(255)'

class ProductDetail(Schema):
    product_id = ForeignKey('Product')
    store_id = ForeignKey('Store')
    shopify_product_id = 'varchar(255)'

class Report(Schema):
    category = 'varchar(255)'
    content = 'text'

class Table:
    product_table           = Product(DATABASE_LOCATION)
    product_detail_table    = ProductDetail(DATABASE_LOCATION)
    store_table             = Store(DATABASE_LOCATION)
    report_table            = Report(DATABASE_LOCATION)

class ProductTable(Table):
    # Product LCRUD
    @classmethod
    def list(cls, page, page_size):
        result = cls.product_table.list((page - 1) * page_size, page_size)
        for r in result:
            r and cls.product_detail_table.fill(r, 'configs')
            r['created_at'] = from_timestamp(int(r['created_at']))
            r['modified_at'] = from_timestamp(int(r['modified_at']))
        return result

    @classmethod
    def create(cls, data):
        sku = data.get('sku', '')
        created_at = str(get_timestamp())
        modified_at = created_at
        if len(cls.product_table.query(f'select * from Product where sku={repr(sku)}')) == 0:
            return cls.product_table.insert({
                'sku'           : sku,
                'created_at'    : created_at,
                'modified_at'   : modified_at
            })
        return 0

    @classmethod
    def read(cls, id):
        r = cls.product_table[id]
        if r:
            cls.product_detail_table.fill(r, 'configs')
            r['created_at'] = from_timestamp(int(r['created_at']))
            r['modified_at'] = from_timestamp(int(r['modified_at']))
        return r

    @classmethod
    def update_product_modified_timestamp(cls, product_id):
        r = cls.product_table[product_id]
        if r:
            r['modified_at'] = str(get_timestamp())
            cls.product_table[product_id] = r

    # Config LCRUD
    @classmethod
    def create_config(cls, data):
        product_id          = data.get('product_id', '')
        store_id            = data.get('store_id', '')
        shopify_product_id  = data.get('shopify_product_id', '')
        if cls.product_table[product_id] and cls.store_table[store_id]:
            is_config_exists = cls.product_detail_table.query(
                f'select * from ProductDetail where product_id={product_id} and store_id={store_id}')
            if is_config_exists:
                return 0
            cls.update_product_modified_timestamp(product_id)
            return cls.product_detail_table.insert({
                'product_id'            : product_id,
                'store_id'              : store_id,
                'shopify_product_id'    : shopify_product_id
            })
        return 0

    @classmethod
    def update_config(cls, id, data):
        product_id          = data.get('product_id', '')
        store_id            = data.get('store_id', '')
        shopify_product_id  = data.get('shopify_product_id', '')
        if cls.product_table[product_id] and cls.store_table[store_id]:
            is_config_exists = cls.product_detail_table.query(
                f'select * from ProductDetail where product_id={product_id} and store_id={store_id}')
            if not is_config_exists:
                return False
            cls.update_product_modified_timestamp(product_id)
            cls.product_detail_table[id] = {
                'product_id'            : product_id,
                'store_id'              : store_id,
                'shopify_product_id'    : shopify_product_id
            }
            return True
        return False

    @classmethod
    def delete_config(cls, id, data):
        product_id = data.get('product_id', '')
        config = cls.product_detail_table[id]
        if str(config['product_id']) == str(product_id):
            cls.update_product_modified_timestamp(product_id)
            cls.product_detail_table.remove(id)
            return True
        return False

class StoreTable(Table):
    @classmethod
    def list(cls, page, page_size):
        return cls.store_table.list((page - 1) * page_size, page_size)

    @classmethod
    def create(cls, data):
        domain          = data.get('domain', '')
        shopify_name    = data.get('shopify_name', '')
        access_key      = data.get('access_key', '')
        if len(cls.store_table.query(f'select * from Store where domain={repr(domain)}')) == 0:
            cls.store_table.insert({
                'domain'        : domain,
                'shopify_name'  : shopify_name,
                'access_key'    : access_key
            })
            return True
        return False

    @classmethod
    def read(cls, id):
        return cls.store_table[id]

    @classmethod
    def update(cls, id, data):
        domain = data.get('domain', '')
        shopify_name = data.get('shopify_name', '')
        access_key = data.get('access_key', '')
        store = cls.store_table[id]
        if store:
            cls.store_table[id] = {
                'domain': domain,
                'shopify_name': shopify_name,
                'access_key': access_key
            }
            return True
        return False
