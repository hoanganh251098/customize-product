from flask import Blueprint, render_template, abort, send_from_directory, make_response, request, redirect, flash, jsonify, url_for
import apidev.model as model
from apidev.shopify import ShopifyStore
from concurrent.futures import ThreadPoolExecutor
import traceback
import requests
import time
import json
import jwt
import os

router = Blueprint('apidev', __name__)

@router.route('/')
def index():
    return jsonify({
        'message': 'ok'
    })

def parse_pagination():
    return (
        max(int(request.args.get('page') or 1), 1),                     # page: default 1 min 1
        min(max(int(request.args.get('limit') or 20), 10), 500)         # limit: default 20 min 10 max 500
    )

@router.route('/stores', methods=['GET', 'POST'])
def serve_stores():
    page, limit = parse_pagination()
    if request.method == 'GET':
        return jsonify({
            'data': model.StoreTable.list(page, limit)
        })
    else:
        data = {
            'domain': request.json.get('domain', ''),
            'shopify_name': request.json.get('shopify_name', ''),
            'access_key': request.json.get('access_key', '')
        }
        created_id = model.StoreTable.create(data)
        if not created_id:
            return jsonify({
                'message': 'Failed to create!'
            }), 400
        return jsonify({
            'data': created_id
        }), 201

@router.route('/stores/<id>', methods=['GET', 'PUT', 'DELETE'])
def serve_stores_id(id):
    if request.method == 'GET':
        r = model.StoreTable.read(id)
        if not r:
            return jsonify({
                'message': 'Store not found!'
            }), 404
        return jsonify({
            'data': r
        })
    elif request.method == 'PUT':
        data = {
            'domain': request.json.get('domain', ''),
            'shopify_name': request.json.get('shopify_name', ''),
            'access_key': request.json.get('access_key', '')
        }
        is_modified = model.StoreTable.update(id, data)
        if not is_modified:
            return jsonify({
                'message': 'Failed to modify!'
            }), 400
        return jsonify({
            'message': 'Modified!'
        }), 200

@router.route('/stores/<id>/connection_status', methods=['GET'])
def serve_stores_id_connection_status(id):
    store = model.StoreTable.read(id)
    if not store:
        return jsonify({
            'message': 'Store not found!'
        }), 404
    access_key = store['access_key']
    shopify_name = store['shopify_name']
    headers = {"Accept": "application/json", "Content-Type": "application/json"}
    auth = (access_key.split(':')[0], access_key.split(':')[1])
    r = requests.get(f'https://{shopify_name}.myshopify.com/admin/shop.json', auth=auth, headers=headers, verify=False, timeout=10)
    if r.status_code < 400:
        return jsonify({
            'message': 'OK'
        })
    else:
        return jsonify({
            'message': 'Error'
        })

@router.route('/products', methods=['GET', 'POST'])
def serve_products():
    page, limit = parse_pagination()
    if request.method == 'GET':
        return jsonify({
            'data': model.ProductTable.list(page, limit)
        })
    else:
        data = {
            'sku': request.json.get('sku', '')
        }
        created_id = model.ProductTable.create(data)
        if not created_id:
            return jsonify({
                'message': 'Failed to create!',
            }), 400
        return jsonify({
            'data': created_id
        }), 201

@router.route('/products/<id>', methods=['GET'])
def serve_products_id(id):
    if request.method == 'GET':
        r = model.ProductTable.read(id)
        if not r:
            return jsonify({
                'message': 'Product not found!'
            }), 404
        return jsonify({
            'data': r
        })

@router.route('/products/<id>/configs', methods=['GET', 'POST'])
def serve_products_id_configs(id):
    if request.method == 'GET':
        return jsonify({
            'data': model.ProductTable.read(id)['configs']
        })
    else:
        data = {
            'product_id': id,
            'store_id': request.json.get('store_id', ''),
            'shopify_product_id': request.json.get('shopify_product_id', '')
        }
        created_id = model.ProductTable.create_config(data)
        if not created_id:
            return jsonify({
                'message': 'Failed to create!'
            }), 400
        return jsonify({
            'data': created_id
        }), 201

@router.route('/products/<productid>/configs/<configid>', methods=['PUT', 'DELETE'])
def serve_products_id_configs_id(productid, configid):
    if request.method == 'PUT':
        data = {
            'product_id': productid,
            'store_id': request.json.get('store_id', ''),
            'shopify_product_id': request.json.get('shopify_product_id', '')
        }
        is_modified = model.ProductTable.update_config(configid, data)
        if not is_modified:
            return jsonify({
                'message': 'Failed to modify!'
            }), 400
        return jsonify({
            'message': 'Modified!'
        }), 200
    else:
        data = {
            'product_id': productid
        }
        is_removed = model.ProductTable.delete_config(configid, data)
        if not is_removed:
            return jsonify({
                'message': 'Failed to remove!'
            }), 400
        return jsonify({
            'message': 'Removed!'
        }), 200

class IframeSyncProcess:
    def __init__(self, source_store, target_store):
        self.id = None
        self.source_store = source_store
        self.target_store = target_store
        self.results = []
        self.error = None
        self.error_code = None
        self.status = 'pending'
    
    def jsonify(self):
        data = {
            'id': self.id,
            'source_store': self.source_store,
            'target_store': self.target_store,
            'results': self.results,
            'error': self.error,
            'error_code': self.error_code,
            'status': self.status,
            'nr_errors': None,
            'nr_synced': None,
            'total': None,
        }
        if not self.error:
            data['nr_errors'] = 0
            data['nr_synced'] = 0
            data['total'] = len(self.results)
            for result in self.results:
                if result['error']:
                    data['nr_errors'] += 1
                elif result['synchronized']:
                    data['nr_synced'] += 1
        return data
    
    def finalize(self):
        self.status = 'done'

    def run(self):
        self.status = 'running'
        source_store = self.source_store
        target_store = self.target_store

        stores_config = json.loads(open('../design-upload/stores.json').read())
        sku_list = os.listdir('../design-upload/extract')
        store_keys = {}
        for storename, v in stores_config.items():
            access_key = v['access_key']
            store_keys[storename] = access_key
        if source_store not in store_keys:
            self.error = {
                'message': f'Source store "{source_store}" is not configured.'
            }
            self.error_code = 400
            self.finalize()
            return None
        if target_store not in store_keys:
            self.error = {
                'message': f'Target store "{target_store}" is not configured.'
            }
            self.error_code = 400
            self.finalize()
            return None
        source_store_manager = ShopifyStore(source_store, store_keys[source_store])
        target_store_manager = ShopifyStore(target_store, store_keys[target_store])
        if not source_store_manager.test_connection():
            self.error = {
                'message': f'Cannot connect to source store "{source_store}"'
            }
            self.error_code = 500
            self.finalize()
            return None
        if not target_store_manager.test_connection():
            self.error = {
                'message': f'Cannot connect to target store "{target_store}"'
            }
            self.error_code = 500
            self.finalize()
            return None
        for sku in sku_list:
            sku_result = {
                'sku': sku,
                'iframe_url': None,
                'source_product_id': None,
                'target_product_id': None,
                'synchronized': False,
                'error': None
            }
            try:
                source_product_id = source_store_manager.find_product_id_by_sku(sku)
                target_product_id = target_store_manager.find_product_id_by_sku(sku)
                sku_result['source_product_id'] = source_product_id
                sku_result['target_product_id'] = target_product_id
                if source_product_id and target_product_id:
                    metafields = source_store_manager.get_metafields(sku)
                    iframe_url = metafields.get('iframe_url')
                    sku_result['iframe_url'] = iframe_url
                    errors = target_store_manager.set_metafields(sku, {
                        'iframe_url': iframe_url
                    })
                    if errors:
                        sku_result['error'] = errors[0]
                    else:
                        sku_result['synchronized'] = True
            except Exception as e:
                sku_result['error'] = str(e)
            self.results += [sku_result]
        self.finalize()

class IframeSyncProcessManager:
    broker = ThreadPoolExecutor(max_workers=1)
    processes = {}

    @classmethod
    def get_process(cls, id):
        return cls.processes[int(id)]

    @classmethod
    def create_process(cls, source_store, target_store):
        process = IframeSyncProcess(source_store, target_store)
        process_id = len(cls.processes)
        process.id = process_id
        cls.processes[process_id] = process
        def job():
            process.run()
        cls.broker.submit(job)
        return process_id
    
    @classmethod
    def remove_process(cls, id):
        cls.processes[int(id)] = None


@router.route('/iframe_sync/<source_store>/<target_store>', methods=['POST'])
def sync(source_store, target_store):
    process_id = IframeSyncProcessManager.create_process(source_store, target_store)
    return jsonify({
        'data': {
            'process_id': process_id
        }
    })

@router.route('/iframe_sync/processes', methods=['GET'])
def get_sync_processes():
    processes = IframeSyncProcessManager.processes
    result = []
    for k, process in processes.items():
        if process:
            result += [process.jsonify()]
    return jsonify({
        'data': result
    })

@router.route('/iframe_sync/processes/<process_id>', methods=['GET', 'DELETE'])
def serve_sync_process(process_id):
    if request.method == 'GET':
        process = IframeSyncProcessManager.get_process(process_id)
        return jsonify({
            'data': process.jsonify()
        }), (process.error_code or 200)
    elif request.method == 'DELETE':
        IframeSyncProcessManager.remove_process(process_id)
        return jsonify({
            'message': 'ok'
        })
    return jsonify({
        'message': 'Method Not Allowed'
    }), 405
