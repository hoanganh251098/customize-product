import requests
import json
from functools import lru_cache

class ShopifyStore:
    def __init__(self, name, access_key):
        self.name = name
        self.access_key = access_key
        self.origin = f'https://{access_key}@{name}.myshopify.com'

    @lru_cache(maxsize=256)
    def find_product_id_by_sku(self, sku):
        search_url = self.origin + f'/admin/products.json?handle={sku}'
        r = requests.get(search_url)
        for product in r.json()['products']:
            if product['handle'] == sku:
                return product['id']

    def get_metafields(self, sku):
        product_id = self.find_product_id_by_sku(sku)
        url = self.origin + f'/admin/products/{product_id}/metafields.json'
        r = requests.get(url)
        result = {}
        for metafield in r.json()['metafields']:
            result[metafield['key']] = metafield['value']
        return result
    
    def set_metafields(self, sku, metafields):
        product_id = self.find_product_id_by_sku(sku)
        errors = []
        for k, v in metafields.items():
            url = self.origin + f'/admin/products/{product_id}/metafields.json'
            r = requests.post(url, json={
                'metafield': {
                    'namespace': 'TAT',
                    'key': k,
                    'value': v,
                    'value_type': 'string'
                }
            })
            if r.status_code >= 400:
                errors += [r.json()]
        return errors

    def test_connection(self):
        url = self.origin + '/admin/shop.json'
        r = requests.get(url)
        return r.status_code < 400
